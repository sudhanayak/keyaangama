<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subcategory extends CI_Controller {
    function __construct() {
        parent::__construct();
         $this->load->helper("encryptionpwd");
         $this->load->model('Adminmodel');
         $this->load->library("pagination");
         $this->load->library('form_validation');
         $this->load->library('upload');
    }
     public function index() {
         redirect('viewSubcategory');
        } 
    // for subcategory
    public function addSubcategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
     // to fetch the data fro category drop down
        $data['admin']="add_subcategories";
        $table2 ="keyaan_category";
        $start =0;
        $limit =100;
        $search ='';
        $data['category']= $this->Adminmodel->get_current_page_records($table2,$limit,$start,$column=null,$value=null,@$search,@$searchColumn);
    //******    
        $subcategory_name = $this->input->post('subcategory_name');     
        if($subcategory_name!=''){            
            $check_data = array(
            "subcategory_name" => $this->input->post('subcategory_name')    
            );
            $tablename = "keyaan_subcategory";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Subcategory name already exist</div>');
                $this->load->view('admin/'.$data['admin'],$data);
            }else{
                if (isset($_FILES['image'])) {
                    $config_media['upload_path'] = './uploads/subcategory';
                    $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('image')) {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $image    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0) {
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        redirect('addSubcategory');
                    }        
                }else {
                    $image    = "";
                }
                if (isset($_FILES['subcat_banner'])) {
                    $config_media1['upload_path'] = './uploads/subcatbanner';
                    $config_media1['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';
                    $config_media1['max_size']   = '1000000000000000'; // whatever you need
                     $this->upload->initialize($config_media1);
                    $error = [];
                    if ( ! $this->upload->do_upload('subcat_banner'))
                    {
                        $error1[] = array('error_image' => $this->upload->display_errors());
                    }
                    else
                    {
                        $data1[] = array('upload_image' => $this->upload->data());
                    }
                    $subcat_banner  = $data1[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        redirect('addSubcategory');
                    }
                } else {
                    $subcat_banner    = "";
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $icon = $this->input->post('icon')=="" ? "":$this->input->post('icon');  
                $cat_id = $this->input->post('cat_id')=="" ? "":$this->input->post('cat_id');
                $price_selection = $this->input->post('price_selection')=="" ? "":$this->input->post('price_selection');
                if($price_selection == 1) {
                     $price = "";
                }else{
                     $price = $this->input->post('price')=="" ? "":$this->input->post('price');
                }
                if($price_selection == 0) {
                    $price_from = ""; 
                    $price_to = ""; 
                }else{
                    $price_from = $this->input->post('price_from')=="" ? "":$this->input->post('price_from'); 
                    $price_to = $this->input->post('price_to')=="" ? "":$this->input->post('price_to'); 
                }
                $meta_tag = $this->input->post('meta_tag')=="" ? "":$this->input->post('meta_tag');
                $meta_keyword = $this->input->post('meta_keyword')=="" ? "":$this->input->post('meta_keyword');  
                $meta_detail = $this->input->post('meta_detail')=="" ? "":$this->input->post('meta_detail');
                $dataCat = array(
                    'subcategory_name'=> $subcategory_name ,
                    'icon'  => $icon,
                    'price_selection'  =>$price_selection,
                    'price_from'  =>$price_from,
                    'price_to'   =>  $price_to,
                    'price'       =>  $price,
                    'meta_tag'       =>  $meta_tag,
                    'meta_keyword'       =>  $meta_keyword,
                    'meta_detail'   =>  $meta_detail,
                    'image'        => $image,
                    'subcat_banner' => $subcat_banner,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updatedby'     => $added_by,
                    'cat_id'        => $cat_id
                );
                $table="keyaan_subcategory";
                $result = $this->Adminmodel->insertRecordQueryList($table,$dataCat);
                if($result){
                        $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Subcategory Added</div>');
                }
                else{
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Subcategory not inserted</div>') ;
                }           
                $this->load->view('admin/'.$data['admin'],$data);   
            }
        }else{
            //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
            $this->load->view('admin/'.$data['admin'],$data);   
        }       
    }
    public function viewSubcategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
            redirect('admin');
        }
        $table ="keyaan_subcategory";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "subcategory/viewSubcategory";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'subcategory_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'subcategory_name');
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
        if($result){
            foreach ($result as $key => $field) {
                $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['cat_id'],'id','category_name','keyaan_category')  ;
                if($field['image'] !=""){
                $result[$key]['image'] = base_url()."uploads/subcategory/".$field['image'];  
                }else{
                   $result[$key]['image'] = "0";   
                }
            } 
            $data['result'] = $result;
        } else {
            $result[] = [];
            $data['result'] = $result;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_subcategories',$data);
    } 
        
    public function editSubcategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
            redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "keyaan_subcategory";
        $tablename2 = "keyaan_category";
        $start =0;
        $limit =100;
        $data['category']= $this->Adminmodel->get_current_page_records($tablename2,$limit,$start,$column=null,$value=null,@$search,@$searchColumn);
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        foreach ($result as $key => $field) {
            $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['cat_id'],'id','category_name','keyaan_category')  ;
            if($field['image'] !=""){
                $result[$key]['image'] = base_url()."uploads/subcategory/".$field['image'];  
            }else{
               $result[$key]['image'] = "0";   
            }
        }
        $data['result'] = $result ;
        if($result) {
            $this->load->view('admin/edit_subcategories',$data); 
        } else {
            $url='viewSubcategory';
            redirect($url);
        }  
    }
                
    // update subcategory 
    public function updateSubcategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
            redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $subcategory_name = $this->input->post('subcategory_name');
        $cat_id = $this->input->post('cat_id');
        if($subcategory_name!=''){            
            $check_data = array(
            "subcategory_name" => $subcategory_name,
            "cat_id" => $cat_id,
            "id !=" =>$id   
        );   
        $tablename = "keyaan_subcategory";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">   Subategory name already exist</div>') ;
            }else{
                if($_FILES['image']['size'] > 0){
                    $config_media['upload_path'] = './uploads/subcategory';
                    $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('image'))
                    {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $image    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                            $url='subcategory/editSubcategory/'.$id;
                                 
                    }        
                }
                if (isset($_FILES['subcat_banner'])) {
                    $config_media1['upload_path'] = './uploads/subcatbanner';
                    $config_media1['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';
                    $config_media1['max_size']   = '1000000000000000'; // whatever you need
                     $this->upload->initialize($config_media1);
                    $error = [];
                    if ( ! $this->upload->do_upload('subcat_banner'))
                    {
                        $error1[] = array('error_image' => $this->upload->display_errors());
                    }
                    else
                    {
                        $data1[] = array('upload_image' => $this->upload->data());
                    }
                    $subcat_banner  = $data1[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        redirect('addSubcategory');
                    }
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $icon = $this->input->post('icon')=="" ? "":$this->input->post('icon');
                $price_selection = $this->input->post('price_selection')=="" ? "":$this->input->post('price_selection');
                if($price_selection == 1) {
                     $price = "";
                }else{
                     $price = $this->input->post('price')=="" ? "":$this->input->post('price');
                }
                if($price_selection == 0) {
                    $price_from = ""; 
                    $price_to = ""; 
                }else{
                    $price_from = $this->input->post('price_from')=="" ? "":$this->input->post('price_from'); 
                    $price_to = $this->input->post('price_to')=="" ? "":$this->input->post('price_to'); 
                }
                $meta_tag = $this->input->post('meta_tag')=="" ? "":$this->input->post('meta_tag');
                $meta_keyword = $this->input->post('meta_keyword')=="" ? "":$this->input->post('meta_keyword'); 
                $meta_detail = $this->input->post('meta_detail')=="" ? "":$this->input->post('meta_detail');  
                $id =$this->input->post('id');
                $dataSubcat = array(
                    'subcategory_name'=> $subcategory_name ,
                    'icon'  => $icon, 
                    'price_selection'  =>$price_selection,
                    'price_from'  =>$price_from,
                    'price_to'   =>  $price_to,
                    'price'       =>  $price,
                    'meta_tag'       =>  $meta_tag,
                    'meta_keyword'       =>  $meta_keyword,
                    'meta_detail'       =>  $meta_detail,   
                    "cat_id" => $cat_id,
                    'updated_at'     => $date,
                    'updatedby'     => $added_by
                );
                if($image!=""){
                    $imgArray =array('image'=> $image);
                    $dataSubcat= array_merge($dataSubcat,$imgArray);
                }
                if($subcat_banner!=""){
                    $imgArray =array('subcat_banner'=> $subcat_banner);
                    $dataSubcat= array_merge($dataSubcat,$imgArray);
                }
                $table="keyaan_subcategory";
                $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Subategory Updated</div>');
                }else{
                    $url='subcategory/editSubcategory/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error,Subcategory not Updated.</div>') ;
                }
            } 
            $url='subcategory/editSubcategory/'.$id;
            redirect($url);
        } else {   
            $url='subcategory/editSubcategory/'.$id;
            redirect($url); 
        }
    }
    function subcatEnable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'0'
        );
        $table="keyaan_subcategory";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewSubcategory';
        redirect($url);
    }      
    function subcatDisable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'1'
        );
        $table="keyaan_subcategory";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewSubcategory';
        redirect($url);
    }
     public function subcatAjax(){
        $id =$this->input->post('id');
        $result = $this->Adminmodel->getAjaxdata('cat_id',$id,'keyaan_subcategory');
        $data['resultSubcat'] =$result;
        $this->load->view('admin/subcatAjax',$data);
    }
    function deleteSubcategory($id) {
        $id=$id;
        $result = $this->Adminmodel->delmultipleImage($id,'keyaan_subcategory','subcategory','subcatbanner','','','image','subcat_banner','','','id');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>