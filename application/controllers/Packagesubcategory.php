<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packagesubcategory extends CI_Controller {
    function __construct() {
        parent::__construct();
         $this->load->helper("encryptionpwd");
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('form_validation');
    }
    public function index() {
         redirect('viewPackagesubcategory');
        } 

    public function viewPackagesubcategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="package_subcategory";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Packagesubcategory/viewPackagesubcategory";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'subcategory_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'subcategory_name');
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
        if($result){
            foreach ($result as $key => $field) {
                $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','package_category');
            } 
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_package_subcategory',$data);
    }  

    public function addPackagesubcategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table2 ="package_category";
        $start =0;
        $limit =100;
        $search ='';
        $dataBefore['category']= $this->Adminmodel->get_current_page_records($table2,$limit,$start,$column=null,$value=null,@$search,@$searchColumn);
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;
        $subcategory_name = $this->input->post('subcategory_name');       
        if($subcategory_name!=''){            
            $check_data = array(
            "subcategory_name" => $this->input->post('subcategory_name')    
            );
            $tablename = "package_subcategory";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Sub Category name already exist</div>');
                $this->load->view('admin/add_package_subcategory',$dataBefore); 
            }else{
                if (isset($_FILES['image'])) {
                    $config_media['upload_path'] = './uploads/package_subcategory';
                    $config_media['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                    $this->load->library('upload',$config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('image'))
                    {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $image    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        redirect('addPackagesubcategory');
                    }        
                } else {
                    $image    = "";
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $base_price = $this->input->post('base_price')=="" ? "":$this->input->post('base_price');
                $details = $this->input->post('details')=="" ? "":$this->input->post('details');
                $category_id = $this->input->post('category_id')=="" ? "":$this->input->post('category_id');                        
                $data = array(
                    'subcategory_name'=> $subcategory_name ,
                    'category_id'  =>$category_id,
                    'base_price'       =>  $base_price,
                    'details'   =>  $details,
                    'image'        => $image,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="package_subcategory";
                $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Package Sub Category Inserted</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Package Sub Category not inserted</div>') ;
                }
                $this->load->view('admin/add_package_subcategory',$dataBefore);   
            }
        }else {
            //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
            $this->load->view('admin/add_package_subcategory',$dataBefore);    
        }       
    }
    
    public function editPackagesubcategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "package_subcategory";
        $table2 = "package_category";
         $start =0;
        $limit =100;
        $search ='';
        $data['category']= $this->Adminmodel->get_current_page_records($table2,$limit,$start,$column=null,$value=null,@$search,@$searchColumn);
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        foreach ($result as $key => $field) {
            $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','package_category');
            if($field['image'] !=""){
                $result[$key]['image'] = base_url()."uploads/package_subcategory/".$field['image'];  
            }else{
               $result[$key]['image'] = "0";   
            }
        }
        $data['result'] = $result[0];
        if($result) {
            $this->load->view('admin/edit_package_subcategory',$data);
        } else {
            $url='viewPackagesubcategory';
            redirect($url);
        }
        
    }
    public function updatePackagesubcategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $subcategory_name = $this->input->post('subcategory_name');       
        if($subcategory_name!=''){            
            $check_data = array(
                "subcategory_name" => $subcategory_name,
                "id !=" =>$id   
            );
            $tablename = "package_subcategory";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Package Sub Category already exist</div>');
            }else{
                if($_FILES['image']['size'] > 0){
                    $config_media['upload_path'] = './uploads/package_subcategory';
                    $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                    $this->load->library('upload',$config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('image')){
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else{
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $image    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                                 redirect('editPackagesubcategory');
                    }        
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $base_price = $this->input->post('base_price')=="" ? "":$this->input->post('base_price');
                $details = $this->input->post('details')=="" ? "":$this->input->post('details');
                $category_id = $this->input->post('category_id')=="" ? "":$this->input->post('category_id');   
                $id =$this->input->post('id');
                $data = array(
                    'subcategory_name'=> $subcategory_name ,
                    'category_id'  =>$category_id,
                    'base_price'       =>  $base_price,
                    'details'   =>  $details,                            
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                if($image!=""){
                    $imgArray =array('image'=> $image);
                    $data= array_merge($data,$imgArray);
                }
                $table="package_subcategory";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Package Sub Category Updated.</div>');
                }else{
                    $url='Packagesubcategory/editPackagesubcategory/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Package Sub Category not updated.</div>') ;
                }   
            } 
            $url='Packagesubcategory/editPackagesubcategory/'.$id;
            redirect($url);
        }else {   
            $url='Packagesubcategory/editPackagesubcategory/'.$id;
            redirect($url); 
        }
    }
    public function packagesubcatAjax(){
        $id =$this->input->post('id');
        $result = $this->Adminmodel->getAjaxdata('category_id',$id,'package_subcategory');
        $data['resultSubcat'] =$result;
        $this->load->view('admin/packagesubcatAjax',$data);
    }
    function packagesubcategoryEnable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'0'
        );
        $table="package_subcategory";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewPackagesubcategory';
            redirect($url);
    }      
    function packagesubcategoryDisable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'1'
        );
        $table="package_subcategory";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewPackagesubcategory';
        redirect($url);
    }
    function deletePackagesubcategory($id) {
        $id=$id;
        $result = $this->Adminmodel->delImage($id,'package_subcategory','package_subcategory','image');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }

}
?>