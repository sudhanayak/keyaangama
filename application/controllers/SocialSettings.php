<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SocialSettings extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->helper("encryptionpwd");
        $this->load->library("pagination");
    }
    public function index() {
        redirect('SocialSettings/viewSocialSettings');
    }
    public function viewSocialSettings(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="social_setting";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
        $config = array();
        $config['reuse_query_string'] = true;
        $config["base_url"] = base_url() . "SocialSettings/viewSocialSettings";
        $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
        $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'location_name');//search
        $config["per_page"] = PERPAGE_LIMIT;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
        $limit =$config["per_page"];
        $start=$page;
        $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'link');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        if($result){
            $data['result'] = $result;
        }else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_social_settings',$data);
    }
    public function addSocialSettings(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;
        $type = $this->input->post('type');       
        if($type!=''){
            $check_data = array(
            "type" => $this->input->post('type')
            );
            $tablename = "social_setting";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Type already exist</div>') ;
                $this->load->view('admin/add_social_settings',$dataBefore);
            }else{
                    
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;
                $link = $this->input->post('link')=="" ? "":$this->input->post('link');
                $date     = date("Y-m-d H:i:s");
                $data = array(
                    'type'=> $type ,
                    'link'=> $link ,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="social_setting";
                $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                        $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Inserted Successfully!</div>');
                }
                else{
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Data not inserted</div>') ;
                }           
                $this->load->view('admin/add_social_settings',$dataBefore); 
            } 
        } else {
            //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
            $this->load->view('admin/add_social_settings',$dataBefore);    
        }     
    }
    public function editSocialSettings(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "social_setting";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $data['result'] = $result ;
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        if($result) {
         $this->load->view('admin/edit_social_settings',$data);
        }else {
          $url='viewSocialSettings';
          redirect($url);
        }
    }
    public function updateSocialSettings(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $type = $this->input->post('type');      
        if($type!=''){            
        $check_data = array(
        "type" => $type,
        "id !=" =>$id   
        );
        $tablename = "social_setting";
        $checkData = $this->Adminmodel->existData($check_data,$tablename) ;

        if($checkData > 0){
            $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Type already exist</div>') ;
        }else{
            $admin = $this->session->userdata('userCode');
            $added_by = $admin!='' ? $admin:'admin' ;          
            $date     = date("Y-m-d H:i:s");
            $link = $this->input->post('link')=="" ? "":$this->input->post('link'); 
            $id =$this->input->post('id');
            $data = array(
                'type'=> $type ,
                'link'  => $link,
                'updated_at'     => $date,
                'updated_by'     => $added_by
            );
            $table="social_setting";
            $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
            if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Social Settings Updated</div>');
                    $this->load->view('admin/edit_social_settings',$data);
            }
            else{
                $url='SocialSettings/editSocialSettings/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps Some error</div>') ;
            }  
            }
            $url='SocialSettings/editSocialSettings/'.$id;
            redirect($url);
        }
        else
        {   
            $url='SocialSettings/editSocialSettings/'.$id;
            redirect($url);   
        }
    }
    function SocialSettingsEnable($id) {
        $id=$id;
        $dataSubcat =array(
           'keyaan_status' =>'0'
        );
        $table="social_setting";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewSocialSettings';
        redirect($url);
    }      
    function SocialSettingsDisable($id) {
        $id=$id;
        $dataSubcat =array(
           'keyaan_status' =>'1'
        );
        $table="social_setting";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewSocialSettings';
        redirect($url);
    }
    function deleteSocialSettings($id) {
        $id=$id;
        $result = $this->Adminmodel->delRow($id,'social_setting');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>