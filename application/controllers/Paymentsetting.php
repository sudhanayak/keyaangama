<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Paymentsetting extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
    }
    public function index() {
         redirect('viewPaymentsetting');
        } 
    public function addPaymentsetting() {
      if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
      {
      redirect('admin');
      }
      $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
      $dataBefore['basicsettingsList']=$resultBasicsettings;
       $payment_mode = $this->input->post('payment_mode');
         $payment_gateway = $this->input->post('payment_gateway');
         $payment_status = $this->input->post('payment_status');
            if($payment_mode!=''){
            $check_data = array(
            "payment_mode" => $payment_mode
            );
            $tablename = "payment_setting";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Payment already exist</div>');
                $this->load->view('admin/add_paymentsetting',$dataBefore);
            }else{
                    $admin = $this->session->userdata('userCode');
                    $added_by = $admin!='' ? $admin:'admin' ;
                    if($payment_mode == 1) {
                     $payment_gateway = "";
                     $payment_status = "";
                     $merchant_keys = "";
                     $salt_keys = "";
                    }else{
                      $payment_gateway = $this->input->post('payment_gateway')=="" ? "":$this->input->post('payment_gateway');
                      $payment_status = $this->input->post('payment_status')=="" ? "":$this->input->post('payment_status');
                      $merchant_keys = $this->input->post('merchant_keys')=="" ? "":$this->input->post('merchant_keys');
                      $salt_keys = $this->input->post('salt_keys')=="" ? "":$this->input->post('salt_keys');
                    }
                    $date     = date("Y-m-d H:i:s");
                    $data = array(
                        'payment_mode'=> $payment_mode ,
                        'payment_gateway'=> $payment_gateway ,
                        'payment_status'=> $payment_status ,
                        'merchant_keys'=> $merchant_keys ,
                        'salt_keys'    => $salt_keys ,
                        'created_by'     => $added_by ,
                        'created_at'     => $date,
                        'updated_at'     => $date,
                        'updated_by'     => $added_by
                    );
                    $table="payment_setting";
                    $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                    if($result){
                            $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Payment Inserted Successfully!</div>');
                    }
                    else{
                            $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp!Payment not inserted</div>');
                    }           
                    $this->load->view('admin/add_paymentsetting',$dataBefore); 
                }
            }else
            {
                //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
                $this->load->view('admin/add_paymentsetting',$dataBefore);    
            }     
      } 
    public function viewPaymentsetting(){
       $table ="payment_setting";
       $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Paymentsetting/viewPaymentsetting";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'payment_gateway');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'payment_gateway');
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
            if($result){
                $data['result'] = $result ;
            } else {
                $result[] = [] ;
                $data['result'] = $result ;
            }
            $data['searchVal'] = $search !='null'?$search:"";
            $this->load->view('admin/view_paymentsetting',$data);

    }
    public function editPaymentsetting(){
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "payment_setting";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $data['result'] = $result ;
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        if($result) {
            $this->load->view('admin/edit_paymentsetting',$data);
        } else {
            $url='viewPaymentsetting';
            redirect($url);
        }
    }
    public function updatePaymentsetting(){
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
         $payment_mode = $this->input->post('payment_mode');
         $payment_gateway = $this->input->post('payment_gateway');
         $payment_status = $this->input->post('payment_status');      
         if($payment_mode!='' && $payment_gateway!='' && $payment_status!=''){            
             $check_data = array(
             "payment_mode" => $payment_mode,
             "payment_gateway" => $payment_gateway,
             "payment_status" => $payment_status,
             "id !=" =>$id   
             );
             $tablename = "payment_setting";
             $checkData = $this->Adminmodel->existData($check_data,$tablename) ;

             if($checkData > 0){
                 $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Payment already exist</div>') ;
             }else{
                 $admin = $this->session->userdata('userCode');
                 $added_by = $admin!='' ? $admin:'admin' ;
                 if($payment_mode == 1) {
                     $payment_gateway = "";
                     $payment_status = "";
                     $merchant_keys = "";
                     $salt_keys = "";
                }else{
                  $payment_gateway = $this->input->post('payment_gateway')=="" ? "":$this->input->post('payment_gateway');
                  $payment_status = $this->input->post('payment_status')=="" ? "":$this->input->post('payment_status');
                 $merchant_keys = $this->input->post('merchant_keys')=="" ? "":$this->input->post('merchant_keys');
                 $salt_keys = $this->input->post('salt_keys')=="" ? "":$this->input->post('salt_keys');
                }
                 $date     = date("Y-m-d H:i:s");
                 $id =$this->input->post('id');
                 $data = array(
                     'payment_mode'=> $payment_mode ,
                     'payment_gateway'=> $payment_gateway ,
                     'payment_status'=> $payment_status ,
                     'merchant_keys'=> $merchant_keys ,
                     'salt_keys'    => $salt_keys ,
                     'created_at'     => $date,
                     'created_by'     => $added_by ,
                     'updated_at'     => $date,
                     'updated_by'     => $added_by
                 );
                 $table="payment_setting";
                 $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                 if($result){
                         $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Payment Updated</div>');
                 }
                 else{
                        $url='Paymentsetting/editPaymentsetting/'.$id;
                         redirect($url);
                         $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps Some error</div>') ;
                 }
             } 
             $url='Paymentsetting/editPaymentsetting/'.$id;
             redirect($url);
         }
         else
         {   
             $url='Paymentsetting/editPaymentsetting/'.$id;
             redirect($url);    
         }

     }
     function PaymentsettingEnable($id)
      {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'0'
        );
        $table="payment_setting";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='Paymentsetting/viewPaymentsetting';
        redirect($url);
      }      
      function PaymentsettingDisable($id)
        {
          $id=$id;
          $dataSubcat =array(
              'keyaan_status' =>'1'
          );
          $table="payment_setting";
          $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
          $url='Paymentsetting/viewPaymentsetting';
          redirect($url);
        }
      function deletePaymentsettings($id) {
        $id=$id;
        $result = $this->Adminmodel->delRow($id,'payment_setting');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
    
}

