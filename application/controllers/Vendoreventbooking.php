<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Vendoreventbooking extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
    }
    public function index() {
         redirect('viewVendoreventbooking');
        } 
    public function viewVendoreventbooking(){
         if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $table ="event_booking";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Vendoreventbooking/viewVendoreventbooking";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $vendor_code = $this->session->userdata('vendorCode');
       $result = $this->Adminmodel->getEventbooking($table,'vendor_code',$vendor_code);
        if($result){
            foreach ($result as $key => $field) {
                $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['cat_id'],'id','category_name','keyaan_category')  ;
                $data['result'] = $result ;
            }
        } else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('vendor/view_eventbooking',$data);
    }
    public function VendoreventbookingDetails(){
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $table ="event_booking";
        $start=0;
        $perPage = 100;
        $enquiryId = $this->uri->segment('3');
        if($enquiryId !=""){
            @$column = "id";
            @$value  = $enquiryId;
        }
        $search ='';
        $result = $this->Adminmodel->get_current_page_records($table,$perPage,$start,@$column,@$value,@$search,@$searchColumn);
        if($result){
            foreach ($result as $key => $field) {
                $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['cat_id'],'id','category_name','keyaan_category')  ;
                $data['result'] = $result ;
            }
            $data['result'] = $result ;
            $this->load->view('vendor/eventbooking_details',$data);
        } else {
            redirect('Vendoreventbooking/viewEventbooking');
            // $this->load->view('admin/view_eventbooking');
        }
    }
}
?>
