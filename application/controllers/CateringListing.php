<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class CateringListing extends CI_Controller
{
	function __construct() {
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->model('Adminmodel');       
        }
	function index()
	{
        self::cateringList();
		
	}
    public function cateringList(){
        if($this->session->userdata('userCCode') !=""){
            $userCode = $this->session->userdata('userCCode');
            $cartCount = $this->Adminmodel->getcartCount($userCode);
        }else{
            $cartCount=0;
        }
        $data['menuCategoryAll'] = self::getCategoryList('keyaan_category');
        $data['menuPackageAll'] = self::getCategoryPackList('package_category');
        $resultCategoryAll = $this->Adminmodel->getAjaxdataCountry('keyaan_category'); 
        $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $cateringCategory = $this->Adminmodel->getAjaxdataCountry('catering_category');
        $cateringSubcategory = $this->Adminmodel->getAjaxdataCountry('catering_subcategory');
        $catid= $this->uri->segment(2);
        $cityid= $this->uri->segment(3);
        $priceBase= $this->uri->segment(4);
        if($catid!="" && $cityid=="" && $priceBase==""){
            $cityid=""; 
            $priceBase ="";
            $catid = $catid;
        }
        $start=0;
        $perPage = 100;    

        //if($start!="" && $perPage!=""){
        $table="keyaan_vendor";      
        $result = $this->Adminmodel->cateringListing($table,$perPage,$start,@$catid,@$cityid,@$priceBase,@$sorting);   
        $data['category'] = $this->Adminmodel->getSingleColumnName($catid,'id',
        'category_name','keyaan_category') ;
        if($result > 0){
            foreach ($result as $key => $field) {  
                $result[$key]['basic_image'] = base_url()."uploads/basic_image/".$field['basic_image'];
                $result[$key]['logo'] = base_url()."uploads/vendorlogo/".$field['logo'];
            } 
            $resultSubcat=array();
            $data['result'] = $result ;
            $data['categoryList']=$resultCategoryAll;
            $data['catid'] =$catid; //to dispaly category related vendors
            $data['cityid'] =$cityid;
            $data['priceBase'] =$priceBase;
            $data['cityList']=$resultCities;
            $data['basicsettingsList']=$resultBasicsettings;
            $data['cartCount']=$cartCount;
            $data['cateringCategory']=$cateringCategory;
            $data['cateringSubcategory']=$cateringSubcategory;
            $data['catsegment'] =$this->uri->segment(2); 
            $data['citysegment'] =$this->uri->segment(3); 
            $data['pricesegment'] =$this->uri->segment(4);
            $this->load->view('user/catering_list',$data);
        } else{
            $resultSubcat=array();
            $result =array();
            $data['result'] = $result;
            $data['categoryList']=$resultCategoryAll; 
            $data['catid'] =$catid; //to dispaly category related vendors
            $data['cityid'] =$cityid;
            $data['priceBase'] =$priceBase;
            $data['cityList']=$resultCities;
            $data['basicsettingsList']=$resultBasicsettings;
            $data['cartCount']=$cartCount; 
            $data['cateringCategory']=$cateringCategory;
            $data['cateringSubcategory']=$cateringSubcategory;
            $data['catsegment'] =$this->uri->segment(2); 
            $data['citysegment'] =$this->uri->segment(3); 
            $data['pricesegment'] =$this->uri->segment(4);
            $this->load->view('user/catering_list',$data);
        }
    } 
    
    //For Filter
    public function venueFilter() {
        $catid= $this->input->post('category');
        $cityid= $this->input->post('city');
        $venue_type = $this->input->post('venue_type');
        $venue_price = $this->input->post('venue_price');
        $audience = $this->input->post('audience');
        $rating_filter = $this->input->post('rating_filter');
        $facility = $this->input->post('facility');
        if($venue_price !=""){
            $priceBase= $venue_price;
        } else{
            $priceBase= $this->input->post('price');
        }

        if($catid!="" && $cityid=="" && $priceBase==""){
            $cityid=""; 
            $priceBase ="";
            $catid = $catid;
        }
        $start=0;
        $perPage = 100;    

        //if($start!="" && $perPage!=""){
        $table="venues";  
        //$thmecount =$this->Adminmodel->themeListingCount($table,$perPage,$start,@$catid,@$cityid,@$priceBase,@$subcatId,@$rating_filter,@$sorting);        
        $result = $this->Adminmodel->venueListing($table,$perPage,$start,@$catid,@$cityid,
        @$priceBase,@$venue_type,@$audience,@$rating_filter,@$facility);  
        $data['category'] = $this->Adminmodel->getSingleColumnName($catid,'id',
        'category_name','keyaan_category') ;
        if($result > 0){
            foreach ($result as $key => $field) {  
                $result[$key]['image'] = base_url()."uploads/venue_images/".$field['image'];
                $facilityList = $this->Adminmodel->facilityList('venue_facilities',$field['vendor_code'],$field['venue_code']);
                foreach ($facilityList as $key => $field1) {  
                    $facilityList[$key]['facility_name'] = $this->Adminmodel->getSingleColumnName($field1['facility_id'],'id','faciility_name','facilities');
                }
            } 
            print_r($result);exit;
            $resultSubcat=array();
            $data['result'] = $result ;
            $data['catid'] =$catid; //to dispaly category related vendors
            $data['cityid'] =$cityid;
            $data['priceBase'] =$priceBase;
            $this->load->view('user/venue_filter_ajax',$data);
        } else{
            $resultSubcat=array();
            $result =array();
            $data['result'] = $result;
            $data['catid'] =$catid; //to dispaly category related vendors
            $data['cityid'] =$cityid;
            $data['priceBase'] =$priceBase;
            $this->load->view('user/venue_filter_ajax',$data);
        }
    }
    public function getCategoryList(){

        $result = $this->Adminmodel->getCategoryList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryList($field['id']);
             
        } 

        return $result ;   

    }
    public function getSubCategoryList($catId){
          
        $result = $this->Adminmodel->getSubCategoryList($catId);
        foreach ($result as $key => $field) {
             $result[$key]['subsubcatList'] = $this->Adminmodel->getSubsubCategoryList($field['id']) ;
        } 

        return $result ;   

    }
    public function getCategoryPackList(){

        $result = $this->Adminmodel->getCategoryPackList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryPackList($field['id']); 
        } 
        return $result ;   
    }
    public function getSubCategoryPackList($catId){  
        $result = $this->Adminmodel->getSubCategoryPackList($catId);
        return $result ;   
    }
}
?>