<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Country extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
    }
     public function index() {
         redirect('viewcountry');
        } 
    	public function addcountry(){
          if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
          {
            redirect('admin');
          }
          $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
          $dataBefore['basicsettingsList']=$resultBasicsettings;
	      	$country_name = $this->input->post('country_name');       
	        if($country_name!=''){            
	        $check_data = array(
	            "country_name" => $this->input->post('country_name')    
	        );
	        $tablename = "keyaan_countries";
	        $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
	        if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger">Country name already exist</div>') ;
                $url='addcountry';
                redirect($url);
	        }else{ 
	              $admin = $this->session->userdata('userCode');
	              $added_by = $admin!='' ? $admin:'admin' ;
	              $country_code = $this->input->post('country_code')=="" ? "":$this->input->post('country_code');
	              $date     = date("Y-m-d H:i:s");
	              $data = array(
	                  'country_code'=> $country_code ,
	                  'country_name'=> $country_name ,
	                  'created_by'     => $added_by ,
	                  'created_at'     => $date,
	                  'updated_at'     => $date,
	                  'updated_by'     => $added_by
	              );
	                $table="keyaan_countries";
	                $result = $this->Adminmodel->insertRecordQueryList($table,$data);
	                if($result){
	                   $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Country Added Successfully!</div>');
	                }
	                else{
	                   $this->session->set_flashdata('msg','<div class="alert alert-danger">opp! Data not inserted</div>') ;
	                }           
	                $url='addcountry';
	                redirect($url);
	            }
	            }else
	            {
	                //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
	                $this->load->view('admin/add_country',$dataBefore);    
	            }     
	        }
        public function viewcountry(){
          if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
          {
            redirect('admin');
          }
           $table ="keyaan_countries";
           $search = ($this->input->get("search"))? $this->input->get("search") : "null";
           $config = array();
           $config['reuse_query_string'] = true;
           $config["base_url"] = base_url() . "Country/viewcountry";
           $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
           $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'country_name');//search
           $config["per_page"] = PERPAGE_LIMIT;
           $config["uri_segment"] = 3;
           $config['full_tag_open'] = "<ul class='pagination'>";
           $config['full_tag_close'] = '</ul>';
           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';
           $config['cur_tag_open'] = '<li class="active"><a href="#">';
           $config['cur_tag_close'] = '</a></li>';
           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';
           $config['first_tag_open'] = '<li>';
           $config['first_tag_close'] = '</li>';
           $config['last_tag_open'] = '<li>';
           $config['last_tag_close'] = '</li>';
           $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';
           $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';
           $this->pagination->initialize($config);
           $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
           $data["links"] = $this->pagination->create_links();
           $limit =$config["per_page"];
           $start=$page;
           $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'country_name');
            $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
            $data['basicsettingsList']=$resultBasicsettings;
              if($result){
                $data['result'] = $result ;
              } else {
                $result[] = [] ;
                $data['result'] = $result ;
              }
              $data['searchVal'] = $search !='null'?$search:"";
              $this->load->view('admin/viewCountry',$data);
            }
            public function editcountry(){
    	        $id = $this->uri->segment('3');
    	        if($id==''){
    	            redirect('adminLogin');
    	        }
    	                $tablename = "keyaan_countries";
    	        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
              $data['result'] = $result ;
              $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
              $data['basicsettingsList']=$resultBasicsettings;
                if($result) {
                    $this->load->view('admin/edit_country',$data);
                } else {
                    $url='viewcountry';
                    redirect($url);
                }
    	        

    	    }
    	public function updatecountry(){
          if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
          {
            redirect('admin');
          }
           $id = $this->input->post('id');
            if(empty($id)){
                redirect('adminLogin');
            }
            $country_name = $this->input->post('country_name');      
             if($country_name!=''){            
                $check_data = array(
                "country_name" => $country_name,
                "id !=" =>$id   
               );
                 $tablename = "keyaan_countries";
                 $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
                 if($checkData > 0){
                     $this->session->set_flashdata('msg','<div class="alert alert-danger">Country name already exist</div>') ;
                 }else{
                      $admin = $this->session->userdata('userCode');
                      $added_by = $admin!='' ? $admin:'admin' ;     
                      $date     = date("Y-m-d H:i:s");
                      $country_code = $this->input->post('country_code')=="" ? "":$this->input->post('country_code'); 
                      $id =$this->input->post('id');
                      $data = array(
                          'country_name'=> $country_name ,
                          'country_code'  => $country_code,                            
                          'updated_at'     => $date,
                          'updated_by'     => $added_by
                      );
                      $table="keyaan_countries";
                      $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                      if($result){
                         $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Country Updated</div>');
                       }
                      else{
                          $url='country/editcountry/'.$id;
                          redirect($url);
                          $this->session->set_flashdata('msg','<div class="alert alert-danger">Opps Some error</div>') ;
                      }
                 } 
                 $url='country/editcountry/'.$id;
                 redirect($url);
             }
             else
             {   
                 $url='country/editcountry/'.$id;
                 redirect($url);    
             }

         }
         function countryEnable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'keyaan_status' =>'0'
            );
            $table="keyaan_countries";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='country/viewcountry';
            redirect($url);
        }
        function countryDisable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'keyaan_status' =>'1'
            );
            $table="keyaan_countries";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='country/viewcountry';
            redirect($url);
        }
         public function country(){
           $this->load->view('admin/countryAjax');
       }
        public function countryCode(){
            $id =$this->input->post('id');
            $countryCode = $this->Adminmodel->getSingleColumnName($id,'id','country_code','keyaan_countries');
            $data['countryCode'] =$countryCode;
            $this->load->view('admin/countryCode',$data);
        }
       function deletecountry($id) {
        $id=$id;
        $result = $this->Adminmodel->delRow($id,'keyaan_countries');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }

}
?>
