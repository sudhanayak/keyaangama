<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Theme extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('image_lib');
    }
    public function index() { 
        redirect('viewTheme');          
    }
    //For Add Theme
    public function addTheme() { 
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $min='1452';
        $max='8569';
        $themeCode =rand($min,$max);
        $dataBefore =[];
        $catList =[];
        $this->load->library('upload');
        $theme_name = $this->input->post('theme_name');
        $vendor_code = $this->session->userdata('vendorCode');
        $resultCategory = $this->Adminmodel->getCategoryName('vendor_categories','vendor_code',$vendor_code);
        $resultCity = $this->Adminmodel->getCategoryName('vendor_cities','vendor_code',$vendor_code);
        $d=0;
        foreach ($resultCategory as $key => $field) {
            $cid = $field['category_id'];
            $cval =  $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','keyaan_category') ; 
            $catList[$d]['id'] =$cid;
            $catList[$d]['category_name'] =$cval;
            $d++;
        }
        $dataBefore['category'] = self::array_multi_unique($catList);
        $vendor_code = $this->session->userdata('vendorCode');
        $added_by = $vendor_code!='' ? $vendor_code:'vendor' ;
        if($theme_name!=''){            
            $check_data = array(
            "theme_name" => $this->input->post('theme_name'),
            "vendor_code" => $this->input->post('vendor_code')    
            );
            $tablename = "vendor_themelist";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;           
            $date     = date("Y-m-d H:i:s");
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Theme name already exist</div>') ;
                $this->load->view('vendor/add_theme',$dataBefore);
            }else{


                 if($_FILES['theme_banner']['size'] > 0) {
                $config_media['upload_path'] = './uploads/bannerimage';
                $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                $config_media['max_size']   = '1000000000000000'; // whatever you need
                // $this->load->library($config_media);
                $this->upload->initialize($config_media);
                $error = [];
                if ( ! $this->upload->do_upload('theme_banner'))
                {
                    $error[] = array('error_image' => $this->upload->display_errors()); 
                }
                else
                {
                    $data[] = array('upload_image' => $this->upload->data());
                }       
                $banner_image = $data[0]['upload_image']['file_name'];
                $source_path = './uploads/bannerimage/' .$banner_image;
                $target_path = './uploads/thumbnail/bannerimage';
                $this->resizeImage($source_path,$target_path);
                if(count($error) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in banner image uploads</div>') ;
                    redirect('addTheme');
                    //echo"banner image error";
                }        
            }else{
                $banner_image = "";
            }
                $imgage=count($_FILES['theme_image']['name']);
                if($imgage !=''){
                   $i=0;
                   $filesCount = count($_FILES['theme_image']['name']);
                   for($i = 0; $i < $filesCount; $i++){
                       $_FILES['file']['name']     = $_FILES['theme_image']['name'][$i];
                       $_FILES['file']['type']     = $_FILES['theme_image']['type'][$i];
                       $_FILES['file']['tmp_name'] = $_FILES['theme_image']['tmp_name'][$i];
                       $_FILES['file']['error']     = $_FILES['theme_image']['error'][$i];
                       $_FILES['file']['size']     = $_FILES['theme_image']['size'][$i];
                       // File upload configuration
                       $uploadPath = './uploads/themeimage/';
                       $config9['upload_path'] = $uploadPath;
                       $config9['allowed_types'] = 'jpg|jpeg|png|gif';
                       // Load and initialize upload library
                       $this->upload->initialize($config9);
                       // Upload file to server
                       if($this->upload->do_upload('file')){
                           // Uploaded file data
                           $fileData = $this->upload->data();
                           $uploadData[$i]['file_name'] = $fileData['file_name'];
                           $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                           $source_path = './uploads/themeimage/' .$uploadData[$i]['file_name'];
                            $target_path = './uploads/thumbnail/themeimage';
                            $this->resizeImage($source_path,$target_path);
                           $data = array(
                               'vendor_code'=> $vendor_code ,
                               'theme_image' => $uploadData[$i]['file_name'],
                               'theme_code'=> $themeCode ,
                              'media_type'=>'1'
                           );
                          $result = $this->Adminmodel->insertRecordQueryList('vendor_theme_details',$data);
                        }
                    }
                }
                if(@$_FILES['theme_video']['size'] > 0) {
                    $config_media6['upload_path'] = './uploads/themevideo';
                    $config_media6['allowed_types'] = 'mp4|avi|flv|wmv|mpeg|mp3|3GPP';
                    $config_media6['max_size']  = '1000000000000000'; // whatever you need
                    //  $this->load->library($config_media6);
                    $this->upload->initialize($config_media6);
                    $error4 = [];
                    if ( ! $this->upload->do_upload('theme_video'))
                    {
                        $error4[] = array('error_image' => $this->upload->display_errors());
                    }
                    else
                    {
                        $data7[] = array('upload_image' => $this->upload->data());
                    }
                    $imagevideo = $data7[0]['upload_image']['file_name'];
                    if(count($error4) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in video uploads</div>') ;
                        redirect('addTheme');
                        //echo"video uploads error";
                    }else{
                        $data = array(
                            'vendor_code'=> $vendor_code ,
                            'theme_image' => $imagevideo,
                            'theme_code'=> $themeCode ,
                            'media_type'=>'2'
                        );
                        $result = $this->Adminmodel->insertRecordQueryList('vendor_theme_details',$data);
                    }
                }else{
                    $imagevideo = "";
                }    
                $theme_name = $this->input->post('theme_name')=="" ? "":$this->input->post('theme_name');
                $cat_id = $this->input->post('cat_id') =="" ? "":$this->input->post('cat_id');
                $subcat_id = $this->input->post('subcat_id') =="" ? "":$this->input->post('subcat_id');
                $subsubcat_id = $this->input->post('subsubcat_id') =="" ? "":$this->input->post('subsubcat_id');
                $price = $this->input->post('price')=="" ? "":$this->input->post('price');
                $inclusions = $this->input->post('inclusions')=="" ? "":$this->input->post('inclusions');
                $pre_requisites = $this->input->post('pre_requisites')=="" ? "":$this->input->post('pre_requisites');
                $facts = $this->input->post('facts')=="" ? "":$this->input->post('facts');
                $terms_and_conditins = $this->input->post('terms_and_conditins')=="" ? "":$this->input->post('terms_and_conditins');
                $theme_detail = $this->input->post('theme_detail')=="" ? "":$this->input->post('theme_detail');
                $theme_note = $this->input->post('theme_note')=="" ? "":$this->input->post('theme_note');
                $data = array(
                    'theme_name'=> $theme_name ,
                    'cat_id' =>  $cat_id,
                    'subcat_id' =>  $subcat_id,
                    'subsubcat_id' =>  $subsubcat_id,
                    'vendor_code' =>  $vendor_code,
                    'theme_code'=> $themeCode ,
                    'price'       =>  $price,
                    'inclusions'=> $inclusions ,
                    'pre_requisites'=> $pre_requisites ,
                    'facts'=> $facts ,
                    'terms_and_conditins'=> $terms_and_conditins ,
                    'theme_detail'=> $theme_detail ,
                    'theme_note' => $theme_note,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'theme_banner'=> $banner_image
                );
                $table="vendor_themelist";
                $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $themeData = array(
                        'default_theme'=> '1' ,
                    );   
                    $where = array(
                        "vendor_code"=>$vendor_code,
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList2('keyaan_vendor',$themeData,$where);
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Theme Inserted</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Theme not inserted</div>') ;
                }
                $this->load->view('vendor/add_theme',$dataBefore);   
            }
        }else {
            //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
            $this->load->view('vendor/add_theme',$dataBefore);    
        } 
    }
    //Edit Theme
    public function editTheme() {   
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $dataBefore =[];  
        $catList =[];
        $id = $this->uri->segment('3');        
        $this->load->library('upload');
        $vendor_code = $this->session->userdata('vendorCode');
        $resultCategory = $this->Adminmodel->getCategoryName('vendor_categories','vendor_code',$vendor_code);
        $added_by = $vendor_code!='' ? $vendor_code:'vendor' ;           
        $date     = date("Y-m-d H:i:s");
        $d=0;
        foreach ($resultCategory as $key => $field) {
            $cid = $field['category_id'];
            $cval =  $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','keyaan_category') ; 
            $catList[$d]['id'] =$cid;
            $catList[$d]['category_name'] =$cval;
            $d++;
        }
        $dataBefore['category'] = self::array_multi_unique($catList);
        if(!empty($id)){
            $tablename = "vendor_themelist";
            $result = $this->Adminmodel->singleRecordData('id', $id,$tablename);
            if($result){
                foreach ($result as $key => $field) {
                    $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['cat_id'],'id','category_name','keyaan_category') ;
                    $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($field['subcat_id'],'id','subcategory_name','keyaan_subcategory') ;
                    $result[$key]['subsubcategory'] = $this->Adminmodel->getSingleColumnName($field['subsubcat_id'],'id','subsubcat_name','keyaan_subsubcategory') ;
                    $result['themeimglist'] = $this->Adminmodel->getgallery($field['theme_code'],'vendor_theme_details');
                    if($field['theme_banner'] !=""){
                     $result[$key]['theme_banner'] = base_url()."uploads/bannerimage/".$field['theme_banner'];                    
                    } 
                }
                $dataBefore['result'] = replace_empty_values($result);
                $this->load->view('vendor/edit_theme',$dataBefore);
            } else {
                $url='viewTheme';
                redirect($url);
            }
        }else{
            redirect('Mastervendor');
        }
    }
    //Update Vendor
    public  function updateTheme(){
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $theme_name =$this->input->post('theme_name');
        $id =$this->input->post('id');
        $this->load->library('upload');
        $vendor_code = $this->session->userdata('vendorCode');
        $added_by = $vendor_code!='' ? $vendor_code:'vendor' ;           
        $date     = date("Y-m-d H:i:s");
        if(!empty($theme_name) && !empty($id) && !empty($vendor_code)){
           if($_FILES['theme_banner']['size'] > 0) {
                    $config_media['upload_path'] = './uploads/bannerimage';
                    $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                   // $this->load->library($config_media);
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('theme_banner')) {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $banner_image = $data[0]['upload_image']['file_name'];
                    $source_path = './uploads/bannerimage/' .$banner_image;
                    $target_path = './uploads/thumbnail/bannerimage';
                    $this->resizeImage($source_path,$target_path);
                    $imgArr = array(
                        "theme_banner"=>$banner_image ,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('vendor_themelist',$imgArr,'id',$id);
                    if(count($error) >0){
                     $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in banner image uploads</div>') ;
                        $url='Theme/editTheme/'.$id;
                        redirect($url);
                    }        
                }else{
                    $banner_image = "";
                }

          
            // for Theme Video
            $themeCode =$this->input->post('theme_code');
            if(@$_FILES['theme_video']['size'] > 0) {
                $config_media6['upload_path'] = './uploads/themevideo';
                $config_media6['allowed_types'] = 'mp4|avi|flv|wmv|mpeg|mp3|3GPP';  
                $config_media6['max_size']  = '1000000000000000'; // whatever you need
            //  $this->load->library($config_media6);
                $this->upload->initialize($config_media6);
                $error4 = [];
                if ( ! $this->upload->do_upload('theme_video'))
                {
                    $error4[] = array('error_image' => $this->upload->display_errors());    
                }
                else
                {
                    $data7[] = array('upload_image' => $this->upload->data());
                }       
                $imagevideo = $data7[0]['upload_image']['file_name'];
                $imgArr20 = array(
                    "theme_image"=>$imagevideo,
                    "updated_date"=>$date  
                );   
                $where = array(
                    "vendor_code"=>$vendor_code,
                    "theme_code"=> $themeCode,
                    "media_type"=>'2'
                    );
                $resultUpdate = $this->Adminmodel->updateRecordQueryList2('vendor_theme_details',$imgArr20,'vendor_code',$vendor_code,$where);
                if(count($error4) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in video uploads</div>') ;
                    redirect('updateTheme');
                    //echo"video uploads error";
                }else{
                    $video="";
                }        
            }else{
                $imagevideo = "";
            }
            
            // for Multiple Theme Images
            $imgage=count($_FILES['theme_image']['name']);   
            if($imgage !=''){                               
                $i=0; 
                $filesCount = count($_FILES['theme_image']['name']);
                for($i = 0; $i < $filesCount; $i++){
                $_FILES['file']['name']     = $_FILES['theme_image']['name'][$i];
                $_FILES['file']['type']     = $_FILES['theme_image']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['theme_image']['tmp_name'][$i];
                $_FILES['file']['error']     = $_FILES['theme_image']['error'][$i];
                $_FILES['file']['size']     = $_FILES['theme_image']['size'][$i];
                // File upload configuration
                $uploadPath = './uploads/themeimage/';
                $config9['upload_path'] = $uploadPath;
                $config9['allowed_types'] = 'jpg|jpeg|png|gif';
                // Load and initialize upload library
                $this->upload->initialize($config9);
                // Upload file to server
                    if($this->upload->do_upload('file')){
                        // Uploaded file data
                        $fileData = $this->upload->data();
                        $uploadData[$i]['file_name'] = $fileData['file_name'];
                        $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                        $source_path = './uploads/themeimage/' .$uploadData[$i]['file_name'];
                        $target_path = './uploads/thumbnail/themeimage';
                        $this->resizeImage($source_path,$target_path);
                        $imgArr20 = array(
                            "theme_image"=>$uploadData[$i]['file_name'],
                            "updated_date"=>$date,
                            "vendor_code"=>$vendor_code,
                            "theme_code"=>$themeCode,
                            "media_type"=>'1'
                        );                            
                        $resultUpdate = $this->Adminmodel->insertRecordQueryList('vendor_theme_details',$imgArr20);
                    }
                }
            }                
            $dateCurrent= date("Y-m-d H:i:s");
            $theme_name = $this->input->post('theme_name')=="" ? "":$this->input->post('theme_name');
            $cat_id = $this->input->post('cat_id') =="" ? "":$this->input->post('cat_id');
            $subcat_id = $this->input->post('subcat_id') =="" ? "":$this->input->post('subcat_id');
            $subsubcat_id = $this->input->post('subsubcat_id') =="" ? "":$this->input->post('subsubcat_id');
            $price = $this->input->post('price')=="" ? "":$this->input->post('price');
            $inclusions = $this->input->post('inclusions')=="" ? "":$this->input->post('inclusions');
            $pre_requisites = $this->input->post('pre_requisites')=="" ? "":$this->input->post('pre_requisites');
            $facts = $this->input->post('facts')=="" ? "":$this->input->post('facts');
            $terms_and_conditins = $this->input->post('terms_and_conditins')=="" ? "":$this->input->post('terms_and_conditins');
            $theme_detail = $this->input->post('theme_detail')=="" ? "":$this->input->post('theme_detail');
            $theme_note = $this->input->post('theme_note')=="" ? "":$this->input->post('theme_note');
            $created_at = date("Y-m-d H:i:s");
            $updated_at = date("Y-m-d H:i:s");
            $created_by = 1;
            $updated_by = 1;

            $dataVendor = array(
                'theme_name'=> $theme_name ,
                'cat_id' =>  $cat_id,
                'subcat_id' =>  $subcat_id,
                'subsubcat_id' =>  $subsubcat_id,
                'vendor_code' =>  $vendor_code,
                'theme_code'=> $themeCode ,
                'price'       =>  $price,
                'inclusions'=> $inclusions ,
                'pre_requisites'=> $pre_requisites ,
                'facts'=> $facts ,
                'terms_and_conditins'=> $terms_and_conditins ,
                'theme_detail'=> $theme_detail ,
                'theme_note'=> $theme_note ,
            );
            $tableVendor="vendor_themelist";
            $result = $this->Adminmodel->updateRecordQueryList($tableVendor,$dataVendor,'id',$id);
            $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Theme Updated Successfully.</div>') ;
            $url='Theme/editTheme/'.$id;
            redirect($url); 
        }  else{
        //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;                   
        $url='Theme/editTheme/'.$id;
        redirect($url);
        }
    }
    //For View Theme
    public function viewTheme(){
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $table="vendor_themelist";
        $input  = json_decode(file_get_contents('php://input'), true);
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Theme/viewTheme";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'theme_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $vendor_code = $this->session->userdata('vendorCode');
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,'vendor_code',$vendor_code,$search,'theme_name');
        if($result){
            foreach ($result as $key => $field) {
                $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['cat_id'],'id',
                    'category_name','keyaan_category') ;
                $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($field['subcat_id'],'id','subcategory_name','keyaan_subcategory') ;
                $result[$key]['subsubcategory'] = $this->Adminmodel->getSingleColumnName($field['subsubcat_id'],'id','subsubcat_name','keyaan_subsubcategory') ;
            }
            $data['result'] = $result ;
        }
        else{
            $result = [];
            $data['result'] = $result ;
        }        
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('vendor/view_theme',$data);
    }
    public function themeDetails() {
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
       $input  = json_decode(file_get_contents('php://input'), true);
       $start=0;
       $perPage = 100;
       $catId = $this->uri->segment('3');
       //if($start!="" && $perPage!=""){
       $table="vendor_themelist";
       if($catId !=""){
          @$column = "id";
          @$value  = $catId;
       }
       $search ='';
      
       $result1 = $this->Adminmodel->get_current_page_records($table,$perPage,$start,@$column,@$value,@$search,@$searchColumn);
       $result = replace_attr($result1);
       if($result){
            foreach ($result as $key => $field) {
                $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['cat_id'],'id',
                    'category_name','keyaan_category') ;
                $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($field['subcat_id'],'id','subcategory_name','keyaan_subcategory') ;
                $result[$key]['subsubcategory'] = $this->Adminmodel->getSingleColumnName($field['subsubcat_id'],'id','subsubcat_name','keyaan_subsubcategory') ;
                
                     $result[$key]['theme_banner'] = base_url()."uploads/bannerimage/".$field['theme_banner'];                    
                     
                $result['themeimglist'] = $this->Adminmodel->getgallery($field['theme_code'],'vendor_theme_details');
           }
           $data['result'] = $result ;
           $this->load->view('vendor/theme_details',$data);
       }
       else{
          $url='viewTheme';
          redirect($url);
       }
    }

    function ThemeEnable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'0'
        );
        $table="vendor_themelist";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewTheme';
            redirect($url);
    }
    function ThemeDisable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'1'
        );
        $table="vendor_themelist";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewTheme';
        redirect($url);
    }
    public function deleteImage(){
        $id           =$this->input->post('id');
        $table        = $this->input->post('table');
        $folder       = $this->input->post('folder');
        $selectColumn = $this->input->post('selectColumn');
        $result       = $this->Adminmodel->delImage($id,$table,$folder,$selectColumn);
        $data['result'] =$result;
        $this->load->view('vendor/deleteImgAjax',$data);
    }
    //To remove duplicate values
    public function array_multi_unique($multiArray){
        $uniqueArray = array();
        foreach($multiArray as $subArray){
            if(!in_array($subArray, $uniqueArray)){
                $uniqueArray[] = $subArray;
            }
        }
        return $uniqueArray;
    }
    function deleteTheme($theme_code){
        $theme_code=$theme_code;
        $id = $this->uri->segment('4');
        $result = $this->Adminmodel->delImage($id,'vendor_themelist','bannerimage','theme_banner');
        $result1 = $this->Adminmodel->delmultipleImagefile($theme_code,'vendor_theme_details','themeimage','themevideo','theme_image','media_type','theme_code');
        redirect($_SERVER['HTTP_REFERER']);
        }
    //For Thumnail Images
    public function resizeImage($source_path,$target_path)
   {
         $config_manip = array(
         'image_library' => 'gd2',
         'source_image' => $source_path,
         'new_image' => $target_path,
         'maintain_ratio' => TRUE,
         'create_thumb' => TRUE,
         'thumb_marker' => '',
         'width' => 150,
         'height' => 150
       );
       $this->image_lib->clear();
       $this->image_lib->initialize($config_manip);
       $this->image_lib->resize();
   }
}    
?>