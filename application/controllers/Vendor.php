<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('image_lib');
    }
    public function index() {
         redirect('viewVendors');
        } 
    //For Vendors    
    public function vendorListAll_post(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $input  = json_decode(file_get_contents('php://input'), true); 
        $start=0;
        $perPage = 100;
        $catId =$input['catId'];
   
        //if($start!="" && $perPage!=""){
        $table="keyaan_vendor";
        if($catId !=""){
            $column = "cat_id";
            $value  = $catId;
        }
        $result = $this->Adminmodel->get_current_page_records($table,$perPage,$start,$column,$value);
        if($result){
            foreach ($result as $key => $field) {
                $result1 = $this->Adminmodel->getId($field['vendor_code'],'vendor_categories') ;
                foreach ($result1 as $key => $field1) {
                    $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field1['category_id'],'id','category_name','keyaan_category') ;
                    $result[$key]['cat_id'] = $field1['category_id'] ;
                }
                if($field['logo'] !=""){
                $result[$key]['logo'] = base_url()."uploads/vendorlogo/".$field['logo'];  
                }else{
                   $result[$key]['logo'] = "0";   
                }
            }
             $url='viewCategory';
             redirect($url);
        } else{
            $url='viewCategory';
            redirect($url);
        }
    }
    public function addVendor(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $dataBefore =[];    
        $vendorName =$this->input->post('name');
        $this->load->library('upload');       
        $resultCountry = $this->Adminmodel->getAjaxdataCountry('keyaan_countries');
        $resultCategory = $this->Adminmodel->getAjaxdataCountry('keyaan_category');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;
        $dataBefore['resultCnt'] = $resultCountry; 
        $dataBefore['resultCat'] = $resultCategory;
        if(!empty($vendorName)){
            // for logo start here   
            $min='1452';
            $max='8569';
            $vendorCode =rand($min,$max);
            $min1='2691';
            $max1='7537';
            $service_code =rand($min1,$max1);
            $min_theme='1236';
            $max_theme='9874';
            $theme_code =rand($min_theme,$max_theme);
            $tablename_image ="";
            if($_FILES['logo']['size'] > 0) {
                $config_media['upload_path'] = './uploads/vendorlogo';
                $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                $config_media['max_size']   = '1000000000000000'; // whatever you need
                // $this->load->library($config_media);
                $this->upload->initialize($config_media);
                $error = [];
                if ( ! $this->upload->do_upload('logo'))
                {
                    $error[] = array('error_image' => $this->upload->display_errors()); 
                }
                else
                {
                    $data[] = array('upload_image' => $this->upload->data());
                }       
                $imagelogo = $data[0]['upload_image']['file_name'];
                if(count($error) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in logo uploads</div>') ;
                    redirect('addVendor');
                    //echo"image logo error";
                }        
            }else{
                $imagelogo = "";
            }
            if($_FILES['basic_image']['size'] > 0) {
                $config_media['upload_path'] = './uploads/basic_image';
                $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                $config_media['max_size']   = '1000000000000000'; // whatever you need
               // $this->load->library($config_media);
                $this->upload->initialize($config_media);
                $error1 = [];
                if ( ! $this->upload->do_upload('basic_image'))
                {
                    $error1[] = array('error_image' => $this->upload->display_errors()); 
                }
                else
                {
                    $data1[] = array('upload_image' => $this->upload->data());
                }       
                $basic_image = $data1[0]['upload_image']['file_name'];
                if(count($error1) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in basic_image uploads</div>') ;
                    redirect('addVendor');
                    //echo"basic_image error";
                }        
            }else{
                $basic_image = "";
            }
            if($_FILES['bank_upload_cancel_cheque']['size'] > 0) {
                $config_media2['upload_path'] = './uploads/check_image';
                $config_media2['allowed_types'] = 'gif|jpg|png|jpeg';   
                $config_media2['max_size']  = '1000000000000000'; // whatever you need
               // $this->load->library($config_media2);
                $this->upload->initialize($config_media2);
                $error2 = [];
                if ( ! $this->upload->do_upload('bank_upload_cancel_cheque')) {
                    $error2[] = array('error_image' => $this->upload->display_errors());    
                }
                else {
                    $data2[] = array('upload_image' => $this->upload->data());
                }       
                $bank_upload_cancel_cheque = $data2[0]['upload_image']['file_name'];
                if(count($error2) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in cancel check image uploads</div>') ;
                   redirect('addVendor');
                    //echo"cheque error";
                }        
            }else{
                $bank_upload_cancel_cheque = "";
            }  
            if($_FILES['proofImg']['size'] > 0) {
                $config_media3['upload_path'] = './uploads/proofimage';
                $config_media3['allowed_types'] = 'gif|jpg|png|jpeg';   
                $config_media3['max_size']  = '1000000000000000'; // whatever you need
                //  $this->load->library($config_media3);
                $this->upload->initialize($config_media3);
                $error = [];
                if ( ! $this->upload->do_upload('proofImg')) {
                    $error[] = array('error_image' => $this->upload->display_errors()); 
                }
                else {
                    $data5[] = array('upload_image' => $this->upload->data());
                }       
                $proofImg = $data5[0]['upload_image']['file_name'];
                if(count($error) >0){
                   $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in proof image uploads</div>') ;
                   redirect('addVendor');
                   //echo"proof image";
                }        
            }else{
                $proofImg = "";
            } 
            // for video proofImg
            if(@$_FILES['video']['size'] > 0) {
                $config_media6['upload_path'] = './uploads/vendorvideo';
                $config_media6['allowed_types'] = 'mp4|avi|flv|wmv|mpeg|mp3|3GPP';  
                $config_media6['max_size']  = '1000000000000000'; // whatever you need
              //  $this->load->library($config_media6);
                $this->upload->initialize($config_media6);
                $error4 = [];
                if ( ! $this->upload->do_upload('video'))
                {
                    $error4[] = array('error_image' => $this->upload->display_errors());    
                }
                else
                {
                    $data7[] = array('upload_image' => $this->upload->data());
                }       
                $imagevideo = $data7[0]['upload_image']['file_name'];
                if(count($error4) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in video uploads</div>') ;
                    redirect('addVendor');
                    //echo"video uploads error";
                }else{
                    $data = array(
                        'vendor_code'=> $vendorCode ,
                        'image' => $imagevideo,
                        'media_type'=>'2'
                    );
                    $result = $this->Adminmodel->insertRecordQueryList('vendor_image_details',$data);
                }        
            }else{
                $imagevideo = "";
            }
            // for logo end here    
            $imgage=count($_FILES['image']['name']);
            if($imgage !=''){                               
                $i=0; 
                $filesCount = count($_FILES['image']['name']);
                for($i = 0; $i < $filesCount; $i++){
                    $_FILES['file']['name']     = $_FILES['image']['name'][$i];
                    $_FILES['file']['type']     = $_FILES['image']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['image']['tmp_name'][$i];
                    $_FILES['file']['error']     = $_FILES['image']['error'][$i];
                    $_FILES['file']['size']     = $_FILES['image']['size'][$i];
                    
                    // File upload configuration
                    $uploadPath = './uploads/vendorimage/';
                    $config9['upload_path'] = $uploadPath;
                    $config9['allowed_types'] = 'jpg|jpeg|png|gif';
                    
                    // Load and initialize upload library
                    $this->upload->initialize($config9);
                    // Upload file to server
                    if($this->upload->do_upload('file')){
                        // Uploaded file data
                        $fileData = $this->upload->data();
                        $uploadData[$i]['file_name'] = $fileData['file_name'];
                        $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                        $source_path = './uploads/vendorimage/' .$uploadData[$i]['file_name'];
                        $target_path = './uploads/thumbnail/vendorimage';
                        $this->resizeImage($source_path,$target_path);
                        $data = array(
                            'vendor_code'=> $vendorCode ,
                            'image' => $uploadData[$i]['file_name'],
                           'media_type'=>'1'
                        );
                       $result = $this->Adminmodel->insertRecordQueryList('vendor_image_details',$data);
                    }
                }
                             
                $dateCurrent= date("Y-m-d H:i:s");

                $username     = $this->input->post('username') ==""?"":$this->input->post('username');
                $password     = $this->input->post('password') ==""?"":$this->input->post('password'); 
                $name     = $this->input->post('name') ==""?"":$this->input->post('name');
                $email          = $this->input->post('email') =="" ? "":$this->input->post('email');
                $mobile         = $this->input->post('mobile') =="" ? "":$this->input->post('mobile');
                $alt_mobile     = $this->input->post('alt_mobile') =="" ? "":$this->input->post('alt_mobile');
                $proof_number   = $this->input->post('proof_number') =="" ? "":$this->input->post('proof_number');
                $proof_type     = $this->input->post('proof_type') =="" ? "0":$this->input->post('proof_type');
                $basic_image    = $basic_image;
                $company_name   = $this->input->post('company_name') =="" ? "":$this->input->post('company_name');
                $business_name  = $this->input->post('business_name') =="" ? "":$this->input->post('business_name');
                $business_email = $this->input->post('business_email') =="" ? "":$this->input->post('business_email');
                $land_line_no   = $this->input->post('landline_number') =="" ? "0":$this->input->post('landline_number');
                $business_mobnumber= $this->input->post('business_mobnumber') =="" ? "0":$this->input->post('business_mobnumber');
                $business_pancard = $this->input->post('business_pancard') =="" ? "0":$this->input->post('business_pancard');
                $company_reg_number = $this->input->post('company_reg_number') =="" ? "0":$this->input->post('company_reg_number');
                $business_address = $this->input->post('business_address') =="" ? "":$this->input->post('business_address');
                $address_proof = $this->input->post('address_proof') =="" ? "":$this->input->post('address_proof');
                $website_link = $this->input->post('website_link') =="" ? "":$this->input->post('website_link');
                $logo = $imagelogo;
                $proofImg = $proofImg;
                $vendor_detail = $this->input->post('vendor_detail') =="" ? "":$this->input->post('vendor_detail');
                $about_vendor = $this->input->post('about_vendor') =="" ? "":$this->input->post('about_vendor');
                $search_text = $this->input->post('search_text') =="" ? "":$this->input->post('search_text');
                $fb_link = $this->input->post('fb_link') ==""?"":$this->input->post('fb_link');
                $twitter_link = $this->input->post('twitter_link') =="" ? "":$this->input->post('twitter_link');
                $google_plus_link = $this->input->post('google_plus_link') =="" ? "":$this->input->post('google_plus_link');
                $linkedln_link = $this->input->post('linkedln_link') =="" ? "":$this->input->post('linkedln_link');
                $cat_id = $this->input->post('cat_id') =="" ? "":$this->input->post('cat_id');
                $subcat_id = $this->input->post('subcat_id') =="" ? "":$this->input->post('subcat_id');
                $subsubcat_id = $this->input->post('subsubcat_id') =="" ? "":$this->input->post('subsubcat_id');
                $bank_name = $this->input->post('bank_name') =="" ? "":$this->input->post('bank_name');
                $bank_account_no = $this->input->post('bank_account_no') =="" ? "":$this->input->post('bank_account_no');
                $bank_upload_cancel_cheque = $bank_upload_cancel_cheque;
                $bank_branch = $this->input->post('bank_branch') =="" ? "":$this->input->post('bank_branch');
                $bank_ifsc = $this->input->post('bank_ifsc') =="" ? "":$this->input->post('bank_ifsc');
                $bank_account_name = $this->input->post('bank_account_name') =="" ? "":$this->input->post('bank_account_name');
                $place_description = $this->input->post('place_description') =="" ? "":$this->input->post('place_description');
                $country_id = $this->input->post('country_id') =="" ? "":$this->input->post('country_id');
                $state_id = $this->input->post('state_id') =="" ? "":$this->input->post('state_id');
                $district_id = $this->input->post('district_id') =="" ? "":$this->input->post('district_id');
                $city_id = $this->input->post('city_id') =="" ? "":$this->input->post('city_id');
                $pincode_id = $this->input->post('pincode_id') =="" ? "":$this->input->post('pincode_id');
                $location_id = $this->input->post('location_id') =="" ? "":$this->input->post('location_id');
                $category_base_price = $this->input->post('category_base_price') =="" ? "":$this->input->post('category_base_price');
                $subcat_base_price = $this->input->post('subcat_base_price') =="" ? "":$this->input->post('subcat_base_price');
                $sub_sub_cat_base_price = $this->input->post('sub_sub_cat_base_price') =="" ? "":$this->input->post('sub_sub_cat_base_price');            
                $landline_code = $this->input->post('landline_code') =="" ? "":$this->input->post('landline_code');
                $business_mobile_code = $this->input->post('business_mobile_code') =="" ? "":$this->input->post('business_mobile_code');
                $address = $this->input->post('address') =="" ? "":$this->input->post('address');
                $created_at = date("Y-m-d H:i:s");
                $updated_at = date("Y-m-d H:i:s");
                $created_by = 1;
                $updated_by = 1;

                //FOR Categories
                if($cat_id != '' && $cat_id != '0' && $cat_id != NULL && $cat_id != 'Select Category'){
                    $data = array(
                        'vendor_code'=> $vendorCode ,
                        'service_code'=> $service_code ,
                        'category_id' => $cat_id,
                    );
                    $result = $this->Adminmodel->insertRecordQueryList('vendor_categories',$data);
                } 
                //FOR Sub Categories
                if($subcat_id != '' && $subcat_id != '0' && $subcat_id != NULL && $subcat_id != 'Select Sub Category'){
                    $data = array(
                        'vendor_code'=> $vendorCode ,
                        'service_code'=> $service_code ,
                        'category_id' => $cat_id,
                        'sub_category_id' => $subcat_id,
                    );
                    $result = $this->Adminmodel->insertRecordQueryList('vendor_sub_categories',$data);
                } 
                //FOR Sub Sub Categories
                    if($subsubcat_id != '' && $subsubcat_id != '0' && $subsubcat_id != NULL && $subsubcat_id != 'Select Sub Sub Category'){
                    $data = array(
                        'vendor_code'=> $vendorCode ,
                        'service_code'=> $service_code ,
                        'category_id' => $cat_id,
                        'sub_category_id' => $subcat_id,
                        'sub_sub_category_id' => $subsubcat_id,
                    );
                    $result = $this->Adminmodel->insertRecordQueryList('vendor_sub_sub_categories',$data);
                }
                //FOR Cities
                if($city_id != '' && $city_id != '0' && $city_id != NULL && $city_id != 'Select City'){
                    $data = array(
                        'vendor_code'=> $vendorCode ,
                        'service_code'=> $service_code ,
                        'city_id' => $city_id,
                    );
                    $result = $this->Adminmodel->insertRecordQueryList('vendor_cities',$data);
                }
                //FOR Vendor Login
                $vendorData = array(
                    'vendor_code'=> $vendorCode ,
                    'username' => $username,
                    'password' => $password,
                    'useremail' => $email,
                );
                $result = $this->Adminmodel->insertRecordQueryList('vendor_login',$vendorData);
                //FOR Vendor Themes
                // $themeData = array(
                //     'vendor_code'=> $vendorCode ,
                //     'theme_code'=> $theme_code ,
                //     'cat_id' => $cat_id,
                //     'subcat_id' => $subcat_id,
                //     'subsubcat_id' => $subsubcat_id,
                //     'price' => $category_base_price,
                //     // 'theme_banner' => $basic_image,
                // );
                // $result = $this->Adminmodel->insertRecordQueryList('vendor_themelist',$themeData);

                $dataVendor = array(
                    'vendor_code'=> $vendorCode,
                    'username'=>$username,
                    'password'=>$password,
                    'name'=>$name,
                    'email' => $email,
                    'mobile' => $mobile,
                    'alt_mobile' => $alt_mobile,
                    'proof_type' => $proof_type,
                    'proof_number' => $proof_number,
                    'address' => $address,
                    'company_name' => $company_name,
                    'business_name' => $business_name,
                    'business_email' => $business_email,
                    'landline_number' => $land_line_no,
                    'landline_code'=> $landline_code,
                    'business_mobnumber'=> $business_mobnumber,
                    'business_mobile_code'=>$business_mobile_code,
                    'business_pancard' => $business_pancard,
                    'company_reg_number' => $company_reg_number,
                    'business_address' => $business_address,
                    'address_proof' => $address_proof,
                    'website_link' => $website_link,
                    'vendor_detail' => $vendor_detail,
                    'about_vendor' => $about_vendor,
                    'search_text' => $search_text,
                    'fb_link' => $fb_link,
                    'twitter_link' => $twitter_link,
                    'google_plus_link' => $google_plus_link,
                    'linkedln_link' => $linkedln_link,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at,
                    'updated_by' => $updated_by,
                    'country_id'=>$country_id,
                    'state_id'=>$state_id,
                    'district_id'=>$district_id,
                    'city_id'=>$city_id,
                    'pincode_id'=>$pincode_id,
                    'location_id'=>$location_id,
                    'category_base_price'=>$category_base_price,
                    'subcat_base_price'=>$subcat_base_price,
                    'sub_sub_cat_base_price'=>$sub_sub_cat_base_price,
                    'bank_name'=>$bank_name,
                    'bank_account_no'=>$bank_account_no,
                    'bank_branch'=>$bank_branch,
                    'bank_ifsc'=>$bank_ifsc,
                    'bank_account_name'=>$bank_account_name,
                    'logo'=>$logo,
                    'basic_image'=>$basic_image,
                    'proofImg'=>$proofImg,
                    'bank_upload_cancel_cheque'=>$bank_upload_cancel_cheque
                );
                $tableVendor="keyaan_vendor";
                $result = $this->Adminmodel->insertRecordQueryList($tableVendor,$dataVendor);
                
                $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Vendor Added Successfully</div>') ;


              // for idcard generate 


           $this->load->library('html2pdf');
	    
	    $this->html2pdf->folder('./assets/pdfs/');
	    $this->html2pdf->filename($vendorCode.".pdf");
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $dataPdf = array(
	    	'name' => $name,
	    	'vendorCode' => $vendorCode,
                'image' => $logo
	    );
	    //Load html view
	    $this->html2pdf->html($this->load->view('pdf', $dataPdf, true));
	    
	    //Check that the PDF was created before we send it
	    if($path = $this->html2pdf->create('save')) {	    	
			$this->load->library('email');

			$this->email->from('admin@keyaan.co', 'Keyaan');
			$this->email->to($email); 
			
			$this->email->subject('ID card from keyaan');
			$this->email->message('Thanks for register with Keyaan');	

			$this->email->attach($path);

			$this->email->send();
			
						
	    }






















                    $this->load->view('admin/add_vendor',$dataBefore);  
            }  else{
                 $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps!Some error occured, Vendor not added.</div>') ;
                   $this->load->view('admin/add_vendor',$dataBefore);   
            }
        } else{
           //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
            $this->load->view('admin/add_vendor',$dataBefore);  
        }
    }


    public function editVendor(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
            {
              redirect('admin');
            }
        $dataBefore =[];  
        $id = $this->uri->segment('3');        
        $this->load->library('upload');       
        $resultCountry = $this->Adminmodel->getAjaxdataCountry('keyaan_countries');
        $resultCategory = $this->Adminmodel->getAjaxdataCountry('keyaan_category');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');    
        $admin = $this->session->userdata('userCode');
        $added_by = $admin!='' ? $admin:'admin' ;           
        $date     = date("Y-m-d H:i:s");
        if(!empty($id)){
            $tablename = "keyaan_vendor";
            $result = $this->Adminmodel->singleRecordData('id', $id,$tablename);
            if($result){
                foreach ($result as $key => $field) {
                    $catId=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                    'category_id','vendor_categories') ;
                    $result[$key]['service_code']=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','service_code','vendor_categories') ;
                    $result[$key]['theme_code']=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','theme_code','vendor_themelist') ;
                    $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($catId,'id',
                    'category_name','keyaan_category') ;
                    $result[$key]['cat_id'] = $catId ;
                    $subcat_id=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                    'sub_category_id','vendor_sub_categories') ;
                    $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($subcat_id,'id',
                    'subcategory_name','keyaan_subcategory') ;
                    $result[$key]['subcat_id'] = $subcat_id ;
                    $subsubcat_id=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','sub_sub_category_id','vendor_sub_sub_categories') ;
                    $result[$key]['subsubcategory'] = $this->Adminmodel->getSingleColumnName($subsubcat_id,'id',
                    'subsubcat_name','keyaan_subsubcategory') ;
                    $result[$key]['subsubcat_id'] = $subsubcat_id ;
                    $result[$key]['country'] = $this->Adminmodel->getSingleColumnName($field['country_id'],'id','country_name','keyaan_countries') ;
                    $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($field['state_id'],'id','state_name','keyaan_states') ;
                    $result[$key]['district'] = $this->Adminmodel->getSingleColumnName($field['district_id'],'id','district_name','keyaan_districts') ;
                    $result[$key]['city'] = $this->Adminmodel->getSingleColumnName($field['city_id'],'id','city_name','keyaan_cities') ;
                    $result[$key]['pincode'] = $this->Adminmodel->getSingleColumnName($field['pincode_id'],'id','pincode','keyaan_pincodes') ;
                    $result[$key]['location'] = $this->Adminmodel->getSingleColumnName($field['location_id'],'id','location_name','keyaan_locations') ;
                    if($field['proofImg'] !=""){
                    $result[$key]['proofImg'] = base_url()."uploads/proofimage/".$field['proofImg'];
                    $result['imglist'] = $this->Adminmodel->getgallery($field['vendor_code'],'vendor_image_details');
                    }else{
                       $result[$key]['proofImg'] = "0";
                       $result['imglist'] = "";
                    }
                    if($field['bank_upload_cancel_cheque'] !=""){
                    $result[$key]['bank_upload_cancel_cheque'] = base_url()."uploads/check_image/".$field['bank_upload_cancel_cheque'];
                    $result['imglist'] = $this->Adminmodel->getgallery($field['vendor_code'],'vendor_image_details') ;
                    }else{
                       $result[$key]['bank_upload_cancel_cheque'] = "0";
                       $result['imglist'] = ""; 
                    }
                    if($field['basic_image'] !=""){
                    $result[$key]['basic_image'] = base_url()."uploads/basic_image/".$field['basic_image'];
                    $result['imglist'] = $this->Adminmodel->getgallery($field['vendor_code'],'vendor_image_details') ;
                    }else{
                       $result[$key]['basic_image'] = "0";
                       $result['imglist'] = ""; 
                    }
                    if($field['logo'] !=""){
                    $result[$key]['logo'] = base_url()."uploads/vendorlogo/".$field['logo'];
                    $result['imglist'] = $this->Adminmodel->getgallery($field['vendor_code'],'vendor_image_details') ;
                    }else{
                       $result[$key]['logo'] = "0";
                       $result['imglist'] = ""; 
                    }
                }
            $dataBefore['result'] = replace_empty_values($result); 
            $dataBefore['resultCnt'] = $resultCountry; 
            $dataBefore['resultCat'] = $resultCategory;
            $dataBefore['basicsettingsList']=$resultBasicsettings;
            $this->load->view('admin/edit_vendor',$dataBefore);
            } else {
                $url='viewVendors';
                redirect($url);
            }
        }else{
            redirect('admin');
        }
    }
    //Update Vendor
    public  function updateVendor(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $vendorName =$this->input->post('name');
        $id =$this->input->post('id');
        $this->load->library('upload');
        $admin = $this->session->userdata('userCode');
        $added_by = $admin!='' ? $admin:'admin' ;           
        $date     = date("Y-m-d H:i:s");
        $vendor_code =$this->input->post('vendor_code');
        $service_code =$this->input->post('service_code');
        $theme_code =$this->input->post('theme_code');
            if(!empty($vendorName) && !empty($id) && !empty($vendor_code)){            
                // for logo start here 
                $vendorCode =$this->input->post('vendor_code');
                $tablename_image ="";
                if($_FILES['logo']['size'] > 0) {
                    $config_media['upload_path'] = './uploads/vendorlogo';
                    $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                   // $this->load->library($config_media);
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('logo')) {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $imagelogo = $data[0]['upload_image']['file_name'];
                    $imgArr = array(
                        "logo"=>$imagelogo ,
                        "updated_by"=>$added_by,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('keyaan_vendor',$imgArr,'id',$id);
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in logo uploads</div>') ;
                        $url='Vendor/editVendor/'.$id;
                        redirect($url);
                    }        
                }else{
                    $imagelogo = "";
                }
                if($_FILES['basic_image']['size'] > 0) {
                    $config_media['upload_path'] = './uploads/basic_image';
                    $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                   // $this->load->library($config_media);
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('basic_image'))
                    {
                        $error2[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data1[] = array('upload_image' => $this->upload->data());
                    }       
                    $basic_image = $data1[0]['upload_image']['file_name'];
                    $imgArr = array(
                        "basic_image"=>$basic_image ,
                        "updated_by"=>$added_by,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('keyaan_vendor',$imgArr,'id',$id);
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in basic_image uploads</div>') ;
                        $url='Vendor/editVendor/'.$id;
                        redirect($url);
                        
                    }        
                }else{
                    $basic_image = "";
                }
                if($_FILES['bank_upload_cancel_cheque']['size'] > 0) {
                    $config_media2['upload_path'] = './uploads/check_image';
                    $config_media2['allowed_types'] = 'gif|jpg|png|jpeg';   
                    $config_media2['max_size']  = '1000000000000000'; // whatever you need
                   // $this->load->library($config_media2);
                    $this->upload->initialize($config_media2);
                    $error2 = [];
                    if ( ! $this->upload->do_upload('bank_upload_cancel_cheque'))  {
                        $error2[] = array('error_image' => $this->upload->display_errors());    
                    }
                    else  {
                        $data2[] = array('upload_image' => $this->upload->data());
                    }       
                    $bank_upload_cancel_cheque = $data2[0]['upload_image']['file_name'];
                    $imgArr2 = array(
                        "bank_upload_cancel_cheque"=>$bank_upload_cancel_cheque ,
                        "updated_by"=>$added_by,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('keyaan_vendor',$imgArr2,'id',$id);
                    if(count($error2) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in bank_upload_cancel_cheque uploads</div>') ;
                        $url='Vendor/editVendor/'.$id;
                        redirect($url);
                    }        
                }else{
                    $bank_upload_cancel_cheque = "";
                }  
                if($_FILES['proofImg']['size'] > 0) {
                    $config_media3['upload_path'] = './uploads/proofimage';
                    $config_media3['allowed_types'] = 'gif|jpg|png|jpeg';   
                    $config_media3['max_size']  = '1000000000000000'; // whatever you need
                  //  $this->load->library($config_media3);
                    $this->upload->initialize($config_media3);
                    $error = [];
                    if ( ! $this->upload->do_upload('proofImg')) {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else {
                        $data5[] = array('upload_image' => $this->upload->data());
                    }       
                    $proofImg = $data5[0]['upload_image']['file_name'];
                    $imgArr2 = array(
                        "proofImg"=>$proofImg,
                        "updated_by"=>$added_by,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('keyaan_vendor',$imgArr2,'id',$id);
                    if(count($error) >0){
                       $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in proof image uploads</div>') ;
                      $url='Vendor/editVendor/'.$id;
                      redirect($url);
                    }        
                }else{
                    $proofImg = "";
                } 
                // for video proofImg
                if(@$_FILES['video']['size'] > 0) {
                    $config_media6['upload_path'] = './uploads/vendorvideo';
                    $config_media6['allowed_types'] = 'mp4|avi|flv|wmv|mpeg|mp3|3GPP';  
                    $config_media6['max_size']  = '1000000000000000'; // whatever you need
                  //  $this->load->library($config_media6);
                    $this->upload->initialize($config_media6);
                    $error4 = [];
                    if ( ! $this->upload->do_upload('video'))
                    {
                        $error4[] = array('error_image' => $this->upload->display_errors());    
                    }
                    else
                    {
                        $data7[] = array('upload_image' => $this->upload->data());
                    }       
                    $imagevideo = $data7[0]['upload_image']['file_name'];
                    $imgArr20 = array(
                        "image"=>$imagevideo,
                        "updated_by"=>$added_by,
                        "updated_date"=>$date  
                    );   
                    $where = array(
                        "vendor_code"=>$vendorCode,
                        "media_type"=>'2'
                        );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList2('vendor_image_details',$imgArr20,'vendor_code',$vendorCode,$where);
                    if(count($error4) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in video uploads</div>') ;
                        $url='Vendor/editVendor/'.$id;
                        redirect($url);
                    }else{
                        $vodeo="";
                    }        
                }else{
                    $imagevideo = "";
                }
                
                // for logo end here    
                $imgage=count($_FILES['image']['name']);   
                if($imgage !=''){                               
                $i=0; 
                $filesCount = count($_FILES['image']['name']);
                for($i = 0; $i < $filesCount; $i++){
                $_FILES['file']['name']     = $_FILES['image']['name'][$i];
                $_FILES['file']['type']     = $_FILES['image']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['image']['tmp_name'][$i];
                $_FILES['file']['error']     = $_FILES['image']['error'][$i];
                $_FILES['file']['size']     = $_FILES['image']['size'][$i];
                // File upload configuration
                $uploadPath = './uploads/vendorimage/';
                $config9['upload_path'] = $uploadPath;
                $config9['allowed_types'] = 'jpg|jpeg|png|gif';
                // Load and initialize upload library
                $this->upload->initialize($config9);
                // Upload file to server
                    if($this->upload->do_upload('file')){
                        // Uploaded file data
                        $fileData = $this->upload->data();
                        $uploadData[$i]['file_name'] = $fileData['file_name'];
                        $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                        $source_path = './uploads/vendorimage/' .$uploadData[$i]['file_name'];
                        $target_path = './uploads/thumbnail/vendorimage';
                        $this->resizeImage($source_path,$target_path);
                        $imgArr20 = array(
                            "image"=>$uploadData[$i]['file_name'],
                            "updated_by"=>$added_by,
                            "updated_date"=>$date,
                            "vendor_code"=>$vendorCode,
                            "media_type"=>'1'
                        );                            
                        $resultUpdate = $this->Adminmodel->insertRecordQueryList('vendor_image_details',$imgArr20);
                    }
                }
             }  
                
                              
                $dateCurrent= date("Y-m-d H:i:s");
                $username     = $this->input->post('username') ==""?"":$this->input->post('username');
                $password     = $this->input->post('password') ==""?"":$this->input->post('password');  
                $name     = $this->input->post('name') ==""?"":$this->input->post('name');
                $email          = $this->input->post('email') =="" ? "":$this->input->post('email');
                $mobile         = $this->input->post('mobile') =="" ? "":$this->input->post('mobile');
                $alt_mobile     = $this->input->post('alt_mobile') =="" ? "":$this->input->post('alt_mobile');
                $proof_number   = $this->input->post('proof_number') =="" ? "":$this->input->post('proof_number');
                $proof_type     = $this->input->post('proof_type') =="" ? "0":$this->input->post('proof_type');
                $company_name   = $this->input->post('company_name') =="" ? "":$this->input->post('company_name');
                $business_name  = $this->input->post('business_name') =="" ? "":$this->input->post('business_name');
                $business_email = $this->input->post('business_email') =="" ? "":$this->input->post('business_email');
                $land_line_no   = $this->input->post('landline_number') =="" ? "0":$this->input->post('landline_number');
                $business_mobnumber= $this->input->post('business_mobnumber') =="" ? "0":$this->input->post('business_mobnumber');
                $business_pancard = $this->input->post('business_pancard') =="" ? "0":$this->input->post('business_pancard');
                $company_reg_number = $this->input->post('company_reg_number') =="" ? "0":$this->input->post('company_reg_number');
                $business_address = $this->input->post('business_address') =="" ? "":$this->input->post('business_address');
                $address_proof = $this->input->post('address_proof') =="" ? "":$this->input->post('address_proof');
                $website_link = $this->input->post('website_link') =="" ? "":$this->input->post('website_link');
                $logo = $imagelogo;
                $proofImg = $proofImg;
                $basic_image = $basic_image;
                $vendor_detail = $this->input->post('vendor_detail') =="" ? "":$this->input->post('vendor_detail');
                $about_vendor = $this->input->post('about_vendor') =="" ? "":$this->input->post('about_vendor');
                $search_text = $this->input->post('search_text') =="" ? "":$this->input->post('search_text');
                $fb_link = $this->input->post('fb_link') ==""?"":$this->input->post('fb_link');
                $twitter_link = $this->input->post('twitter_link') =="" ? "":$this->input->post('twitter_link');
                $google_plus_link = $this->input->post('google_plus_link') =="" ? "":$this->input->post('google_plus_link');
                $linkedln_link = $this->input->post('linkedln_link') =="" ? "":$this->input->post('linkedln_link');
                $cat_id = $this->input->post('cat_id') =="" ? "":$this->input->post('cat_id');
                $subcat_id = $this->input->post('subcat_id') =="" ? "":$this->input->post('subcat_id');
                $subsubcat_id = $this->input->post('subsubcat_id') =="" ? "":$this->input->post('subsubcat_id');
                $bank_name = $this->input->post('bank_name') =="" ? "":$this->input->post('bank_name');
                $bank_account_no = $this->input->post('bank_account_no') =="" ? "":$this->input->post('bank_account_no');
                $bank_upload_cancel_cheque = $bank_upload_cancel_cheque;
                $bank_branch = $this->input->post('bank_branch') =="" ? "":$this->input->post('bank_branch');
                $bank_ifsc = $this->input->post('bank_ifsc') =="" ? "":$this->input->post('bank_ifsc');
                $bank_account_name = $this->input->post('bank_account_name') =="" ? "":$this->input->post('bank_account_name');
                $place_description = $this->input->post('place_description') =="" ? "":$this->input->post('place_description');
                $country_id = $this->input->post('country_id') =="" ? "":$this->input->post('country_id');
                $state_id = $this->input->post('state_id') =="" ? "":$this->input->post('state_id');
                $district_id = $this->input->post('district_id') =="" ? "":$this->input->post('district_id');
                $city_id = $this->input->post('city_id') =="" ? "":$this->input->post('city_id');
                $pincode_id = $this->input->post('pincode_id') =="" ? "":$this->input->post('pincode_id');
                $location_id = $this->input->post('location_id') =="" ? "":$this->input->post('location_id');
                $category_base_price = $this->input->post('category_base_price') =="" ? "":$this->input->post('category_base_price');
                $subcat_base_price = $this->input->post('subcat_base_price') =="" ? "":$this->input->post('subcat_base_price');
                $sub_sub_cat_base_price = $this->input->post('sub_sub_cat_base_price') =="" ? "":$this->input->post('sub_sub_cat_base_price');            
                $landline_code = $this->input->post('landline_code') =="" ? "":$this->input->post('landline_code');
                $business_mobile_code = $this->input->post('business_mobile_code') =="" ? "":$this->input->post('business_mobile_code');
                $address = $this->input->post('address') =="" ? "":$this->input->post('address');
                $created_at = date("Y-m-d H:i:s");
                $updated_at = date("Y-m-d H:i:s");
                $created_by = 1;
                $updated_by = 1;

                //FOR Categories
                $catData = array(
                    'category_id' => $cat_id,
                    "updated_by"=>$added_by,
                    "updated_date"=>$date  
                );   
                $where = array(
                    "vendor_code"=>$vendorCode,
                    'service_code'=> $service_code ,
                );
                $resultUpdate = $this->Adminmodel->updateRecordQueryList2('vendor_categories',$catData,$where);
                //FOR Sub Categories
                $subcatData = array(
                    'category_id' => $cat_id,
                    'sub_category_id' => $subcat_id,
                    "updated_by"=>$added_by,
                    "updated_date"=>$date  
                );   
                $where = array(
                    "vendor_code"=>$vendorCode,
                    'service_code'=> $service_code ,
                );
                $resultUpdate = $this->Adminmodel->updateRecordQueryList2('vendor_sub_categories',$subcatData,$where);
                //FOR Sub Sub Categories
                $subsubcatData = array(
                    'category_id' => $cat_id,
                    'sub_category_id' => $subcat_id,
                    'sub_sub_category_id' => $subsubcat_id,
                    "updated_by"=>$added_by,
                    "updated_date"=>$date  
                );   
                $where = array(
                    "vendor_code"=>$vendorCode,
                    'service_code'=> $service_code ,
                );
                $resultUpdate = $this->Adminmodel->updateRecordQueryList2('vendor_sub_sub_categories',$subsubcatData,$where);
                //FOR Cities
                $cityData = array(
                    'city_id' => $city_id,
                    "updated_by"=>$added_by,
                    "updated_date"=>$date  
                );   
                $where = array(
                    "vendor_code"=>$vendorCode,
                    'service_code'=> $service_code ,
                );
                $resultUpdate = $this->Adminmodel->updateRecordQueryList2('vendor_cities',$cityData,$where);
                //FOR Vendor Login
                $vendorData = array(
                    'username' => $username,
                    'useremail' => $email,
                    'password' => $password
                );   
                $where = array(
                    "vendor_code"=>$vendorCode,
                );
                $resultUpdate = $this->Adminmodel->updateRecordQueryList2('vendor_login',$vendorData,$where);
                //FOR Vendor Themes
                // $catData = array(
                //     'vendor_code'=> $vendorCode ,
                //     'theme_code'=> $theme_code ,
                //     'cat_id' => $cat_id,
                //     'subcat_id' => $subcat_id,
                //     'subsubcat_id' => $subsubcat_id,
                //     'price' => $category_base_price,
                //     // 'theme_banner' => $basic_image,  
                // );   
                // $where = array(
                //     "vendor_code"=>$vendorCode,
                //     'theme_code'=> $theme_code 
                // );
                // $resultUpdate = $this->Adminmodel->updateRecordQueryList2('vendor_themelist',$catData,$where);
                $dataVendor = array(
                    'username'=>$username,
                    'password'=>$password,
                    'name'=>$name,
                    'email' => $email,
                    'mobile' => $mobile,
                    'alt_mobile' => $alt_mobile,
                    'proof_type' => $proof_type,
                    'proof_number' => $proof_number,
                    'address' => $address,
                    'company_name' => $company_name,
                    'business_name' => $business_name,
                    'business_email' => $business_email,
                    'landline_number' => $land_line_no,
                    'landline_code'=> $landline_code,
                    'business_mobnumber'=> $business_mobnumber,
                    'business_mobile_code'=>$business_mobile_code,
                    'business_pancard' => $business_pancard,
                    'company_reg_number' => $company_reg_number,
                    'business_address' => $business_address,
                    'address_proof' => $address_proof,
                    'website_link' => $website_link,
                    'vendor_detail' => $vendor_detail,
                    'about_vendor' => $about_vendor,
                    'search_text' => $search_text,
                    'fb_link' => $fb_link,
                    'twitter_link' => $twitter_link,
                    'google_plus_link' => $google_plus_link,
                    'linkedln_link' => $linkedln_link,
                    'updated_by' => $updated_by,
                    'country_id'=>$country_id,
                    'state_id'=>$state_id,
                    'district_id'=>$district_id,
                    'city_id'=>$city_id,
                    'pincode_id'=>$pincode_id,
                    'location_id'=>$location_id,
                    'category_base_price'=>$category_base_price,
                    'subcat_base_price'=>$subcat_base_price,
                    'sub_sub_cat_base_price'=>$sub_sub_cat_base_price,
                    'bank_name'=>$bank_name,
                    'bank_account_no'=>$bank_account_no,
                    'bank_branch'=>$bank_branch,
                    'bank_ifsc'=>$bank_ifsc,
                    'bank_account_name'=>$bank_account_name,
                );
                $tableVendor="keyaan_vendor";
                $result = $this->Adminmodel->updateRecordQueryList($tableVendor,$dataVendor,'id',$id);
                $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Vendor Updated Successfully.</div>') ;
                $url='Vendor/editVendor/'.$id;
                redirect($url); 
        }           
         else{
           //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;                   
           $url='Vendor/editVendor/'.$id;
           redirect($url);
        }
    }

    public function viewVendors(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table="keyaan_vendor";
        $input  = json_decode(file_get_contents('php://input'), true);
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Vendor/viewVendors";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'name');
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
        if($result){
            foreach ($result as $key => $field) {
                //$result1 = $this->Adminmodel->getId($field['vendor_code'],'vendor_categories') ;
$catId=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
'category_id','vendor_categories') ;
                    $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($catId,'id',
                    'category_name','keyaan_category') ;;
                    $result[$key]['cat_id'] = $catId ;
               
                if($field['logo'] !=""){
                    $result[$key]['logo'] = base_url()."uploads/vendorlogo/".$field['logo'];  
                }else{
                    $result[$key]['logo'] = "0";  
                }
            }
            $data['result'] = $result ;
        }
        else{
            $result = [];
            $data['result'] = $result ;
        }        
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_vendors',$data);
    }
    public function vendorDetails(){
      if(!is_logged_in())  // if you add in constructor no need write each function in above controller.
       {
         redirect('admin');
       }
       $input  = json_decode(file_get_contents('php://input'), true);
       $start=0;
       $perPage = 100;
       $catId = $this->uri->segment('3');
       //if($start!="" && $perPage!=""){
       $table="keyaan_vendor";
       if($catId !=""){
          @$column = "id";
          @$value  = $catId;
       }
       $search ='';
      
       $result1 = $this->Adminmodel->get_current_page_records($table,$perPage,$start,@$column,@$value,@$search,@$searchColumn);
       $result=replace_attr($result1);
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
       if($result){
            foreach ($result as $key => $field) {
                 $catId=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                    'category_id','vendor_categories') ;
                $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($catId,'id',
                'category_name','keyaan_category') ;
                $result[$key]['cat_id'] = $catId ;
                $subcat_id=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                'sub_category_id','vendor_sub_categories') ;
                $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($subcat_id,'id',
                'subcategory_name','keyaan_subcategory') ;
                $result[$key]['subcat_id'] = $subcat_id ;
                $subsubcat_id=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','sub_sub_category_id','vendor_sub_sub_categories') ;
                $result[$key]['subsubcategory'] = $this->Adminmodel->getSingleColumnName($subsubcat_id,'id',
                'subsubcat_name','keyaan_subsubcategory') ;
                $result[$key]['subsubcat_id'] = $subsubcat_id ; 
                $result[$key]['country'] = $this->Adminmodel->getSingleColumnName($field['country_id'],'id','country_name','keyaan_countries') ;
                $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($field['state_id'],'id','state_name','keyaan_states') ;
                $result[$key]['district'] = $this->Adminmodel->getSingleColumnName($field['district_id'],'id','district_name','keyaan_districts') ;
                $result[$key]['city'] = $this->Adminmodel->getSingleColumnName($field['city_id'],'id','city_name','keyaan_cities') ;
                $result[$key]['pincode'] = $this->Adminmodel->getSingleColumnName($field['pincode_id'],'id','pincode','keyaan_pincodes') ;
                $result[$key]['location'] = $this->Adminmodel->getSingleColumnName($field['location_id'],'id','location_name','keyaan_locations') ;
               if($field['basic_image'] !=""){
                   $result[$key]['basic_image'] = base_url()."uploads/basic_image/".$field['basic_image'];
               }else{
                 $result[$key]['basic_image'] = "0";  
               }
               if($field['bank_upload_cancel_cheque'] !=""){
                   $result[$key]['bank_upload_cancel_cheque'] = base_url()."uploads/check_image/".$field['bank_upload_cancel_cheque'];
               }else{
                 $result[$key]['bank_upload_cancel_cheque'] = "0";  
               }
               if($field['logo'] !=""){
               $result[$key]['logo'] = base_url()."uploads/vendorlogo/".$field['logo'];  
               }else{
                 $result[$key]['logo'] = "0";  
               }
               $result['imglist'] = $this->Adminmodel->getgallery($field['vendor_code'],'vendor_image_details') ;
           }
           $data['result'] = $result ;
           $this->load->view('admin/vendor_Details',$data);
       }
       else{
          $url='viewVendors';
          redirect($url);
       }
   }
    function vendorEnable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'0'
        );
        $table="keyaan_vendor";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewVendors';
        redirect($url);
    }      
    function vendorDisable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'1'
        );
        $table="keyaan_vendor";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewVendors';
        redirect($url);
    }  
    public function deleteImage(){
        $id           =$this->input->post('id');
        $table        = $this->input->post('table');
        $folder       = $this->input->post('folder');
        $selectColumn = $this->input->post('selectColumn');
        $result       = $this->Adminmodel->delImage($id,$table,$folder,$selectColumn);
        $data['result'] =$result;
        $this->load->view('admin/deleteImgAjax',$data);
    }
    function deleteVendor($vendor_code){
        $vendor_code=$vendor_code;
        $result = $this->Adminmodel->delmultipleImage($vendor_code,'keyaan_vendor','basic_image','proofimage','vendorlogo','check_image','basic_image','proofImg','logo','bank_upload_cancel_cheque','vendor_code');
        $result1 = $this->Adminmodel->delmultipleImagefile($vendor_code,'vendor_image_details','vendorimage','vendorvideo','image','media_type','vendor_code');
        redirect($_SERVER['HTTP_REFERER']);
        }
    //For Thumnail Images
    public function resizeImage($source_path,$target_path) {
         $config_manip = array(
         'image_library' => 'gd2',
         'source_image' => $source_path,
         'new_image' => $target_path,
         'maintain_ratio' => TRUE,
         'create_thumb' => TRUE,
         'thumb_marker' => '',
         'width' => 150,
         'height' => 150
       );
       $this->image_lib->clear();
       $this->image_lib->initialize($config_manip);
       $this->image_lib->resize();
   }

}