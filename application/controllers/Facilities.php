<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facilities extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('form_validation');
    }
    public function index() {
         redirect('viewFacilities');
        } 
    //Sub Sub Category
   public function addFacility(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
            redirect('admin');
        }
        $dataBefore =[];
        $faciility_name =$this->input->post('faciility_name');
        $resultCountry = $this->Adminmodel->getAjaxdataCountry('venue_types');
        $dataBefore['venue_type_id'] = $resultCountry;
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings; 
        if(!empty($faciility_name)){
            $faciility_name = $this->input->post('faciility_name');
            $check_data = array(
                "faciility_name" => $faciility_name
            );
            $tablename = "facilities";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;

            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger">Facility name already exist</div>') ;
            }else{
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ; 
                $date= date("Y-m-d H:i:s");
                $venue_type_id = $this->input->post('venue_type_id') =="" ? "":$this->input->post('venue_type_id');
                $dataDistrict = array(
                    'venue_type_id'     => $venue_type_id ,
                    'faciility_name'  => $faciility_name,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $tableDistrict="facilities";
                $result = $this->Adminmodel->insertRecordQueryList($tableDistrict,$dataDistrict);
                
                $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Facility Added Successfully</div>') ;
            }
            $this->load->view('admin/add_facility',$dataBefore);  
        }
        else{
                /*$this->session->set_flashdata('msg','<div class="alert alert-danger">fail</div>') ;*/
                $this->load->view('admin/add_facility',$dataBefore);   
        }
    }
    public function viewFacilities(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="facilities";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Facilities/viewFacilities";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'faciility_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'subsubcat_name');
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
        if($result){
            foreach ($result as $key => $field) {
                $result[$key]['venue_type'] = $this->Adminmodel->getSingleColumnName($field['venue_type_id'],'id','venue_type','venue_types')  ;
            } 
            $data['result'] = $result;
        } else {
            $result = [];
            $data['result'] = $result;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_facilities',$data);
    }
    public function editFacility(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "facilities";
        $tablename2 = "venue_types";
        $start =0;
        $limit =100;
        $data['venue_types']= $this->Adminmodel->get_current_page_records($tablename2,$limit,$start,$column=null,$value=null,@$search,@$searchColumn);
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        foreach ($result as $key => $field) {
            $result[$key]['venue_type'] = $this->Adminmodel->getSingleColumnName($field['venue_type_id'],'id','venue_type','venue_types');
        }
        $data['result'] = $result ;
        if($result) {
            $this->load->view('admin/edit_facility',$data);
        } else {
            $url='viewFacilities';
            redirect($url);
        }
    }
    public function updateFacility(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $faciility_name = $this->input->post('faciility_name');
        $venue_type_id = $this->input->post('venue_type_id');
        if($faciility_name!=''){            
            $check_data = array(
            "faciility_name" => $faciility_name,
            "venue_type_id" =>$venue_type_id,
            "id !=" =>$id
            );
            $tablename = "facilities";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
                if($checkData > 0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Facility name already exist</div>') ;
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;         
                $date     = date("Y-m-d H:i:s");
                $id =$this->input->post('id');
                $dataSubcat = array(
                 'venue_type_id' => $venue_type_id,
                 'faciility_name'=> $faciility_name,  
                 'updated_at'     => $date,
                 'updated_by'     => $added_by
                );
                $table="facilities";
                $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
                if($result){
                     $this->session->set_flashdata('msg','<div class="alert alert-success subcatupdate updateSuss">Facilities Updated</div>');
                } else{
                    $url='Facilities/editFacility/'.$id;
                     redirect($url);
                     $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps Some error</div>') ;
                }  
            
            $url='Facilities/editFacility/'.$id;
            redirect($url);
        } else {   
            $url='Facilities/editFacility/'.$id;
            redirect($url);    
        }
    }
    function facilityEnable($id) {
        $id=$id;
        $dataSubcat =array(
           'keyaan_status' =>'0'
        );
        $table="facilities";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewFacilities';
        redirect($url);
    }      
    function facilityDisable($id) {
        $id=$id;
        $dataSubcat =array(
           'keyaan_status' =>'1'
        );
        $table="facilities";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewFacilities';
        redirect($url);
    }
    public function facilityAjax(){
        $id =$this->input->post('id');
        $result = $this->Adminmodel->getAjaxdata('venue_type_id',$id,'facilities');
        $data['resultFacility'] =$result;
        $this->load->view('vendor/facilityAjax',$data);
    }
}
?>