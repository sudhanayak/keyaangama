<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Subcatlisting extends CI_Controller
{
	function __construct() {
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->model('Adminmodel');       
        }
	function index()
	{
        redirect('searchList');
		
	}
    public function subcatlist(){
        $resultCategoryAll = $this->Adminmodel->getAjaxdataCountry('keyaan_category'); 
            $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities'); 
            if($this->session->userdata('userCCode') !=""){
            $userCode = $this->session->userdata('userCCode');
            $cartCount = $this->Adminmodel->getcartCount($userCode);
        }else{
            $cartCount=0;
        }
        $data['menuCategoryAll'] = self::getCategoryList('keyaan_category');
        $data['menuPackageAll'] = self::getCategoryPackList('package_category');
        $resultCategoryAll = $this->Adminmodel->getAjaxdataCountry('keyaan_category'); 
        $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $start=0;
        $perPage = 100;   
        $sorting = $this->input->post('sorting');
        $subcatid = $this->uri->segment(2);

        //if($start!="" && $perPage!=""){
        $table="keyaan_vendor";  
        //$thmecount =$this->Adminmodel->themeListingCount($table,$perPage,$start,@$catid);        
        $result = $this->Adminmodel->subcatListing($table,$perPage,$start,@$subcatid,@$price,@$rating,@$sorting);
        $data['subcategory'] = $this->Adminmodel->getSingleColumnName($subcatid,'id',
        'subcategory_name','keyaan_subcategory') ;
        if($result > 0){
            foreach ($result as $key => $field) {                 
                        
                //For Cat Id
                $cat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','category_id','vendor_categories') ;
                $data['category'] = $this->Adminmodel->getSingleColumnName($cat_id,'id',
                'category_name','keyaan_category') ;
                $result[$key]['priceType']= $this->Adminmodel->getSingleColumnName($cat_id,'id',
                'price_selection','keyaan_category');
                //For Subcat Id
                $subcat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','sub_category_id','vendor_sub_categories') ;
                $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($subcat_id,'id','subcategory_name','keyaan_subcategory') ;               
                //For subsubcat Id
                $subsubcat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','sub_sub_category_id','vendor_sub_sub_categories') ;
                $result[$key]['subsubcategory'] = $this->Adminmodel->getSingleColumnName($subsubcat_id,'id','subsubcat_name','keyaan_subsubcategory') ;                    
                // get the state id from vendor 
                $vendor_name=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                'name','keyaan_vendor') ;
                $result[$key]['listingBy']=$vendor_name;
                $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($field['state_id'],'id','state_name','keyaan_states') ;
                $result[$key]['city']  = $this->Adminmodel->getSingleColumnName($field['city_id'],'id','city_name','keyaan_cities') ;
                if($field['basic_image'] != ''){
                    $result[$key]['image'] = base_url() . "uploads/basic_image/".$field['basic_image'];
                }
                if($field['logo'] != ''){
                    $result[$key]['logo'] = base_url() . "uploads/vendorlogo/".$field['logo'];
                }
            } 
            $resultSubcat=array();        
            $data['result'] = $result ;
            $data['categoryList']=$resultCategoryAll;
            $data['cityList']=$resultCities;
            $data['basicsettingsList']=$resultBasicsettings;
            $data['cartCount']=$cartCount; 
            //print_r($result);exit;  
            $this->load->view('user/subcat_listing',$data);
        } else{
            $resultSubcat=array();
            $result =array();
            $data['result'] = $result;
            $data['categoryList']=$resultCategoryAll; 
            $data['cityList']=$resultCities;
            $data['basicsettingsList']=$resultBasicsettings;
            $data['cartCount']=$cartCount; 
            $this->load->view('user/subcat_listing',$data);
        }
    }
     
    public function subcatfilter(){
        $resultCategoryAll = $this->Adminmodel->getAjaxdataCountry('keyaan_category'); 
            $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities'); 
            if($this->session->userdata('userCCode') !=""){
            $userCode = $this->session->userdata('userCCode');
            $cartCount = $this->Adminmodel->getcartCount($userCode);
        }else{
            $cartCount=0;
        }
        $data['menuCategoryAll'] = self::getCategoryList('keyaan_category');
        $data['menuPackageAll'] = self::getCategoryPackList('package_category');
        $resultCategoryAll = $this->Adminmodel->getAjaxdataCountry('keyaan_category'); 
        $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities');
        $start=0;
        $perPage = 100;   
        $sorting = $this->input->post('sorting');
        $subcatid = $this->uri->segment(2);

        //if($start!="" && $perPage!=""){
        $table="keyaan_vendor";  
        //$thmecount =$this->Adminmodel->themeListingCount($table,$perPage,$start,@$catid);        
        $result = $this->Adminmodel->subcatListing($table,$perPage,$start,@$subcatid,@$price,@$rating);
        $data['subcategory'] = $this->Adminmodel->getSingleColumnName($subcatid,'id',
        'subcategory_name','keyaan_subcategory') ;
        if($result > 0){
            foreach ($result as $key => $field) {                 
                        
                //For Cat Id
                $cat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','category_id','vendor_categories') ;
                $data['category'] = $this->Adminmodel->getSingleColumnName($cat_id,'id',
                'category_name','keyaan_category') ;
                $result[$key]['priceType']= $this->Adminmodel->getSingleColumnName($cat_id,'id',
                'price_selection','keyaan_category');
                //For Subcat Id
                $subcat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','sub_category_id','vendor_sub_categories') ;
                $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($subcat_id,'id','subcategory_name','keyaan_subcategory') ;               
                //For subsubcat Id
                $subsubcat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','sub_sub_category_id','vendor_sub_sub_categories') ;
                $result[$key]['subsubcategory'] = $this->Adminmodel->getSingleColumnName($subsubcat_id,'id','subsubcat_name','keyaan_subsubcategory') ;                    
                // get the state id from vendor 
                $vendor_name=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                'name','keyaan_vendor') ;
                $result[$key]['listingBy']=$vendor_name;
                $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($field['state_id'],'id','state_name','keyaan_states') ;
                $result[$key]['city']  = $this->Adminmodel->getSingleColumnName($field['city_id'],'id','city_name','keyaan_cities') ;
                if($field['basic_image'] != ''){
                    $result[$key]['image'] = base_url() . "uploads/basic_image/".$field['basic_image'];
                }
                if($field['logo'] != ''){
                    $result[$key]['logo'] = base_url() . "uploads/vendorlogo/".$field['logo'];
                }
            } 
            $resultSubcat=array();        
            $data['result'] = $result ;
            $data['categoryList']=$resultCategoryAll;
            $data['cityList']=$resultCities;
            $data['cartCount']=$cartCount; 
            //print_r($result);exit;  
            $this->load->view('user/subcat_listing',$data);
        } else{
            $resultSubcat=array();
            $result =array();
            $data['result'] = $result;
            $data['categoryList']=$resultCategoryAll; 
            $data['cityList']=$resultCities;
            $data['cartCount']=$cartCount; 
            $this->load->view('user/subcat_listing',$data);
        }
    }

    public function getCategoryList(){

        $result = $this->Adminmodel->getCategoryList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryList($field['id']);
             
        } 

        return $result ;   

    }
    public function getSubCategoryList($catId){
          
        $result = $this->Adminmodel->getSubCategoryList($catId);
        foreach ($result as $key => $field) {
             $result[$key]['subsubcatList'] = $this->Adminmodel->getSubsubCategoryList($field['id']) ;
        } 

        return $result ;   

    } 
    public function getCategoryPackList(){

        $result = $this->Adminmodel->getCategoryPackList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryPackList($field['id']); 
        } 
        return $result ;   
    }
    public function getSubCategoryPackList($catId){  
        $result = $this->Adminmodel->getSubCategoryPackList($catId);
        return $result ;   
    }
}
?>