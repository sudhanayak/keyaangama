<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Masterdegree extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
    }
    public function index() {
         redirect('viewMasterdegree');
        } 
         
    public function addMasterdegree(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
        redirect('admin');
        }
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;
        $masterdegree_name =$this->input->post('masterdegree_name');
        if($masterdegree_name!=''){            
        $check_data = array(
        "masterdegree_name" => $this->input->post('masterdegree_name')    
        );
        $tablename = "keyaan_masterdegree";
         $checkData = $this->Adminmodel->existData($check_data,$tablename);
        if($checkData > 0){
            $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Masterdegree name already exist</div>') ;
            $this->load->view('admin/add_masterdegree',$dataBefore);
        }else{
            $admin = $this->session->userdata('userCode');
            $added_by = $admin!='' ? $admin:'admin' ;           
            $date     = date("Y-m-d H:i:s");
            $data = array(
                'masterdegree_name'=> $masterdegree_name,
                'created_by'     => $added_by ,
                'created_at'     => $date,
                'updated_at'     => $date,
                'updated_by'     => $added_by
            );
            $table="keyaan_masterdegree";
            $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Masterdegree Inserted</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Masterdegree not inserted</div>') ;
                }
                $this->load->view('admin/add_masterdegree',$dataBefore);   
            }
        }else{
            $this->load->view('admin/add_masterdegree',$dataBefore);
            }
        }
                
    public function viewMasterdegree(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="keyaan_masterdegree";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Masterdegree/viewMasterdegree";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'masterdegree_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'masterdegree_name');
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
        if($result){
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_masterdegree',$data);
    }     
           
    public function editMasterdegree(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "keyaan_masterdegree";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $data['result'] = $result ;
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        if($result) {
           $this->load->view('admin/edit_masterdegree',$data);
       } else {
           $url='viewMasterdegree';
           redirect($url);
       }
            
        }
    public function updateMasterdegree(){
       if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $masterdegree_name = $this->input->post('masterdegree_name');       
        if($masterdegree_name!=''){            
            $check_data = array(
                "masterdegree_name" => $masterdegree_name,
                "id !=" =>$id   
            );
            $tablename = "keyaan_masterdegree";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Masterdegree name already exist</div>') ;
            }else{
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $id =$this->input->post('id');
                $data = array(
                    'masterdegree_name'=> $masterdegree_name,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="keyaan_masterdegree";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Masterdegree Updated.</div>');
                }else{
                    $url='Masterdegree/editMasterdegree/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Masterdegree not updated.</div>') ;
                }   
            } 
            $url='Masterdegree/editMasterdegree/'.$id;
            redirect($url);
        }else {   
            $url='Masterdegree/editMasterdegree/'.$id;
            redirect($url); 
        }
        
}
        function MasterdegreeEnable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'keyaan_status' =>'0'
            );
            $table="keyaan_masterdegree";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Masterdegree/viewMasterdegree';
            redirect($url);
        }      
        function MasterdegreeDisable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'keyaan_status' =>'1'
            );
            $table="keyaan_masterdegree";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Masterdegree/viewMasterdegree';
            redirect($url);
        }
        function deleteMasterdegree($id){
        $id=$id;
        $result = $this->Adminmodel->delRow($id,'keyaan_masterdegree');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>
