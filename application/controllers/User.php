<?php 
error_reporting(0);
ob_flush();
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Controller
{
    function __construct() {
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->model('Adminmodel');       
        }
    function index()
    {

       $this->session->unset_userdata('catId');
       $this->session->unset_userdata('cityId');
       $this->session->unset_userdata('priceList');
       if($this->session->userdata('userCCode') !=""){
            $userCode = $this->session->userdata('userCCode');
            $cartCount = $this->Adminmodel->getcartCount($userCode);
          }else{
            $cartCount=0;
          }
        $resultbanners = self::getAjaxdataCountry('banner');
        $resultCities = self::getAjaxdataCountry('keyaan_cities');
        $resultCategory = self::getAjaxdataCountry('keyaan_category');
        $resultCategoryAll = self::getCategoryList('keyaan_category');   
        $resultCategoryPackAll = self::getCategoryPackList('package_category');
        $resultBasicsettings = self::getAjaxdataSettings('keyaan_basic_settings');  
        $admin = $this->session->userdata('userCode');
        $data['bannerList']=$resultbanners;
        $data['categoryList']=$resultCategory;
        $data['cityList']=$resultCities;
        $data['basicsettingsList']=$resultBasicsettings;
        $package = $this->Adminmodel->packagescategory();
        foreach ($package as $key => $value) {
              
            $package[$key]['packageImg'] = base_url() . "uploads/packagecategory/".$value['image'];
        }
       // $packageCategory = $this->Adminmodel->packagescategory();
        $result = $this->Adminmodel->ratedVendors();
       if($result > 0){
           foreach ($result as $key => $field) {

             //For Cat Id
             $cat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','category_id','vendor_categories') ;
             $data['category'] = $this->Adminmodel->getSingleColumnName($cat_id,'id',
             'category_name','keyaan_category') ;
             $result[$key]['priceType']= $this->Adminmodel->getSingleColumnName($cat_id,'id',
             'price_selection','keyaan_category');
             //For Subcat Id
             $subcat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','sub_category_id','vendor_sub_categories') ;
             $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($subcat_id,'id','subcategory_name','keyaan_subcategory') ;
             //For subsubcat Id
             $subsubcat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','sub_sub_category_id','vendor_sub_sub_categories') ;
             $result[$key]['subsubcategory'] = $this->Adminmodel->getSingleColumnName($subsubcat_id,'id','subsubcat_name','keyaan_subsubcategory') ;
             // get the state id from vendor
             $state_id=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
             'state_id','keyaan_vendor') ;
             $vendor_name=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
             'name','keyaan_vendor') ;
             // get the city id from vendor
             $city_id=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
             'city_id','keyaan_vendor') ;
             $result[$key]['listingBy']=$vendor_name;
             $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($state_id,'id','state_name','keyaan_states') ;
             $result[$key]['city']  = $this->Adminmodel->getSingleColumnName($city_id,'id','city_name','keyaan_cities') ;
             if($field['basic_image'] != ''){
                 $result[$key]['image'] = base_url() . "uploads/basic_image/".$field['basic_image'];
             }
             if($field['logo'] != ''){
                 $result[$key]['logo'] = base_url() . "uploads/vendorlogo/".$field['logo'];
             }
               $resultSubcat = $this->Adminmodel->getSubCategoryList($cat_id);
               $data['resultSubcat'] =$resultSubcat;
         }

        $resultSubcat=array();
        $data['result'] = $result ;
        $data['cartCount']=$cartCount; 
        $data['package']=$package;
        $data['menuCategoryAll']=$resultCategoryAll;
        $data['menuPackageAll']=$resultCategoryPackAll;
        $admin = $this->session->userdata('userCode');
         $this->load->view('user/index',$data);
       }
       else{
           $resultSubcat=array();
           $result =array();
           $data['result'] = $result;
            $data['cartCount']=$cartCount; 
            $data['menuCategoryAll']=$resultCategoryAll;
            $data['menuPackageAll']=$resultCategoryPackAll;
           $this->load->view('user/index',$data);
       }

        
    }
    public function getAjaxdataCountry($table){
        //$id =$this->input->post('id');
        $result1=[];
        $this->db->where('keyaan_status','0');
        $query = $this->db->get($table);
        $result = $query->result_array();
        
        if($result){
            $result1 =$result;
            return  $result1 ;
        }         
        else{
            
            return $result1 ;
        }
    } 
     public function getAjaxdataSettings($table){
        $result1=[];
        $query = $this->db->get($table);
        $result = $query->result_array();
        
        if($result){
            $result1 =$result;
            return  $result1 ;
        }         
        else{
            return $result1 ;
        }
    }
    public function userLogin(){          
            if($this->input->post('username')){                  
                $where= array(
                    'username'=>$this->input->post('username'),
                    'password' =>$this->input->post('password'),                    
                );    
                $checkLogin = $this->Adminmodel->loginCustomer($where);
                $json_data = array();
                if($checkLogin){
                    $this->session->set_userdata('isCUserLoggedIn',TRUE);
                    $this->session->set_userdata('userCId',$checkLogin['name']);
                    $this->session->set_userdata('userCCode',$checkLogin['id']);
                    $this->session->set_userdata('userCEmail',$checkLogin['email']);
                    $this->session->set_userdata('userCMobile',$checkLogin['mobile']);
                    $json_data['status']='TRUE';
                    $json_data['responseCode']=0;                    
                    $json_data['message']=$checkLogin['name']; 
                    $data['json_data']= $json_data;
                    $this->load->view('user/ajax_all',$data);                   
                  //return  json_encode($json_data);
                } else{                            
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=1;                    
                    $json_data['message']="FAIL"; 
                    $data['json_data']= $json_data;
                    $this->load->view('user/ajax_all',$data); 
                   // return json_encode($json_data);
                }
            }                             
    }
    // for signUp
    public function userSignUp(){
        if($this->input->post('mobile')){                  
            $where= array(
                'email'=>$this->input->post('email'),
                'mobile' =>$this->input->post('mobile'),                    
            );    
            $checkUser = $this->Adminmodel->existCustomer($where);
            $json_data = array();
            if($checkUser==1){    
                //email exist           
                $json_data['status']='TRUE';
                $json_data['responseCode']=1;                    
                $json_data['message']="sucess";                    
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            }
            elseif($checkUser==2){ 
                // mobile exist              
                $json_data['status']='TRUE';
                $json_data['responseCode']=2;                    
                $json_data['message']="sucess";                    
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            } 
            elseif($checkUser==0){ 
                // insert otp and call otp screeen 
                // otp_type 1 means reg
                $code='1234';
                $otpdata = array(
                    'mobile' =>$this->input->post('mobile'), 
                    'otp_code' =>$code,
                    'otp_type' =>1
                );
                $table = 'keyaan_mobileotp';
                $otpResult = $this->Adminmodel->insertRecordQueryList($table,$otpdata);
                if($otpResult){
                    $json_data['status']='TRUE';
                    $json_data['responseCode']=0;                    
                    $json_data['message']="sucess";
                }else{
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=10;                    
                    $json_data['message']="OPPS SOME DB ERROR when insert in otp table";
                }                                    
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            }
            else{                            
                $json_data['status']='FALSE';
                $json_data['responseCode']=10;                    
                $json_data['message']="some error in model "; 
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            }
        }else{
            $json_data['status']='FALSE';
            $json_data['responseCode']=100;                    
            $json_data['message']="please enter all the mandatory fields"; 
            $data['json_data']= $json_data;
            $this->load->view('user/ajax_all',$data); 
        }
    }
    // this function is used for verify otp if sucess insert one record in users table 
    public function userVerifyOtp(){
        if($this->input->post('mobile')){                          
                $where= array(
                    'mobile' =>$this->input->post('mobile'), 
                    'otp_code' =>$this->input->post('code'),
                    'otp_type' =>1                 
                );  
                $table = 'keyaan_mobileotp';  
                $checkOtpVerify = $this->Adminmodel->existData($where,$table);
                $json_data = array();
                if($checkOtpVerify==1){ 
                    $user_code = rand(159753,357493);
                   // insert otp and call otp screeen 
                   // otp_type 1 means reg
                    $regData= array(
                        'user_code'=>$user_code,
                        'name' =>$this->input->post('name'), 
                        'email' =>$this->input->post('email'),
                        'mobile' =>$this->input->post('mobile'),
                        'password' =>$this->input->post('password'), 

                    );  
                    $table = 'users';
                    $checkLogin = $this->Adminmodel->insertRecordQueryListReg($table,$regData);
                    if($checkLogin){

                        $this->session->set_userdata('isCUserLoggedIn',TRUE);
                        $this->session->set_userdata('userCId',$checkLogin['name']);
                        $this->session->set_userdata('userCCode',$checkLogin['id']);
                        $this->session->set_userdata('userCEmail',$checkLogin['email']);
                        $this->session->set_userdata('userCMobile',$checkLogin['mobile']);
                        $json_data['status']='TRUE';
                        $json_data['responseCode']=0;                    
                        $json_data['message']=$checkLogin;
                    }else{
                        $json_data['status']='FALSE';
                        $json_data['responseCode']=10;                    
                        $json_data['message']="OPPS SOME DB ERROR";
                    }                                    
                    $data['json_data']= $json_data;
                    $this->load->view('user/ajax_all',$data); 
                }else{                            
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=1;                    
                    $json_data['message']="otp verify fail"; 
                    $data['json_data']= $json_data;
                    $this->load->view('user/ajax_all',$data); 
                }
            }else{
                $json_data['status']='FALSE';
                $json_data['responseCode']=3;                    
                $json_data['message']="please enter all the mandatory fields"; 
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            }
    }

    // for get password 

     // for signUp
    public function userForgetPassword(){
        if($this->input->post('mobile')){                  
            $where= array(
                'mobile' =>$this->input->post('mobile'),                    
            );    
            $checkUser = $this->Adminmodel->existCustomer2($where);
            $json_data = array();
            if($checkUser==0){    
                //MOBILE NOT EXIST        
                $json_data['status']='FALSE';
                $json_data['responseCode']=1;                    
                $json_data['message']="sucess";                    
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            }
            elseif($checkUser==1){ 
                // insert otp and call otp screeen 
                // otp_type 1 means reg
                $code='1234';
                $otpdata = array(
                    'mobile'   =>$this->input->post('mobile'), 
                    'otp_code' =>$code,
                    'otp_type' =>2
                );
                $table = 'keyaan_mobileotp';
                $otpResult = $this->Adminmodel->insertRecordQueryList($table,$otpdata);
                if($otpResult){
                    $json_data['status']='TRUE';
                    $json_data['responseCode']=0;                    
                    $json_data['message']="sucess";
                }else{
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=10;                    
                    $json_data['message']="OPPS SOME DB ERROR when insert in otp table";
                }                                    
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            }
            else{                            
                $json_data['status']='FALSE';
                $json_data['responseCode']=10;                    
                $json_data['message']="some error in model "; 
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            }
        }else{
            $json_data['status']='FALSE';
            $json_data['responseCode']=100;                    
            $json_data['message']="please enter all the mandatory fields"; 
            $data['json_data']= $json_data;
            $this->load->view('user/ajax_all',$data); 
        }
    }



     public function forgetVerifyOtp(){
        if($this->input->post('mobile')){                          
                $where= array(
                    'mobile' =>$this->input->post('mobile'), 
                    'otp_code' =>$this->input->post('code'),
                    'otp_type' =>2                 
                );  
                $table = 'keyaan_mobileotp';  
                $checkOtpVerify = $this->Adminmodel->existData($where,$table);
                $json_data = array();
                if($checkOtpVerify==1){ 
                 
                    $json_data['status']='TRUE';
                    $json_data['responseCode']=0;                    
                    $json_data['message']="sucessfully";
                    $data['json_data']= $json_data;
                    $this->load->view('user/ajax_all',$data); 
                }else{                            
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=1;                    
                    $json_data['message']="otp verify fail"; 
                    $data['json_data']= $json_data;
                    $this->load->view('user/ajax_all',$data); 
                }
            }else{
                $json_data['status']='FALSE';
                $json_data['responseCode']=3;                    
                $json_data['message']="please enter all the mandatory fields"; 
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            }
    }

    public function getCategoryList(){

        $result = $this->Adminmodel->getCategoryList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryList($field['id']);
             
        } 

        return $result ;   

    }
    public function getSubCategoryList($catId){
          
        $result = $this->Adminmodel->getSubCategoryList($catId);
        foreach ($result as $key => $field) {
             $result[$key]['subsubcatList'] = $this->Adminmodel->getSubsubCategoryList($field['id']) ;
        } 

        return $result ;   

    } 

    public function getCategoryPackList(){

        $result = $this->Adminmodel->getCategoryPackList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryPackList($field['id']); 
        } 
        return $result ;   
    }
    public function getSubCategoryPackList($catId){  
        $result = $this->Adminmodel->getSubCategoryPackList($catId);
        return $result ;   
    }












    //Logout Functionality 
    public function logout(){
        $this->session->unset_userdata('isCUserLoggedIn');
        $this->session->unset_userdata('userCId');
        $this->session->unset_userdata('userCCode');
        $url=base_url();
        redirect($url);
    } 
}
?>