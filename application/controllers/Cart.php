<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Cart extends CI_Controller
{
	function __construct() {
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->model('Adminmodel');       
        }
	function index()
	{
        if($this->session->userdata('userCCode') ==""){
            $url=base_url();
            redirect($url);
        }
       $this->session->unset_userdata('catId');
       $this->session->unset_userdata('cityId');
       $this->session->unset_userdata('priceList');
       if($this->session->userdata('userCCode') !=""){
             $userCode = $this->session->userdata('userCCode');
             $cartCount = $this->Adminmodel->getcartCount($userCode);
           }else{
             $cartCount=0;
           }
           $data['menuCategoryAll'] = self::getCategoryList('keyaan_category');
           $data['menuPackageAll'] = self::getCategoryPackList('package_category');
        $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities');
        $resultCategory = $this->Adminmodel->getAjaxdataCountry('keyaan_category');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');    
        
       $data['categoryList']=$resultCategory;
        $data['cityList']=$resultCities;
        $data['basicsettingsList']=$resultBasicsettings;

        $userCode = $this->session->userdata('userCCode');
        $result = $this->Adminmodel->cartDetail($userCode);
        $result1 = $this->Adminmodel->packagecartDetail($userCode);
        if($result > 0 && $result1 > 0){
            foreach ($result as $key => $field) {
                $result[$key]['vendorName']= $this->Adminmodel->getSingleColumnName($field['vendor_id'],'vendor_code','name','keyaan_vendor') ;
                $result[$key]['itemName']= $this->Adminmodel->getSingleColumnName($field['item_id'],'id','theme_name','vendor_themelist') ;
            }
            foreach ($result1 as $key => $field) {
                $result1[$key]['packageName']= $this->Adminmodel->getSingleColumnName($field['package_id'],'package_code','package_name','package') ;
                $result1[$key]['itineraryName']= $this->Adminmodel->getSingleColumnName($field['iteinerary_id'],'itinerary_code','itenerary_name','package_detail') ;
            }
            $resultSubcat=array();
            $data['result'] = $result ;
            $data['result1'] = $result1 ;
            $data['cartCount']=$cartCount; 
            $this->load->view('user/cart',$data);
        } else{
            $resultSubcat=array();
            $result =array();
            $data['result'] = $result;
            $data['cartCount']=$cartCount; 
            $this->load->view('user/cart',$data);
        }

		
	}
    

    function deleteCart($id) {
        $id=$id;
        $userCode=$this->session->userdata('userCCode');
        $result = $this->Adminmodel->delCart($id,$userCode);
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }

    function deletePackageCart($id) {
        $id=$id;
        $userCode=$this->session->userdata('userCCode');
        $result = $this->Adminmodel->delPackageCart($id,$userCode);
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function getCategoryList(){

        $result = $this->Adminmodel->getCategoryList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryList($field['id']);
             
        } 

        return $result ;   

    }
    public function getSubCategoryList($catId){
          
        $result = $this->Adminmodel->getSubCategoryList($catId);
        foreach ($result as $key => $field) {
             $result[$key]['subsubcatList'] = $this->Adminmodel->getSubsubCategoryList($field['id']) ;
        } 

        return $result ;   

    }
    public function getCategoryPackList(){

        $result = $this->Adminmodel->getCategoryPackList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryPackList($field['id']); 
        } 
        return $result ;   
    }
    public function getSubCategoryPackList($catId){  
        $result = $this->Adminmodel->getSubCategoryPackList($catId);
        return $result ;   
    }

    //Logout Functionality 
    public function logout(){
        $this->session->unset_userdata('isCUserLoggedIn');
        $this->session->unset_userdata('userCId');
        $this->session->unset_userdata('userCCode');
        $url=base_url();
        redirect($url);
    } 
}
?>