<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
    }
    public function index() {
        redirect('viewEmployee');
    }
    public function addEmployee() {
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $dataBefore =[];    
        $emp_fname =$this->input->post('emp_fname');
        $resultCountry = $this->Adminmodel->getAjaxdataCountry('keyaan_countries');
        $dataBefore['resultCnt'] = $resultCountry;
        $dataBefore['resultCnt1'] = $resultCountry;
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;
        $this->load->library('upload');
        if(!empty($emp_fname)){
            // for logo start here   
            $min='1452';
            $max='8569';
            $emp_code =rand($min,$max);
            if($_FILES['emp_profile_img']['size'] > 0) {
                $config_media['upload_path'] = './uploads/employeeProfileImage';
                $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                $config_media['max_size']   = '1000000000000000'; // whatever you need
                // $this->load->library($config_media);
                $this->upload->initialize($config_media);
                $error = [];
                if ( ! $this->upload->do_upload('emp_profile_img'))
                {
                    $error[] = array('error_image' => $this->upload->display_errors()); 
                }
                else
                {
                    $data[] = array('upload_image' => $this->upload->data());
                }       
                $emp_profile_img = $data[0]['upload_image']['file_name'];
                if(count($error) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in employe profile uploads</div>') ;
                    redirect('addEmployee');
                    //echo"employe profile error";
                }        
            }else{
                $emp_profile_img = "";
            }

            //For Resume
            if($_FILES['resume']['size'] > 0) {
                $config_media['upload_path'] = './uploads/empResumes';
                $config_media['allowed_types'] = 'pdf|doc|docx';     
                $config_media['max_size']   = '1000000000000000'; // whatever you need
                // $this->load->library($config_media);
                $this->upload->initialize($config_media);
                $error1 = [];
                if ( ! $this->upload->do_upload('resume'))
                {
                    $error1[] = array('error_image' => $this->upload->display_errors()); 
                }
                else
                {
                    $data1[] = array('upload_image' => $this->upload->data());
                }     
                $resume = $data1[0]['upload_image']['file_name'];
                if(count($error1) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Resume uploads</div>') ;
                    redirect('addVendor');
                    //echo"resume error";
                }        
            }else{
                $resume = "";
            }
            
            // for Document
            $doc_img=count($_FILES['doc_img']['name']);
            if($doc_img !=''){                               
                $i=0; 
                $filesCount = count($_FILES['doc_img']['name']);
                for($i = 0; $i < $filesCount; $i++){
                    $_FILES['file']['name']     = $_FILES['doc_img']['name'][$i];
                    $_FILES['file']['type']     = $_FILES['doc_img']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['doc_img']['tmp_name'][$i];
                    $_FILES['file']['error']     = $_FILES['doc_img']['error'][$i];
                    $_FILES['file']['size']     = $_FILES['doc_img']['size'][$i];
                    
                    // File upload configuration
                    $uploadPath = './uploads/employeeDocuments/';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';
                    
                    // Load and initialize upload library
                    $this->upload->initialize($config);
                    // Upload file to server
                    if($this->upload->do_upload('file')){
                        // Uploaded file data
                        $fileData = $this->upload->data();
                        $uploadData[$i]['file_name'] = $fileData['file_name'];
                        $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                        $data = array(
                            'emp_code'=> $emp_code ,
                            'doc_img' => $uploadData[$i]['file_name'],
                            'doc_type'=>'1'
                        );
                       $result = $this->Adminmodel->insertRecordQueryList('emp_document_detail',$data);
                    }
                }
            }
            $dateCurrent= date("Y-m-d H:i:s");
            $emp_fname     = $this->input->post('emp_fname') ==""?"":$this->input->post('emp_fname');
            $emp_lname     = $this->input->post('emp_lname') ==""?"":$this->input->post('emp_lname'); 
            $emp_mobile     = $this->input->post('emp_mobile') ==""?"":$this->input->post('emp_mobile');
            $emp_email          = $this->input->post('emp_email') =="" ? "":$this->input->post('emp_email');
            $emp_dob         = $this->input->post('emp_dob') =="" ? "":$this->input->post('emp_dob');
            $emp_age     = $this->input->post('emp_age') =="" ? "":$this->input->post('emp_age');
            $emp_marital_status   = $this->input->post('emp_marital_status') =="" ? "":$this->input->post('emp_marital_status');
            $emp_designation     = $this->input->post('emp_designation') =="" ? "0":$this->input->post('emp_designation');
            $emp_profile_img    = $emp_profile_img;
            $resume = $resume;
            $any_disability   = $this->input->post('any_disability') =="" ? "":$this->input->post('any_disability');
            $disability_detail  = $this->input->post('disability_detail') =="" ? "":$this->input->post('disability_detail');
            $master_degree  = $this->input->post('master_degree') =="" ? "":$this->input->post('master_degree');
            $master_passout  = $this->input->post('master_passout') =="" ? "":$this->input->post('master_passout');
            $master_spl  = $this->input->post('master_spl') =="" ? "":$this->input->post('master_spl');
            $graduation  = $this->input->post('graduation') =="" ? "":$this->input->post('graduation');
            $graduation_passout  = $this->input->post('graduation_passout') =="" ? "":$this->input->post('graduation_passout');
            $graduation_spl  = $this->input->post('graduation_spl') =="" ? "":$this->input->post('graduation_spl');
            $graduation_intitutes  = $this->input->post('graduation_intitutes') =="" ? "":$this->input->post('graduation_intitutes');
            $master_intitutes  = $this->input->post('master_intitutes') =="" ? "":$this->input->post('master_intitutes');
            $inter  = $this->input->post('inter') =="" ? "":$this->input->post('inter');
            $inter_passout  = $this->input->post('inter_passout') =="" ? "":$this->input->post('inter_passout');
            $inter_spl  = $this->input->post('inter_spl') =="" ? "":$this->input->post('inter_spl');
            $inter_intitutes  = $this->input->post('inter_intitutes') =="" ? "":$this->input->post('inter_intitutes');
            $father_name  = $this->input->post('father_name') =="" ? "":$this->input->post('father_name');
            $mother_name  = $this->input->post('mother_name') =="" ? "":$this->input->post('mother_name');
            $per_country  = $this->input->post('per_country') =="" ? "":$this->input->post('per_country');
            $per_state  = $this->input->post('per_state') =="" ? "":$this->input->post('per_state');
            $per_district  = $this->input->post('per_district') =="" ? "":$this->input->post('per_district');
            $per_city  = $this->input->post('per_city') =="" ? "":$this->input->post('per_city');
            $per_pincode  = $this->input->post('per_pincode') =="" ? "":$this->input->post('per_pincode');
            $per_address  = $this->input->post('per_address') =="" ? "":$this->input->post('per_address');
            $res_country  = $this->input->post('res_country') =="" ? "":$this->input->post('res_country');
            $res_state  = $this->input->post('res_state') =="" ? "":$this->input->post('res_state');
            $res_district  = $this->input->post('res_district') =="" ? "":$this->input->post('res_district');
            $res_city  = $this->input->post('res_city') =="" ? "":$this->input->post('res_city');
            $res_pincode  = $this->input->post('res_pincode') =="" ? "":$this->input->post('res_pincode');
            $res_address  = $this->input->post('res_address') =="" ? "":$this->input->post('res_address');
            $grade_type  = $this->input->post('grade_type') =="" ? "":$this->input->post('grade_type');
            $total_salary  = $this->input->post('total_salary') =="" ? "":$this->input->post('total_salary');
            $take_home  = $this->input->post('take_home') =="" ? "":$this->input->post('take_home');
            $pf  = $this->input->post('pf') =="" ? "":$this->input->post('pf');
            $pf_amount  = $this->input->post('pf_amount') =="" ? "":$this->input->post('pf_amount');
            $bank_name  = $this->input->post('bank_name') =="" ? "":$this->input->post('bank_name');
            $account_number  = $this->input->post('account_number') =="" ? "":$this->input->post('account_number');
            $designation  = $this->input->post('designation') =="" ? "":$this->input->post('designation');
            $company_name  = $this->input->post('company_name') =="" ? "":$this->input->post('company_name');
            $emp_mode  = $this->input->post('emp_mode') =="" ? "":$this->input->post('emp_mode');
            $from_date  = $this->input->post('from_date') =="" ? "":$this->input->post('from_date');
            $to_date  = $this->input->post('to_date') =="" ? "":$this->input->post('to_date');
            $created_at = date("Y-m-d H:i:s");
            $updated_at = date("Y-m-d H:i:s");
            $created_by = 1;
            $updated_by = 1;
            if($emp_dob!=""){
                $date_array = explode("/",$emp_dob); // split the array
                $var_day = $date_array[0]; //day seqment
                $var_month = $date_array[1]; //month segment
                $var_year = $date_array[2]; //year segment
                $emp_dob1= "$var_year-$var_month-$var_day"; // join them together
            }
            if($from_date!=""){
            $date_array = explode("/",$from_date); // split the array
            $var_day = $date_array[0]; //day seqment
            $var_month = $date_array[1]; //month segment
            $var_year = $date_array[2]; //year segment
            $from_date1= "$var_year-$var_month-$var_day"; // join them together
            }
            if($to_date!=""){
            $date_array = explode("/",$to_date); // split the array
            $var_day = $date_array[0]; //day seqment
            $var_month = $date_array[1]; //month segment
            $var_year = $date_array[2]; //year segment
            $to_date1= "$var_year-$var_month-$var_day"; // join them together
            }

            //FOR Education Details
            $educationData = array(
                'emp_code'=> $emp_code ,
                'master_degree' => $master_degree,
                'master_passout' => $master_passout,
                'master_spl' => $master_spl,
                'graduation' => $graduation,
                'graduation_passout' => $graduation_passout,
                'graduation_spl' => $graduation_spl,
                'graduation_intitutes' => $graduation_intitutes,
                'master_intitutes' => $master_intitutes,
                'inter' => $inter,
                'inter_passout' => $inter_passout,
                'inter_spl' => $inter_spl,
                'inter_intitutes' => $inter_intitutes,
            );
            $result = $this->Adminmodel->insertRecordQueryList('emp_education_detail',$educationData);

            //For Family Details
            $familyData = array(
                'emp_code'=> $emp_code ,
                'father_name' => $father_name,
                'mother_name' => $mother_name,
                'per_country' => $per_country,
                'per_state' => $per_state,
                'per_district' => $per_district,
                'per_city' => $per_city,
                'per_pincode' => $per_pincode,
                'per_address' => $per_address,
                'res_country' => $res_country,
                'res_state  ' => $res_state   ,
                'res_district' => $res_district,
                'res_city' => $res_city,
                'res_pincode' => $res_pincode,
                'res_address' => $res_address,
            );
            $result = $this->Adminmodel->insertRecordQueryList('emp_family_detail',$familyData);

            //FOR Salary Details
            $salaryData = array(
                'emp_code'=> $emp_code ,
                'grade_type' => $grade_type,
                'total_salary' => $total_salary,
                'take_home' => $take_home,
                'pf	' => $pf	,
                'pf_amount' => $pf_amount,
                'bank_name  ' => $bank_name   ,
                'account_number' => $account_number,
            );
            $result = $this->Adminmodel->insertRecordQueryList('emp_salary',$salaryData);

            //FOR Work Details
            $workData = array(
                'emp_code'=> $emp_code ,
                'designation' => $designation,
                'company_name' => $company_name,
                'emp_mode' => $emp_mode,
                'from_date	' => $from_date1	,
                'to_date' => $to_date1,
            );
            $result = $this->Adminmodel->insertRecordQueryList('emp_work_history',$workData);

            $employeeData = array(
                'emp_code'=> $emp_code,
                'emp_fname'=>$emp_fname,
                'emp_lname'=>$emp_lname,
                'emp_mobile'=>$emp_mobile,
                'emp_email' => $emp_email,
                'emp_dob' => $emp_dob1,
                'emp_age' => $emp_age,
                'emp_marital_status' => $emp_marital_status,
                'emp_designation' => $emp_designation,
                'any_disability' => $any_disability,
                'emp_profile_img' => $emp_profile_img,
                'resume'=>$resume,
                'disability_detail' => $disability_detail
            );
            $tableVendor="emp_basic_detail";
            $result = $this->Adminmodel->insertRecordQueryList($tableVendor,$employeeData);
            if($result) {
                $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Employee Added Successfully</div>') ;
                $this->load->view('admin/add_employee',$dataBefore);  
            } else {
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps!Some error occured, Employee not added.</div>') ;
               $this->load->view('admin/add_employee',$dataBefore);  
            } 
        } else {
            //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
            $this->load->view('admin/add_employee',$dataBefore);  
        }
    }

    public function editEmployee() {
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
            {
              redirect('admin');
            }
        $dataBefore =[];  
        $emp_code = $this->uri->segment('3');
        $resultCountry = $this->Adminmodel->getAjaxdataCountry('keyaan_countries');
         $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');       
        $this->load->library('upload');      
        $admin = $this->session->userdata('userCode');
        $added_by = $admin!='' ? $admin:'admin' ;
        $dataBefore['resultCnt'] = $resultCountry;
        $dataBefore['resultCnt1'] = $resultCountry;
        $dataBefore['basicsettingsList']=$resultBasicsettings;           
        $date     = date("Y-m-d H:i:s");
        if(!empty($emp_code)){
            $tablename = "emp_basic_detail";
            $result = $this->Adminmodel->singleRecordData('emp_code', $emp_code,$tablename);
            $result1 = $this->Adminmodel->singleRecordData('emp_code', $emp_code,'emp_document_detail');
            $result2 = $this->Adminmodel->singleRecordData('emp_code', $emp_code,'emp_education_detail');
            $result3 = $this->Adminmodel->singleRecordData('emp_code', $emp_code,'emp_family_detail');
            $result4 = $this->Adminmodel->singleRecordData('emp_code', $emp_code,'emp_salary');
            $result5 = $this->Adminmodel->singleRecordData('emp_code', $emp_code,'emp_work_history');
            if($result){
                foreach ($result as $key => $field) {
                    if($field['emp_profile_img'] !=""){
                        $result[$key]['emp_profile_img'] = base_url()."uploads/employeeProfileImage/".$field['emp_profile_img'];
                    } else {
                        $result[$key]['emp_profile_img'] = "0";
                    }
                    $result[$key]['dob'] = date("d/m/Y", strtotime($field['emp_dob']));
                }
                if($result1) {
                    foreach ($result1 as $key => $field1) {
                        $result1['imglist'] = $this->Adminmodel->getgallery($field['emp_code'],'emp_document_detail');
                    }
                }
                if($result3) {
                    foreach ($result3 as $key => $field3) {
                        $result3[$key]['per_country_name'] = $this->Adminmodel->getSingleColumnName($field3['per_country'],'id','country_name','keyaan_countries') ;
                        $result3[$key]['per_state_name'] = $this->Adminmodel->getSingleColumnName($field3['per_state'],'id','state_name','keyaan_states') ;
                        $result3[$key]['per_district_name'] = $this->Adminmodel->getSingleColumnName($field3['per_district'],'id','district_name','keyaan_districts') ;
                        $result3[$key]['per_city_name'] = $this->Adminmodel->getSingleColumnName($field3['per_city'],'id','city_name','keyaan_cities') ;
                        $result3[$key]['per_pincode_name'] = $this->Adminmodel->getSingleColumnName($field3['per_pincode'],'id','pincode','keyaan_pincodes') ;
                        $result3[$key]['per_address_name'] = $this->Adminmodel->getSingleColumnName($field3['per_address'],'id','location_name','keyaan_locations') ;
                        $result3[$key]['res_country_name'] = $this->Adminmodel->getSingleColumnName($field3['res_country'],'id','country_name','keyaan_countries') ;
                        $result3[$key]['res_state_name'] = $this->Adminmodel->getSingleColumnName($field3['res_state'],'id','state_name','keyaan_states') ;
                        $result3[$key]['res_district_name'] = $this->Adminmodel->getSingleColumnName($field3['res_district'],'id','district_name','keyaan_districts') ;
                        $result3[$key]['res_city_name'] = $this->Adminmodel->getSingleColumnName($field3['res_city'],'id','city_name','keyaan_cities') ;
                        $result3[$key]['res_pincode_name'] = $this->Adminmodel->getSingleColumnName($field3['res_pincode'],'id','pincode','keyaan_pincodes') ;
                        $result3[$key]['res_address_name'] = $this->Adminmodel->getSingleColumnName($field3['res_address'],'id','location_name','keyaan_locations') ;
                    }
                }
                if($result5) {
                    foreach ($result5 as $key => $field5) {
                        $result[$key]['fromDate'] = date("d/m/Y", strtotime($field5['from_date']));
                        $result[$key]['toDate'] = date("d/m/Y", strtotime($field5['to_date']));
                    }
                }
                $dataBefore['result'] = replace_empty_values($result);
                $dataBefore['result1'] = $result1;
                $dataBefore['result2'] = replace_empty_values($result2);
                $dataBefore['result3'] = replace_empty_values($result3);
                $dataBefore['result4'] = replace_empty_values($result4);
                $dataBefore['result5'] = replace_empty_values($result5);
                $dataBefore['resultCat'] = $resultCountry;
                $this->load->view('admin/edit_employee',$dataBefore);  
            } else {
                $url='viewEmployee';
                redirect($url);
            }
        } else {
            redirect('admin');
        }
    }

    public function updateEmployee() {
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $emp_fname =$this->input->post('emp_fname');
        $id =$this->input->post('id');
        $this->load->library('upload');
        $admin = $this->session->userdata('userCode');
        $added_by = $admin!='' ? $admin:'admin' ;           
        $date     = date("Y-m-d H:i:s");
        $emp_code =$this->input->post('emp_code');
        if(!empty($emp_fname) && !empty($emp_code)){ 
            // for Employee profile Image
            $tablename_image ="";
            if($_FILES['emp_profile_img']['size'] > 0) {
                $config_media['upload_path'] = './uploads/employeeProfileImage';
                $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                $config_media['max_size']   = '1000000000000000'; // whatever you need
               // $this->load->library($config_media);
                $this->upload->initialize($config_media);
                $error = [];
                if ( ! $this->upload->do_upload('emp_profile_img')) {
                    $error[] = array('error_image' => $this->upload->display_errors()); 
                }
                else {
                    $data[] = array('upload_image' => $this->upload->data());
                }       
                $emp_profile_img = $data[0]['upload_image']['file_name'];
                $imgArr = array(
                    "emp_profile_img"=>$emp_profile_img ,
                    "update_by"=>$added_by,
                    "updated_at"=>$date  
                );
                $resultUpdate = $this->Adminmodel->updateRecordQueryList('emp_basic_detail',$imgArr,'emp_code',$emp_code);
                if(count($error) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Employee Profile uploads</div>') ;
                    $url='Employee/editEmployee/'.$emp_code;
                    redirect($url);
                }        
            }else{
                $emp_profile_img = "";
            }
            //For Resumes
            if($_FILES['resume']['size'] > 0) {
                $config_media['upload_path'] = './uploads/empResumes';
                $config_media['allowed_types'] = 'pdf|doc|docx';    
                $config_media['max_size']   = '1000000000000000'; // whatever you need
               // $this->load->library($config_media);
                $this->upload->initialize($config_media);
                $error = [];
                if ( ! $this->upload->do_upload('resume')) {
                    $error[] = array('error_image' => $this->upload->display_errors()); 
                }
                else {
                    $data[] = array('upload_image' => $this->upload->data());
                }       
                $resume = $data[0]['upload_image']['file_name'];
                $imgArr = array(
                    "resume"=>$resume ,
                    "update_by"=>$added_by,
                    "updated_at"=>$date  
                );
                $resultUpdate = $this->Adminmodel->updateRecordQueryList('emp_basic_detail',$imgArr,'emp_code',$emp_code);
                if(count($error) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Resume uploads</div>') ;
                    $url='Employee/editEmployee/'.$emp_code;
                    redirect($url);
                }        
            }else{
                $resume = "";
            }
            // for Documents
            $doc_img=count($_FILES['doc_img']['name']);   
            if($doc_img !=''){                               
                $i=0; 
                $filesCount = count($_FILES['doc_img']['name']);
                for($i = 0; $i < $filesCount; $i++){
                $_FILES['file']['name']     = $_FILES['doc_img']['name'][$i];
                $_FILES['file']['type']     = $_FILES['doc_img']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['doc_img']['tmp_name'][$i];
                $_FILES['file']['error']     = $_FILES['doc_img']['error'][$i];
                $_FILES['file']['size']     = $_FILES['doc_img']['size'][$i];
                // File upload configuration
                $uploadPath = './uploads/employeeDocuments/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                // Load and initialize upload library
                $this->upload->initialize($config);
                // Upload file to server
                    if($this->upload->do_upload('file')){
                        // Uploaded file data
                        $fileData = $this->upload->data();
                        $uploadData[$i]['file_name'] = $fileData['file_name'];
                        $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");

                        $imgArr20 = array(
                            "doc_img"=>$uploadData[$i]['file_name'],
                            "emp_code"=>$emp_code,
                            "doc_type"=>'1'
                        );                            
                        $resultUpdate = $this->Adminmodel->insertRecordQueryList('emp_document_detail',$imgArr20);
                    }
                }
            }
            $dateCurrent= date("Y-m-d H:i:s");
            $emp_fname  = $this->input->post('emp_fname') ==""?"":$this->input->post('emp_fname');
            $emp_lname  = $this->input->post('emp_lname') ==""?"":$this->input->post('emp_lname'); 
            $emp_mobile = $this->input->post('emp_mobile') ==""?"":$this->input->post('emp_mobile');
            $emp_email  = $this->input->post('emp_email') =="" ? "":$this->input->post('emp_email');
            $emp_dob    = $this->input->post('emp_dob') =="" ? "":$this->input->post('emp_dob');
            $emp_age     = $this->input->post('emp_age') =="" ? "":$this->input->post('emp_age');
            $emp_marital_status   = $this->input->post('emp_marital_status') =="" ? "":$this->input->post('emp_marital_status');
            $emp_designation     = $this->input->post('emp_designation') =="" ? "0":$this->input->post('emp_designation');
            $emp_profile_img    = $emp_profile_img;
            $any_disability   = $this->input->post('any_disability') =="" ? "":$this->input->post('any_disability');
            $disability_detail  = $this->input->post('disability_detail') =="" ? "":$this->input->post('disability_detail');
            $master_degree  = $this->input->post('master_degree') =="" ? "":$this->input->post('master_degree');
            $master_passout  = $this->input->post('master_passout') =="" ? "":$this->input->post('master_passout');
            $master_spl  = $this->input->post('master_spl') =="" ? "":$this->input->post('master_spl');
            $graduation  = $this->input->post('graduation') =="" ? "":$this->input->post('graduation');
            $graduation_passout  = $this->input->post('graduation_passout') =="" ? "":$this->input->post('graduation_passout');
            $graduation_spl  = $this->input->post('graduation_spl') =="" ? "":$this->input->post('graduation_spl');
            $graduation_intitutes  = $this->input->post('graduation_intitutes') =="" ? "":$this->input->post('graduation_intitutes');
            $master_intitutes  = $this->input->post('master_intitutes') =="" ? "":$this->input->post('master_intitutes');
            $inter  = $this->input->post('inter') =="" ? "":$this->input->post('inter');
            $inter_passout  = $this->input->post('inter_passout') =="" ? "":$this->input->post('inter_passout');
            $inter_spl  = $this->input->post('inter_spl') =="" ? "":$this->input->post('inter_spl');
            $inter_intitutes  = $this->input->post('inter_intitutes') =="" ? "":$this->input->post('inter_intitutes');
            $father_name  = $this->input->post('father_name') =="" ? "":$this->input->post('father_name');
            $mother_name  = $this->input->post('mother_name') =="" ? "":$this->input->post('mother_name');
            $per_country  = $this->input->post('per_country') =="" ? "":$this->input->post('per_country');
            $per_state  = $this->input->post('per_state') =="" ? "":$this->input->post('per_state');
            $per_district  = $this->input->post('per_district') =="" ? "":$this->input->post('per_district');
            $per_city  = $this->input->post('per_city') =="" ? "":$this->input->post('per_city');
            $per_pincode  = $this->input->post('per_pincode') =="" ? "":$this->input->post('per_pincode');
            $per_address  = $this->input->post('per_address') =="" ? "":$this->input->post('per_address');
            $res_country  = $this->input->post('res_country') =="" ? "":$this->input->post('res_country');
            $res_state  = $this->input->post('res_state') =="" ? "":$this->input->post('res_state');
            $res_district  = $this->input->post('res_district') =="" ? "":$this->input->post('res_district');
            $res_city  = $this->input->post('res_city') =="" ? "":$this->input->post('res_city');
            $res_pincode  = $this->input->post('res_pincode') =="" ? "":$this->input->post('res_pincode');
            $res_address  = $this->input->post('res_address') =="" ? "":$this->input->post('res_address');
            $grade_type  = $this->input->post('grade_type') =="" ? "":$this->input->post('grade_type');
            $total_salary  = $this->input->post('total_salary') =="" ? "":$this->input->post('total_salary');
            $take_home  = $this->input->post('take_home') =="" ? "":$this->input->post('take_home');
            $pf  = $this->input->post('pf') =="" ? "":$this->input->post('pf');
            $pf_amount  = $this->input->post('pf_amount') =="" ? "":$this->input->post('pf_amount');
            $bank_name  = $this->input->post('bank_name') =="" ? "":$this->input->post('bank_name');
            $account_number  = $this->input->post('account_number') =="" ? "":$this->input->post('account_number');
            $designation  = $this->input->post('designation') =="" ? "":$this->input->post('designation');
            $company_name  = $this->input->post('company_name') =="" ? "":$this->input->post('company_name');
            $emp_mode  = $this->input->post('emp_mode') =="" ? "":$this->input->post('emp_mode');
            $from_date  = $this->input->post('from_date') =="" ? "":$this->input->post('from_date');
            $to_date  = $this->input->post('to_date') =="" ? "":$this->input->post('to_date');
            $created_at = date("Y-m-d H:i:s");
            $updated_at = date("Y-m-d H:i:s");
            $created_by = 1;
            $updated_by = 1;
            if($emp_dob!=""){
                $date_array = explode("/",$emp_dob); // split the array
                $var_day = $date_array[0]; //day seqment
                $var_month = $date_array[1]; //month segment
                $var_year = $date_array[2]; //year segment
                $emp_dob1= "$var_year-$var_month-$var_day"; // join them together
            }
            if($from_date!=""){
            $date_array = explode("/",$from_date); // split the array
            $var_day = $date_array[0]; //day seqment
            $var_month = $date_array[1]; //month segment
            $var_year = $date_array[2]; //year segment
            $from_date1= "$var_year-$var_month-$var_day"; // join them together
            }
            if($to_date!=""){
            $date_array = explode("/",$to_date); // split the array
            $var_day = $date_array[0]; //day seqment
            $var_month = $date_array[1]; //month segment
            $var_year = $date_array[2]; //year segment
            $to_date1= "$var_year-$var_month-$var_day"; // join them together
            }
            //FOR Education Details
            $educationData = array(
                'master_degree' => $master_degree,
                'master_passout' => $master_passout,
                'master_spl' => $master_spl,
                'graduation' => $graduation,
                'graduation_passout' => $graduation_passout,
                'graduation_spl' => $graduation_spl,
                'graduation_intitutes' => $graduation_intitutes,
                'master_intitutes' => $master_intitutes,
                'inter' => $inter,
                'inter_passout' => $inter_passout,
                'inter_spl' => $inter_spl,
                'inter_intitutes' => $inter_intitutes
            );   
            $where = array(
                "emp_code"=>$emp_code,
            );
            $resultUpdate = $this->Adminmodel->updateRecordQueryList2('emp_education_detail',$educationData,$where);
            //For Family Details
            $familyData = array(
                'father_name' => $father_name,
                'mother_name' => $mother_name,
                'per_country' => $per_country,
                'per_state' => $per_state,
                'per_district' => $per_district,
                'per_city' => $per_city,
                'per_pincode' => $per_pincode,
                'per_address' => $per_address,
                'res_country' => $res_country,
                'res_state  ' => $res_state   ,
                'res_district' => $res_district,
                'res_city' => $res_city,
                'res_pincode' => $res_pincode,
                'res_address' => $res_address
            );  
            $where = array(
                "emp_code"=>$emp_code,
            );
            $resultUpdate = $this->Adminmodel->updateRecordQueryList2('emp_family_detail',$familyData,$where);
            //FOR Salary Details
            $salaryData = array(
                'grade_type' => $grade_type,
                'total_salary' => $total_salary,
                'take_home' => $take_home,
                'pf	' => $pf	,
                'pf_amount' => $pf_amount,
                'bank_name  ' => $bank_name   ,
                'account_number' => $account_number
            );   
            $where = array(
                "emp_code"=>$emp_code,
            );
            $resultUpdate = $this->Adminmodel->updateRecordQueryList2('emp_salary',$salaryData,$where);
            //FOR Work Details
            $workData = array(
                'designation' => $designation,
                'company_name' => $company_name,
                'emp_mode' => $emp_mode,
                'from_date	' => $from_date1,
                'to_date' => $to_date1
            );
            $where = array(
                "emp_code"=>$emp_code,
            );
            $resultUpdate = $this->Adminmodel->updateRecordQueryList2('emp_work_history',$workData,$where);
            $employeeData = array(
                'emp_code'=> $emp_code,
                'emp_fname'=>$emp_fname,
                'emp_lname'=>$emp_lname,
                'emp_mobile'=>$emp_mobile,
                'emp_email' => $emp_email,
                'emp_dob' => $emp_dob1,
                'emp_age' => $emp_age,
                'emp_marital_status' => $emp_marital_status,
                'emp_designation' => $emp_designation,
                'any_disability' => $any_disability,
                'disability_detail' => $disability_detail
            );
            $tableEmployee="emp_basic_detail";
            $result = $this->Adminmodel->updateRecordQueryList($tableEmployee,$employeeData,'emp_code',$emp_code);
            $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Employee Data Updated Successfully.</div>') ;
            $url='Employee/editEmployee/'.$emp_code;
            redirect($url);
        } else {
            $url='Employee/editEmployee/'.$emp_code;
            redirect($url);
        }
    }

    public function viewEmployee() {
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table="emp_basic_detail";
        $input  = json_decode(file_get_contents('php://input'), true);
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
        $config = array();
        $config['reuse_query_string'] = true;
        $config["base_url"] = base_url() . "Employee/viewEmployee";
        $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
        $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'emp_fname');//search
        $config["per_page"] = PERPAGE_LIMIT;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
        $limit =$config["per_page"];
        $start=$page;
        $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'emp_fname');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        if($result){
            foreach ($result as $key => $field) {
                if($field['emp_profile_img'] !=""){
                    $result[$key]['emp_profile_img'] = base_url()."uploads/employeeProfileImage/".$field['emp_profile_img'];  
                }else{
                    $result[$key]['emp_profile_img'] = "0";  
                }
            }
            $data['result'] = $result ;
        } else {
            $result = [];
            $data['result'] = $result ;
        }        
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_employee',$data);
    }

    public function employeeDetails() {
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller.
       {
         redirect('admin');
       }
       $input  = json_decode(file_get_contents('php://input'), true);
       $start=0;
       $perPage = 100;
       $emp_code = $this->uri->segment('3');
       //if($start!="" && $perPage!=""){
       $table="emp_basic_detail";
       if($emp_code !=""){
          @$column = "emp_code";
          @$value  = $emp_code;
       }
       $search ='';
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
       $result_basic = $this->Adminmodel->get_current_page_records($table,$perPage,$start,@$column,@$value,
       @$search,@$searchColumn);
       $result=replace_attr($result_basic);
       $result1 = $this->Adminmodel->get_current_page_records('emp_document_detail',$perPage,$start,@$column,@$value,@$search,@$searchColumn);
       $result_edc = $this->Adminmodel->get_current_page_records('emp_education_detail',$perPage,$start,@$column,@$value,@$search,@$searchColumn);
       $result2=replace_attr($result_edc);
       $result_fam = $this->Adminmodel->get_current_page_records('emp_family_detail',$perPage,$start,@$column,@$value,@$search,@$searchColumn);
       $result3=replace_attr($result_fam);
       $result_sal = $this->Adminmodel->get_current_page_records('emp_salary',$perPage,$start,@$column,@$value,@$search,@$searchColumn);
       $result4=replace_attr($result_sal);
       $result_work = $this->Adminmodel->get_current_page_records('emp_work_history',$perPage,$start,@$column,@$value,@$search,@$searchColumn);
	   $result5=replace_attr($result_work);
       if($result){
            foreach ($result as $key => $field) {
                if($field['emp_profile_img'] !=""){
                    $result[$key]['emp_profile_img'] = base_url()."uploads/employeeProfileImage/".$field['emp_profile_img'];
                } else {
                    $result[$key]['emp_profile_img'] = "0";
                }
            }
            if($result1) {
                foreach ($result1 as $key => $field1) {
                    $result1['imglist'] = $this->Adminmodel->getgallery($field['emp_code'],'emp_document_detail');
                }
            }
            if($result3) {
                foreach ($result3 as $key => $field3) {
                    $result3[$key]['per_country_name'] = $this->Adminmodel->getSingleColumnName($field3['per_country'],'id','country_name','keyaan_countries') ;
                    $result3[$key]['per_state_name'] = $this->Adminmodel->getSingleColumnName($field3['per_state'],'id','state_name','keyaan_states') ;
                    $result3[$key]['per_district_name'] = $this->Adminmodel->getSingleColumnName($field3['per_district'],'id','district_name','keyaan_districts') ;
                    $result3[$key]['per_city_name'] = $this->Adminmodel->getSingleColumnName($field3['per_city'],'id','city_name','keyaan_cities') ;
                    $result3[$key]['per_pincode_name'] = $this->Adminmodel->getSingleColumnName($field3['per_pincode'],'id','pincode','keyaan_pincodes') ;
                    $result3[$key]['per_address_name'] = $this->Adminmodel->getSingleColumnName($field3['per_address'],'id','location_name','keyaan_locations') ;
                    $result3[$key]['res_country_name'] = $this->Adminmodel->getSingleColumnName($field3['res_country'],'id','country_name','keyaan_countries') ;
                    $result3[$key]['res_state_name'] = $this->Adminmodel->getSingleColumnName($field3['res_state'],'id','state_name','keyaan_states') ;
                    $result3[$key]['res_district_name'] = $this->Adminmodel->getSingleColumnName($field3['res_district'],'id','district_name','keyaan_districts') ;
                    $result3[$key]['res_city_name'] = $this->Adminmodel->getSingleColumnName($field3['res_city'],'id','city_name','keyaan_cities') ;
                    $result3[$key]['res_pincode_name'] = $this->Adminmodel->getSingleColumnName($field3['res_pincode'],'id','pincode','keyaan_pincodes') ;
                    $result3[$key]['res_address_name'] = $this->Adminmodel->getSingleColumnName($field3['res_address'],'id','location_name','keyaan_locations') ;
                }
            }
            $data['result'] = $result ;
            $data['result1'] = $result1 ;
            $data['result2'] = $result2 ;
            $data['result3'] = $result3 ;
            $data['result4'] = $result4 ;
            $data['result5'] = $result5 ;
            $this->load->view('admin/employee_details',$data);
       }
       else{
          $url='viewEmployee';
          redirect($url);
       }
    }
    function employeeEnable($emp_code) {
        $emp_code=$emp_code;
        $employeeData =array(
            'keyaan_status' =>'0'
        );
        $result = $this->Adminmodel->updateRecordQueryList("emp_basic_detail",$employeeData,'emp_code',$emp_code);
        $result = $this->Adminmodel->updateRecordQueryList("emp_document_detail",$employeeData,'emp_code',$emp_code);
        $result = $this->Adminmodel->updateRecordQueryList("emp_education_detail",$employeeData,'emp_code',$emp_code);
        $url='viewEmployee';
        redirect($url);
    }      
    function employeeDisable($emp_code) {
        $emp_code=$emp_code;
        $employeeData =array(
            'keyaan_status' =>'1'
        );
        $result = $this->Adminmodel->updateRecordQueryList("emp_basic_detail",$employeeData,'emp_code',$emp_code);
        $result = $this->Adminmodel->updateRecordQueryList("emp_document_detail",$employeeData,'emp_code',$emp_code);
        $result = $this->Adminmodel->updateRecordQueryList("emp_education_detail",$employeeData,'emp_code',$emp_code);
        $url='viewEmployee';
        redirect($url);
    }  
    public function deleteImage(){
        $id           =$this->input->post('id');
        $table        = $this->input->post('table');
        $folder       = $this->input->post('folder');
        $selectColumn = $this->input->post('selectColumn');
        $result       = $this->Adminmodel->delImage($id,$table,$folder,$selectColumn);
        $data['result'] =1;
        $this->load->view('admin/deleteImgAjax',$data);



    }
    function deleteEmployee($emp_code){

      
        $result = $this->Adminmodel->delmultipleImage($emp_code,'emp_basic_detail','employeeProfileImage','empResumes','','','emp_profile_img','resume','','','emp_code');
        $result1 = $this->Adminmodel->delmultipleImagefile($emp_code,'emp_document_detail','employeeDocuments','','doc_img','doc_type','emp_code');
        $result3 = $this->Adminmodel->delmultipletables($emp_code,'emp_education_detail','emp_code');
        $result4 = $this->Adminmodel->delmultipletables($emp_code,'emp_family_detail','emp_code');
        $result5 = $this->Adminmodel->delmultipletables($emp_code,'emp_salary','emp_code');
        $result6 = $this->Adminmodel->delmultipletables($emp_code,'emp_work_history','emp_code');
        redirect($_SERVER['HTTP_REFERER']);
        }

}
?>