<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendorproductprice extends CI_Controller {
    function __construct() {
        parent::__construct();
         $this->load->helper("encryptionpwd");
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('form_validation');
        $this->load->library('upload'); 
    }
    public function index() {
        self::viewVendorproductprice();
    } 
    public function viewVendorproductprice(){
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $table ="vendorproduct_weights_prices";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Vendorproductprice/viewVendorproductprice";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'product_id');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="mdi mdi-skip-backward"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="mdi mdi-skip-forward"></i>'; 
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'product_id');
       if($result) {
            foreach ($result as $key => $field) {
                $result[$key]['product'] = $this->Adminmodel->getSingleColumnName($field['product_id'],'id','product_name','vendor_products') ;
                $result[$key]['proweight'] = $this->Adminmodel->getSingleColumnName($field['product_weight_id'],'id','product_weight','prodcut_weights') ;
            }
        }
        if($result){
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";  
        $this->load->view('vendor/view_vendorproductprice',$data);
    }  

    public function addVendorproductprice(){
       if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $resultProduct = $this->Adminmodel->getAjaxdataCountry('vendor_products');
        $dataBefore['resultProduct'] = $resultProduct;
        $vendor = $this->session->userdata('vendorCode');
        $product_price = $this->input->post('product_price');
        if($product_price!=''){    
            $check_data = array(
                "product_id" => $this->input->post('product_id'),
                "product_weight_id" => $this->input->post('product_weight_id')
            );
            $tablename = "vendorproduct_weights_prices";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Vendor product Price already exist</div>') ;
                $this->load->view('vendor/add_vendorproductprice',$dataBefore);
            }else{
                $added_by = $vendor!='' ? $vendor:'vendor';
                $date     = date("Y-m-d H:i:s");
                $product_id = $this->input->post('product_id')=="" ? "":$this->input->post('product_id');
                $product_sub_cat_id = $this->Adminmodel->getSingleColumnName($product_id,'id','sub_category_id','vendor_products');
                $product_weight_id = $this->input->post('product_weight_id')=="" ? "":$this->input->post('product_weight_id');    
                $data = array(
                    'product_id'=> $product_id ,  
                    'product_sub_cat_id' =>  $product_sub_cat_id,
                    'product_weight_id' =>  $product_weight_id,
                    'product_price'   =>  $product_price,
                    'vendor_code'=>   $vendor,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="vendorproduct_weights_prices";
                $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Vendor product Price Inserted</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Vendor product Price not inserted</div>') ;
                }
                $this->load->view('vendor/add_vendorproductprice',$dataBefore);
            }
        }else {
            $this->load->view('vendor/add_vendorproductprice',$dataBefore);    
        }       
    }
    
    public function editVendorproductprice(){
       if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $id = $this->uri->segment('3');
        $resultProduct = $this->Adminmodel->getAjaxdataCountry('vendor_products');
        $data['resultProduct'] = $resultProduct;
        if($id==''){
            redirect('vendorLogin');
        }
        $tablename = "vendorproduct_weights_prices";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        if($result) {
            foreach ($result as $key => $field) {
                $result[$key]['product'] = $this->Adminmodel->getSingleColumnName($field['product_id'],'id','product_name','vendor_products') ;
                $result[$key]['proweight'] = $this->Adminmodel->getSingleColumnName($field['product_weight_id'],'id','product_weight','prodcut_weights') ;
            }
            $data['result'] = $result[0] ;
            $this->load->view('vendor/edit_vendorproductprice',$data);
        } else {
            $url='viewVendorproductprice';
            redirect($url);
        }
    }
    public function updateVendorproductprice(){
       if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('vendorLogin');
        }
        $product_price = $this->input->post('product_price');
        if($product_price!=''){            
            $check_data = array(
                "product_id" => $this->input->post('product_id'),
                "product_weight_id" => $this->input->post('product_weight_id'),
                "id !=" => $this->input->post('id')
            );
            $tablename = "vendorproduct_weights_prices";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Vendor product Price already exist</div>');
            }else{
                $vendor = $this->session->userdata('vendorCode');
                $added_by = $vendor!='' ? $vendor:'vendor';           
                $date     = date("Y-m-d H:i:s");
                $product_id = $this->input->post('product_id')=="" ? "":$this->input->post('product_id');
                $product_sub_cat_id = $this->Adminmodel->getSingleColumnName($product_id,'id','sub_category_id','vendor_products');
                $product_weight_id = $this->input->post('product_weight_id')=="" ? "":$this->input->post('product_weight_id');    
                $data = array(
                    'product_id'=> $product_id ,  
                    'product_sub_cat_id' =>  $product_sub_cat_id,
                    'product_weight_id' =>  $product_weight_id,
                    'product_price'   =>  $product_price,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="vendorproduct_weights_prices";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">vendor product Price Updated.</div>');
                }else{
                     $url='Vendorproductprice/editVendorproductprice/'.$id;
                     redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Vendor product Price not updated.</div>') ;
                }   
            } 
            $url='Vendorproductprice/editVendorproductprice/'.$id;
            redirect($url);
        }else {   
            $url='Vendorproductprice/editVendorproductprice/'.$id;
            redirect($url); 
        }
    }
    function vendorproductpriceEnable($id) {
        $id=$id;
        $dataproweight =array(
            'keyaan_status' =>'0'
        );
        $table="vendorproduct_weights_prices";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataproweight,'id',$id);
        $url='viewVendorproductprice';
            redirect($url);
    }      
    function vendorproductpriceDisable($id) {
        $id=$id;
        $dataproweight =array(
            'keyaan_status' =>'1'
        );
        $table="vendorproduct_weights_prices";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataproweight,'id',$id);
        $url='viewVendorproductprice';
        redirect($url);
    }
    public function proweight(){
        $id =$this->input->post('id');
        $subcategoryId = $this->Adminmodel->getSingleColumnName($id,'id','sub_category_id','vendor_products');
        $result = $this->Adminmodel->getAjaxdata('subcategory_id',$subcategoryId,'prodcut_weights');
        $data['resultproweight'] =$result;
        $this->load->view('admin/proweightAjax',$data);
    }
    function deletevendorroductprice($id) {
        $id=$id;
        $result = $this->Adminmodel->delRow($id,'vendorproduct_weights_prices');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>