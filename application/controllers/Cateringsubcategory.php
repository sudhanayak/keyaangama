<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cateringsubcategory extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('form_validation');
        $this->load->library('upload'); 
    }
    public function index() {
        self::viewCateringsubcategory();
    } 

    public function viewCateringsubcategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="catering_subcategory";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Categorysubcategory/viewCategorysubcategory";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'subcategory_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="mdi mdi-skip-backward"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="mdi mdi-skip-forward"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'subcategory_name');
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
       if($result) {
            foreach ($result as $key => $field) {
                $result[$key]['mastercategory'] = $this->Adminmodel->getSingleColumnName($field['master_category_id'],'id','master_category_name','master_cateringcategory') ;
                $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','catering_category') ;
            }
        }
        if($result){
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";  
        $this->load->view('admin/view_cateringsubcategory',$data);
    }  

    public function addCateringsubcategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $resultCategory = $this->Adminmodel->getAjaxdataCountry('master_cateringcategory');
        $dataBefore['resultCat'] = $resultCategory;
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings; 
        $subcategory_name = $this->input->post('subcategory_name');       
        if($subcategory_name!=''){            
            $check_data = array(
            "subcategory_name" => $this->input->post('subcategory_name')
            );
            $tablename = "catering_subcategory";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Catering subcategory already exist</div>');
                $this->load->view('admin/add_cateringsubcategory',$dataBefore);
            }else{
                if (isset($_FILES['subcat_webimage'])) {
                    $config_media['upload_path'] = './uploads/subcat_webimage';
                    $config_media['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('subcat_webimage'))
                    {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $subcat_webimage    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Catering subcategory webimage uploads</div>') ;
                        redirect('addCategorysubcategory');
                    }        
                } else {
                    $subcat_webimage    = "";
                }
                if (isset($_FILES['subcat_appimage'])) {
                    $config_media1['upload_path'] = './uploads/subcat_appimage';
                    $config_media1['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media1['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media1);
                    $error = [];
                    if ( ! $this->upload->do_upload('subcat_appimage'))
                    {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data1[] = array('upload_image' => $this->upload->data());
                    }       
                    $subcat_appimage    = $data1[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Catering subcategory uploads</div>') ;
                        redirect('addCategorysubcategory');
                    }        
                } else {
                    $subcat_appimage    = "";
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $master_category_id = $this->input->post('master_category_id')=="" ? "":$this->input->post('master_category_id');
                $category_id = $this->input->post('category_id')=="" ? "":$this->input->post('category_id');           
                $data = array(
                    'master_category_id'=> $master_category_id ,                    
                    'category_id'       =>  $category_id,
                    'subcategory_name'       =>  $subcategory_name,
                    'subcat_webimage'   =>  $subcat_webimage,
                    'subcat_appimage'   =>  $subcat_appimage,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="catering_subcategory";
                $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Catering subcategory Inserted</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Catering subcategory not inserted</div>') ;
                }
                $this->load->view('admin/add_cateringsubcategory',$dataBefore); 
            }
        }else {
            $this->load->view('admin/add_cateringsubcategory',$dataBefore);    
        }       
    }
    
    public function editCateringsubcategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        $resultCategory = $this->Adminmodel->getAjaxdataCountry('master_cateringcategory');
        $data['resultCat'] = $resultCategory;
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings; 
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "catering_subcategory";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        if($result) {
            foreach ($result as $key => $field) {
                $result[$key]['mastercategory'] = $this->Adminmodel->getSingleColumnName($field['master_category_id'],'id','master_category_name','master_cateringcategory') ;
                $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','catering_category') ;
            }
            $data['result'] = $result[0];
            $this->load->view('admin/edit_cateringsubcategory',$data);
        } else {
            $url='viewCateringsubcategory';
            redirect($url);
        }
    }
    public function updateCateringsubcategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $subcategory_name = $this->input->post('subcategory_name');       
        if($subcategory_name!=''){            
            $check_data = array(
                "subcategory_name" => $subcategory_name,
                "id !=" =>$id   
            );
            $tablename = "catering_subcategory";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Catering subcategory already exist</div>') ;
            }else{
                if($_FILES['subcat_webimage']['size'] > 0){
                    $config_media['upload_path'] = './uploads/subcat_webimage';
                    $config_media['allowed_types'] = 'gif|jpg|jpeg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need                 
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('subcat_webimage')){
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else{
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $subcat_webimage    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>');
                        redirect('editCateringsubcategory');
                    }        
                } else {
                    $subcat_webimage    = "";
                }
                if($_FILES['subcat_appimage']['size'] > 0){
                    $config_media1['upload_path'] = './uploads/subcat_appimage';
                    $config_media1['allowed_types'] = 'gif|jpg|jpeg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media1['max_size']   = '1000000000000000'; // whatever you need                 
                    $this->upload->initialize($config_media1);
                    $error = [];
                    if ( ! $this->upload->do_upload('subcat_appimage')){
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else{
                        $data1[] = array('upload_image' => $this->upload->data());
                    }       
                    $subcat_appimage    = $data1[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        redirect('editCateringsubcategory');
                    }        
                } else {
                    $subcat_appimage    = "";
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");                
                $master_category_id = $this->input->post('master_category_id')=="" ? "":$this->input->post('master_category_id');
                $category_id = $this->input->post('category_id')=="" ? "":$this->input->post('category_id');
                $data = array(
                    'master_category_id'=> $master_category_id ,
                    'category_id'=> $category_id ,                    
                    'subcategory_name'       =>  $subcategory_name,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                if($subcat_webimage!=""){
                    $imgArray =array('subcat_webimage'=> $subcat_webimage);
                    $data= array_merge($data,$imgArray);
                }
                if($subcat_appimage!=""){
                    $imgArray =array('subcat_appimage'=> $subcat_appimage);
                    $data= array_merge($data,$imgArray);
                }
                $table="catering_subcategory";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Catering subcategory Updated.</div>');
                }else{
                    $url='Cateringsubcategory/editCateringsubcategory/'.$id;
                    redirect($url); 
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Catering subcategory not updated.</div>') ;
                }   
            } 
            $url='Cateringsubcategory/editCateringsubcategory/'.$id;
            redirect($url); 
        }else {   
            $url='Cateringsubcategory/editCateringsubcategory/'.$id;
            redirect($url); 
        }
    }
    function cateringsubcatEnable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'0'
        );
        $table="catering_subcategory";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewCateringsubcategory';
            redirect($url);
    }      
    function cateringsubcatDisable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'1'
        );
        $table="catering_subcategory";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewCateringsubcategory';
        redirect($url);
    }
    public function cateringsubcatAjax(){
        $id =$this->input->post('id');
        $result = $this->Adminmodel->getAjaxdata('category_id',$id,'catering_subcategory');
        $data['resultSubcat'] =$result;
        $this->load->view('admin/cateringsubcatAjax',$data);
    }
    function deleteCateringsubcategory($id) {
        $id=$id;
        $result = $this->Adminmodel->delmultipleImage($id,'catering_subcategory','subcat_webimage','subcat_appimage','','','subcat_webimage','subcat_appimage','','','id');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>