<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Testimonials extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
    }
    public function index() {
         redirect('viewTestimonials');
        } 
         
    public function addTestimonials(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
        redirect('admin');
        }
        $name =$this->input->post('name');
        if($name!=''){            
        $check_data = array(
        "name" => $this->input->post['name']    
        );
        $tablename = "testimonials";
        $checkData = $this->Adminmodel->existData($check_data,$tablename);
        if($checkData > 0){
            $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Testimonials name already exist </div>') ;
        }else{
            $admin = $this->session->userdata('userCode');
            $added_by = $admin!='' ? $admin:'admin' ;
            $message = $this->input->post('message')=="" ? "":$this->input->post('message');
            $date     = date("Y-m-d H:i:s");
            $data = array(
                'name'           => $name,
                'message'        => $message,
                'created_by'     => $added_by ,
                'created_at'     => $date,
                'updated_at'     => $date,
                'updated_by'     => $added_by
            );
            $table="testimonials";
            $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Testimonials Inserted</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Testimonials not inserted</div>') ;
                }
                $this->load->view('admin/add_testimonials');   
            }
        }else{
            $this->load->view('admin/add_testimonials');
            }
        }
                
    public function viewTestimonials(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="testimonials";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Testimonials/viewTestimonials";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'name');
        if($result){
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_testimonials',$data);
    }
            
           
    public function editTestimonials(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "testimonials";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $data['result'] = $result ;
        if($result) {
           $this->load->view('admin/edit_testimonials',$data);
       } else {
           $url='viewTestimonials';
           redirect($url);
       }
            
        }
    public function updateTestimonials(){
       if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $name = $this->input->post('name');       
        if($name!=''){            
            $check_data = array(
                "name" => $name,
                "id !=" =>$id   
            );
            $tablename = "testimonials";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Testimonials name already exist</div>') ;
            }else{
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;
                $message = $this->input->post('message')=="" ? "":$this->input->post('message');
                $date     = date("Y-m-d H:i:s");
                $id =$this->input->post('id');
                $data = array(
                    'name'           => $name,
                    'message'        => $message,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="testimonials";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Testimonials Updated.</div>');
                }else{
                    $url='Testimonials/editTestimonials/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Testimonials not updated.</div>') ;
                }   
            } 
            $url='Testimonials/editTestimonials/'.$id;
            redirect($url);
        }else {   
            $url='Testimonials/editTestimonials/'.$id;
            redirect($url); 
        }
        
}
        function TestimonialsEnable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'keyaan_status' =>'0'
            );
            $table="testimonials";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Testimonials/viewTestimonials';
            redirect($url);
        }      
        function TestimonialsDisable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'keyaan_status' =>'1'
            );
            $table="testimonials";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Testimonials/viewTestimonials';
            redirect($url);
        }
}
?>
