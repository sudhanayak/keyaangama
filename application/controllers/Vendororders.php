<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Vendororders extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
    }
    public function index() {
         redirect('viewVendororders');
        } 

    public function viewVendororders(){
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $table ="orders_detail";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Vendororders/viewVendororders";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'order_id');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $vendor_code = $this->session->userdata('vendorCode');
       $result = $this->Adminmodel->getVendorOrders($table,$limit,$start,'vendor_id',$vendor_code);
        if($result){
            foreach ($result as $key => $field) {
                $result[$key]['user_name'] = $this->Adminmodel->getSingleColumnName($field['user_id'],'id','name','users');
                $result[$key]['ordersList'] = self::ordersList('users_orders',$field['order_id'],$field['user_id']);
            }
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('vendor/view_vendororders',$data);
    }     
    public function vendorOrderDetails(){
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $table ="users_orders";
        $order_id = $this->uri->segment('3');
        $user_id = $this->uri->segment('4');
        $result = $this->Adminmodel->orderDetails($table,$order_id,$user_id);
        foreach ($result as $key => $field) {
            $result[$key]['user_name'] = $this->Adminmodel->getSingleColumnName($field['user_id'],'id','name','users');
            if($field['payment_status'] == 0) {
                $data['payment_status'] = 'Not Paid';
            } elseif($field['payment_status'] == 1) {
                $data['payment_status'] = 'Paid';
            }
            if($field['payment_type'] == 0) {
                $data['payment_type'] = 'COD';
            } elseif($field['payment_type'] == 1) {
                $data['payment_type'] = 'Online';
            }
            if($field['advance_amount'] == 0 || $field['advance_amount'] == '') {
                $data['advance_amount'] = '--';
            } else {
                $data['advance_amount'] = $field['advance_amount'];
            }
        }
        $eventDetails = $this->Adminmodel->orderEventDetails('orders_detail',$order_id,$user_id);
        foreach ($eventDetails as $key => $field) {
            $eventDetails[$key]['itemName']= $this->Adminmodel->getSingleColumnName($field['item_id'],'id','theme_name','vendor_themelist') ;
        }
        $packageDetails = $this->Adminmodel->orderPackageDetails('orders_detail',$order_id,$user_id);
        foreach ($packageDetails as $key => $field) {
            $packageDetails[$key]['packageName']= $this->Adminmodel->getSingleColumnName($field['package_id'],'package_code','package_name','package') ;
            $packageDetails[$key]['itineraryName']= $this->Adminmodel->getSingleColumnName($field['itinerary_id'],'itinerary_code','itenerary_name','package_detail') ;
        }
        $billingDetails = $this->Adminmodel->orderDetails('users_billing_address',$order_id,$user_id);
        foreach ($billingDetails as $key => $field) {
            $billingDetails[$key]['country']= $this->Adminmodel->getSingleColumnName($field['billing_country'],'id','country_name','keyaan_countries') ;
            $billingDetails[$key]['state']= $this->Adminmodel->getSingleColumnName($field['billing_state'],'id','state_name','keyaan_states') ;
            $billingDetails[$key]['city']= $this->Adminmodel->getSingleColumnName($field['billing_city'],'id','city_name','keyaan_cities') ;
            $billingDetails[$key]['pincode']= $this->Adminmodel->getSingleColumnName($field['billing_pincode'],'id','pincode','keyaan_pincodes') ;
        }
        if($result && $eventDetails || $packageDetails && $billingDetails){
            $data['eventDetails'] = $eventDetails;
            $data['packageDetails'] = $packageDetails;
            $data['billingDetails'] = $billingDetails[0];
            $data['result'] = $result[0] ;
            $this->load->view('vendor/vendororder_details',$data);
        } else {
            redirect('viewVendororders');
        }
    }
    public function ordersList($table,$order_id,$user_id){
        $orders = $this->Adminmodel->orderDetails($table,$order_id,$user_id);
        if($orders) {
            foreach ($orders as $key => $field1) {
                if($field1['payment_status'] == 0) {
                    $orders[$key]['payment_status'] = 'Not Paid';
                } elseif($field1['payment_status'] == 1) {
                    $orders[$key]['payment_status'] = 'Paid';
                }
                if($field1['payment_type'] == 0) {
                    $orders[$key]['payment_type'] = 'COD';
                } elseif($field1['payment_type'] == 1) {
                    $orders[$key]['payment_type'] = 'Online';
                }
                if($field1['advance_amount'] == 0 || $field1['advance_amount'] == '') {
                    $orders[$key]['advance_amount'] = '--';
                } else {
                    $orders[$key]['advance_amount'] = $field1['advance_amount'];
                }
                $orders[$key]['total_amount'] = $field1['total_amount'];
            }
            return $orders;
        } else {
            $orders = [];
            return false;
        }
    }
}
?>
