<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resumes extends CI_Controller {
    function __construct() {
        parent::__construct();
         $this->load->helper("encryptionpwd");
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('form_validation');
    }
    public function index() {
        redirect('viewResumes');
    }
    public function viewResumes() {
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="keyaan_resumes";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
        $config = array();
        $config['reuse_query_string'] = true;
        $config["base_url"] = base_url() . "Resumes/viewResumes";
        $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
        $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'name');//search
        $config["per_page"] = PERPAGE_LIMIT;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
        $limit =$config["per_page"];
        $start=$page;
        $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'name');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
            if($result){
                $data['result'] = $result ;
            } else {
                $result[] = [] ;
                $data['result'] = $result ;
            }
            $data['searchVal'] = $search !='null'?$search:"";
            $this->load->view('admin/view_resumes',$data);
    }

    public function addResumes() {
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;
        $name = $this->input->post('name');
        $this->load->library('upload');
        if($name!=''){
            if($_FILES['resume']['size'] > 0) {
                $config_media['upload_path'] = './uploads/resumes';
                $config_media['allowed_types'] = 'pdf|doc|docx';     
                $config_media['max_size']   = '1000000000000000'; // whatever you need
                // $this->load->library($config_media);
                $this->upload->initialize($config_media);
                $error = [];
                if ( ! $this->upload->do_upload('resume'))
                {
                    $error[] = array('error_image' => $this->upload->display_errors()); 
                }
                else
                {
                    $data[] = array('upload_image' => $this->upload->data());
                }     
                $resume = $data[0]['upload_image']['file_name'];
                if(count($error) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Resume uploads</div>') ;
                    redirect('addResumes');
                    //echo"resume error";
                }        
            }else{
                $resume = "";
            }
            $admin = $this->session->userdata('userCode');
            $added_by = $admin!='' ? $admin:'admin' ;           
            $date     = date("Y-m-d H:i:s");
            $email = $this->input->post('email')=="" ? "":$this->input->post('email');
            $mobile_number = $this->input->post('mobile_number') =="" ? "":$this->input->post('mobile_number');
            $resume = $resume;
            $experience_years = $this->input->post('experience_years') =="" ? "":$this->input->post('experience_years');
            $experience_months = $this->input->post('experience_months') =="" ? "":$this->input->post('experience_months');
            $ctc = $this->input->post('ctc') =="" ? "":$this->input->post('ctc');
            $ectc = $this->input->post('ectc') =="" ? "":$this->input->post('ectc');
            $notice_period = $this->input->post('notice_period') =="" ? "":$this->input->post('notice_period');
            $data = array(
                'name'=> $name ,
                'email'=> $email ,
                'mobile_number'  => $mobile_number,
                'experience_years'=>$experience_years,
                'experience_months'=>$experience_months,
                'ctc'=>$ctc,
                'ectc'=>$ectc,
                'notice_period'=>$notice_period,
                'resume'=>$resume,
                'created_by'     => $added_by ,
                'created_at'     => $date,
                'updated_at'     => $date,
                'updated_by'     => $added_by
            );
            $table="keyaan_resumes";
            $result = $this->Adminmodel->insertRecordQueryList($table,$data);
            if($result){
                $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Resumes Inserted</div>');
            } else {
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Resumes not inserted</div>') ;
            }
            $this->load->view('admin/add_resumes',$dataBefore);
        } else {
            //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
            $this->load->view('admin/add_resumes',$dataBefore);    
        }
    }

    public function editResumes() {
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "keyaan_resumes";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $data['result'] = $result ;
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        if($result) {
            foreach ($result as $key => $field) {
                $this->load->view('admin/edit_resumes',$data);
            }
        } else {
            $url='viewResumes';
            redirect($url);
        }
    }
    
    public function updateResumes() {
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $name = $this->input->post('name');
        $this->load->library('upload');      
        if($name!=''){
            if($_FILES['resume']['size'] > 0) {
                $config_media['upload_path'] = './uploads/resumes';
                $config_media['allowed_types'] = 'pdf|doc|docx';    
                $config_media['max_size']   = '1000000000000000'; // whatever you need
                // $this->load->library($config_media);
                $this->upload->initialize($config_media);
                $error = [];
                if ( ! $this->upload->do_upload('resume')) {
                    $error[] = array('error_image' => $this->upload->display_errors()); 
                }
                else {
                    $data[] = array('upload_image' => $this->upload->data());
                }       
                $resume = $data[0]['upload_image']['file_name'];
                $imgArr = array(
                    "resume"=>$resume ,
                    "updated_by"=>$added_by,
                    "updated_at"=>$date  
                );
                $resultUpdate = $this->Adminmodel->updateRecordQueryList('keyaan_resumes',$imgArr,'id',$id);
                if(count($error) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in logo uploads</div>') ;
                    $url='Vendor/editVendor/'.$id;
                    redirect($url);
                }        
            }else{
                $resume = "";
            }
            $admin = $this->session->userdata('userCode');
            $added_by = $admin!='' ? $admin:'admin' ;           
            $date     = date("Y-m-d H:i:s");
            $email = $this->input->post('email')=="" ? "":$this->input->post('email');
            $mobile_number = $this->input->post('mobile_number') =="" ? "":$this->input->post('mobile_number');
            $experience_years = $this->input->post('experience_years') =="" ? "":$this->input->post('experience_years');
            $experience_months = $this->input->post('experience_months') =="" ? "":$this->input->post('experience_months');
            $ctc = $this->input->post('ctc') =="" ? "":$this->input->post('ctc');
            $ectc = $this->input->post('ectc') =="" ? "":$this->input->post('ectc');
            $notice_period = $this->input->post('notice_period') =="" ? "":$this->input->post('notice_period');
            $id =$this->input->post('id');
            $data = array(
                'name'=> $name ,
                'email'=> $email ,
                'mobile_number'  => $mobile_number,
                'experience_years'=>$experience_years,
                'experience_months'=>$experience_months,
                'ctc'=>$ctc,
                'ectc'=>$ectc,
                'notice_period'=>$notice_period,
                'created_by'     => $added_by ,
                'created_at'     => $date,
                'updated_at'     => $date,
                'updated_by'     => $added_by
            );
            $table=" keyaan_resumes";
            $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
            if($result){
                $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Resume Updated.</div>');
            }else{
                $url='Resumes/editResumes/'.$id;
                redirect($url);
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Resumes not updated.</div>') ;
            }
            $url='Resumes/editResumes/'.$id;
            redirect($url);
        }else {   
            $url='Resumes/editResumes/'.$id;
            redirect($url); 
        }
    }
    // public function careerDetails() {
    //     if(!is_logged_in())  // if you add in constructor no need write each function in above controller.
    //    {
    //      redirect('admin');
    //    }
    //    $input  = json_decode(file_get_contents('php://input'), true);
    //    $start=0;
    //    $perPage = 100;
    //    $careerId = $this->uri->segment('3');
    //    //if($start!="" && $perPage!=""){
    //    $table="keyaan_resumes";
    //    if($careerId !=""){
    //       @$column = "id";
    //       @$value  = $careerId;
    //    }
    //    $search ='';
      
    //    $result1 = $this->Adminmodel->get_current_page_records($table,$perPage,$start,@$column,@$value,@$search,@$searchColumn);
    //    $result=replace_attr($result1);
    //    if($result){
    //         foreach ($result as $key => $field) {
    //         }
    //         $data['result'] = $result ;
    //         $this->load->view('admin/career_details',$data);
    //     } else {
    //         $url='careerDetails';
    //         redirect($url);
    //     }
    // }
    function ResumesEnable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'0'
        );
        $table="keyaan_resumes";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewResumes';
        redirect($url);
    }      
    function ResumesDisable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'1'
        );
        $table="keyaan_resumes";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewResumes';
        redirect($url);
    }
    function deleteResume($id) {
        $id=$id;
        $result = $this->Adminmodel->delImage($id,'keyaan_resumes','resumes','resume');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>