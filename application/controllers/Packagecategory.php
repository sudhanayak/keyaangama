<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packagecategory extends CI_Controller {
    function __construct() {
        parent::__construct();
         $this->load->helper("encryptionpwd");
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('form_validation');
    }
    public function index() {
         redirect('viewPackagecategory');
        } 

    public function viewPackagecategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="package_category";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Packagecategory/viewPackagecategory";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'category_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'category_name');
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
        if($result){
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_packagecategory',$data);
    }  

    public function addPackagecategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;
        $category_name = $this->input->post('category_name');       
        if($category_name!=''){            
            $check_data = array(
            "category_name" => $this->input->post('category_name')    
            );
            $tablename = "package_category";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Category name already exist</div>');
                $this->load->view('admin/add_packagecategory',$dataBefore); 
            }else{
                if (isset($_FILES['image'])) {
                    $config_media['upload_path'] = './uploads/packagecategory';
                    $config_media['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                    $this->load->library('upload',$config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('image'))
                    {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $image    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        redirect('addPackagecategory');
                    }        
                } else {
                    $image    = "";
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $base_price = $this->input->post('base_price')=="" ? "":$this->input->post('base_price');
                $details = $this->input->post('details')=="" ? "":$this->input->post('details');
                $category_type = $this->input->post('category_type')=="" ? "":$this->input->post('category_type');                        
                $data = array(
                    'category_name'=> $category_name ,
                    'category_type'  =>$category_type,
                    'base_price'       =>  $base_price,
                    'details'   =>  $details,
                    'image'        => $image,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="package_category";
                $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Package Category Inserted</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Package Category not inserted</div>') ;
                }
                $this->load->view('admin/add_packagecategory',$dataBefore);   
            }
        }else {
            //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
            $this->load->view('admin/add_packagecategory',$dataBefore);    
        }       
    }
    
    public function editPackagecategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "package_category";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $data['result'] = $result[0];
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        if($result) {
            $this->load->view('admin/edit_packagecategory',$data);
        } else {
            $url='viewPackagecategory';
            redirect($url);
        }
        
    }
    public function updatePackagecategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $category_name = $this->input->post('category_name');       
        if($category_name!=''){            
            $check_data = array(
                "category_name" => $category_name,
                "id !=" =>$id   
            );
            $tablename = "package_category";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Package Category already exist</div>');
            }else{
                if($_FILES['image']['size'] > 0){
                    $config_media['upload_path'] = './uploads/packagecategory';
                    $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                    $this->load->library('upload',$config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('image')){
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else{
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $image    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                                 redirect('editPackagecategory');
                    }        
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $base_price = $this->input->post('base_price')=="" ? "":$this->input->post('base_price');
                $details = $this->input->post('details')=="" ? "":$this->input->post('details');
                $category_type = $this->input->post('category_type')=="" ? "":$this->input->post('category_type');   
                $id =$this->input->post('id');
                $data = array(
                    'category_name'=> $category_name ,
                    'category_type'  =>$category_type,
                    'base_price'       =>  $base_price,
                    'details'   =>  $details,                            
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                if($image!=""){
                    $imgArray =array('image'=> $image);
                    $data= array_merge($data,$imgArray);
                }
                $table="package_category";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Package Category Updated.</div>');
                }else{
                    $url='Packagecategory/editPackagecategory/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Package Category not updated.</div>') ;
                }   
            } 
            $url='Packagecategory/editPackagecategory/'.$id;
            redirect($url);
        }else {   
            $url='Packagecategory/editPackagecategory/'.$id;
            redirect($url); 
        }
    }
    function packagecategoryEnable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'0'
        );
        $table="package_category";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewPackagecategory';
            redirect($url);
    }      
    function packagecategoryDisable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'1'
        );
        $table="package_category";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewPackagecategory';
        redirect($url);
    }
    function deletePackagecategory($id) {
        $id=$id;
        $result = $this->Adminmodel->delImage($id,'package_category','packagecategory','image');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }

}
?>