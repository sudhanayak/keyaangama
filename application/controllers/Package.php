<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Package extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('form_validation');
        $this->load->library('upload'); 
        $this->load->library('image_lib');
    }
    public function index() {
         self::viewPackage();
        } 
    // add Package
    public function addPackage(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $min='1254';
        $max='8659';
        $packageCode =rand($min,$max);
        // to fetch the data from category and itinerary drop down
        $table2 ="package_category";
        $start =0;
        $limit =100;
        $search ='';
        $dataBefore['category']= $this->Adminmodel->get_current_page_records($table2,$limit,$start,$column=null,$value=null,@$search,@$searchColumn);
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;
        $package_name   = $this->input->post('package_name');
        if($package_name!=''){
            $check_data = array(
            "package_name" => $this->input->post('package_name')    
            );
            $tablename = "package";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Package already exist</div>');
                 $this->load->view('admin/add_package',$dataBefore);
            }else{
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                // for Multiple Package Images 
                $imgage=count($_FILES['image']['name']);   
                if($imgage !=''){                               
                    $i=0; 
                    $filesCount = count($_FILES['image']['name']);
                    for($i = 0; $i < $filesCount; $i++){
                        $_FILES['file']['name']     = $_FILES['image']['name'][$i];
                        $_FILES['file']['type']     = $_FILES['image']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['image']['tmp_name'][$i];
                        $_FILES['file']['error']     = $_FILES['image']['error'][$i];
                        $_FILES['file']['size']     = $_FILES['image']['size'][$i];
                        // File upload configuration
                        $uploadPath = './uploads/packageimg/';
                        $config9['upload_path'] = $uploadPath;
                        $config9['allowed_types'] = 'jpg|jpeg|png|gif';
                        // Load and initialize upload library
                        $this->upload->initialize($config9);
                        // Upload file to server
                        if($this->upload->do_upload('file')){
                            // Uploaded file data
                            $fileData = $this->upload->data();
                            $uploadData[$i]['file_name'] = $fileData['file_name'];
                            $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                            $source_path = './uploads/packageimg/' .$uploadData[$i]['file_name'];
                            $target_path = './uploads/thumbnail/packageimg';
                            $this->resizeImage($source_path,$target_path);
                            $imgArr20 = array(
                                "image"=>$uploadData[$i]['file_name'],
                                "updated_by"=>$added_by,
                                "updated_date"=>$date,
                                "package_code"=>$packageCode,
                                "media_type"=>'1'
                            );                            
                            $resultUpdate = $this->Adminmodel->insertRecordQueryList('package_images',$imgArr20);
                        }
                    }
                }
                
                $number_of_days = $this->input->post('number_of_days')=="" ? "1":$this->input->post('number_of_days');
                $package_detail = $this->input->post('package_detail')=="" ? "":$this->input->post('package_detail');
                $price = $this->input->post('price')=="" ? "":$this->input->post('price');    
                $category_id = $this->input->post('category_id')=="" ? "":$this->input->post('category_id');
                $sub_category_id = $this->input->post('sub_category_id')=="" ? "":$this->input->post('sub_category_id');
                $dataCat = array(
                    'package_name'  => $package_name,
                    'package_code'=> $packageCode ,
                    'price'       =>  $price,
                    'number_of_days'  =>  $number_of_days,
                    'package_detail'   =>  $package_detail,
                    'image'         => '',
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by,
                    'category_id'    => $category_id,
                    'sub_category_id'    => $sub_category_id
                );
                $table="package";
                $result = $this->Adminmodel->insertRecordQueryList($table,$dataCat);
                $highlights = $this->input->post('highlights')=="" ? "":$this->input->post('highlights');
                 $highlightsCount = count($_POST['highlights']);
                for($i = 0; $i < $highlightsCount; $i++){
                    $data = array(
                            'package_code'=> $packageCode ,
                            'highlights'=> $highlights[$i]
                        );
                $result = $this->Adminmodel->insertRecordQueryList('package_highlights',$data);
                }
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Package Added</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Package not inserted</div>') ;
                }           
                $this->load->view('admin/add_package',$dataBefore);   
            }
        }else{
            $this->load->view('admin/add_package',$dataBefore);
        }
    }
    public function viewPackage(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="package";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Package/viewPackage";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'package_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'package_name');
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
        if($result){
            foreach ($result as $key => $field) {
                $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','package_category');
            } 
            
            $data['result'] = $result;
        } else {
            $result = [];
            $data['result'] = $result;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_package',$data);
    }
    public function editPackage(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "package";
        $tablename2 = "package_category";
        $start =0;
        $limit =100;
        $data['category']= $this->Adminmodel->get_current_page_records($tablename2,$limit,$start,$column=null,$value=null,@$search,@$searchColumn);
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        foreach ($result as $key => $field) {
           $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','package_category')  ;
           $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($field['sub_category_id'],'id','subcategory_name','package_subcategory')  ;
           $result[$key]['packageimage'] = 
           $this->Adminmodel->getpackageImages($field['package_code'],'package_images');
           $result[$key]['packagehighlights'] =
          $this->Adminmodel->getpackageHighlights($field['package_code'],'package_highlights');
        }
        $data['result'] = $result[0];
        if($result) {
            $this->load->view('admin/edit_package',$data);
        } else {
            $url='viewPackage';
            redirect($url);
        }
        
    }
    public function updatePackage(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $packageCode =$this->input->post('package_code');
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $package_name = $this->input->post('package_name');
        if($package_name!=''){            
            $check_data = array(
            "package_name" => $package_name,
            "id !=" =>$id   
            );   
            $tablename = "package";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
             $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Package already exist</div>') ;
            }else{
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;         
                $date     = date("Y-m-d H:i:s");
                $imgage=count($_FILES['image']['name']);   
                if($imgage !=''){                               
                    $i=0; 
                    $filesCount = count($_FILES['image']['name']);
                    for($i = 0; $i < $filesCount; $i++){
                        $_FILES['file']['name']     = $_FILES['image']['name'][$i];
                        $_FILES['file']['type']     = $_FILES['image']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['image']['tmp_name'][$i];
                        $_FILES['file']['error']     = $_FILES['image']['error'][$i];
                        $_FILES['file']['size']     = $_FILES['image']['size'][$i];
                        // File upload configuration
                        $uploadPath = './uploads/packageimg/';
                        $config9['upload_path'] = $uploadPath;
                        $config9['allowed_types'] = 'jpg|jpeg|png|gif';
                        // Load and initialize upload library
                        $this->upload->initialize($config9);
                        // Upload file to server
                        if($this->upload->do_upload('file')){
                            // Uploaded file data
                            $fileData = $this->upload->data();
                            $uploadData[$i]['file_name'] = $fileData['file_name'];
                            $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                            $source_path = './uploads/packageimg/' .$uploadData[$i]['file_name'];
                            $target_path = './uploads/thumbnail/packageimg';
                            $this->resizeImage($source_path,$target_path);
                            $imgArr20 = array(
                                "image"=>$uploadData[$i]['file_name'],
                                "updated_by"=>$added_by,
                                "updated_date"=>$date,
                                "package_code"=>$packageCode,
                                "media_type"=>'1'
                            );                            
                            $resultUpdate = $this->Adminmodel->insertRecordQueryList('package_images',$imgArr20);
                        }
                    }
                }
                $highlights = $this->input->post('highlights')=="" ? "":$this->input->post('highlights');
               $highlightsCount = count($_POST['highlights']);
               for($i = 0; $i < $highlightsCount; $i++){
                    $data = array(
                        'package_code'=> $packageCode ,
                        'highlights'=> $highlights[$i]
                    );
                $result = $this->Adminmodel->insertRecordQueryList('package_highlights',$data);
                   }
                $package_detail = $this->input->post('package_detail')=="" ? "":$this->input->post('package_detail');
                $price = $this->input->post('price')=="" ? "":$this->input->post('price');
                $category_id = $this->input->post('category_id')=="" ? "":$this->input->post('category_id');
                $sub_category_id = $this->input->post('sub_category_id')=="" ? "":$this->input->post('sub_category_id');
                $cat_type = $this->Adminmodel->getSingleColumnName($category_id,'id','category_type','package_category');
                if($cat_type == 0){
                $number_of_days = 1;
                }else{
                  $number_of_days = $this->input->post('number_of_days')=="" ? "1":$this->input->post('number_of_days');
                }
                $dataSubcat = array(
                 'itenerary_id' => $itenerary_id,
                 'package_name'=> $package_name ,
                 'package_code'=> $packageCode ,
                 'number_of_days'  =>$number_of_days,
                 'price'       =>  $price,
                 'package_detail'       =>  $package_detail,  
                 'updated_at'     => $date,
                 'updated_by'     => $added_by,
                 "category_id" => $category_id,
                 "sub_category_id" => $sub_category_id
                );
                if($image!=""){
                 $imgArray =array('image'=> $image);
                $dataSubcat= array_merge($dataSubcat,$imgArray);
                }
                $table="package";
                $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
                if($result){
                     $this->session->set_flashdata('msg','<div class="alert alert-success subcatupdate updateSuss">Package Updated</div>');
                } else{
                    $url='Package/editPackage/'.$id;
                     redirect($url);
                     $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps Some error</div>') ;
                }  
            } 
            $url='Package/editPackage/'.$id;
            redirect($url);
        } else {   
            $url='Package/editPackage/'.$id;
            redirect($url);    
        }
    }
     public function packageDetails(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $start=0;
        $perPage = 100;
        $catId = $this->uri->segment('3');
        $table ="package";
        if($catId !=""){
          @$column = "id";
          @$value  = $catId;
       }
       $search ='';
       $result1 = $this->Adminmodel->get_current_page_records($table,$perPage,$start,@$column,@$value,@$search,@$searchColumn);
       $result = replace_attr($result1);
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
        if($result){
            foreach ($result as $key => $field) {
                $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','package_category');
                $result[$key]['packageimage'] = 
                $this->Adminmodel->getpackageImages($field['package_code'],'package_images');
                $result[$key]['packagehighlights'] =
               $this->Adminmodel->getpackageHighlights($field['package_code'],'package_highlights');
            }
            $data['result'] = $result[0] ;
            $this->load->view('admin/package_details',$data);
        }
        else{
            $url='viewPackage';
            redirect($url);
        }
    }
    function packageEnable($id) {
        $id=$id;
        $dataSubcat =array(
           'keyaan_status' =>'0'
        );
        $table="package";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewPackage';
        redirect($url);
    }      
    function packageDisable($id) {
        $id=$id;
        $dataSubcat =array(
           'keyaan_status' =>'1'
        );
        $table="package";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewPackage';
        redirect($url);
    }
     public function packageAjax(){
        $id =$this->input->post('id');
        $result = $this->Adminmodel->getAjaxdata('category_id',$id,'package');
        $data['resultSubsubcat'] =$result;
        $this->load->view('admin/packageAjax',$data);
    } 
    public function deleteImage(){
        $id           =$this->input->post('id');
        $table        = $this->input->post('table');
        $folder       = $this->input->post('folder');
        $selectColumn = $this->input->post('selectColumn');
        $result       = $this->Adminmodel->delImage($id,$table,$folder,$selectColumn);
        $data['result'] =$result;
        $this->load->view('admin/deleteImgAjax',$data);
    }
    function deletePackage($package_code){
        $package_code=$package_code;
        $result1 = $this->Adminmodel->delmultipleImage($package_code,'package','packageimg','','','','image','','','','package_code');
        $result2 = $this->Adminmodel->delmultipleImagefile($package_code,'package_image_details','itineraryimg','','images','media_type','package_code');
        $result3 = $this->Adminmodel->delmultipletables($package_code,'package_detail','package_code');
        redirect($_SERVER['HTTP_REFERER']);
        }
        //For Thumnail Images
    public function resizeImage($source_path,$target_path)
    {
          $config_manip = array(
          'image_library' => 'gd2',
          'source_image' => $source_path,
          'new_image' => $target_path,
          'maintain_ratio' => TRUE,
          'create_thumb' => TRUE,
          'thumb_marker' => '',
          'width' => 150,
          'height' => 150
        );
        $this->image_lib->clear();
        $this->image_lib->initialize($config_manip);
        $this->image_lib->resize();
    }
    public function deletePackagehighlights(){
        $id           =$this->input->post('id');
        $table        = $this->input->post('table');
        $result       = $this->Adminmodel->delRow($id,$table);
        $data['result'] =$result;
    }
}
?>