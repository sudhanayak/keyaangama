<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Listing extends CI_Controller
{
	function __construct() {
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->model('Adminmodel');       
        }
	function index()
	{
        redirect('searchList');
		
	}
        public function themeListing(){
            if($this->session->userdata('userCCode') !=""){
            $userCode = $this->session->userdata('userCCode');
            $cartCount = $this->Adminmodel->getcartCount($userCode);
          }else{
            $cartCount=0;
          }
            $resultCategoryAll = $this->Adminmodel->getAjaxdataCountry('keyaan_category'); 
             $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities'); 
             if($this->session->userdata('userCCode') !=""){
                $userCode = $this->session->userdata('userCCode');
                $cartCount = $this->Adminmodel->getcartCount($userCode);
            }else{
                $cartCount=0;
            }
            $data['menuCategoryAll'] = self::getCategoryList('keyaan_category');
            $data['menuPackageAll'] = self::getCategoryPackList('package_category');
            $resultCategoryAll = $this->Adminmodel->getAjaxdataCountry('keyaan_category'); 
            $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities');
            $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
            $catid= $this->uri->segment(2);
            $cityid= $this->uri->segment(3);
            $priceBase= $this->uri->segment(4);
            if($catid!="" && $cityid=="" && $priceBase==""){
                $cityid=""; 
                $priceBase ="";
                $catid = $catid;
             }
             if($cityid!="" && $priceBase!=""){
                $url = "/".$cityid."/".$priceBase;
             }
             if($cityid!="" && $priceBase==""){
                $url = "/".$cityid;
             }
        $start=0;
        $perPage = 100;   
        $sorting = $this->input->post('sorting');    

      //if($start!="" && $perPage!=""){
          $table="keyaan_vendor";  
          $thmecount =$this->Adminmodel->themeListingCount($table,$perPage,$start,@$catid,@$cityid,
            @$priceBase,@$subcatId,@$rating_filter,@$sorting);        
          $result = $this->Adminmodel->themeListing($table,$perPage,$start,@$catid,@$cityid,
            @$priceBase,@$subcatid,@$rating_filter);  
           $data['category'] = $this->Adminmodel->getSingleColumnName($catid,'id',
          'category_name','keyaan_category') ;
          if($result > 0){
              foreach ($result as $key => $field) {                 
                            
                //For Cat Id
                $cat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','category_id','vendor_categories') ;
                $data['category'] = $this->Adminmodel->getSingleColumnName($cat_id,'id',
                'category_name','keyaan_category') ;
                $result[$key]['priceType']= $this->Adminmodel->getSingleColumnName($cat_id,'id',
                'price_selection','keyaan_category');
                //For Subcat Id
                $subcat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','sub_category_id','vendor_sub_categories') ;
                $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($subcat_id,'id','subcategory_name','keyaan_subcategory') ;               
                //For subsubcat Id
                $subsubcat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','sub_sub_category_id','vendor_sub_sub_categories') ;
                $result[$key]['subsubcategory'] = $this->Adminmodel->getSingleColumnName($subsubcat_id,'id','subsubcat_name','keyaan_subsubcategory') ;                    
                // get the state id from vendor 
                $state_id=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                'state_id','keyaan_vendor') ;
                $vendor_name=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                'name','keyaan_vendor') ;
                // get the city id from vendor 
                $city_id=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                'city_id','keyaan_vendor') ;
                $result[$key]['listingBy']=$vendor_name;
                $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($state_id,'id','state_name','keyaan_states') ;
                $result[$key]['city']  = $this->Adminmodel->getSingleColumnName($city_id,'id','city_name','keyaan_cities') ;
                if($field['basic_image'] != ''){
                    $result[$key]['image'] = base_url() . "uploads/basic_image/".$field['basic_image'];
                }
                if($field['logo'] != ''){
                    $result[$key]['logo'] = base_url() . "uploads/vendorlogo/".$field['logo'];
                }
                // $result[$key]['image'] = base_url() . "uploads/basic_image/".$this->Adminmodel->getThemeImg($field['vendor_code']);
            } 
            $resultSubcat=array();
            $resultSubcat = $this->Adminmodel->getSubCategoryList($catid);

             
            $data['resultSubcat'] =$resultSubcat;            
            $data['result'] = $result ;
            $data['categoryList']=$resultCategoryAll;
            $data['catid'] =$catid; //to dispaly category related vendors
            $data['cityList']=$resultCities;
            $data['basicsettingsList']=$resultBasicsettings;
            $data['cartCount']=$cartCount; 
            $data['catsegment'] =$this->uri->segment(2); 
            $data['citysegment'] =$this->uri->segment(3); 
            $data['pricesegment'] =$this->uri->segment(4);
            $data['themeCount'] = $thmecount;
            if($catid == 2) {
                redirect("venueListing/".$catid.$url);
            } elseif($catid == 3) {
                redirect("catering/".$catid.$url);
            } else {
                $this->load->view('user/listing',$data);
            }
          } else{
            $resultSubcat=array();
            $resultSubcat = $this->Adminmodel->getSubCategoryList($catid);
            $data['resultSubcat'] =$resultSubcat;
            $result =array();
            $data['result'] = $result;
            $data['categoryList']=$resultCategoryAll; 
            $data['catid'] =$catid; //to dispaly category related vendors
            $data['cityList']=$resultCities;
            $data['basicsettingsList']=$resultBasicsettings;
            $data['cartCount']=$cartCount; 
            $data['catsegment'] =$this->uri->segment(2); 
            $data['citysegment'] =$this->uri->segment(3); 
            $data['pricesegment'] =$this->uri->segment(4);
            $data['themeCount'] = $thmecount;
            if($catid == 2) {
                redirect("venueListing/".$catid.$url);
            } elseif($catid == 3) {
                redirect("catering/".$catid.$url);
            } else {
                $this->load->view('user/listing',$data);
            }
        }
    }

      public function filter(){

        $catid= $this->input->post('catgement');

        $cityid= $this->input->post('citygement');
        $subcatId =$this->input->post('subcatId');
        $price_filter = $this->input->post('price_filter');
        $rating_filter = $this->input->post('rating_filter');
        $themeCountLimit = $this->input->post('themeCountLimit');
        $sorting = $this->input->post('sorting');
        if($price_filter !=""){
          $priceBase= $price_filter;
        }
        else{
          $priceBase= $this->input->post('pricegement');
        }
        
        if($catid!="" && $cityid=="" && $priceBase==""){
            $cityid=""; 
            $priceBase ="";
            $catid = $catid;
        }    
        //$start=$themeCountLimit !=""?$themeCountLimit:1;
        //$perPage = 2;  
        $start=0;
        $perPage = 100;         
      //if($start!="" && $perPage!=""){
          $table="keyaan_vendor"; 
          $thmecount =$this->Adminmodel->themeListingCount($table,$perPage,$start,@$catid,@$cityid,
            @$priceBase,@$subcatId,@$rating_filter,@$sorting);  

          $result = $this->Adminmodel->themeListing($table,$perPage,$start,@$catid,@$cityid,
            @$priceBase,@$subcatId,$rating_filter); 
           $data['category'] = 
          $this->Adminmodel->getSingleColumnName($catid,'id','category_name','keyaan_category') ;

          if($result > 0){
              foreach ($result as $key => $field) {                                             
                //For Cat Id
                $cat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','category_id','vendor_categories') ;
                $data['category'] = $this->Adminmodel->getSingleColumnName($cat_id,'id',
                'category_name','keyaan_category') ;
                $result[$key]['priceType']= $this->Adminmodel->getSingleColumnName($cat_id,'id',
                'price_selection','keyaan_category');
                //For Subcat Id
                $subcat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','sub_category_id','vendor_sub_categories') ;
                $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($subcat_id,'id','subcategory_name','keyaan_subcategory') ;               
                //For subsubcat Id
                $subsubcat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','sub_sub_category_id','vendor_sub_sub_categories') ;
                $result[$key]['subsubcategory'] = $this->Adminmodel->getSingleColumnName($subsubcat_id,'id','subsubcat_name','keyaan_subsubcategory') ;                    
                // get the state id from vendor 
                $state_id=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                'state_id','keyaan_vendor') ;
                $vendor_name=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                'name','keyaan_vendor') ;
                // get the city id from vendor 
                $city_id=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                'city_id','keyaan_vendor') ;
                $result[$key]['listingBy']=$vendor_name;
                $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($state_id,'id','state_name','keyaan_states') ;
                $result[$key]['city']  = $this->Adminmodel->getSingleColumnName($city_id,'id','city_name','keyaan_cities') ;
                if($field['basic_image'] != ''){
                    $result[$key]['image'] = base_url() . "uploads/basic_image/".$field['basic_image'];
                }
                if($field['logo'] != ''){
                    $result[$key]['logo'] = base_url() . "uploads/vendorlogo/".$field['logo'];
                } 
            } 
            $resultSubcat=array();
            $resultSubcat = $this->Adminmodel->getSubCategoryList($catid);        
            $data['result'] = $result ;
            $data['themeCount'] = $thmecount;
            $this->load->view('user/listfilter_ajax',$data);
          }
          else{              
              $result =array();
              $data['themeCount'] = $thmecount;
              $data['result'] = $result;              
              $this->load->view('user/listfilter_ajax',$data);
          }




      
    }
      
    public function getCategoryList(){

        $result = $this->Adminmodel->getCategoryList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryList($field['id']);
             
        } 

        return $result ;   

    }
    public function getSubCategoryList($catId){
          
        $result = $this->Adminmodel->getSubCategoryList($catId);
        foreach ($result as $key => $field) {
             $result[$key]['subsubcatList'] = $this->Adminmodel->getSubsubCategoryList($field['id']) ;
        } 

        return $result ;   

    } 
    public function getCategoryPackList(){

        $result = $this->Adminmodel->getCategoryPackList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryPackList($field['id']); 
        } 
        return $result ;   
    }
    public function getSubCategoryPackList($catId){  
        $result = $this->Adminmodel->getSubCategoryPackList($catId);
        return $result ;   
    }
}
?>