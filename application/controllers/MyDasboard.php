<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MyDasboard extends CI_Controller
{
    function __construct() {
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->model('Adminmodel');       
        }
    function viewMyProfile()
    {   
        if($this->session->userdata('userCCode') !=""){
            $userCode = $this->session->userdata('userCCode');
            $cartCount = $this->Adminmodel->getcartCount($userCode);
        }else{
            $cartCount=0;
        }
        $data['cartCount']=$cartCount;
        $data['menuCategoryAll'] = self::getCategoryList('keyaan_category');
        $data['menuPackageAll'] = self::getCategoryPackList('package_category');
        $resultCategoryAll = $this->Adminmodel->getAjaxdataCountry('keyaan_category'); 
        $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings'); 
        $data['categoryList']=$resultCategoryAll;
        $data['cityList']=$resultCities;
        $data['basicsettingsList']=$resultBasicsettings;
        if(@$this->session->userdata(isCUserLoggedIn)) {
        $tablename ="users";
        $userId = $this->session->userdata('userCCode');
        $result = $this->Adminmodel->singleRecordData('id',$userId,$tablename);
        $data['result'] = $result ;
        $this->load->view('user/my_profile',$data);
        }
        else {
            $this->load->view('user/login',$data);
        }

    }

    function UpdateMyProfile()
    {
        if($this->session->userdata('userCCode') !=""){
            $userCode = $this->session->userdata('userCCode');
            $cartCount = $this->Adminmodel->getcartCount($userCode);
        }else{
            $cartCount=0;
        }
        $data['menuCategoryAll'] = self::getCategoryList('keyaan_category');
        $data['menuPackageAll'] = self::getCategoryPackList('package_category');
        $data['cartCount']=$cartCount;
        $resultCategoryAll = $this->Adminmodel->getAjaxdataCountry('keyaan_category'); 
        $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings'); 
        $data['categoryList']=$resultCategoryAll;
        $data['cityList']=$resultCities;
        $data['basicsettingsList']=$resultBasicsettings;
        if(@$this->session->userdata(isCUserLoggedIn)) {
        $tablename ="users";
        $userId = $this->session->userdata('userCCode');
        $result = $this->Adminmodel->singleRecordData('id',$userId,$tablename);
        $data['result'] = $result ;
        $this->load->view('user/update_profile',$data);
        }
        else {
            $this->load->view('user/login',$data);
        }

    }

    public function ChangeProfile(){
        if($this->session->userdata('userCCode') !=""){
            $userCode = $this->session->userdata('userCCode');
            $cartCount = $this->Adminmodel->getcartCount($userCode);
        }else{
            $cartCount=0;
        }
        $data['menuCategoryAll'] = self::getCategoryList('keyaan_category');
        $data['menuPackageAll'] = self::getCategoryPackList('package_category');
        $data['cartCount']=$cartCount;
        $resultCategoryAll = $this->Adminmodel->getAjaxdataCountry('keyaan_category'); 
        $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities'); 
        $data['categoryList']=$resultCategoryAll;
        $data['cityList']=$resultCities;
        $id =$this->input->post('id');
            $json_data = array();
                // insert otp and call otp screeen 
                // otp_type 1 means reg
                $data = array(
                    'name' =>$this->input->post('name'), 
                    'mobile' =>$this->input->post('mobile'),
                    'email' =>$this->input->post('email')
                );
                $table = 'users';
                $otpResult = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($otpResult){
                    $json_data['status']='TRUE';
                    $json_data['responseCode']=0;                    
                    $json_data['message']="sucess";
                }else{
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=10;                    
                    $json_data['message']="OPPS SOME DB ERROR when insert in otp table";
                }                                    
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data);
    }

    function ViewChangePassword()
    {
        if($this->session->userdata('userCCode') !=""){
            $userCode = $this->session->userdata('userCCode');
            $cartCount = $this->Adminmodel->getcartCount($userCode);
        }else{
            $cartCount=0;
        }
        $data['menuCategoryAll'] = self::getCategoryList('keyaan_category');
        $data['menuPackageAll'] = self::getCategoryPackList('package_category');
        $data['cartCount']=$cartCount;
        $resultCategoryAll = $this->Adminmodel->getAjaxdataCountry('keyaan_category'); 
        $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings'); 
        $data['categoryList']=$resultCategoryAll;
        $data['cityList']=$resultCities;
        $data['basicsettingsList']=$resultBasicsettings;
        if(@$this->session->userdata(isCUserLoggedIn)) {
        $tablename ="users";
        $userId = $this->session->userdata('userCCode');
        $result = $this->Adminmodel->singleRecordData('id',$userId,$tablename);
        $data['result'] = $result ;
        $this->load->view('user/change_password',$data);
        }
        else {
            $this->load->view('user/login',$data);
        }

    }

    public function ChangePassword(){
        if($this->session->userdata('userCCode') !=""){
            $userCode = $this->session->userdata('userCCode');
            $cartCount = $this->Adminmodel->getcartCount($userCode);
        }else{
            $cartCount=0;
        }
        $data['menuCategoryAll'] = self::getCategoryList('keyaan_category');
        $data['menuPackageAll'] = self::getCategoryPackList('package_category');
        $data['cartCount']=$cartCount;
        $resultCategoryAll = $this->Adminmodel->getAjaxdataCountry('keyaan_category'); 
        $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities'); 
        $data['categoryList']=$resultCategoryAll;
        $data['cityList']=$resultCities;
        $id =$this->input->post('id');
        $new_password =$this->input->post('new_password');
        if($this->input->post('id')){
            $where= array(
                    'password' =>$this->input->post('old_password')
                );  
                $table = 'users';
                $checkUser = $this->Adminmodel->existData1($where,'id',$id);
            $json_data = array();
            if($checkUser <=0 ){    
                //email exist           
                $json_data['status']='TRUE';
                $json_data['responseCode']=1;                    
                $json_data['message']="PLEASE Old password";                    
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            }
            elseif($checkUser > 0){
                $data = array(
                    'password' =>$this->input->post('new_password')
                );
                $table = 'users';
                $otpResult = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($otpResult){
                    $json_data['status']='TRUE';
                    $json_data['responseCode']=0;                    
                    $json_data['message']="sucess";
                }else{
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=10;                    
                    $json_data['message']="OPPS SOME DB ERROR when insert in otp table";
                }                                    
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            }
            else{                            
                $json_data['status']='FALSE';
                $json_data['responseCode']=10;                    
                $json_data['message']="some error in model "; 
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            }
        }else{
            $json_data['status']='FALSE';
            $json_data['responseCode']=100;                    
            $json_data['message']="please enter all the mandatory fields"; 
            $data['json_data']= $json_data;
            $this->load->view('user/ajax_all',$data); 
        }
    }
    function wishlist()
   {
       if($this->session->userdata('userCCode') !=""){
           $userCode = $this->session->userdata('userCCode');
           $cartCount = $this->Adminmodel->getcartCount($userCode);
       }else{
           $cartCount=0;
       }
       $data['cartCount']=$cartCount;
       $data['menuCategoryAll'] = self::getCategoryList('keyaan_category');
       $data['menuPackageAll'] = self::getCategoryPackList('package_category');
       $resultCategoryAll = $this->Adminmodel->getAjaxdataCountry('keyaan_category');
       $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities');
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['categoryList']=$resultCategoryAll;
       $data['cityList']=$resultCities;
       $data['basicsettingsList']=$resultBasicsettings;
       if(@$this->session->userdata(isCUserLoggedIn)) {
       $userId = $this->session->userdata('userCCode');
       $result = $this->Adminmodel->myFav($userId);
       $data['result'] = $result ;
       $this->load->view('user/wishlist',$data);
       }
       else {
           $this->load->view('user/login',$data);
       }

   }
    public function getCategoryList(){

        $result = $this->Adminmodel->getCategoryList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryList($field['id']);
             
        } 

        return $result ;   

    }
    public function getSubCategoryList($catId){
          
        $result = $this->Adminmodel->getSubCategoryList($catId);
        foreach ($result as $key => $field) {
             $result[$key]['subsubcatList'] = $this->Adminmodel->getSubsubCategoryList($field['id']) ;
        } 

        return $result ;   

    } 
    public function getCategoryPackList(){

        $result = $this->Adminmodel->getCategoryPackList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryPackList($field['id']); 
        } 
        return $result ;   
    }
    public function getSubCategoryPackList($catId){  
        $result = $this->Adminmodel->getSubCategoryPackList($catId);
        return $result ;   
    }
}
?>