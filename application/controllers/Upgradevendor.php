<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Upgradevendor extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
    }
    public function index() {
         redirect('addUpgradevendor');
        } 
         
    public function addUpgradevendor(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
        redirect('admin');
        }
        $dataBefore =[];
        $min='1252';
        $max='8468';
        $order_id =rand($min,$max);
        $vendorCode = $this->uri->segment('2');
        $dataBefore['vendorCode'] = $vendorCode;
        $resultPackage = $this->Adminmodel->getAjaxdataCountry('vendor_package');
        $dataBefore['resultPackage'] = $resultPackage;
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;
        $package_id =$this->input->post('package_id');
        $vendor_package = $this->Adminmodel->getSingleColumnName($vendorCode,'vendor_code','vendor_package','keyaan_vendor');
        if($package_id!=''){         
        $tablename = "vendor_order";
        $admin = $this->session->userdata('userCode');
        $added_by = $admin!='' ? $admin:'admin' ;           
        $date     = date("Y-m-d H:i:s");
        $amount = $this->input->post('amount') =="" ? "":$this->input->post('amount');
        $duration = $this->input->post('duration') =="" ? "":$this->input->post('duration');
        $payment_type = $this->input->post('payment_type') =="" ? "":$this->input->post('payment_type');
        $payment_status = $this->input->post('payment_status') =="" ? "1":$this->input->post('payment_status');
        if($payment_type == 1) {
            $cash_detail = $this->input->post('cash_detail') =="" ? "":$this->input->post('cash_detail');
            $cheque_number = '';
            $cheque_detail = '';
        } elseif($payment_type == 2) {
            $cash_detail = '';
            $cheque_number = $this->input->post('cheque_number') =="" ? "":$this->input->post('cheque_number');
            $cheque_detail = $this->input->post('cheque_detail') =="" ? "":$this->input->post('cheque_detail');
        }
        $order_date = date("Y-m-d H:i:s");
        $start_date = date("Y-m-d");
        $today_date = date("Y-m-d H:i:s");
        $end_date = date("Y-m-d", strtotime($start_date . " +".$duration."days"));
        if($end_date > $today_date && $package_id == $vendor_package){
            $expire_date = date("Y-m-d", strtotime($end_date . " +".$duration."days"));
        }else{
            $expire_date = date("Y-m-d", strtotime($today_date ." +".$duration."days"));
        }
        $dataCat = array(
            'end_date'   => $expire_date,
            'vendor_package'   => $package_id,
        );
        $where = array(
            'vendor_code'=> $vendorCode,
        );
        $resultUpdate = $this->Adminmodel->updateRecordQueryList2('keyaan_vendor',$dataCat,$where);
        if($expire_date > $today_date){
            $i = 1;
        }elseif($expire_date < $today_date ){
            $i = 0;
        }
        $dataCat = array(
            'vendor_status'  => $i,
            'vendor_type'   => $i,
        );
        $where = array(
            'vendor_code'=> $vendorCode,
        );
        $resultUpdate = $this->Adminmodel->updateRecordQueryList2('keyaan_vendor',$dataCat,$where);
        $data = array(
            'order_id'=> $order_id,
            'vendor_code'=> $vendorCode,
            'package_id'=> $package_id,
            'amount'        =>$amount,
            'duration'     =>$duration,
            'payment_type'    =>$payment_type,
            'payment_status'     =>$payment_status,
            'cash_detail'     =>$cash_detail,
            'cheque_number'     =>$cheque_number,
            'cheque_detail'     => $cheque_detail,
            'order_date'     => $order_date,
            'start_date'     => $start_date,
            'end_date'     => $end_date,
            'created_by'     => $added_by ,
            'created_at'     => $date,
            'updated_at'     => $date,
            'updated_by'     => $added_by
        );
        $table="vendor_order";
        $result = $this->Adminmodel->insertRecordQueryList($table,$data);
            if($result){
                $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Upgradevendor Inserted</div>');
            }
            else{
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Upgradevendor not inserted</div>') ;
            }
            $this->load->view('admin/add_upgradevendor',$dataBefore); 
        }else{
            $this->load->view('admin/add_upgradevendor',$dataBefore);
        }
    }

    public function viewUpgradevendor(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller.
        {
          redirect('admin');
        }
        $table ="vendor_order";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
        $config = array();
        $config['reuse_query_string'] = true;
        $config["base_url"] = base_url() . "Upgradevendor/viewUpgradevendor";
        $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
        $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'');//search
        $config["per_page"] = PERPAGE_LIMIT;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
        $limit =$config["per_page"];
        $start=$page;
        $vendorCode = $this->uri->segment('2');
        $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,'vendor_code',$vendorCode,$search,'');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        if($result){
            foreach ($result as $key => $field) {
            $result[$key]['package'] = $this->Adminmodel->getSingleColumnName($field['package_id'],'id','package_name','vendor_package');
            }
    
            $data['result'] = $result;
        } else {
            $result = [];
            $data['result'] = $result;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_upgradevendor',$data);
    }
    
    public function UpgradevendorDetails(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller.
        {
          redirect('admin');
        }
        $vendorCode = $this->uri->segment('2');
        $order_id = $this->uri->segment('3');
        $table ="vendor_order";
        $result1 = $this->Adminmodel->get_upgradevendor_details($table,$vendorCode,$order_id);
        $result=replace_attr($result1);
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        if($result){
          foreach ($result as $key => $field) {
                $result[$key]['package'] = $this->Adminmodel->getSingleColumnName($field['package_id'],'id','package_name','vendor_package')  ;
                $data['result'] = $result ;
            }
            $data['result'] = $result ;
            $this->load->view('admin/upgradevendor_details',$data);
        } else {
            $url='viewUpgradevendor';
            redirect($url);
        }
    }

    public function upgradevendorAjax(){
        $id =$this->input->post('id');
        $result['price'] = $this->Adminmodel->getSingleColumnName($id,'id','price','vendor_package');
        $result['duration'] = $this->Adminmodel->getSingleColumnName($id,'id','duration','vendor_package');
        $data['json_data'] =$result;
       $this->load->view('user/ajax_all',$data);  
    } 
       
}
?>
