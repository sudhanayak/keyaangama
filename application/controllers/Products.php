<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Products extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->library('upload');
    }
     public function index() {
        self::viewProducts();
      } 
    public function addProducts(){
          if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
          {
            redirect('admin');
          }
        $dataBefore =[];
        $product_name =$this->input->post('product_name');
        $resultCategory = $this->Adminmodel->getAjaxdataCountry('master_cateringcategory');
        $dataBefore['resultCat'] = $resultCategory;
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;  
        if(!empty($product_name)){
            $master_category_id = $this->input->post('master_category_id');
            $category_id = $this->input->post('category_id');
            $sub_category_id = $this->input->post('sub_category_id');
            $check_data = array(
            "master_category_id" => $master_category_id,
            "category_id" => $category_id,
            "sub_category_id" => $sub_category_id,
            "product_name" => $product_name
            );
            $tablename = "products";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger">Products already exist</div>') ;
            }else{
                if (isset($_FILES['product_image'])) {
                $config_media['upload_path'] = './uploads/product_image';
                $config_media['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                $config_media['max_size']   = '1000000000000000'; // whatever you need
                $this->upload->initialize($config_media);
                $error = [];
                if ( ! $this->upload->do_upload('product_image'))
                {
                    $error[] = array('error_image' => $this->upload->display_errors()); 
                }
                else
                {
                    $data[] = array('upload_image' => $this->upload->data());
                }       
                $product_image    = $data[0]['upload_image']['file_name'];
                if(count($error) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Product Image uploads</div>') ;
                    redirect('addProducts');
                }        
            } else {
                $product_image    = "";
            }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;    
                $dateCurrent= date("Y-m-d H:i:s");
                $master_category_id = $this->input->post('master_category_id') =="" ? "":$this->input->post('master_category_id');
                $category_id = $this->input->post('category_id') =="" ? "":$this->input->post('category_id');
                $sub_category_id = $this->input->post('sub_category_id') =="" ? "":$this->input->post('sub_category_id');
                $product_type = $this->input->post('product_type') =="" ? "":$this->input->post('product_type');
                $product_description = $this->input->post('product_description') =="" ? "":$this->input->post('product_description');
                $dataCity = array(
                    'master_category_id'=> $master_category_id,
                    'category_id'=> $category_id,
                    'sub_category_id'=> $sub_category_id,
                    'product_name'=> $product_name,
                    'product_description'=> $product_description,
                    'product_type'=> $product_type,
                    'product_image'=>$product_image,
                    'created_by'     => $added_by ,
                    'created_at'     => $dateCurrent,
                    'updated_at'     => $dateCurrent,
                    'updated_by'     => $added_by
                );
                $tableCity="products";
                $result = $this->Adminmodel->insertRecordQueryList($tableCity,$dataCity);
                if($result) {
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Products Added Successfully</div>') ;
                } else {
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Products not Added</div>') ;
                }
                $this->load->view('admin/add_products',$dataBefore);
            } 
        }
        else{
                /*$this->session->set_flashdata('msg','<div class="alert alert-danger">fail</div>') ;*/
                $this->load->view('admin/add_products',$dataBefore);   
        }
    }
        public function viewProducts(){
              if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
              {
                redirect('admin');
              }
            $table ="products";
            $search = ($this->input->get("search"))? $this->input->get("search") : "null";
           $config = array();
           $config['reuse_query_string'] = true;
           $config["base_url"] = base_url() . "Products/viewProducts";
           $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
           $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'product_name');//search
           $config["per_page"] = PERPAGE_LIMIT;
           $config["uri_segment"] = 3;
           $config['full_tag_open'] = "<ul class='pagination'>";
           $config['full_tag_close'] = '</ul>';
           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';
           $config['cur_tag_open'] = '<li class="active"><a href="#">';
           $config['cur_tag_close'] = '</a></li>';
           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';
           $config['first_tag_open'] = '<li>';
           $config['first_tag_close'] = '</li>';
           $config['last_tag_open'] = '<li>';
           $config['last_tag_close'] = '</li>';
           $config['prev_link'] = '<i class="mdi mdi-skip-backward"></i>';
           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';
           $config['next_link'] = '<i class="mdi mdi-skip-forward"></i>';
           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';
           $this->pagination->initialize($config);
           $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
           $data["links"] = $this->pagination->create_links();
           $limit =$config["per_page"];
           $start=$page;
           $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'product_name');
           $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
           $data['basicsettingsList']=$resultBasicsettings;
                    if($result){
                        foreach ($result as $key => $field) {
                            $result[$key]['mastercategory'] = $this->Adminmodel->getSingleColumnName($field['master_category_id'],'id','master_category_name','master_cateringcategory') ;
                            $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','catering_category') ;
                            $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($field['sub_category_id'],'id','subcategory_name','catering_subcategory');
                        } 
                        $data['result'] = $result;
                    } else {
                        $result[] = [] ;
                        $data['result'] = $result ;
                    }
                    $data['searchVal'] = $search !='null'?$search:"";
                    $this->load->view('admin/view_products',$data);
                }
   public function editProducts(){
      if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
      $id = $this->uri->segment('3');
      $resultCategory = $this->Adminmodel->getAjaxdataCountry('master_cateringcategory');
        $data['resultCat'] = $resultCategory; 
      if($id==''){
           redirect('adminLogin');
      }
      $tablename = "products";
      $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
      $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
      $data['basicsettingsList']=$resultBasicsettings;
      if($result) {
          foreach ($result as $key => $field) {
              $result[$key]['mastercategory'] = $this->Adminmodel->getSingleColumnName($field['master_category_id'],'id','master_category_name','master_cateringcategory') ;
              $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','catering_category') ;
              $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($field['sub_category_id'],'id','subcategory_name','catering_subcategory');
          }
          $data['result'] = $result[0] ;
          $this->load->view('admin/edit_products',$data);
      } else {
          $url='viewProducts';
          redirect($url);
      }
    }
    public function updateProducts(){
       if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $product_name = $this->input->post('product_name');       
        if($product_name!=''){            
            $check_data = array(
                "master_category_id" => $master_category_id,
                "category_id" => $category_id,
                "sub_category_id" => $sub_category_id,
                "product_name" => $product_name,
                "id !=" =>$id   
            );
            $tablename = "products";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Products already exist</div>') ;
            }else{
              if($_FILES['product_image']['size'] > 0){
                    $config_media['upload_path'] = './uploads/product_image';
                    $config_media['allowed_types'] = 'gif|jpg|jpeg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need                 
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('product_image')){
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else{
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $product_image    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        redirect('editProducts');
                    }        
                } else {
                    $product_image    = "";
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;            
                $date     = date("Y-m-d H:i:s");                
                $master_category_id = $this->input->post('master_category_id')=="" ? "":$this->input->post('master_category_id');
                $category_id = $this->input->post('category_id')=="" ? "":$this->input->post('category_id');
                 $sub_category_id = $this->input->post('sub_category_id') =="" ? "":$this->input->post('sub_category_id');
                $product_type = $this->input->post('product_type') =="" ? "":$this->input->post('product_type');
                $product_description = $this->input->post('product_description') =="" ? "":$this->input->post('product_description');
                $data = array(
                    'master_category_id'=> $master_category_id,
                    'category_id'=> $category_id,
                    'sub_category_id'=> $sub_category_id,
                    'product_name'=> $product_name,
                    'product_description'=> $product_description,
                    'product_type'=> $product_type,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                if($product_image!=""){
                    $imgArray =array('product_image'=> $product_image);
                    $data= array_merge($data,$imgArray);
                }
                $table="products";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss"> Products Updated.</div>');
                }else{
                    $url='Products/editProducts/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error,Products not updated.</div>') ;
                }   
            } 
            $url='Products/editProducts/'.$id;
            redirect($url);
        }else {   
            $url='Products/editProducts/'.$id;
            redirect($url); 
        }
    }     
    
        function productsEnable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'keyaan_status' =>'0'
            );
            $table="products";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='viewProducts';
            redirect($url);
        }      
        function productsDisable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'keyaan_status' =>'1'
            );
            $table="products";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='viewProducts';
            redirect($url);
        }
       
        function deleteProducts($id) {
        $id=$id;
        $result = $this->Adminmodel->delImage($id,'products','product_image','product_image');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>