<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Careers extends CI_Controller {
    function __construct() {
        parent::__construct();
         $this->load->helper("encryptionpwd");
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('form_validation');
    }
    public function index() {
        redirect('viewCareers');
    }
    public function viewCareers() {
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="keyaan_careers";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
        $config = array();
        $config['reuse_query_string'] = true;
        $config["base_url"] = base_url() . "Careers/viewCareers";
        $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
        $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'job_title');//search
        $config["per_page"] = PERPAGE_LIMIT;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
        $limit =$config["per_page"];
        $start=$page;
        $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'job_title');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
            if($result){
                $data['result'] = $result ;
            } else {
                $result[] = [] ;
                $data['result'] = $result ;
            }
            $data['searchVal'] = $search !='null'?$search:"";
            $this->load->view('admin/view_careers',$data);
    }

    public function addCareers() {
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $job_title = $this->input->post('job_title');
        $dataBefore =[];
        $resultCountry = $this->Adminmodel->getAjaxdataCountry('keyaan_countries');
        $dataBefore['resultCnt'] = $resultCountry; 
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;     
        if($job_title!=''){            
            $check_data = array(
            "job_title" => $this->input->post('job_title')
            );
            $min='1452';
            $max='8569';
            $job_code =rand($min,$max);
            $tablename = "keyaan_careers";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Career already exist</div>') ;
            }else{
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $job_detail = $this->input->post('job_detail')=="" ? "":$this->input->post('job_detail');
                $country_id = $this->input->post('country_id') =="" ? "":$this->input->post('country_id');
                $state_id = $this->input->post('state_id') =="" ? "":$this->input->post('state_id');
                $district_id = $this->input->post('district_id') =="" ? "":$this->input->post('district_id');
                $city_id = $this->input->post('city_id') =="" ? "":$this->input->post('city_id');
                $pincode_id = $this->input->post('pincode_id') =="" ? "":$this->input->post('pincode_id');
                $location_id = $this->input->post('location_id') =="" ? "":$this->input->post('location_id');
                $package = $this->input->post('package') =="" ? "":$this->input->post('package');
                $skills_required = $this->input->post('skills_required') =="" ? "":$this->input->post('skills_required');
                $job_type = $this->input->post('job_type') =="" ? "":$this->input->post('job_type');
                $no_of_positions = $this->input->post('no_of_positions') =="" ? "":$this->input->post('no_of_positions');
                $exprience_years = $this->input->post('exprience_years') =="" ? "":$this->input->post('exprience_years');
                $exprience_months = $this->input->post('exprience_months') =="" ? "":$this->input->post('exprience_months');
                $notice_period = $this->input->post('notice_period') =="" ? "":$this->input->post('notice_period');
                $data = array(
                    'job_title'=> $job_title ,
                    'job_code'=> $job_code ,
                    'job_detail'  => $job_detail,
                    'country_id'=>$country_id,
                    'state_id'=>$state_id,
                    'district_id'=>$district_id,
                    'city_id'=>$city_id,
                    'pincode_id'=>$pincode_id,
                    'location_id'=>$location_id,
                    'package'=>$package,
                    'skills_required'=>$skills_required,
                    'job_type'=>$job_type,
                    'no_of_positions'=>$no_of_positions,
                    'exprience_years'=>$exprience_years,
                    'exprience_months'=>$exprience_months,
                    'notice_period'=> $notice_period,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="keyaan_careers";
                $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Careers Inserted</div>');
                } else {
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Careers not inserted</div>') ;
                }
                $this->load->view('admin/add_careers',$dataBefore);   
            }
        } else {
            $this->load->view('admin/add_careers',$dataBefore);    
        }
    }

    public function editCareers() {
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        $dataBefore =[];
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "keyaan_careers";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $resultCountry = $this->Adminmodel->getAjaxdataCountry('keyaan_countries');
        $data['result'] = $result ;
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        if($result) {
            foreach ($result as $key => $field) {
                $result[$key]['country'] = $this->Adminmodel->getSingleColumnName($field['country_id'],'id','country_name','keyaan_countries') ;
                $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($field['state_id'],'id','state_name','keyaan_states') ;
                $result[$key]['district'] = $this->Adminmodel->getSingleColumnName($field['district_id'],'id','district_name','keyaan_districts') ;
                $result[$key]['city'] = $this->Adminmodel->getSingleColumnName($field['city_id'],'id','city_name','keyaan_cities') ;
                $result[$key]['pincode'] = $this->Adminmodel->getSingleColumnName($field['pincode_id'],'id','pincode','keyaan_pincodes') ;
                $result[$key]['location'] = $this->Adminmodel->getSingleColumnName($field['location_id'],'id','location_name','keyaan_locations') ;
                $dataBefore['result'] = replace_empty_values($result); 
                $dataBefore['resultCnt'] = $resultCountry;
                $dataBefore['basicsettingsList']=$resultBasicsettings;
                $this->load->view('admin/edit_careers',$dataBefore);
            }
        } else {
            $url='viewCareers';
            redirect($url);
        }
    }
    
    public function updateCareers() {
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $job_title = $this->input->post('job_title');       
        if($job_title!=''){            
            $check_data = array(
                "job_title" => $job_title,
                "id !=" =>$id   
            );
            $tablename = "keyaan_careers";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Careers already exist</div>') ;
            }else{
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $job_detail = $this->input->post('job_detail')=="" ? "":$this->input->post('job_detail');
                $country_id = $this->input->post('country_id') =="" ? "":$this->input->post('country_id');
                $state_id = $this->input->post('state_id') =="" ? "":$this->input->post('state_id');
                $district_id = $this->input->post('district_id') =="" ? "":$this->input->post('district_id');
                $city_id = $this->input->post('city_id') =="" ? "":$this->input->post('city_id');
                $pincode_id = $this->input->post('pincode_id') =="" ? "":$this->input->post('pincode_id');
                $location_id = $this->input->post('location_id') =="" ? "":$this->input->post('location_id');
                $package = $this->input->post('package')=="" ? "":$this->input->post('package');
                $skills_required = $this->input->post('skills_required') =="" ? "":$this->input->post('skills_required');
                $job_type = $this->input->post('job_type') =="" ? "":$this->input->post('job_type');
                $no_of_positions = $this->input->post('no_of_positions') =="" ? "":$this->input->post('no_of_positions');
                $exprience_years = $this->input->post('exprience_years') =="" ? "":$this->input->post('exprience_years');
                $exprience_months = $this->input->post('exprience_months') =="" ? "":$this->input->post('exprience_months');
                $notice_period = $this->input->post('notice_period') =="" ? "":$this->input->post('notice_period');
                $id =$this->input->post('id');
                $data = array(
                    'job_title'=> $job_title ,
                    'job_detail'=> $job_detail,
                    'country_id'=>$country_id,
                    'state_id'=>$state_id,
                    'district_id'=>$district_id,
                    'city_id'=>$city_id,
                    'pincode_id'=>$pincode_id,
                    'location_id'=>$location_id,
                    'package'  =>$package,
                    'skills_required'=>$skills_required,
                    'job_type'=>$job_type,
                    'no_of_positions'=>$no_of_positions,
                    'exprience_years'=>$exprience_years,
                    'exprience_months'=>$exprience_months,
                    'notice_period'=>$notice_period,                            
                    'updated_at'=>$date,
                    'updated_by'=>$added_by
                );
                $table=" keyaan_careers";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Career Updated.</div>');
                }else{
                    $url='Careers/editCareers/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Careers not updated.</div>') ;
                }   
            } 
            $url='Careers/editCareers/'.$id;
            redirect($url);
        }else {   
            $url='Careers/editCareers/'.$id;
            redirect($url); 
        }
    }
    public function careerDetails() {
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller.
       {
         redirect('admin');
       }
       $input  = json_decode(file_get_contents('php://input'), true);
       $start=0;
       $perPage = 100;
       $careerId = $this->uri->segment('3');
       //if($start!="" && $perPage!=""){
       $table="keyaan_careers";
       if($careerId !=""){
          @$column = "id";
          @$value  = $careerId;
       }
       $search ='';
      
       $result1 = $this->Adminmodel->get_current_page_records($table,$perPage,$start,@$column,@$value,@$search,@$searchColumn);
       $result=replace_attr($result1);
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
       if($result){
            foreach ($result as $key => $field) {
                
                $result[$key]['country'] = $this->Adminmodel->getSingleColumnName($field['country_id'],'id','country_name','keyaan_countries') ;
                $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($field['state_id'],'id','state_name','keyaan_states') ;
                $result[$key]['district'] = $this->Adminmodel->getSingleColumnName($field['district_id'],'id','district_name','keyaan_districts') ;
                $result[$key]['city'] = $this->Adminmodel->getSingleColumnName($field['city_id'],'id','city_name','keyaan_cities') ;
                $result[$key]['pincode'] = $this->Adminmodel->getSingleColumnName($field['pincode_id'],'id','pincode','keyaan_pincodes') ;
                $result[$key]['location'] = $this->Adminmodel->getSingleColumnName($field['location_id'],'id','location_name','keyaan_locations') ;
            }
            $data['result'] = $result ;
            $this->load->view('admin/career_details',$data);
        } else {
            $url='viewCareers';
            redirect($url);
        }
    }
    function CareersEnable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'0'
        );
        $table="keyaan_careers";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewCareers';
        redirect($url);
    }      
    function CareersDisable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'1'
        );
        $table="keyaan_careers";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewCareers';
        redirect($url);
    }
    function deleteCareers($id) {
        $id=$id;
        $result = $this->Adminmodel->delRow($id,'keyaan_careers');
        $data['result'] = $result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>