<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendorpackage extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('form_validation');
    }
    public function index() {
         self::viewVendorpackage();
        } 
    // add Package
    public function addVendorpackage(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;
        $package_name   = $this->input->post('package_name');
        if($package_name!=''){
            $check_data = array(
            "package_name" => $this->input->post('package_name')    
            );
            $tablename = "vendor_package";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Vendor Package already exist</div>');
                 $this->load->view('admin/add_vendorpackage',$dataBefore);
            }else{
                    $admin = $this->session->userdata('userCode');
                    $added_by = $admin!='' ? $admin:'admin' ;           
                    $date     = date("Y-m-d H:i:s");
                    $duration = $this->input->post('duration')=="" ? "":$this->input->post('duration');
                    $package_detail = $this->input->post('package_detail')=="" ? "":$this->input->post('package_detail');
                    $price = $this->input->post('price')=="" ? "":$this->input->post('price');
                    $data = array(
                        'package_name'  => $package_name,
                        'price'       =>  $price,
                        'duration'  =>  $duration,
                        'package_detail'   =>  $package_detail,
                        'created_by'     => $added_by ,
                        'created_at'     => $date,
                        'updated_at'     => $date,
                        'updated_by'     => $added_by
                    );
                    $table="vendor_package";
                    $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                    if($result){
                        $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Vendor package Added</div>');
                    }
                    else{
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Vendor package not inserted</div>') ;
                    }           
                    $this->load->view('admin/add_vendorpackage',$dataBefore);   
                }
        }else{
            $this->load->view('admin/add_vendorpackage',$dataBefore);
        }
      }
    
 
    public function viewVendorpackage(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="vendor_package";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Vendorpackage/viewVendorpackage";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'package_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'package_name');
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
        if($result){
           $data['result'] = $result;
        }else {
            $result[] = [] ;
            $data['result'] = $result;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_vendorpackage',$data);
    }
     public function editVendorpackage(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "vendor_package";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $data['result'] = $result[0] ;
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        if($result) {
           $this->load->view('admin/edit_vendorpackage',$data);
       } else {
           $url='viewVendorpackage';
           redirect($url);
       }
            
        }
    public function updateVendorpackage(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $package_name = $this->input->post('package_name');
        if($package_name!=''){            
            $check_data = array(
            "package_name" => $package_name,
            "id !=" =>$id   
            );   
            $tablename = "vendor_package";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
             $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Vendor Package already exist</div>') ;
            }else{
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;         
                $date     = date("Y-m-d H:i:s");
                $package_detail = $this->input->post('package_detail')=="" ? "":$this->input->post('package_detail');
                $price = $this->input->post('price')=="" ? "":$this->input->post('price');
                $duration = $this->input->post('duration')=="" ? "":$this->input->post('duration');
                $data = array(
                 'package_name'=> $package_name ,
                 'duration'  =>$duration,
                 'price'       =>  $price,
                 'package_detail'       =>  $package_detail,  
                 'updated_at'     => $date,
                 'updated_by'     => $added_by
                );
                $table="vendor_package";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                     $this->session->set_flashdata('msg','<div class="alert alert-success subcatupdate updateSuss">Vendor Package Updated</div>');
                } else{
                    $url='Vendorpackage/editVendorpackage/'.$id;
                     redirect($url);
                     $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps Some error</div>') ;
                }  
            } 
            $url='Vendorpackage/editVendorpackage/'.$id;
            redirect($url);
        } else {   
            $url='Vendorpackage/editVendorpackage/'.$id;
            redirect($url);    
        }
    }
    function vendorpackageEnable($id) {
        $id=$id;
        $dataSubcat =array(
           'keyaan_status' =>'0'
        );
        $table="vendor_package";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewVendorpackage';
        redirect($url);
    }      
    function vendorpackageDisable($id) {
        $id=$id;
        $dataSubcat =array(
           'keyaan_status' =>'1'
        );
        $table="vendor_package";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewVendorpackage';
        redirect($url);
    }
    function deleteVendorpackage($id) {
        $id=$id;
        $result = $this->Adminmodel->delRow($id,'vendor_package');
        $data['result'] = $result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>