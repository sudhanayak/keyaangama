<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
    function __construct() {
        parent::__construct();
         $this->load->helper("encryptionpwd");
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('form_validation');
        $this->load->library('upload');
    }
    public function index() {
         redirect('viewCategory');
        } 

    public function viewCategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="keyaan_category";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Category/viewCategory";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'category_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'category_name');
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
        if($result){
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_category',$data);
    }  

    public function addCategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;
        $category_name = $this->input->post('category_name');       
        if($category_name!=''){            
            $check_data = array(
            "category_name" => $this->input->post('category_name')    
            );
            $tablename = "keyaan_category";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Category name already exist</div>');
                $this->load->view('admin/add_category',$dataBefore); 
            }else{
                if (isset($_FILES['image'])) {
                    $config_media['upload_path'] = './uploads/category';
                    $config_media['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('image'))
                    {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $image    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        redirect('addCategory');
                    }        
                } else {
                    $image    = "";
                }
                if (isset($_FILES['category_banner'])) {
                    $config_media1['upload_path'] = './uploads/categorybanner';
                    $config_media1['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';
                    $config_media1['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media1);
                    $error = [];
                    if ( ! $this->upload->do_upload('category_banner'))
                    {
                        $error1[] = array('error_image' => $this->upload->display_errors());
                    }
                    else
                    {
                        $data1[] = array('upload_image' => $this->upload->data());
                    }
                    $category_banner  = $data1[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        redirect('addCategory');
                    }
                } else {
                    $category_banner    = "";
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $icon = $this->input->post('icon')=="" ? "":$this->input->post('icon');
                $price_selection = $this->input->post('price_selection')=="" ? "":$this->input->post('price_selection');
                if($price_selection == 1) {
                     $price = "";
                }else{
                     $price = $this->input->post('price')=="" ? "":$this->input->post('price');
                }
                if($price_selection == 0) {
                    $price_from = ""; 
                    $price_to = ""; 
                }else{
                    $price_from = $this->input->post('price_from')=="" ? "":$this->input->post('price_from'); 
                    $price_to = $this->input->post('price_to')=="" ? "":$this->input->post('price_to'); 
                }
                $meta_tag = $this->input->post('meta_tag')=="" ? "":$this->input->post('meta_tag');
                $meta_keyword = $this->input->post('meta_keyword')=="" ? "":$this->input->post('meta_keyword');
                $meta_detail = $this->input->post('meta_detail')=="" ? "":$this->input->post('meta_detail');
                $category_type = $this->input->post('category_type')=="" ? "":$this->input->post('category_type');                        
                $data = array(
                    'category_name'=> $category_name ,
                    'icon'  => $icon,
                    'price_selection'  =>$price_selection,
                    'price_from'  =>$price_from,
                    'price_to'   =>  $price_to,
                    'price'       =>  $price,
                    'meta_tag'       =>  $meta_tag,
                    'meta_keyword'   =>  $meta_keyword,
                    'meta_detail'   =>  $meta_detail,
                    'category_type'   =>  $category_type,
                    'image'        => $image,
                    'category_banner' => $category_banner,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updatedby'     => $added_by
                );
                $table="keyaan_category";
                $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Category Inserted</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Category not inserted</div>') ;
                }
                $this->load->view('admin/add_category',$dataBefore);   
            }
        }else {
            //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
            $this->load->view('admin/add_category',$dataBefore);    
        }       
    }
    
    public function editCategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "keyaan_category";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $data['result'] = $result ;
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        if($result) {
            $this->load->view('admin/edit_category',$data);
        } else {
            $url='viewCategory';
            redirect($url);
        }
        
    }
    public function updateCategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $category_name = $this->input->post('category_name');       
        if($category_name!=''){            
            $check_data = array(
                "category_name" => $category_name,
                "id !=" =>$id   
            );
            $tablename = "keyaan_category";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Category name already exist</div>') ;
            }else{
                if($_FILES['image']['size'] > 0){
                    $config_media['upload_path'] = './uploads/category';
                    $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('image')){
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else{
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $image    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                                 redirect('editCategory');
                    }        
                }
                if (isset($_FILES['category_banner'])) {
                    $config_media1['upload_path'] = './uploads/categorybanner';
                    $config_media1['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';
                    $config_media1['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media1);
                    $error = [];
                    if ( ! $this->upload->do_upload('category_banner'))
                    {
                        $error1[] = array('error_image' => $this->upload->display_errors());
                    }
                    else
                    {
                        $data1[] = array('upload_image' => $this->upload->data());
                    }
                    $category_banner  = $data1[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        redirect('addCategory');
                    }
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $icon = $this->input->post('icon')=="" ? "":$this->input->post('icon');
                $price_selection = $this->input->post('price_selection')=="" ? "":$this->input->post('price_selection');
                if($price_selection == 1) {
                     $price = "";
                }else{
                     $price = $this->input->post('price')=="" ? "":$this->input->post('price');
                }
                if($price_selection == 0) {
                    $price_from = ""; 
                    $price_to = ""; 
                }else{
                    $price_from = $this->input->post('price_from')=="" ? "":$this->input->post('price_from'); 
                    $price_to = $this->input->post('price_to')=="" ? "":$this->input->post('price_to'); 
                }
                $meta_tag = $this->input->post('meta_tag')=="" ? "":$this->input->post('meta_tag');
                $meta_keyword = $this->input->post('meta_keyword')=="" ? "":$this->input->post('meta_keyword');
                $meta_detail = $this->input->post('meta_detail')=="" ? "":$this->input->post('meta_detail');
                $category_type = $this->input->post('category_type')=="" ? "":$this->input->post('category_type');   
                $id =$this->input->post('id');
                $data = array(
                    'category_name'=> $category_name ,
                    'icon'          => $icon,
                    'price_selection'  =>$price_selection,
                    'price_from'  =>$price_from,
                    'price_to'   =>  $price_to,
                    'price'       =>  $price,
                    'meta_tag'       =>  $meta_tag,
                    'meta_keyword'       =>  $meta_keyword,
                    'meta_detail'       =>  $meta_detail,
                    'category_type'       =>  $category_type,                            
                    'updated_at'     => $date,
                    'updatedby'     => $added_by
                );
                if($image!=""){
                    $imgArray =array('image'=> $image);
                    $data= array_merge($data,$imgArray);
                }
                if($category_banner!=""){
                    $imgArray =array('category_banner'=> $category_banner);
                    $data= array_merge($data,$imgArray);
                }
                $table="keyaan_category";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Category Updated.</div>');
                }else{
                    $url='category/editCategory/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Category not updated.</div>') ;
                }   
            } 
            $url='category/editCategory/'.$id;
            redirect($url);
        }else {   
            $url='category/editCategory/'.$id;
            redirect($url); 
        }
    }
    function catEnable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'0'
        );
        $table="keyaan_category";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewCategory';
            redirect($url);
    }      
    function catDisable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'1'
        );
        $table="keyaan_category";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewCategory';
        redirect($url);
    }
    function deleteCategory($id) {
        $id=$id;
        $result = $this->Adminmodel->delmultipleImage($id,'keyaan_category','category','categorybanner','','','image','category_banner','','','id');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }

}
?>