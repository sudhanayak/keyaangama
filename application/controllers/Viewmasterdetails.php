<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Viewmasterdetails extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
    }
    public function masterDetails(){
      if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
       $start=0;
       $perPage = 100;
       $vendor = $this->session->userdata('vendorCode');
       $vendorId=$this->Adminmodel->getSingleColumnName($vendor,'vendor_code',
                  'id','keyaan_vendor');     
       $table="keyaan_vendor";
       if($vendorId !=""){
          @$column = "id";
          @$value  = $vendorId;
       }
       $search ='';
      
       $result1 = $this->Adminmodel->get_current_page_records($table,$perPage,$start,@$column,@$value,@$search,@$searchColumn);
       $result=replace_attr($result1);
       if($result){
            foreach ($result as $key => $field) {
                $catId=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                  'category_id','vendor_categories') ;
                  $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($catId,'id',
                  'category_name','keyaan_category') ;
                  $result[$key]['cat_id'] = $catId ;
                  $subcat_id=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                  'sub_category_id','vendor_sub_categories') ;
                  $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($subcat_id,'id',
                  'subcategory_name','keyaan_subcategory') ;
                  $result[$key]['subcat_id'] = $subcat_id ;
                  $subsubcat_id=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','sub_sub_category_id','vendor_sub_sub_categories') ;
                  $result[$key]['subsubcategory'] = $this->Adminmodel->getSingleColumnName($subsubcat_id,'id',
                  'subsubcat_name','keyaan_subsubcategory') ;
                  $result[$key]['subsubcat_id'] = $subsubcat_id ;
                
                $result[$key]['country'] = $this->Adminmodel->getSingleColumnName($field['country_id'],'id','country_name','keyaan_countries') ;
                $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($field['state_id'],'id','state_name','keyaan_states') ;
                $result[$key]['district'] = $this->Adminmodel->getSingleColumnName($field['district_id'],'id','district_name','keyaan_districts') ;
                $result[$key]['city'] = $this->Adminmodel->getSingleColumnName($field['city_id'],'id','city_name','keyaan_cities') ;
                $result[$key]['pincode'] = $this->Adminmodel->getSingleColumnName($field['pincode_id'],'id','pincode','keyaan_pincodes') ;
                $result[$key]['location'] = $this->Adminmodel->getSingleColumnName($field['location_id'],'id','location_name','keyaan_locations') ;
               if($field['basic_image'] !=""){
                   $result[$key]['basic_image'] = base_url()."uploads/basic_image/".$field['basic_image'];
               }else{
                 $result[$key]['basic_image'] = "0";  
               }
               if($field['bank_upload_cancel_cheque'] !=""){
                   $result[$key]['bank_upload_cancel_cheque'] = base_url()."uploads/check_image/".$field['bank_upload_cancel_cheque'];
               }else{
                 $result[$key]['bank_upload_cancel_cheque'] = "0";  
               }
               if($field['logo'] !=""){
               $result[$key]['logo'] = base_url()."uploads/vendorlogo/".$field['logo'];  
               }else{
                 $result[$key]['logo'] = "0";  
               }
               $result['imglist'] = $this->Adminmodel->getgallery($field['vendor_code'],'vendor_image_details') ;
           }
           $data['result'] = $result ;
           $this->load->view('vendor/master_details',$data);
       }
       else{
          $url=base_url().'Mastervendor';
          redirect($url);
       }
   }
    
   
}


