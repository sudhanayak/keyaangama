<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
    }
     public function index() {
         redirect('viewcms');
        } 
    public function addcms(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;
        $title = $this->input->post('title');       
        if($title!=''){
            $check_data = array(
            "title" => $this->input->post('title')
            );
            $tablename = "keyaan_cms";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">CMS Name already exist</div>') ;
                $this->load->view('admin/add_cms',$dataBefore);
            }else{
                    
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;
                $cms_id = $this->input->post('cms_id')=="" ? "":$this->input->post('cms_id');
                $description = $this->input->post('description')=="" ? "":$this->input->post('description');
                $date     = date("Y-m-d H:i:s");
                $data = array(
                    'cms_id'=> $cms_id ,
                    'title'=> $title ,
                    'description'=> $description ,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="keyaan_cms";
                $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                        $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Cms Inserted Successfully!</div>');
                }
                else{
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Data not inserted</div>') ;
                }           
                $this->load->view('admin/add_cms',$dataBefore); 
            }
        }else
            {
                //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
                $this->load->view('admin/add_cms',$dataBefore);    
            }     
        }
        public function viewcms(){
            if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
            {
              redirect('admin');
            }
            $table ="keyaan_cms";
            $search = ($this->input->get("search"))? $this->input->get("search") : "null";
               $config = array();
               $config['reuse_query_string'] = true;
               $config["base_url"] = base_url() . "Cms/viewcms";
               $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
               $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'title');//search
               $config["per_page"] = PERPAGE_LIMIT;
               $config["uri_segment"] = 3;
               $config['full_tag_open'] = "<ul class='pagination'>";
               $config['full_tag_close'] = '</ul>';
               $config['num_tag_open'] = '<li>';
               $config['num_tag_close'] = '</li>';
               $config['cur_tag_open'] = '<li class="active"><a href="#">';
               $config['cur_tag_close'] = '</a></li>';
               $config['prev_tag_open'] = '<li>';
               $config['prev_tag_close'] = '</li>';
               $config['first_tag_open'] = '<li>';
               $config['first_tag_close'] = '</li>';
               $config['last_tag_open'] = '<li>';
               $config['last_tag_close'] = '</li>';
               $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
               $config['prev_tag_open'] = '<li>';
               $config['prev_tag_close'] = '</li>';
               $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
               $config['next_tag_open'] = '<li>';
               $config['next_tag_close'] = '</li>';
               $this->pagination->initialize($config);
               $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
               $data["links"] = $this->pagination->create_links();
               $limit =$config["per_page"];
               $start=$page;
               $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'title');
               $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
               $data['basicsettingsList']=$resultBasicsettings;
                    if($result){
                        $data['result'] = $result ;
                    } else {
                        $result[] = [] ;
                        $data['result'] = $result ;
                    }
                    $data['searchVal'] = $search !='null'?$search:"";
                    $this->load->view('admin/viewcms',$data);
                }
        public function updatecms(){
            if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
            {
              redirect('admin');
            }
            $id = $this->input->post('id');
            if(empty($id)){
                redirect('adminLogin');
            }
             $title = $this->input->post('title');      
             if($title!=''){            
                 $check_data = array(
                 "title" => $title,
                 "id !=" =>$id   
                 );
                 $tablename = "keyaan_cms";
                 $checkData = $this->Adminmodel->existData($check_data,$tablename) ;

                 if($checkData > 0){
                     $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">CMS already exist</div>') ;
                 }else{

                     $admin = $this->session->userdata('userCode');
                     $added_by = $admin!='' ? $admin:'admin' ;          
                     $date     = date("Y-m-d H:i:s");
                     $cms_id = $this->input->post('cms_id')=="" ? "":$this->input->post('cms_id');
                    $description = $this->input->post('description')=="" ? "":$this->input->post('description'); 
                     $id =$this->input->post('id');
                     $data = array(
                         'cms_id'=> $cms_id ,
                            'title'=> $title ,
                            'description'=> $description ,
                            'created_by'     => $added_by ,
                            'updated_at'     => $date,
                            'updated_by'     => $added_by
                     );
                     $table="keyaan_cms";
                     $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                     if($result){
                             $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">CMS Updated</div>');                                    
                     }
                     else{
                            $url='cms/editcms/'.$id;
                             redirect($url);
                             $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps Some error</div>') ;
                     }  


                 } 
                 $url='cms/editcms/'.$id;
                 redirect($url);
             }
             else
             {   
                 $url='cms/editcms/'.$id;
                 redirect($url);    
             }

         }
        public function editcms(){
            $id = $this->uri->segment('3');
            if($id==''){
                redirect('adminLogin');
            }
            $tablename = "keyaan_cms";
            $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
            $data['result'] = $result ;
            $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
            $data['basicsettingsList']=$resultBasicsettings;
            if($result) {
                $this->load->view('admin/editcms',$data);
            } else {
                $url='viewcms';
                redirect($url);
            }
        }
        function cmsEnable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'keyaan_status' =>'0'
            );
            $table="keyaan_cms";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='cms/viewcms';
            redirect($url);
        }      
        function cmsDisable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'keyaan_status' =>'1'
            );
            $table="keyaan_cms";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='cms/viewcms';
            redirect($url);
        }
        function deletecms($id) {
            $id=$id;
            $result = $this->Adminmodel->delRow($id,'keyaan_cms');
            $data['result'] = $result;
            redirect($_SERVER['HTTP_REFERER']);
        }
}
?>