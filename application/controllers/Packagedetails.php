<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Packagedetails extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');       
    }
    function index() {
        self::redirect('packageDetailList');
    }
    function packageDetailList() {
        $packageCode= $this->uri->segment(2);

        if($this->session->userdata('userCCode') !=""){
            $userCode = $this->session->userdata('userCCode');
            $cartCount = $this->Adminmodel->getcartCount($userCode);
        }else{
            $cartCount=0;
        }
        $data['menuCategoryAll'] = self::getCategoryList('keyaan_category');
        $data['menuPackageAll'] = self::getCategoryPackList('package_category');
        $resultCategoryAll = $this->Adminmodel->getAjaxdataCountry('keyaan_category'); 
        $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $countNumdays = $this->Adminmodel->getSingleColumnName($packageCode,
                'package_code','number_of_days',
                'package');
         $i=1;
        $package = $this->Adminmodel->packages(@$packageCode);
        $packageImg =$this->Adminmodel->getpackageImages($packageCode,'package_images');
       
        $allpackResult = $this->Adminmodel->packagesDays(@$packageCode);
        foreach ($allpackResult as $key_index => $value_data) {

        //$itineraryList = $this->Adminmodel->itineraryList(@$packageCode,$value_data['event_day']);
        
        $allpackResult[$key_index]['itineraryList'] = 
            self::itineraryList($packageCode,$value_data['event_day']);     
        }


            
        $data['categoryList']=$resultCategoryAll;
        $data['cityList']=$resultCities;
        $data['basicsettingsList']=$resultBasicsettings;
        $data['cartCount']=$cartCount;
        $data['package']=$package;
        $data['packageImg']=$packageImg;
        $data['packageCount']=$countNumdays;
        $data['allpackResult']=$allpackResult;
        //print_r($allpackResult);
        
        $this->load->view('user/package_detail',$data); 
    }
    public function itineraryDetail($package_code,$itinerary_id,$days){
         $itineraryResult = $this->Adminmodel->itineraryDetail($package_code,$itinerary_id,$days,0);
         if($itineraryResult){
            foreach ($itineraryResult as $key => $value) {
                $itineraryResult[$key]['imageList']= $this->Adminmodel->getpackagegallery($value['itinerary_code'],'package_image_details');
            }
            return $itineraryResult ;
         }else{
            $arr = array();
            return $arr ;
         }
    }

    public function itineraryList($package_code,$days){
        $itineraryList =$this->Adminmodel->itineraryList($package_code,$days);
        foreach ($itineraryList as $key => $value) {
            $itineraryList[$key]['itenerary'] = 
            $this->Adminmodel->getSingleColumnName($value['itenerary_id'],
            'id','name',
            'package_itinerary'); 
            $itineraryList[$key]['iteneraryDetail'] = 
            self::itineraryDetail($package_code,$value['itenerary_id'],$value['event_day']); 
        }
        return $itineraryList;
    }
    public function getCategoryList(){

        $result = $this->Adminmodel->getCategoryList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryList($field['id']);
             
        } 

        return $result ;   

    }
    public function getSubCategoryList($catId){
          
        $result = $this->Adminmodel->getSubCategoryList($catId);
        foreach ($result as $key => $field) {
             $result[$key]['subsubcatList'] = $this->Adminmodel->getSubsubCategoryList($field['id']) ;
        } 

        return $result ;   

    } 
    public function getCategoryPackList(){

        $result = $this->Adminmodel->getCategoryPackList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryPackList($field['id']); 
        } 
        return $result ;   
    }
    public function getSubCategoryPackList($catId){  
        $result = $this->Adminmodel->getSubCategoryPackList($catId);
        return $result ;   
    }
    public function addToCart(){
        if($this->input->post('package_id')){
            $event_ordcode =rand(1020,7410) ;
            $dataCart= array(
                'cart_code'=>$event_ordcode,
                'package_id' =>$this->input->post('package_id'),
                'user_id' =>$this->input->post('userId'),
                'price' =>$this->input->post('price'),
                'card_type'=>'2'    
            );
            $dataExit = 0;
            $start=0;
            $perPage = 100;
            $package_code = $this->input->post('package_id');
            $itinerary_id = $this->Adminmodel->get_current_page_records('package_detail',$perPage,$start,'package_code',$package_code,@$search,@$searchColumn);
            foreach ($itinerary_id as $key => $value) {
                $dataCart1= array(
                    'package_id' =>$package_code,
                    'iteinerary_id' =>$value['itinerary_code'],
                    'itinerary_price' =>$value['itinerary_price'],
                    'cart_code'=>$event_ordcode,
                    'user_id' =>$this->input->post('userId'),
                );
                $result1 =  $this->Adminmodel->insertRecordQueryList('cart_details',$dataCart1);
            }
            $where = array(
                "package_id"=>$this->input->post('package_id'),
                "user_id"=>$this->input->post('userId'),
            );         
            $table = 'keyaan_cart';  
            $itemExist = $this->Adminmodel->existData($where,$table);
            if($itemExist > 0) {
                $dataExit = 1;
            } else {
                $dataExit = 0;
            }
            if($dataExit == 0){
                $result =  $this->Adminmodel->insertRecordQueryList('keyaan_cart',$dataCart);
                $json_data['status']='TRUE';
                $json_data['responseCode']=0;                    
                $json_data['message']=" sucessfully record inserted in booking  table";
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data);
            }else{
                $json_data['status']='TRUE';
                $json_data['responseCode']=11;                    
                $json_data['message']="Item Already There";
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data);
            }
        }
    }
}    
?>