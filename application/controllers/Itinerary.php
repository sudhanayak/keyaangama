<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itinerary extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('form_validation');
        $this->load->library('upload'); 
        $this->load->library('image_lib');
    }
    public function index() {
         self::viewItinerary();
        } 
    // add Package
    public function addItinerary(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="package_itinerary";
        $start =0;
        $limit =100;
        $search ='';
        $dataBefore['itenerary']= $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,@$search,@$searchColumn);
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;
        $packageCode = $this->uri->segment('3');
        $min='1542';
        $max='9876';
        $itineraryCode =rand($min,$max);
        $dataBefore['packageCode'] = $packageCode;
        $dataBefore['itineraryCode'] = $itineraryCode;
        $dataBefore['number_of_days'] = $this->Adminmodel->getSingleColumnName($packageCode,'package_code','number_of_days','package');
        $dataBefore['package'] = $this->Adminmodel->getSingleColumnName($packageCode,'package_code','package_name','package');
        $itinerary_type   = $this->input->post('itinerary_type');
        $event_day = $this->input->post('event_day')=="" ? "1":$this->input->post('event_day');
        if($itinerary_type!=''){
                $images=count($_FILES['images']['name']);
                if($images !=''){ 
                    $i=0; 
                    $filesCount = count($_FILES['images']['name']);
                    for($i = 0; $i < $filesCount; $i++){
                        $_FILES['file']['name']     = $_FILES['images']['name'][$i];
                        $_FILES['file']['type']     = $_FILES['images']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
                        $_FILES['file']['error']     = $_FILES['images']['error'][$i];
                        $_FILES['file']['size']     = $_FILES['images']['size'][$i];
                        
                        // File upload configuration
                        $uploadPath = './uploads/itineraryimg/';
                        $config9['upload_path'] = $uploadPath;
                        $config9['allowed_types'] = 'jpg|jpeg|png|gif';
                        
                        // Load and initialize upload library
                        $this->upload->initialize($config9);
                        // Upload file to server
                        if($this->upload->do_upload('file')){
                            // Uploaded file data
                            $fileData = $this->upload->data();
                            $uploadData[$i]['file_name'] = $fileData['file_name'];
                            $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                            $source_path = './uploads/itineraryimg/' .$uploadData[$i]['file_name'];
                            $target_path = './uploads/thumbnail/itineraryimg';
                            $this->resizeImage($source_path,$target_path);
                            $data1 = array(
                            'package_code'=> $packageCode ,
                            'itinerary_code'=> $itineraryCode ,
                            'event_day' => $event_day,
                            'images' => $uploadData[$i]['file_name'],
                            'media_type'=>'1'
                            );
                           $result3 = $this->Adminmodel->insertRecordQueryList('package_image_details',$data1);
                        }
                      }
                    }
                    $admin = $this->session->userdata('userCode');
                    $added_by = $admin!='' ? $admin:'admin' ;           
                    $date     = date("Y-m-d H:i:s");
                    $itenerary_id = $this->input->post('itenerary_id')=="" ? "":$this->input->post('itenerary_id');
                    $itenerary_name = $this->input->post('itenerary_name')=="" ? "":$this->input->post('itenerary_name');
                    $itinerary_price = $this->input->post('itinerary_price')=="" ? "":$this->input->post('itinerary_price');
                    $make_it_default = $this->input->post('make_it_default')=="" ? "1":$this->input->post('make_it_default');
                    $itinerary_type = $this->input->post('itinerary_type')=="" ? "":$this->input->post('itinerary_type');
                    if($itinerary_type == 1) {
                     $details = "";
                      }else{
                           $images = $this->input->post('images')=="" ? "":$this->input->post('images');
                      }
                      if($itinerary_type == 2) {
                     $details = $this->input->post('details')=="" ? "":$this->input->post('details');
                      }else{
                          $images = ""; 
                      }
                      if($itinerary_type == 3) {
                     $details = $this->input->post('details')=="" ? "":$this->input->post('details');
                     $images = $this->input->post('images')=="" ? "":$this->input->post('images');
                      }

                      if($make_it_default==0){
                        $default_price = $this->Adminmodel->allItinaryPrice('package_detail',$packageCode);  
                        $all_amount = $default_price + $itinerary_price ;
                        $dataitnary = array(
                            'price'=> $all_amount
                        );
                        $result22 = $this->Adminmodel->updateRecordQueryList('package',$dataitnary,'package_code',$packageCode);
                    }
                    $data = array(
                        'package_code'=> $packageCode ,
                        'itinerary_code'=> $itineraryCode ,
                        'itenerary_id'     => $itenerary_id ,
                        'itenerary_name'=> $itenerary_name ,
                        'itinerary_price'=> $itinerary_price ,
                        'make_it_default'=> $make_it_default ,
                        'itinerary_type' => $itinerary_type,
                        'details' => $details,
                        'event_day' => $event_day,
                        'created_by'     => $added_by ,
                        'created_at'     => $date,
                        'updated_at'     => $date,
                        'updated_by'     => $added_by
                    );
                    $result = $this->Adminmodel->insertRecordQueryList('package_detail',$data);
                    if($result){
                        $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Itinerary Added</div>');
                    }
                    else{
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Itinerary not inserted</div>') ;
                    }           
                    $this->load->view('admin/add_itinerary',$dataBefore);   
                }
        else{
            $this->load->view('admin/add_itinerary',$dataBefore);
        }
    }
    public function editItinerary(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $package_code = $this->uri->segment('3');
        $itinerary_code = $this->uri->segment('4');
        if($itinerary_code==''){
            redirect('adminLogin');
        }
        $tablename = "package_detail";
        $tablename3 = "package_itinerary";
        $start =0;
        $limit =100;
        $data['number_of_days'] = $this->Adminmodel->getSingleColumnName($package_code,'package_code','number_of_days','package');
        $data['itenerary']= $this->Adminmodel->get_current_page_records($tablename3,$limit,$start,$column=null,$value=null,@$search,@$searchColumn);
        $result = $this->Adminmodel->singleRecordData('itinerary_code',$itinerary_code,$tablename);
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        foreach ($result as $key => $field) {
            $result[$key]['itenerary'] = $this->Adminmodel->getSingleColumnName($field['itenerary_id'],'id','name','package_itinerary');
            $itineraryImg = $this->Adminmodel->getpackagegallery($itinerary_code,'package_image_details');
        }
        $data['Count'] = $this->Adminmodel->itinerary_count('package_image_details','itinerary_code',$itinerary_code);
        $data['result'] = $result[0];
        $data['itineraryImg'] = $itineraryImg;
        if($result) {
            $this->load->view('admin/edit_itinerary',$data);
        } else {
            $url='Itinerary/ItineraryDetails/'.$package_code.'/'.$itinerary_code;
            redirect($url);
        }
        
    }
    public function updateItinerary(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $packageCode =$this->input->post('package_code');
        $itineraryCode =$this->input->post('itinerary_code');
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $itinerary_type = $this->input->post('itinerary_type');
        $event_day = $this->input->post('event_day')=="" ? "1":$this->input->post('event_day');
        if($itinerary_type!=''){
                $images=count($_FILES['images']['name']);
                if($images !=''){ 
                    $i=0; 
                    $filesCount = count($_FILES['images']['name']);
                    for($i = 0; $i < $filesCount; $i++){
                        $_FILES['file']['name']     = $_FILES['images']['name'][$i];
                        $_FILES['file']['type']     = $_FILES['images']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
                        $_FILES['file']['error']     = $_FILES['images']['error'][$i];
                        $_FILES['file']['size']     = $_FILES['images']['size'][$i];
                        
                        // File upload configuration
                        $uploadPath = './uploads/itineraryimg/';
                        $config9['upload_path'] = $uploadPath;
                        $config9['allowed_types'] = 'jpg|jpeg|png|gif';
                        $source_path = './uploads/itineraryimg/' .$uploadData[$i]['file_name'];
                        $target_path = './uploads/thumbnail/itineraryimg';
                        $this->resizeImage($source_path,$target_path);
                        // Load and initialize upload library
                        $this->upload->initialize($config9);
                        // Upload file to server
                        if($this->upload->do_upload('file')){
                            // Uploaded file data
                            $fileData = $this->upload->data();
                            $uploadData[$i]['file_name'] = $fileData['file_name'];
                            $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                            $data1 = array(
                            'package_code'=> $packageCode ,
                            'itinerary_code'=> $itineraryCode ,
                            'images' => $uploadData[$i]['file_name'],
                            'event_day' => $event_day,
                            'media_type'=>'1'
                            );
                           $result3 = $this->Adminmodel->insertRecordQueryList('package_image_details',$data1);
                        }
                    }
                  }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;         
                $date     = date("Y-m-d H:i:s");
                $itenerary_name = $this->input->post('itenerary_name')=="" ? "":$this->input->post('itenerary_name');
                $itinerary_type = $this->input->post('itinerary_type')=="" ? "":$this->input->post('itinerary_type');
                $itinerary_price = $this->input->post('itinerary_price')=="" ? "":$this->input->post('itinerary_price');
                $make_it_default = $this->input->post('make_it_default')=="" ? "1":$this->input->post('make_it_default');
                if($itinerary_type == 1) {
                 $details = "";
                  }else{
                       $images = $this->input->post('images')=="" ? "":$this->input->post('images');
                  }
                  if($itinerary_type == 2) {
                 $details = $this->input->post('details')=="" ? "":$this->input->post('details');
                  }else{
                      $images = ""; 
                  }
                  if($itinerary_type == 3) {
                 $details = $this->input->post('details')=="" ? "":$this->input->post('details');
                 $images = $this->input->post('images')=="" ? "":$this->input->post('images');
                  }
                $data = array(
                        'package_code'=> $packageCode ,
                        'itinerary_code'=> $itineraryCode ,
                        'itenerary_name'=> $itenerary_name ,
                        'itinerary_type' => $itinerary_type,
                        'itinerary_price' => $itinerary_price,
                        'make_it_default' => $make_it_default,
                        'event_day' => $event_day,
                        'details' => $details,
                        'updated_at'     => $date,
                        'updated_by'     => $added_by
                    );
                    $result = $this->Adminmodel->updateRecordQueryList('package_detail',$data,'itinerary_code',$itineraryCode);
                if($result){
                     $this->session->set_flashdata('msg','<div class="alert alert-success subcatupdate updateSuss">Itineraries Updated</div>');
                } else{
                    $url='Itinerary/editItinerary/'.$packageCode.'/'.$itineraryCode;
                     redirect($url);
                     $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps Some error</div>') ;
                } 
            $url='Itinerary/editItinerary/'.$packageCode.'/'.$itineraryCode;
            redirect($url);
        } else {   
            $url='Itinerary/editItinerary/'.$packageCode.'/'.$itineraryCode;
            redirect($url);    
        }
    }
     public function ItineraryDetails(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $packageCode = $this->uri->segment('3');
        $start=0;
        $perPage = 100;
        $table ="package_detail";
        if($packageCode !=""){
          @$column = "package_code";
          @$value  = $packageCode;
             $search ='';
       $result1 = $this->Adminmodel->get_current_page_records($table,$perPage,$start,@$column,@$value,@$search,@$searchColumn);
       $result = replace_attr($result1);
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
        if($result){
        foreach ($result as $key => $field) {
          $package = $this->Adminmodel->getSingleColumnName($field['package_code'],'package_code','package_name','package');
          $result[$key]['package_name'] = $package;
                $result[$key]['itenerary'] = $this->Adminmodel->getSingleColumnName($field['itenerary_id'],'id','name','package_itinerary');
          $result[$key]['itineraryImg'] = 
          $this->Adminmodel->getpackagegallery($field['itinerary_code'],'package_image_details');
        }
        $data['Iteneraryname'] = $result ;
        $this->load->view('admin/itinerary_details',$data);
      }
      else{
          $url='viewPackage';
          redirect($url);
       }
    }
  }
  //For Thumnail Images
  public function resizeImage($source_path,$target_path)
  {
        $config_manip = array(
        'image_library' => 'gd2',
        'source_image' => $source_path,
        'new_image' => $target_path,
        'maintain_ratio' => TRUE,
        'create_thumb' => TRUE,
        'thumb_marker' => '',
        'width' => 150,
        'height' => 150
      );
      $this->image_lib->clear();
      $this->image_lib->initialize($config_manip);
      $this->image_lib->resize();
  } 
}
?>
