<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subsubcategory extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('form_validation');
        $this->load->library('upload');
    }
    public function index() {
         redirect('viewSubsubcategory');
        } 
    //Sub Sub Category
    public function addSubsubcategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        // to fetch the data fro category drop down
        $data['admin']="add_subsubcategories";
        $table2 ="keyaan_category";
        $start =0;
        $limit =100;
        $search ='';
        $data['category']= $this->Adminmodel->get_current_page_records($table2,$limit,$start,$column=null,$value=null,@$search,@$searchColumn);
        //****** 
        $subcat_id = $this->input->post('Subcat_id');
        $subsubcat_name   = $this->input->post('subsubcat_name');
        if($subsubcat_name!=''){
            $check_data = array(
            "subsubcat_name" => $this->input->post('subsubcat_name')    
            );
            $tablename = "keyaan_subsubcategory";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Sub Sub Category name already exist</div>');
                 $this->load->view('admin/'.$data['admin'],$data);
            }else{
                if (isset($_FILES['image']))
                {
                    $config_media['upload_path'] = './uploads/subsubcategory';
                    $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('image'))
                    {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $image    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        redirect('admin/addSubsubcategory');
                    }        
                }else{
                    $image    = "";
                }
                if (isset($_FILES['subsubcat_banner'])) {
                    $config_media1['upload_path'] = './uploads/subsubcatbanner';
                    $config_media1['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';
                    $config_media1['max_size']   = '1000000000000000'; // whatever you need
                     $this->upload->initialize($config_media1);
                    $error = [];
                    if ( ! $this->upload->do_upload('subsubcat_banner'))
                    {
                        $error1[] = array('error_image' => $this->upload->display_errors());
                    }
                    else
                    {
                        $data1[] = array('upload_image' => $this->upload->data());
                    }
                    $subsubcat_banner  = $data1[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        redirect('addSubsubcategory');
                    }
                } else {
                    $subsubcat_banner    = "";
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $icon = $this->input->post('icon')=="" ? "":$this->input->post('icon');
                $price_selection = $this->input->post('price_selection')=="" ? "":$this->input->post('price_selection');
                    if($price_selection == 1) {
                    $price = "";
                    }else{
                        $price = $this->input->post('price')=="" ? "":$this->input->post('price');
                    }
                    if($price_selection == 0) {
                        $price_from = ""; 
                        $price_to = ""; 
                    }else{
                        $price_from = $this->input->post('price_from')=="" ? "":$this->input->post('price_from'); 
                        $price_to = $this->input->post('price_to')=="" ? "":$this->input->post('price_to'); 
                    }
                $meta_tag = $this->input->post('meta_tag')=="" ? "":$this->input->post('meta_tag');
                $meta_keyword = $this->input->post('meta_keyword')=="" ? "":$this->input->post('meta_keyword'); 
                $meta_detail = $this->input->post('meta_detail')=="" ? "":$this->input->post('meta_detail');    
                $cat_id = $this->input->post('cat_id')=="" ? "":$this->input->post('cat_id');
                $subcat_id = $this->input->post('Subcat_id')=="" ? "":$this->input->post('Subcat_id');
                $dataCat = array(
                    'subcat_id'     => $subcat_id ,
                    'subsubcat_name'  => $subsubcat_name,
                    'icon'           => $icon,
                    'price_selection'  =>$price_selection,
                    'price_from'  =>$price_from,
                    'price_to'   =>  $price_to,
                    'price'       =>  $price,
                    'meta_tag'       =>  $meta_tag,
                    'meta_keyword'       =>  $meta_keyword,
                    'meta_detail'   =>  $meta_detail,
                    'image'         => $image,
                    'subsubcat_banner' => $subsubcat_banner,
                    'created_by'     => $added_by ,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'updatedby'     => $added_by,
                    'cat_id'        => $cat_id
                );
                $table="keyaan_subsubcategory";
                $result = $this->Adminmodel->insertRecordQueryList($table,$dataCat);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Subsubcategory Added</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Subsubcategory not inserted</div>') ;
                }           
                $this->load->view('admin/'.$data['admin'],$data);   
            }
        }else{
            $this->load->view('admin/add_subsubcategories',$data);
        }
    }
    public function viewSubsubcategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="keyaan_subsubcategory";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "subsubcategory/viewSubsubcategory";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'subsubcat_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'subsubcat_name');
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
        if($result){
            foreach ($result as $key => $field) {
                $subcat_id     = $this->input->post('Subcat_id') =="" ? "0":$this->input->post('subcat_id');
                $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['cat_id'],'id','category_name','keyaan_category')  ;
                $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($field['subcat_id'],'id','subcategory_name','keyaan_subcategory');
                if($field['image'] !=""){
                $result[$key]['image'] = base_url()."uploads/subsubcategory/".$field['image'];  
                }else{
                   $result[$key]['image'] = "0";   
                }
            } 
            $data['result'] = $result;
        } else {
            $result = [];
            $data['result'] = $result;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_subsubcategories',$data);
    }
    public function editSubsubcategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "keyaan_subsubcategory";
        $tablename2 = "keyaan_category";
        $start =0;
        $limit =100;
        $data['category']= $this->Adminmodel->get_current_page_records($tablename2,$limit,$start,$column=null,$value=null,@$search,@$searchColumn);
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        foreach ($result as $key => $field) {
            $subcat_id     = $this->input->post('Subcat_id') =="" ? "0":$this->input->post('subcat_id');
            $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['cat_id'],'id','category_name','keyaan_category');
            $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($field['subcat_id'],'id','subcategory_name','keyaan_subcategory');
            if($field['image'] !=""){
            $result[$key]['image'] = base_url()."uploads/subsubcategory/".$field['image'];  
            }else{
               $result[$key]['image'] = "0";   
            }
        }
        $data['result'] = $result ;
        if($result) {
            $this->load->view('admin/edit_subsubcategories',$data);
        } else {
            $url='viewSubsubcategory';
            redirect($url);
        }
        
    }
    public function updateSubsubcategory(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $subsubcat_name = $this->input->post('subsubcat_name');
        $cat_id = $this->input->post('cat_id');
        $subcat_id     = $this->input->post('Subcat_id');
        if($subsubcat_name!=''){            
            $check_data = array(
            "subsubcat_name" => $subsubcat_name,
            "subcat_id" =>$subcat_id,
            "cat_id" => $cat_id,
            "id !=" =>$id   
            );   
            $tablename = "keyaan_subsubcategory";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
             $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Subsubcategory name already exist</div>') ;
            }else{
                if($_FILES['image']['size'] > 0){
                    $config_media['upload_path'] = './uploads/subsubcategory';
                    $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                    $config_media['max_size']  = '1000000000000000'; // whatever you need
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('image')){
                     $error[] = array('error_image' => $this->upload->display_errors());    
                    }
                    else{
                     $data[] = array('upload_image' => $this->upload->data());
                    }      
                    $image    = $data[0]['upload_image']['file_name'];
                    if(count($error) >0){
                     $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                     $url='subsubcategory/editSubsubcategory/'.$id;
                    }        
                }
                if (isset($_FILES['subsubcat_banner'])) {
                    $config_media1['upload_path'] = './uploads/subsubcatbanner';
                    $config_media1['allowed_types'] = 'jpeg|gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';
                    $config_media1['max_size']   = '1000000000000000'; // whatever you need
                     $this->upload->initialize($config_media1);
                    $error = [];
                    if ( ! $this->upload->do_upload('subsubcat_banner'))
                    {
                        $error1[] = array('error_image' => $this->upload->display_errors());
                    }
                    else
                    {
                        $data1[] = array('upload_image' => $this->upload->data());
                    }
                    $subsubcat_banner  = $data1[0]['upload_image']['file_name'];
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                        redirect('addSubsubcategory');
                    }
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;         
                $date     = date("Y-m-d H:i:s");

                $icon = $this->input->post('icon')=="" ? "":$this->input->post('icon');
                $price_selection = $this->input->post('price_selection')=="" ? "":$this->input->post('price_selection');
                if($price_selection == 1) {
                     $price = "";
                }else{
                     $price = $this->input->post('price')=="" ? "":$this->input->post('price');
                }
                if($price_selection == 0) {
                    $price_from = ""; 
                    $price_to = ""; 
                }else{
                    $price_from = $this->input->post('price_from')=="" ? "":$this->input->post('price_from'); 
                    $price_to = $this->input->post('price_to')=="" ? "":$this->input->post('price_to'); 
                }
                $meta_tag = $this->input->post('meta_tag')=="" ? "":$this->input->post('meta_tag');
                $meta_keyword = $this->input->post('meta_keyword')=="" ? "":$this->input->post('meta_keyword'); 
                $meta_detail = $this->input->post('meta_detail')=="" ? "":$this->input->post('meta_detail'); 
                $id =$this->input->post('id');
                $dataSubcat = array(
                 'subcat_id' => $subcat_id,
                 'subsubcat_name'=> $subsubcat_name ,
                 'price_selection'  =>$price_selection,
                 'price_from'  =>$price_from,
                 'price_to'   =>  $price_to,
                 'price'       =>  $price,
                 'meta_tag'       =>  $meta_tag,
                 'meta_keyword'       =>  $meta_keyword,
                 'meta_detail'       =>  $meta_detail,  
                 'updated_at'     => $date,
                 'updatedby'     => $added_by,
                 'icon'  => $icon, 
                 "cat_id" => $cat_id

                );
                if($image!=""){
                 $imgArray =array('image'=> $image);
                $dataSubcat= array_merge($dataSubcat,$imgArray);
                }
                if($subsubcat_banner!=""){
                    $imgArray =array('subsubcat_banner'=> $subsubcat_banner);
                   $dataSubcat= array_merge($dataSubcat,$imgArray);
                }
                $table="keyaan_subsubcategory";
                $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
                if($result){
                     $this->session->set_flashdata('msg','<div class="alert alert-success subcatupdate updateSuss">Sub subcategory Updated</div>');
                } else{
                    $url='subsubcategory/editSubsubcategory/'.$id;
                     redirect($url);
                     $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps Some error</div>') ;
                }  
            } 
            $url='subsubcategory/editSubsubcategory/'.$id;
            redirect($url);
        } else {   
            $url='subsubcategory/editSubsubcategory/'.$id;
            redirect($url);    
        }
    }
    function subsubcatEnable($id) {
        $id=$id;
        $dataSubcat =array(
           'keyaan_status' =>'0'
        );
        $table="keyaan_subsubcategory";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewSubsubcategory';
        redirect($url);
    }      
    function subsubcatDisable($id) {
        $id=$id;
        $dataSubcat =array(
           'keyaan_status' =>'1'
        );
        $table="keyaan_subsubcategory";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewSubsubcategory';
        redirect($url);
    }
    public function subsubcatAjax(){
        $id =$this->input->post('id');
        $result = $this->Adminmodel->getAjaxdata('subcat_id',$id,'keyaan_subsubcategory');
        $data['resultSubsubcat'] =$result;
        $this->load->view('admin/subsubcatAjax',$data);
    }
    function deleteSubsubcategory($id) {
        $id=$id;
        $result = $this->Adminmodel->delmultipleImage($id,'keyaan_subsubcategory','subsubcategory','subsubcatbanner','','','image','subsubcat_banner','','','id');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    } 
}
?>