<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Portfolio extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
    }
    public function index() {
         redirect('viewPortfolio');
        } 
         
    public function addPortfolio(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
        redirect('admin');
        }
        $this->load->library('upload');
        $title =$this->input->post('title');
        if($title!=''){            
        $check_data = array(
        "title" => $this->input->post['title']    
        );
        $tablename = "portfolio";
        $checkData = $this->Adminmodel->existData($check_data,$tablename);
        if($checkData > 0){
            $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Portfolio already exist</div>') ;
        }else{
          if($_FILES['image']['size'] > 0) {
                $config_media['upload_path'] = './uploads/portfolio_image';
                $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                $config_media['max_size']   = '1000000000000000'; // whatever you need
                // $this->load->library($config_media);
                $this->upload->initialize($config_media);
                $error = [];
                if ( ! $this->upload->do_upload('image'))
                {
                    $error[] = array('error_image' => $this->upload->display_errors()); 
                }
                else
                {
                    $data[] = array('upload_image' => $this->upload->data());
                }       
                $portfolio_image = $data[0]['upload_image']['file_name'];
                if(count($error) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Portfolio image uploads</div>') ;
                    redirect('addPortfolio');
                    //echo"Portfolio image error";
                }        
            }else{
                $portfolio_image = "";
            }
            $admin = $this->session->userdata('userCode');
            $added_by = $admin!='' ? $admin:'admin' ;
            $city = $this->input->post('city')=="" ? "":$this->input->post('city');
            $details = $this->input->post('details')=="" ? "":$this->input->post('details');
            $date     = date("Y-m-d H:i:s");
            $data = array(
                'title'          => $title,
                'city'           => $city,
                'details'        => $details,
                'image'          => $portfolio_image,
                'created_by'     => $added_by ,
                'created_at'     => $date,
                'updated_at'     => $date,
                'updated_by'     => $added_by
            );
            $table="portfolio";
            $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Portfolio Inserted</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Portfolio not inserted</div>') ;
                }
                 $this->load->view('admin/add_portfolio');   
            }
        }else{
             $this->load->view('admin/add_portfolio');
            }
        }
                
    public function viewPortfolio(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="portfolio";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Expenditure/viewExpenditure";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'title');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'title');
        if($result){
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_portfolio',$data);
    }    
    public function editPortfolio(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "portfolio";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $data['result'] = $result ;
        if($result) {
           $this->load->view('admin/edit_portfolio',$data);
       } else {
           $url='viewPortfolio';
           redirect($url);
       }
            
        }
    public function updatePortfolio(){
       if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $this->load->library('upload');
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $title = $this->input->post('title');       
        if($title!=''){            
            $check_data = array(
                "title" => $title,
                "id !=" =>$id   
            );
            $tablename = "portfolio";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Portfolio already exist</div>') ;
            }else{
              if($_FILES['image']['size'] > 0) {
                    $config_media['upload_path'] = './uploads/portfolio_image';
                    $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                   // $this->load->library($config_media);
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('image')) {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $portfolio_image = $data[0]['upload_image']['file_name'];
                    $imgArr = array(
                        "image"=>$portfolio_image ,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('portfolio',$imgArr,'id',$id);
                    if(count($error) >0){
                     $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Portfolio image uploads</div>') ;
                        $url='Portfolio/editPortfolio/'.$id;
                        redirect($url);
                    }        
                }else{
                    $portfolio_image = "";
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;
                $city = $this->input->post('city')=="" ? "":$this->input->post('city');
                $details = $this->input->post('details')=="" ? "":$this->input->post('details');
                $date     = date("Y-m-d H:i:s");
                $id =$this->input->post('id');
                $data = array(
                    'title'          => $title,
                    'city'           => $city,
                    'details'        => $details,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="portfolio";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Portfolio Updated.</div>');
                }else{
                    $url='Portfolio/editPortfolio/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Portfolio not updated.</div>') ;
                }   
            } 
            $url='Portfolio/editPortfolio/'.$id;
            redirect($url);
        }else {   
            $url='Portfolio/editPortfolio/'.$id;
            redirect($url); 
        }
        
    }
        function PortfolioEnable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'keyaan_status' =>'0'
            );
            $table="portfolio";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Portfolio/viewPortfolio';
            redirect($url);
        }      
        function PortfolioDisable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'keyaan_status' =>'1'
            );
            $table="portfolio";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Portfolio/viewPortfolio';
            redirect($url);
        }
}
?>
