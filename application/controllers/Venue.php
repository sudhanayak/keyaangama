<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venue extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('image_lib');
    }
    public function index() { 
        redirect('viewVenue');          
    }
    //For Add Venue
    public function addVenue() { 
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $min='1258';
        $max='7269';
        $venueCode =rand($min,$max);
        $dataBefore =[];
        // to fetch the data from category and itinerary drop down
        $table2 ="venue_types";
        $start =0;
        $limit =100;
        $search ='';
        $dataBefore['resultvenue']= $this->Adminmodel->get_current_page_records($table2,$limit,$start,$column=null,$value=null,@$search,@$searchColumn);
        $this->load->library('upload');
        $venue_name = $this->input->post('venue_name');
        $vendor_code = $this->session->userdata('vendorCode');
        $resultcapacity = $this->Adminmodel->getAjaxdataCountry('venue_capacity');
        $dataBefore['rescapacity'] = $resultcapacity;
        $added_by = $vendor_code!='' ? $vendor_code:'vendor' ;
        $check_data = array(
            "vendor_code" => $this->session->userdata('vendorCode')    
        );
        $tablename = "vendor_categories";
        $dataExists = $this->Adminmodel->existData($check_data,$tablename) ;
        if($dataExists > 0) {
            $dataBefore['catData'] = 1;
        }
        if($venue_name!=''){            
            $check_data = array(
            "venue_name" => $this->input->post('venue_name'),
            "vendor_code" => $this->input->post('vendor_code')    
            );
            $tablename = "venues";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;           
            $date     = date("Y-m-d H:i:s");
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Venue already exist</div>') ;
                $this->load->view('vendor/add_venues',$dataBefore);
            }else{
                 if($_FILES['image']['size'] > 0) {
                $config_media['upload_path'] = './uploads/venue_images';
                $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                $config_media['max_size']   = '1000000000000000'; // whatever you need
                // $this->load->library($config_media);
                $this->upload->initialize($config_media);
                $error = [];
                if ( ! $this->upload->do_upload('image'))
                {
                    $error[] = array('error_image' => $this->upload->display_errors()); 
                }
                else
                {
                    $data[] = array('upload_image' => $this->upload->data());
                }       
                $image = $data[0]['upload_image']['file_name'];
                if(count($error) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in venue image uploads</div>') ;
                    redirect('addVenue');
                }        
            }else{
                $image = "";
            }
                $imgage=count($_FILES['venue_image']['name']);
                if($imgage !=''){
                   $i=0;
                   $filesCount = count($_FILES['venue_image']['name']);
                   for($i = 0; $i < $filesCount; $i++){
                       $_FILES['file']['name']     = $_FILES['venue_image']['name'][$i];
                       $_FILES['file']['type']     = $_FILES['venue_image']['type'][$i];
                       $_FILES['file']['tmp_name'] = $_FILES['venue_image']['tmp_name'][$i];
                       $_FILES['file']['error']     = $_FILES['venue_image']['error'][$i];
                       $_FILES['file']['size']     = $_FILES['venue_image']['size'][$i];
                       // File upload configuration
                       $uploadPath = './uploads/venue_gallery/';
                       $config9['upload_path'] = $uploadPath;
                       $config9['allowed_types'] = 'jpg|jpeg|png|gif';
                       // Load and initialize upload library
                       $this->upload->initialize($config9);
                       // Upload file to server
                       if($this->upload->do_upload('file')){
                           // Uploaded file data
                           $fileData = $this->upload->data();
                           $uploadData[$i]['file_name'] = $fileData['file_name'];
                           $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                           $source_path = './uploads/venue_gallery/' .$uploadData[$i]['file_name'];
                            $target_path = './uploads/thumbnail/venue_gallery';
                            $this->resizeImage($source_path,$target_path);
                           $data = array(
                               'vendor_code'=> $vendor_code ,
                               'venue_image' => $uploadData[$i]['file_name'],
                               'venue_code'=> $venueCode ,
                              'media_type'=>'1'
                           );
                          $result = $this->Adminmodel->insertRecordQueryList('venue_image_details',$data);
                        }
                    }
                }
                if(@$_FILES['video']['size'] > 0) {
                    $config_media6['upload_path'] = './uploads/venuevideo';
                    $config_media6['allowed_types'] = 'mp4|avi|flv|wmv|mpeg|mp3|3GPP';
                    $config_media6['max_size']  = '1000000000000000'; // whatever you need
                    //  $this->load->library($config_media6);
                    $this->upload->initialize($config_media6);
                    $error4 = [];
                    if ( ! $this->upload->do_upload('video'))
                    {
                        $error4[] = array('error_image' => $this->upload->display_errors());
                    }
                    else
                    {
                        $data7[] = array('upload_image' => $this->upload->data());
                    }
                    $imagevideo = $data7[0]['upload_image']['file_name'];
                    if(count($error4) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in video uploads</div>') ;
                        redirect('addVenue');
                    }else{
                        $data = array(
                            'vendor_code'=> $vendor_code ,
                            'venue_image' => $imagevideo,
                            'venue_code'=> $venueCode ,
                            'media_type'=>'2'
                        );
                        $result = $this->Adminmodel->insertRecordQueryList('venue_image_details',$data);
                    }
                }else{
                    $imagevideo = "";
                }    
                $venue_name = $this->input->post('venue_name')=="" ? "":$this->input->post('venue_name');
                $venue_type = $this->input->post('venue_type') =="" ? "":$this->input->post('venue_type');
                $capacity_id = $this->input->post('capacity_id') =="" ? "":$this->input->post('capacity_id');
                $venue_detail = $this->input->post('venue_detail') =="" ? "":$this->input->post('venue_detail');
                $address = $this->input->post('address') =="" ? "":$this->input->post('address');
                $price_start = $this->input->post('price_start')=="" ? "":$this->input->post('price_start');
                $facility_id = $this->input->post('facility_id')=="" ? "":$this->input->post('facility_id');
                $facilityCount = count($_POST['facility_id']);
                   for($i = 0; $i < $facilityCount; $i++){
                        $data = array(
                               'vendor_code'=> $vendor_code ,
                               'facility_id'=> $facility_id[$i],
                               'venue_code'=> $venueCode
                           );
                    $result = $this->Adminmodel->insertRecordQueryList('venue_facilities',$data);
                    }
                $data = array(
                    'venue_name'=> $venue_name ,
                    'venue_type' =>  $venue_type,
                    'venue_detail' =>  $venue_detail,
                    'address' =>  $address,
                    'venue_code'=> $venueCode,
                    'vendor_code' =>  $vendor_code,
                    'capacity_id'=> $capacity_id,
                    'price_start'  =>  $price_start,
                    'created_at'     => $date,
                    'updated_at'     => $date,
                    'image'=> $image
                );
                $table="venues";
                $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Venue Inserted</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Venue not inserted</div>') ;
                }
                $this->load->view('vendor/add_venues',$dataBefore);   
            }
        }else {
            //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
            $this->load->view('vendor/add_venues',$dataBefore);    
        } 
    }
    //Edit Theme
    public function editVenue() { 
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $dataBefore =[];  
        $venueCode = $this->uri->segment('3');
        $tablename2 = "venue_types";
        $this->load->library('upload');
        $start =0;
        $limit =100;
        $search ='';
        $vendor_code = $this->session->userdata('vendorCode');
        $added_by = $vendor_code!='' ? $vendor_code:'vendor' ;           
        $date     = date("Y-m-d H:i:s");
        if(!empty($venueCode)){
            $tablename = "venues";
            $result = $this->Adminmodel->singleRecordData('venue_code', $venueCode,$tablename);
            $dataBefore['rescapacity'] = $this->Adminmodel->getAjaxdataCountry('venue_capacity');
             $dataBefore['resultvenue'] = $this->Adminmodel->getAjaxdataCountry('venue_types');
            if($result){
                foreach ($result as $key => $field) {
                     $result[$key]['venuetype'] = $this->Adminmodel->getSingleColumnName($field['venue_type'],'id','venue_type','venue_types');
                    $result[$key]['capacity'] = $this->Adminmodel->getSingleColumnName($field['capacity_id'],'id','capacity','venue_capacity');
                   $result[$key]['facilitylist']= $this->Adminmodel->get_current_page_records('facilities',$limit,$start,'venue_type_id',$field['venue_type'],@$search,@$searchColumn);
                    $result['venueimglist'] = $this->Adminmodel->getgallery($field['venue_code'],'venue_image_details');
                    if($field['image'] !=""){
                     $result[$key]['image'] = base_url()."uploads/venue_images/".$field['image'];                    
                    } 
                }
            }
            
           
            $dataList =$this->Adminmodel->getSelectedfacilities('venue_facilities',$venueCode);
            $dataBefore['facility'] = $dataList;
            $dataBefore['result'] = $result;
            $this->load->view('vendor/edit_venue',$dataBefore);

        }else{
            redirect('Mastervendor');
        }
    }

  
    //Update Vendor
    public  function updateVenue(){
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $venueCode =$this->input->post('venue_code');
        $venue_name =$this->input->post('venue_name');
        $this->load->library('upload');
        $vendor_code = $this->session->userdata('vendorCode');
        $added_by = $vendor_code!='' ? $vendor_code:'vendor' ;           
        $date     = date("Y-m-d H:i:s");
        if(!empty($venue_name) && !empty($vendor_code)){
           if($_FILES['image']['size'] > 0) {
                    $config_media['upload_path'] = './uploads/venue_images';
                    $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                   // $this->load->library($config_media);
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('image')) {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $image = $data[0]['upload_image']['file_name'];
                    $imgArr = array(
                        "image"=>$image ,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('venues',$imgArr,'venue_code',$venueCode);
                    if(count($error) >0){
                     $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in venue image uploads</div>') ;
                        $url='Venue/editVenue/'.$venueCode;
                        redirect($url);
                    }        
                }else{
                    $image = "";
                }

          
            // for Theme Video
            $venueCode =$this->input->post('venue_code');
            if(@$_FILES['video']['size'] > 0) {
                $config_media6['upload_path'] = './uploads/venuevideo';
                $config_media6['allowed_types'] = 'mp4|avi|flv|wmv|mpeg|mp3|3GPP';  
                $config_media6['max_size']  = '1000000000000000'; // whatever you need
            //  $this->load->library($config_media6);
                $this->upload->initialize($config_media6);
                $error4 = [];
                if ( ! $this->upload->do_upload('video'))
                {
                    $error4[] = array('error_image' => $this->upload->display_errors());    
                }
                else
                {
                    $data7[] = array('upload_image' => $this->upload->data());
                }       
                $imagevideo = $data7[0]['upload_image']['file_name'];
                $imgArr20 = array(
                    "venue_image"=>$imagevideo,
                    "updated_date"=>$date  
                );   
                $where = array(
                    "vendor_code"=>$vendor_code,
                    "venue_code"=> $venueCode,
                    "media_type"=>'2'
                    );
                $resultUpdate = $this->Adminmodel->updateRecordQueryList2('venue_image_details',$imgArr20,'vendor_code',$vendor_code,$where);
                if(count($error4) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in video uploads</div>') ;
                    redirect('updateVenue');
                    //echo"video uploads error";
                }else{
                    $video="";
                }        
            }else{
                $imagevideo = "";
            }
            
            // for Multiple Theme Images
            $imgage=count($_FILES['venue_image']['name']);   
            if($imgage !=''){                               
                $i=0; 
                $filesCount = count($_FILES['venue_image']['name']);
                for($i = 0; $i < $filesCount; $i++){
                $_FILES['file']['name']     = $_FILES['venue_image']['name'][$i];
                $_FILES['file']['type']     = $_FILES['venue_image']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['venue_image']['tmp_name'][$i];
                $_FILES['file']['error']     = $_FILES['venue_image']['error'][$i];
                $_FILES['file']['size']     = $_FILES['venue_image']['size'][$i];
                // File upload configuration
                $uploadPath = './uploads/venue_gallery/';
                $config9['upload_path'] = $uploadPath;
                $config9['allowed_types'] = 'jpg|jpeg|png|gif';
                // Load and initialize upload library
                $this->upload->initialize($config9);
                // Upload file to server
                    if($this->upload->do_upload('file')){
                        // Uploaded file data
                        $fileData = $this->upload->data();
                        $uploadData[$i]['file_name'] = $fileData['file_name'];
                        $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                        $source_path = './uploads/venue_gallery/' .$uploadData[$i]['file_name'];
                        $target_path = './uploads/thumbnail/venue_gallery';
                        $this->resizeImage($source_path,$target_path);
                        $imgArr20 = array(
                            "venue_image"=>$uploadData[$i]['file_name'],
                            "updated_date"=>$date,
                            "vendor_code"=>$vendor_code,
                            "venue_code"=> $venueCode,
                            "media_type"=>'1'
                        );                            
                        $resultUpdate = $this->Adminmodel->insertRecordQueryList('venue_image_details',$imgArr20);
                    }
                }
            }                
            $dateCurrent= date("Y-m-d H:i:s");
            $venue_name = $this->input->post('venue_name')=="" ? "":$this->input->post('venue_name');
            $venue_type = $this->input->post('venue_type') =="" ? "":$this->input->post('venue_type');
            $capacity_id = $this->input->post('capacity_id') =="" ? "":$this->input->post('capacity_id');
            $venue_detail = $this->input->post('venue_detail') =="" ? "":$this->input->post('venue_detail');
            $address = $this->input->post('address') =="" ? "":$this->input->post('address');
            $price_start = $this->input->post('price_start')=="" ? "":$this->input->post('price_start');
            $facility_id = $this->input->post('facility_id')=="" ? "":$this->input->post('facility_id');
            $facilityCount = count($_POST['facility_id']);
             $resultUpdate1 = $this->Adminmodel->delselectedvalue('venue_facilities',$venueCode,$vendor_code);
                   for($i = 0; $i < $facilityCount; $i++){
                        $data = array(
                                'facility_id'=> $facility_id[$i],
                                'venue_code'=> $venueCode,
                                'vendor_code'=> $vendor_code
                           );
                    $resultUpdate = $this->Adminmodel->insertRecordQueryList('venue_facilities',$data);
                    }
            $created_at = date("Y-m-d H:i:s");
            $updated_at = date("Y-m-d H:i:s");
            $created_by = 1;
            $updated_by = 1;

            $dataVendor = array(
                'venue_name'=> $venue_name ,
                'venue_type' =>  $venue_type,
                'venue_detail' =>  $venue_detail,
                'address' =>  $address,
                'vendor_code' =>  $vendor_code,
                'venue_code'=> $venueCode ,
                'capacity_id'=> $capacity_id,
                'price_start'  =>  $price_start,
                
            );   
            $tableVendor="venues";
            $result = $this->Adminmodel->updateRecordQueryList($tableVendor,$dataVendor,'venue_code',$venueCode);
            $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Venue Updated Successfully.</div>') ;
            $url='Venue/editVenue/'.$venueCode;
            redirect($url); 
        }  else{
        //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;                   
        $url='Venue/editVenue/'.$venueCode;
        redirect($url);
        }
    }
    //For View Theme
    public function viewVenue(){
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $table="venues";
        $input  = json_decode(file_get_contents('php://input'), true);
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Venue/viewVenue";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'venue_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $vendor_code = $this->session->userdata('vendorCode');
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,'vendor_code',$vendor_code,$search,'venue_name');
        if($result){
            foreach ($result as $key => $field) {
                $result[$key]['venuetype'] = $this->Adminmodel->getSingleColumnName($field['venue_type'],'id','venue_type','venue_types');
               /* $result[$key]['facility'] = $this->Adminmodel->getSingleColumnName($field['subcat_id'],'id','faciility_name','facilities') ;*/
                
            }
            $data['result'] = $result ;
        }
        else{
            $result = [];
            $data['result'] = $result ;
        }        
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('vendor/view_venue',$data);
    }
    public function venueDetails() {
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
       $input  = json_decode(file_get_contents('php://input'), true);
       $start=0;
       $perPage = 100;
       $venueCode = $this->uri->segment('3');
       //if($start!="" && $perPage!=""){
       $table="venues";
       if($venueCode !=""){
          @$column = "venue_code";
          @$value  = $venueCode;
       }
       $search ='';
      
       $result1 = $this->Adminmodel->get_current_page_records($table,$perPage,$start,@$column,@$value,@$search,@$searchColumn);
       $result = replace_attr($result1);
       if($result){
            foreach ($result as $key => $field) {
                 $result[$key]['venuetype'] = $this->Adminmodel->getSingleColumnName($field['venue_type'],'id','venue_type','venue_types') ;
                    $result[$key]['capacity'] = $this->Adminmodel->getSingleColumnName($field['capacity_id'],'id','capacity','venue_capacity');
                    $result[$key]['facilitylist']= $this->Adminmodel->get_current_page_records('facilities',$perPage,$start,'venue_type_id',$field['venue_type'],@$search,@$searchColumn);
                    $result['venueimglist'] = $this->Adminmodel->getgallery($field['venue_code'],'venue_image_details');
                    if($field['image'] !=""){
                     $result[$key]['image'] = base_url()."uploads/venue_images/".$field['image'];                    
                    } 
           }
            $dataList =$this->Adminmodel->getSelectedfacilities('venue_facilities',$venueCode);
            $dataBefore['facility'] = $dataList;
           $dataBefore['result'] = $result ;
           $this->load->view('vendor/venue_details',$dataBefore);
       }
       else{
          $url='viewVenue';
          redirect($url);
       }
    }

    function venueEnable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'0'
        );
        $table="venues";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewVenue';
            redirect($url);
    }
    function venueDisable($id) {
        $id=$id;
        $dataSubcat =array(
            'keyaan_status' =>'1'
        );
        $table="venues";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
        $url='viewVenue';
        redirect($url);
    }
    public function deleteImage(){
        $id           =$this->input->post('id');
        $table        = $this->input->post('table');
        $folder       = $this->input->post('folder');
        $selectColumn = $this->input->post('selectColumn');
        $result       = $this->Adminmodel->delImage($id,$table,$folder,$selectColumn);
        $data['result'] =$result;
        $this->load->view('vendor/deleteImgAjax',$data);
    }
    function deleteVenue($venueCode){
        $venueCode=$venueCode;
        $id = $this->uri->segment('4');
        $result = $this->Adminmodel->delImage($id,'venues','venue_images','image');
        $result1 = $this->Adminmodel->delmultipleImagefile($venueCode,'venue_image_details','venue_gallery','venuevideo','venue_image','media_type','venue_code');
        redirect($_SERVER['HTTP_REFERER']);
    }
    //For Thumnail Images
    public function resizeImage($source_path,$target_path)
   {
         $config_manip = array(
         'image_library' => 'gd2',
         'source_image' => $source_path,
         'new_image' => $target_path,
         'maintain_ratio' => TRUE,
         'create_thumb' => TRUE,
         'thumb_marker' => '',
         'width' => 150,
         'height' => 150
       );
       $this->image_lib->clear();
       $this->image_lib->initialize($config_manip);
       $this->image_lib->resize();
   }
}    
?>