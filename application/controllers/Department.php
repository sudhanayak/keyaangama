<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Department extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
    }
    public function index() {
         redirect('viewDepartment');
        } 
         
    public function addDepartment(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
        redirect('admin');
        }
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;
        $department_name =$this->input->post('department_name');
        if($department_name!=''){            
        $check_data = array(
        "department_name" => $this->input->post('department_name')    
        );
        $tablename = "department";
        $checkData = $this->Adminmodel->existData($check_data,$tablename);
        if($checkData > 0){
            $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Department name already exist</div>') ;
            $this->load->view('admin/add_department',$dataBefore);
        }else{
            $admin = $this->session->userdata('userCode');
            $added_by = $admin!='' ? $admin:'admin' ;           
            $date     = date("Y-m-d H:i:s");
            $data = array(
                'department_name'=> $department_name,
                'created_by'     => $added_by ,
                'created_at'     => $date,
                'updated_at'     => $date,
                'updated_by'     => $added_by
            );
            $table="department";
            $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Department Inserted</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Department not inserted</div>') ;
                }
                $this->load->view('admin/add_department',$dataBefore);   
            }
        }else{
            $this->load->view('admin/add_department',$dataBefore);
            }
        }
                
    public function viewDepartment(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="department";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Department/viewDepartment";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'department_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'department_name');
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
        if($result){
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_department',$data);
    }
            
           
    public function editDepartment(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "department";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $data['result'] = $result ;
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        if($result) {
           $this->load->view('admin/edit_department',$data);
       } else {
           $url='viewDepartment';
           redirect($url);
       }
            
        }
    public function updateDepartment(){
       if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $department_name = $this->input->post('department_name');       
        if($department_name!=''){            
            $check_data = array(
                "department_name" => $department_name,
                "id !=" =>$id   
            );
            $tablename = "department";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Department name already exist</div>') ;
            }else{
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;           
                $date     = date("Y-m-d H:i:s");
                $id =$this->input->post('id');
                $data = array(
                    'department_name'=> $department_name,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="department";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Department Updated.</div>');
                }else{
                    $url='Department/editDepartment/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Department not updated.</div>') ;
                }   
            } 
            $url='Department/editDepartment/'.$id;
            redirect($url);
        }else {   
            $url='Department/editDepartment/'.$id;
            redirect($url); 
        }
        
}
        function departmentEnable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'keyaan_status' =>'0'
            );
            $table="department";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Department/viewDepartment';
            redirect($url);
        }      
        function departmentDisable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'keyaan_status' =>'1'
            );
            $table="department";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Department/viewDepartment';
            redirect($url);
        }
        function deleteDepartment($id) {
            $id=$id;
            $result = $this->Adminmodel->delRow($id,'department');
            $data['result'] = $result;
            redirect($_SERVER['HTTP_REFERER']);
        }
}
?>
