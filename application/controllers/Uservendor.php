<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uservendor extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('upload');
        $this->load->library('image_lib');
        $this->load->helper("sms");
    }
    public function index() {
        if($this->session->userdata('userCCode') !=""){
            $userCode = $this->session->userdata('userCCode');
            $cartCount = $this->Adminmodel->getcartCount($userCode);
        }else{
            $cartCount=0;
        }
        $dataBefore =[];
        $dataBefore['cartCount'] = $cartCount;
        $dataBefore['vendorName'] = $this->session->userdata('vendorRegName');
        $dataBefore['vendorEmail'] = $this->session->userdata('vendorRegEmail');
        $dataBefore['vendorMobile'] = $this->session->userdata('vendorRegMobile');
        $dataBefore['cityList'] = $this->Adminmodel->getAjaxdataCountry('keyaan_cities');
        $dataBefore['resultCnt'] = $this->Adminmodel->getAjaxdataCountry('keyaan_countries');
        $resultCategory = $this->Adminmodel->getAjaxdataCountry('keyaan_category'); 
        $dataBefore['resultCat'] = $resultCategory;
        $dataBefore['categoryList'] = $resultCategory;
        $dataBefore['menuCategoryAll']=self::getCategoryList('keyaan_category');
        $dataBefore['menuPackageAll']=self::getCategoryPackList('package_category');
        $dataBefore['basicsettingsList'] = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $this->load->view('user/vendor_registration',$dataBefore);
    }
    public function addVendors(){
        if($this->session->userdata('userCCode') !=""){
            $userCode = $this->session->userdata('userCCode');
            $cartCount = $this->Adminmodel->getcartCount($userCode);
        }else{
            $cartCount=0;
        }
        $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities');
        $resultCategory = $this->Adminmodel->getAjaxdataCountry('keyaan_category');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $resultCategoryAll = self::getCategoryList('keyaan_category');   
        $resultCategoryPackAll = self::getCategoryPackList('package_category'); 
        //print_r($_FILES['basic_image']);exit;
        $dataBefore =[];  
        $date= date("Y-m-d H:i:s");  
        $created_at = date("Y-m-d H:i:s");
        $updated_at = date("Y-m-d H:i:s");
        $created_by = 1;
        $added_by = 1;
        $vendorName =$this->session->userdata('vendorRegName');
        $resultCountry = $this->Adminmodel->getAjaxdataCountry('keyaan_countries');
        $dataBefore['resultCnt'] = $resultCountry; 
        $dataBefore['resultCat'] = $resultCategory;
        $dataBefore['basicsettingsList']=$resultBasicsettings; 
        $dataBefore['menuCategoryAll']=$resultCategoryAll;
        $dataBefore['menuPackageAll']=$resultCategoryPackAll;
        $dataBefore['vendorName'] = $vendorName;
        $id = $this->session->userdata('vendorRegId');
        $dataBefore['vendorEmail'] = $this->session->userdata('vendorRegEmail');
        $dataBefore['vendorMobile'] = $this->session->userdata('vendorRegMobile');
        $vendorCode = $this->session->userdata('vendorRegCode');
        if(!empty($vendorName)){
            $min1='1452';
            $max1='8569';
            $service_code =rand($min1,$max1);
            $tablename_image ="";
            if(!empty($vendorName) && !empty($vendorCode)){            
                // for logo start here 
                $tablename_image ="";
                if($_FILES['logo']['size'] > 0) {
                    $config_media['upload_path'] = './uploads/vendorlogo';
                    $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                   // $this->load->library($config_media);
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('logo')) {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $imagelogo = $data[0]['upload_image']['file_name'];
                    $imgArr = array(
                        "logo"=>$imagelogo ,
                        "updated_by"=>$added_by,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('keyaan_vendor',$imgArr,'id',$id);
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in logo uploads</div>') ;
                        $url='Uservendor';
                        redirect($url);
                    }        
                }else{
                    $imagelogo = "";
                }
                if($_FILES['basic_image']['size'] > 0) {
                    $config_media['upload_path'] = './uploads/basic_image';
                    $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                   // $this->load->library($config_media);
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('basic_image'))
                    {
                        $error2[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else
                    {
                        $data1[] = array('upload_image' => $this->upload->data());
                    }       
                    $basic_image = $data1[0]['upload_image']['file_name'];
                    $imgArr = array(
                        "basic_image"=>$basic_image ,
                        "updated_by"=>$added_by,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('keyaan_vendor',$imgArr,'id',$id);
                    if(count($error) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in basic_image uploads</div>') ;
                        $url='Uservendor';
                        redirect($url);
                        
                    }        
                }else{
                    $basic_image = "";
                }
                if($_FILES['bank_upload_cancel_cheque']['size'] > 0) {
                    $config_media2['upload_path'] = './uploads/check_image';
                    $config_media2['allowed_types'] = 'gif|jpg|png|jpeg';   
                    $config_media2['max_size']  = '1000000000000000'; // whatever you need
                   // $this->load->library($config_media2);
                    $this->upload->initialize($config_media2);
                    $error2 = [];
                    if ( ! $this->upload->do_upload('bank_upload_cancel_cheque'))  {
                        $error2[] = array('error_image' => $this->upload->display_errors());    
                    }
                    else  {
                        $data2[] = array('upload_image' => $this->upload->data());
                    }       
                    $bank_upload_cancel_cheque = $data2[0]['upload_image']['file_name'];
                    $imgArr2 = array(
                        "bank_upload_cancel_cheque"=>$bank_upload_cancel_cheque ,
                        "updated_by"=>$added_by,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('keyaan_vendor',$imgArr2,'id',$id);
                    if(count($error2) >0){
                        $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in bank_upload_cancel_cheque uploads</div>') ;
                        $url='Uservendor';
                        redirect($url);
                    }        
                }else{
                    $bank_upload_cancel_cheque = "";
                }  
                if($_FILES['proofImg']['size'] > 0) {
                    $config_media3['upload_path'] = './uploads/proofimage';
                    $config_media3['allowed_types'] = 'gif|jpg|png|jpeg';   
                    $config_media3['max_size']  = '1000000000000000'; // whatever you need
                  //  $this->load->library($config_media3);
                    $this->upload->initialize($config_media3);
                    $error = [];
                    if ( ! $this->upload->do_upload('proofImg')) {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else {
                        $data5[] = array('upload_image' => $this->upload->data());
                    }       
                    $proofImg = $data5[0]['upload_image']['file_name'];
                    $imgArr2 = array(
                        "proofImg"=>$proofImg,
                        "updated_by"=>$added_by,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('keyaan_vendor',$imgArr2,'id',$id);
                    if(count($error) >0){
                       $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in proof image uploads</div>') ;
                      $url='Uservendor';
                      redirect($url);
                    }        
                }else{
                    $proofImg = "";
                } 
                // for video proofImg
            /*if(@$_FILES['video']['size'] > 0) {
                $config_media6['upload_path'] = './uploads/vendorvideo';
                $config_media6['allowed_types'] = 'mp4|avi|flv|wmv|mpeg|mp3|3GPP';  
                $config_media6['max_size']  = '1000000000000000'; // whatever you need
              //  $this->load->library($config_media6);
                $this->upload->initialize($config_media6);
                $error4 = [];
                if ( ! $this->upload->do_upload('video'))
                {
                    $error4[] = array('error_image' => $this->upload->display_errors());    
                }
                else
                {
                    $data7[] = array('upload_image' => $this->upload->data());
                }       
                $imagevideo = $data7[0]['upload_image']['file_name'];
                if(count($error4) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in video uploads</div>') ;
                    redirect('addVendor');
                    //echo"video uploads error";
                }else{
                    $data = array(
                        'vendor_code'=> $vendorCode ,
                        'image' => $imagevideo,
                        'keyaan_status' =>'1',
                        'media_type'=>'2'
                    );
                    $result = $this->Adminmodel->insertRecordQueryList('vendor_image_details',$data);
                }        
            }else{
                $imagevideo = "";
            }*/
                 
                // for logo end here    
                $imgage=count($_FILES['image']['name']);   
                if($imgage !=''){                               
                    $i=0; 
                    $filesCount = count($_FILES['image']['name']);
                    for($i = 0; $i < $filesCount; $i++){
                    $_FILES['file']['name']     = $_FILES['image']['name'][$i];
                    $_FILES['file']['type']     = $_FILES['image']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['image']['tmp_name'][$i];
                    $_FILES['file']['error']     = $_FILES['image']['error'][$i];
                    $_FILES['file']['size']     = $_FILES['image']['size'][$i];
                    // File upload configuration
                    $uploadPath = './uploads/vendorimage/';
                    $config9['upload_path'] = $uploadPath;
                    $config9['allowed_types'] = 'jpg|jpeg|png|gif';
                    // Load and initialize upload library
                    $this->upload->initialize($config9);
                    // Upload file to server
                        if($this->upload->do_upload('file')){
                            // Uploaded file data
                            $fileData = $this->upload->data();
                            $uploadData[$i]['file_name'] = $fileData['file_name'];
                            $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                            $source_path = './uploads/vendorimage/' .$uploadData[$i]['file_name'];
                            $target_path = './uploads/thumbnail/vendorimage';
                            $this->resizeImage($source_path,$target_path);
                            $imgArr20 = array(
                                "image"=>$uploadData[$i]['file_name'],
                                "updated_by"=>$added_by,
                                "updated_date"=>$date,
                                "vendor_code"=>$vendorCode,
                                "keyaan_status"=>'1',
                                "media_type"=>'1'
                            );                       
                            $resultUpdate = $this->Adminmodel->insertRecordQueryList('vendor_image_details',$imgArr20);
                        }
                    }
                }
                $username     = $this->input->post('username') ==""?"":$this->input->post('username');
                $password     = $this->input->post('password') ==""?"":$this->input->post('password');  
                $name     = $this->input->post('name') ==""?"":$this->input->post('name');
                $email          = $this->input->post('email') =="" ? "":$this->input->post('email');
                $mobile         = $this->input->post('mobile') =="" ? "":$this->input->post('mobile');
                $alt_mobile     = $this->input->post('alt_mobile') =="" ? "":$this->input->post('alt_mobile');
                $proof_number   = $this->input->post('proof_number') =="" ? "":$this->input->post('proof_number');
                $proof_type     = $this->input->post('proof_type') =="" ? "0":$this->input->post('proof_type');
                $company_name   = $this->input->post('company_name') =="" ? "":$this->input->post('company_name');
                $business_name  = $this->input->post('business_name') =="" ? "":$this->input->post('business_name');
                $business_email = $this->input->post('business_email') =="" ? "":$this->input->post('business_email');
                $land_line_no   = $this->input->post('landline_number') =="" ? "0":$this->input->post('landline_number');
                $business_mobnumber= $this->input->post('business_mobnumber') =="" ? "0":$this->input->post('business_mobnumber');
                $business_pancard = $this->input->post('business_pancard') =="" ? "0":$this->input->post('business_pancard');
                $company_reg_number = $this->input->post('company_reg_number') =="" ? "0":$this->input->post('company_reg_number');
                $business_address = $this->input->post('business_address') =="" ? "":$this->input->post('business_address');
                $address_proof = $this->input->post('address_proof') =="" ? "":$this->input->post('address_proof');
                $website_link = $this->input->post('website_link') =="" ? "":$this->input->post('website_link');
                $logo = $imagelogo;
                $proofImg = $proofImg;
                $basic_image = $basic_image;
                $vendor_detail = $this->input->post('vendor_detail') =="" ? "":$this->input->post('vendor_detail');
                $about_vendor = $this->input->post('about_vendor') =="" ? "":$this->input->post('about_vendor');
                $search_text = $this->input->post('search_text') =="" ? "":$this->input->post('search_text');
                $fb_link = $this->input->post('fb_link') ==""?"":$this->input->post('fb_link');
                $twitter_link = $this->input->post('twitter_link') =="" ? "":$this->input->post('twitter_link');
                $google_plus_link = $this->input->post('google_plus_link') =="" ? "":$this->input->post('google_plus_link');
                $linkedln_link = $this->input->post('linkedln_link') =="" ? "":$this->input->post('linkedln_link');
                $cat_id = $this->input->post('cat_id') =="" ? "":$this->input->post('cat_id');
                $subcat_id = $this->input->post('subcat_id') =="" ? "":$this->input->post('subcat_id');
                $subsubcat_id = $this->input->post('subsubcat_id') =="" ? "":$this->input->post('subsubcat_id');
                $bank_name = $this->input->post('bank_name') =="" ? "":$this->input->post('bank_name');
                $bank_account_no = $this->input->post('bank_account_no') =="" ? "":$this->input->post('bank_account_no');
                $bank_upload_cancel_cheque = $bank_upload_cancel_cheque;
                $bank_branch = $this->input->post('bank_branch') =="" ? "":$this->input->post('bank_branch');
                $bank_ifsc = $this->input->post('bank_ifsc') =="" ? "":$this->input->post('bank_ifsc');
                $bank_account_name = $this->input->post('bank_account_name') =="" ? "":$this->input->post('bank_account_name');
                $place_description = $this->input->post('place_description') =="" ? "":$this->input->post('place_description');
                $country_id = $this->input->post('country_id') =="" ? "":$this->input->post('country_id');
                $state_id = $this->input->post('state_id') =="" ? "":$this->input->post('state_id');
                $district_id = $this->input->post('district_id') =="" ? "":$this->input->post('district_id');
                $city_id = $this->input->post('city_id') =="" ? "":$this->input->post('city_id');
                $pincode_id = $this->input->post('pincode_id') =="" ? "":$this->input->post('pincode_id');
                $location_id = $this->input->post('location_id') =="" ? "":$this->input->post('location_id');
                $category_base_price = $this->input->post('category_base_price') =="" ? "":$this->input->post('category_base_price');
                $subcat_base_price = $this->input->post('subcat_base_price') =="" ? "":$this->input->post('subcat_base_price');
                $sub_sub_cat_base_price = $this->input->post('sub_sub_cat_base_price') =="" ? "":$this->input->post('sub_sub_cat_base_price');            
                $landline_code = $this->input->post('landline_code') =="" ? "":$this->input->post('landline_code');
                $business_mobile_code = $this->input->post('business_mobile_code') =="" ? "":$this->input->post('business_mobile_code');
                $address = $this->input->post('address') =="" ? "":$this->input->post('address');
                $video     = $this->input->post('video') ==""?"":$this->input->post('video');

                //FOR Categories
                if($cat_id != '' && $cat_id != '0' && $cat_id != NULL && $cat_id != 'Select Category'){
                    $data = array(
                        'vendor_code'=> $vendorCode ,
                        'service_code'=> $service_code ,
                        'category_id' => $cat_id,
                    );
                    $result = $this->Adminmodel->insertRecordQueryList('vendor_categories',$data);
                } 
                //FOR Sub Categories
                if($subcat_id != '' && $subcat_id != '0' && $subcat_id != NULL && $subcat_id != 'Select Sub Category'){
                    $data = array(
                        'vendor_code'=> $vendorCode ,
                        'service_code'=> $service_code ,
                        'category_id' => $cat_id,
                        'sub_category_id' => $subcat_id,
                    );
                    $result = $this->Adminmodel->insertRecordQueryList('vendor_sub_categories',$data);
                } 
                //FOR Sub Sub Categories
                    if($subsubcat_id != '' && $subsubcat_id != '0' && $subsubcat_id != NULL && $subsubcat_id != 'Select Sub Sub Category'){
                    $data = array(
                        'vendor_code'=> $vendorCode ,
                        'service_code'=> $service_code ,
                        'category_id' => $cat_id,
                        'sub_category_id' => $subcat_id,
                        'sub_sub_category_id' => $subsubcat_id,
                    );
                    $result = $this->Adminmodel->insertRecordQueryList('vendor_sub_sub_categories',$data);
                }
                //FOR Cities
                if($city_id != '' && $city_id != '0' && $city_id != NULL && $city_id != 'Select City'){
                    $data = array(
                        'vendor_code'=> $vendorCode ,
                        'service_code'=> $service_code ,
                        'city_id' => $city_id,
                    );
                    $result = $this->Adminmodel->insertRecordQueryList('vendor_cities',$data);
                }
               //FOR Vendor Login
                $vendorData = array(
                    'vendor_code'=> $vendorCode ,
                    'username' => $username,
                    'password' => $password,
                    'useremail' => $email,
                );
                $result = $this->Adminmodel->insertRecordQueryList('vendor_login',$vendorData);
                //For video
                $vendorData = array(
                    'vendor_code'=> $vendorCode ,
                    'image'=>$video ,
                    'keyaan_status' =>'1',
                    'media_type'=>'2'                    
                );
                $result = $this->Adminmodel->insertRecordQueryList('vendor_image_details',$vendorData);
                $dataVendor = array(
                    'username'=>$username,
                    'password'=>$password,
                    'name'=>$name,
                    'email' => $email,
                    'mobile' => $mobile,
                    'alt_mobile' => $alt_mobile,
                    'proof_type' => $proof_type,
                    'proof_number' => $proof_number,
                    'address' => $address,
                    'company_name' => $company_name,
                    'business_name' => $business_name,
                    'business_email' => $business_email,
                    'landline_number' => $land_line_no,
                    'landline_code'=> $landline_code,
                    'business_mobnumber'=> $business_mobnumber,
                    'business_mobile_code'=>$business_mobile_code,
                    'business_pancard' => $business_pancard,
                    'company_reg_number' => $company_reg_number,
                    'business_address' => $business_address,
                    'address_proof' => $address_proof,
                    'website_link' => $website_link,
                    'vendor_detail' => $vendor_detail,
                    'about_vendor' => $about_vendor,
                    'search_text' => $search_text,
                    'fb_link' => $fb_link,
                    'twitter_link' => $twitter_link,
                    'google_plus_link' => $google_plus_link,
                    'linkedln_link' => $linkedln_link,
                    'updated_by' => $added_by,
                    'country_id'=>$country_id,
                    'state_id'=>$state_id,
                    'district_id'=>$district_id,
                    'city_id'=>$city_id,
                    'pincode_id'=>$pincode_id,
                    'location_id'=>$location_id,
                    'category_base_price'=>$category_base_price,
                    'subcat_base_price'=>$subcat_base_price,
                    'sub_sub_cat_base_price'=>$sub_sub_cat_base_price,
                    'bank_name'=>$bank_name,
                    'bank_account_no'=>$bank_account_no,
                    'bank_branch'=>$bank_branch,
                    'bank_ifsc'=>$bank_ifsc,
                    'bank_account_name'=>$bank_account_name,
                    'keyaan_status'=>'1'
                );
                $tableVendor="keyaan_vendor";
                $result = $this->Adminmodel->updateRecordQueryList($tableVendor,$dataVendor,'id',$id);
                $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Vendor Added Successfully</div>') ;
                $dataBefore['cartCount']=$cartCount;
                    $dataBefore['categoryList']=$resultCategory;
                    $dataBefore['cityList']=$resultCities;
                    $this->load->view('user/vendor_registration',$dataBefore);  
            }  else{
                 $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps!Some error occured, Vendor not added.</div>') ;
                 $dataBefore['cartCount']=$cartCount; 
                 $dataBefore['categoryList']=$resultCategory;
                $dataBefore['cityList']=$resultCities;
                   $this->load->view('user/vendor_registration',$dataBefore);   
            }
        } else{
           //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
            $dataBefore['categoryList']=$resultCategory;
            $dataBefore['cityList']=$resultCities;
            $dataBefore['cartCount']=$cartCount; 
            $this->load->view('user/vendor_registration',$dataBefore);  
        }
    }
    // for Vendor Registration
    public function vendorRegistartion(){
        if($this->input->post('mobile')){                  
            $where= array(
                'email'=>$this->input->post('email'),
                'mobile' =>$this->input->post('mobile'),                    
            );    
            $checkUser = $this->Adminmodel->existVendor($where);
            $json_data = array();
            if($checkUser==1){    
                //email exist           
                $json_data['status']='TRUE';
                $json_data['responseCode']=1;                    
                $json_data['message']="sucess";                    
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            }
            elseif($checkUser==2){ 
                // mobile exist              
                $json_data['status']='TRUE';
                $json_data['responseCode']=2;                    
                $json_data['message']="sucess";                    
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            } 
            elseif($checkUser==0){ 
                // insert otp and call otp screeen 
                // otp_type 1 means reg

                
               $code=rand(1000,9999);
               $mobile =$this->input->post('mobile');
               $message1 = urlencode('OTP from Keyaan is '.$code.' . Do not share it with any one.'); 
                $sendSMS = sendMobileSMS($message1,$mobile);

                $otpdata = array(
                    'mobile' =>$this->input->post('mobile'), 
                    'otp_code' =>$code,
                    'otp_type' =>1
                );
                $table = 'vendor_mobileotp';
                $otpResult = $this->Adminmodel->insertRecordQueryList($table,$otpdata);
                if($otpResult){
                    $json_data['status']='TRUE';
                    $json_data['responseCode']=0;                    
                    $json_data['message']="sucess";
                }else{
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=10;                    
                    $json_data['message']="OPPS SOME DB ERROR when insert in otp table";
                }                                    
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            } else{                            
                $json_data['status']='FALSE';
                $json_data['responseCode']=10;                    
                $json_data['message']="some error in model "; 
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            }
        }else{
            $json_data['status']='FALSE';
            $json_data['responseCode']=100;                    
            $json_data['message']="please enter all the mandatory fields"; 
            $data['json_data']= $json_data;
            $this->load->view('user/ajax_all',$data); 
        }
    }
    // this function is used for verify otp if sucess insert one record in users table 
    public function vendorVerifyOtp(){
        if($this->input->post('mobile')){                          
            $where= array(
                'mobile' =>$this->input->post('mobile'), 
                'otp_code' =>$this->input->post('code'),
                'otp_type' =>1                 
            );  
            $table = 'vendor_mobileotp';  
            $checkOtpVerify = $this->Adminmodel->existData($where,$table);
            $json_data = array();
            if($checkOtpVerify==1){ 
                $user_code = rand(1597,7493);
            // insert otp and call otp screeen 
            // otp_type 1 means reg
                $regData= array(
                    'vendor_code'=>$user_code,
                    'name' =>$this->input->post('name'), 
                    'email' =>$this->input->post('email'),
                    'mobile' =>$this->input->post('mobile'),
                );  
                $table = 'keyaan_vendor';
                $checkLogin = $this->Adminmodel->insertVendorRecord($table,$regData);
                if($checkLogin){
                    $this->session->set_userdata('isVendorRegister',TRUE);
                    $this->session->set_userdata('vendorRegId',$checkLogin['id']);
                    $this->session->set_userdata('vendorRegName',$checkLogin['name']);
                    $this->session->set_userdata('vendorRegEmail',$checkLogin['email']);
                    $this->session->set_userdata('vendorRegMobile',$checkLogin['mobile']);
                    $this->session->set_userdata('vendorRegCode',$checkLogin['vendor_code']);
                    $json_data['status']='TRUE';
                    $json_data['responseCode']=0;                    
                    $json_data['message']=$checkLogin;
                }else{
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=10;                    
                    $json_data['message']="OPPS SOME DB ERROR";
                }                                    
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            }else{                            
                $json_data['status']='FALSE';
                $json_data['responseCode']=1;                    
                $json_data['message']="otp verify fail"; 
                $data['json_data']= $json_data;
                $this->load->view('user/ajax_all',$data); 
            }
        }else{
            $json_data['status']='FALSE';
            $json_data['responseCode']=3;                    
            $json_data['message']="please enter all the mandatory fields"; 
            $data['json_data']= $json_data;
            $this->load->view('user/ajax_all',$data); 
        }
    }
    public function getCategoryList(){

        $result = $this->Adminmodel->getCategoryList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryList($field['id']);
             
        } 

        return $result ;   

    }
    public function getSubCategoryList($catId){
          
        $result = $this->Adminmodel->getSubCategoryList($catId);
        foreach ($result as $key => $field) {
             $result[$key]['subsubcatList'] = $this->Adminmodel->getSubsubCategoryList($field['id']) ;
        } 

        return $result ;   

    } 

    public function getCategoryPackList(){

        $result = $this->Adminmodel->getCategoryPackList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryPackList($field['id']); 
        } 
        return $result ;   
    }
    public function getSubCategoryPackList($catId){  
        $result = $this->Adminmodel->getSubCategoryPackList($catId);
        return $result ;   
    }
    //For Thumnail Images
    public function resizeImage($source_path,$target_path) {
        $config_manip = array(
        'image_library' => 'gd2',
        'source_image' => $source_path,
        'new_image' => $target_path,
        'maintain_ratio' => TRUE,
        'create_thumb' => TRUE,
        'thumb_marker' => '',
        'width' => 150,
        'height' => 150
      );
      $this->image_lib->clear();
      $this->image_lib->initialize($config_manip);
      $this->image_lib->resize();
  }
}
?>