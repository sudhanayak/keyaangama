<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH . '/libraries/REST_Controller.php');

/**
 * Description of RestPostController
 * 
 */
class Myservice  extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Adminmodel');
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
       
    }

     /*
    Register   servive 
    parameters--> fname,lname,username,email,mobile,password,gender
    method--> post
    
    */
    public function registerUser_post(){
        $input         = json_decode(file_get_contents('php://input'), true);  
        $fname         = $input['fname'] ;
        $lname         = $input['lname'] ;
        $username      = $input['username'] ;
        $email         = $input['email'] ;
        $mobile        = $input['mobile'];
        $password      = $input['password'];
        $gender        = $input['gender'];
        $date_of_birth = $input['date_of_birth'];
        $date_join     = date('Y-m-d H:i:s');
        $userCheck     = array(
            'username'      => $username ,
            'email'         => $email ,
            'mobile'        => $mobile ,
        );
        $data = array(
            'fname'         => $fname ,
            'lname'         => $lname ,
            'username'      => $username ,
            'email'         => $email ,
            'mobile'        => $mobile ,
            'password'      => $password ,
            'gender'        => $gender ,
            'date_of_birth' => $date_of_birth,
            'date_join'     => $date_join,
            'status'        => 0
        );
        if($fname!='' && $lname!='' && $username!='' && $email!='' && $mobile!='' && $password!='' && $gender!='' && $date_of_birth!=''){
                $checkData = $this->Adminmodel->userExist($userCheck) ;
                if($checkData > 0){
                    $json_data['status']='fail';
                    $json_data['responseCode']=array('100');
                    $json_data['message']=array("You have already register"); 
                    $this->set_response($json_data,REST_Controller::HTTP_OK);
                }
                else {
                    $result = $this->Adminmodel->regUser($data);
                    if($result){
                        // *************  for for mail need to send in server  ************
                        /* 
                            $this->load->library('email');
                            $to = $email ;
                            $from = "sudhakarnayak05@gmail.com" ;
                            $subject ='welcome messages' ;
                            $message = 'Thanks for registering with us';
                            $this->email->from($from,'admin panel');
                            $this->email->to($to);
                            $this->email->subject($subject);
                            $this->email->message($message);
                            $this->email->send();
                        */
                        $userlist =array(
                            'id' => $result,
                            'username'=>$username,
                        ) ;

                        $json_data['status']='Success';
                        $json_data['responseCode']=array('101');
                        $json_data['message']=array("Data inserted successfully."); 
                        $json_data['userDetail']=$userlist;
                        $this->set_response($json_data,REST_Controller::HTTP_OK);
                    }else{
                        $json_data['status']='fail';
                        $json_data['responseCode']=array('102');
                        $json_data['message']=array("Opp! registration fail"); 
                        $this->set_response($json_data,REST_Controller::HTTP_OK);
                    }
                    
            }
        }
        else{
            $json_data['status']='fail';
            $json_data['responseCode']=array('103');
            $json_data['message']=array("Please enter all the mandatory fields"); 
            $this->set_response($data,REST_Controller::HTTP_OK);
        }
    }
    /*
    Login servive 
    parameters--> username , password
    method-->post
    */
    public function loginUser_post(){
        $input =json_decode(file_get_contents('php://input'), true);   
         $data = array($input['username'],$input['password']);
        $result = $this->Adminmodel->userLogin($data);
        if($result)
        {
            $json_data['status']='Success';
            $json_data['responseCode']=array('104');
            $json_data['message']=array("login success");
            $userlist=$result; 
            $json_data['userDetail']=$userlist[0];
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
        else
        {
            $json_data['status']='Fail';
            $json_data['responseCode']=array('105');
            $json_data['message']=array("login fail");
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
    }
     /*
    Userlist  servive 
    parameters--> --
    method-->get

    */
    public function viewUser_get(){
        $result = $this->Adminmodel->getAllrecords();
        if($result)
        {
            $json_data['status']='Success';
            $json_data['responseCode']=array('106');
            $json_data['message']=array("Users detail  retrieved successfully.");
            $userlist=$result; 
            $json_data['userDetail']=$userlist;
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
        else
        {
            $json_data['status']='success';
            $json_data['responseCode']=array('107');
            $json_data['message']=array("Records not found");
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
    }
    
     /*
    Article  servive 
    parameters--> --
    method-->get
    
    */
    public function viewArticle_get(){
        $result = $this->Adminmodel->getArticle();
        if($result)
        {
        $json_data['status']='Success';
        $json_data['responseCode']=array('108');
        $json_data['message']=array("Article detail retrieved successfully.");
        $article=$result; 
        $json_data['articleList']=$article;
        $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
        else
        {
            $json_data['status']='success';
            $json_data['responseCode']=array('109');
            $json_data['message']=array("Records not found");
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
    }
    
     /*
    Article  servive 
    parameters--> --
    method-->get
    
    */
    public function articleDetail_get(){
        $id=$this->input->get('id');
        if($id=='')
        {
            $json_data['status']='fail';
            $json_data['responseCode']=array('108');
            $json_data['message']=array("Article id should not be blanck ");
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
        else{
            $result = $this->Adminmodel->detailArticle($id);
            if($result)
            {
                $json_data['status']='Success';
                $json_data['responseCode']=array('109');
                $json_data['message']=array("Article detail retrieved successfully.");
                $article=$result; 
                $json_data['articleDetail']=$article;
                $this->set_response($json_data,REST_Controller::HTTP_OK); 
            }
            else
            {
                $json_data['status']='success';
                $json_data['responseCode']=array('110');
                $json_data['message']=array("Records not found");
                $this->set_response($json_data,REST_Controller::HTTP_OK); 
            }
        }
    }
     /*
    Services  servive 
    parameters--> --
    method-->get
    
    */
    public function viewService_get(){
        $result = $this->Adminmodel->getAllSrvice();
        if($result)
        {
            $json_data['status']='Success';
            $json_data['responseCode']=array('111');
            $json_data['message']=array("service detail retrieved successfully.");
            $service=$result; 
            $json_data['serviceList']=$service;
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
        else
        {
            $json_data['status']='success';
            $json_data['responseCode']=array('112');
            $json_data['message']=array("Records not found");
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
    }
/*
    public function serviceDetail_get(){
        $id=$this->input->get('id');
        $result = $this->Adminmodel->detailService($id);
        if($result)
        {
            $json_data['status']='Success';
            $json_data['responseCode']=array('113');
            $json_data['message']=array("Service detail retrieved successfully.");
            $article=$result; 
            $json_data['serviceDetail']=$article;
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
        else
        {
            $json_data['status']='success';
            $json_data['responseCode']=array('114');
            $json_data['message']=array("Records not found");
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
    }
*/
    /*
    forget password  servive 
    parameters--> email
    method-->post
    
    */
    public function fogetPassword_post(){
        $input = json_decode(file_get_contents('php://input'), true);  
        $email = $input['email'];
        $result = $this->Adminmodel->forgetPwd($email);
        if($result){
            $json_data['status']='Success';
            $json_data['responseCode']=array('115');
            $json_data['message']=array("Password recovery successfully.");
            $data=$result; 
            $json_data['credentialDetail']=$data;
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
        else{
            $json_data['status']='Fail';
            $json_data['responseCode']=array('116');
            $data=$email; 
            $json_data['credentialDetail']=$data;
            $json_data['message']=array("Records not found");
            $this->set_response($json_data,REST_Controller::HTTP_OK);
        }
    }
    // insert query

    public function insertQuery_post(){
        $input = json_decode(file_get_contents('php://input'), true);  
        $username   = $input['username'];
        $question   = $input['question'];
        $query_time = date('Y-m-d H:i:s');
        $data = array(
            'query_by'    => $username ,
             'question'   => $question ,
             'query_time' => $query_time
        );
        $result = $this->Adminmodel->queInsert($data);
        if($result){
            $json_data['status']='Success';
            $json_data['responseCode']=array('117');
            $json_data['message']=array("Query inserted successfully");
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
        else{
            $json_data['status']='Fail';
            $json_data['responseCode']=array('111');
            $json_data['message']=array("Opp! query not inserted successfully");
            $this->set_response($json_data,REST_Controller::HTTP_OK);
        }
    }
    public function allQuery_get(){
        $result = $this->Adminmodel->getAllquery($data);
        if($result){
            $json_data['status']='Success';
            $json_data['responseCode']=array('118');
            $json_data['message']=array("Query inserted successfully");
            $json_data['queryDetail']=$result;;
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
        else{
            $json_data['status']='Fail';
            $json_data['responseCode']=array('119');
            $json_data['message']=array("Opp! no records found");
            $this->set_response($json_data,REST_Controller::HTTP_OK);
        }
    }

    public function getGallery_get(){
        $result= $this->Adminmodel->viewGal();
        
        if($result){
            foreach ($result as $key => $field) {
                $result[$key]['media'] = base_url().$field['media'];      
            }

            $json_data['status']='Success';
            $json_data['responseCode']=array('120');
            $json_data['message']=array("Gallery detail retrive  successfully");
            $json_data['galleryDetail']=$result ;
            $this->set_response($json_data,REST_Controller::HTTP_OK); 
        }
        else{
            $json_data['status']='Fail';
            $json_data['responseCode']=array('121');
            $json_data['message']=array("Opp! no records found");
            $this->set_response($json_data,REST_Controller::HTTP_OK);
        }
    }
    public function getServiceName($id){
        $this->db->where('id',$id);
        $query = $this->db->get('service');
        $result = $query->row();
        if($result){
            return $result->service_name ;
        }
        else{
            return false ;
        }
    }
    public function getOffer_get(){
        $id=$this->input->get('service_id');
        $result = $this->Adminmodel->getOfferService($id) ;
        if($result){
            foreach ($result as $key => $field) {
                $result[$key]['service_name'] = self::getServiceName($field['service_id']) ;      
            }
            $json_data['status']='Success';
            $json_data['responseCode']=array('122');
            $json_data['message']=array("Offer detail retrive successfully");
            $json_data['offerDetail']=$result ;
            $this->set_response($json_data,REST_Controller::HTTP_OK);    
        }
    }
    public function addOrder_post(){
        $input         = json_decode(file_get_contents('php://input'), true);  
       
        $date_join     = date('Y-m-d H:i:s');
        $data =[];
        $min = 10452;
        $max = 52896 ;
        $order_id = rand($min,$max);
        $data2 = [] ;
        $insert = 0;
        if(count($input) > 0) {
            for($v=0; $v<count($input); $v++) {
                $total_price = $input[$v]['product_qty']*$input[$v]['product_unitprice'];
                $total      = $total + $total_price ;
                $data = array(
                    'order_id'          => $order_id ,
                    'product_id'        => $input[$v]['product_id'] ,
                    'product_qty'       => $input[$v]['product_qty'] ,
                    'product_unitprice' => $input[$v]['product_unitprice']
                );
                $data2[] = $data ;
                $result = $this->Adminmodel->addOrder($data);
                if($result){
                    $insert = $insert+1;
                }
            }
        }
        if(count($input)==$insert){
        $json_data['status']='Success';
        $json_data['responseCode']=array('123');
        $json_data['message']=array("Order detail insert Successfully");
        $json_data['offerDetail']=array('order_id'=>$order_id,'total'=>$total);
        $this->set_response($json_data,REST_Controller::HTTP_OK);
        }
        else{
            $json_data['status']='Fail';
            $json_data['responseCode']=array('124');
            $json_data['message']=array("Opps sorry some error");
            $this->set_response($data2,REST_Controller::HTTP_OK);  
        } 
    } 

    // for chnage password 
    public function changePassword_post(){
        $input = json_decode(file_get_contents('php://input'), true); 
        @$username     = $input['username'];
        @$old_password = $input['old_password'];
        @$new_password =$input['new_password'];
        @$confirm_password = $input['confirm_password'];
        if($old_password=='' || $new_password=='' || $confirm_password==''){
            $json_data['status']='Fail';
            $json_data['responseCode']=array('125');
            $json_data['message']=array("Please enter mandatory fileds ");
            $this->set_response( $json_data,REST_Controller::HTTP_OK);     
            
        }
        elseif($new_password!=$confirm_password){
            $json_data['status']='Fail';
            $json_data['responseCode']=array('126');
            $json_data['message']=array("New and confirm password should be match");
            $this->set_response($json_data,REST_Controller::HTTP_OK);      
        }
         elseif($new_password==$old_password){
            $json_data['status']='Fail';
            $json_data['responseCode']=array('126');
            $json_data['message']=array("New and old password should not  be same");
            $this->set_response($json_data,REST_Controller::HTTP_OK);      
        }
        else{
                $data = array(
                    'username' => $username ,
                    'password' => $old_password,
                    
                    );
                $data1 = array(
                    'username' => $username ,
                    'password' => $new_password,
                    );    
                $result = $this->Adminmodel->checkPassword($data,$data1);
                if($result){
                    $json_data['status']='success';
                    $json_data['responseCode']=array('127');
                    $json_data['message']=array("passsword changed");
                    $this->set_response($json_data,REST_Controller::HTTP_OK); 
                }
                else{
                $json_data['status']='Fail';
                $json_data['responseCode']=array('128');
                $json_data['message']=array("Invalid Old Password");
                $this->set_response( $json_data,REST_Controller::HTTP_OK);      
            
            }
            
            
        }
        
    }

    
}    
?>