<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Venuedetail extends CI_Controller
{
  function __construct() {
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->model('Adminmodel');       
        }
  function index()
  {
    redirect('venueDetails'); 
    
  }
        public function venueDetails(){

          if($this->session->userdata('userCCode') !=""){
            $userCode = $this->session->userdata('userCCode');
            $cartCount = $this->Adminmodel->getcartCount($userCode);
          }else{
            $cartCount=0;
          }
          $data['menuCategoryAll'] = self::getCategoryList('keyaan_category');
          $resultCategoryAll = $this->Adminmodel->getAjaxdataCountry('keyaan_category'); 
          $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities'); 
          $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
         $listid=$this->uri->segment('2');   
         $catId=$this->uri->segment('3');                       
        $start=0;
        $perPage = 100;        
          $table="keyaan_vendor";         
          $result = $this->Adminmodel->themeListingDetail($table,$perPage,$start,@$listid);  
          if($result > 0){
              foreach ($result as $key => $field) {                                                            
                //For Cat Id
                $cat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','category_id','vendor_categories') ;
                $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($cat_id,'id',
                'category_name','keyaan_category') ;
                $result[$key]['priceType']= $this->Adminmodel->getSingleColumnName($cat_id,'id',
                'price_selection','keyaan_category');
                //For Subcat Id
                $subcat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','sub_category_id','vendor_sub_categories') ;
                $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($subcat_id,'id','subcategory_name','keyaan_subcategory') ;               
                //For subsubcat Id
                $subsubcat_id = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','sub_sub_category_id','vendor_sub_sub_categories') ;
                $result[$key]['subsubcategory'] = $this->Adminmodel->getSingleColumnName($subsubcat_id,'id','subsubcat_name','keyaan_subsubcategory') ;                     
                // get the state id from vendor 
                $state_id=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                'state_id','keyaan_vendor') ;
                $vendor_name=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                'name','keyaan_vendor') ;
                // get the city id from vendor 
                $city_id=$this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code',
                'city_id','keyaan_vendor') ;
                $result['vendorimgList'] = $this->Adminmodel->getgallery($field['vendor_code'],'vendor_image_details');
                $data['vendor_name'] = $vendor_name;
                $result[$key]['listingBy']=$vendor_name;
                $result[$key]['state'] = $this->Adminmodel->getSingleColumnName($state_id,'id','state_name','keyaan_states') ;
                $result[$key]['city']  = $this->Adminmodel->getSingleColumnName($city_id,'id','city_name','keyaan_cities') ;
                $data['themeimglist'] = $this->Adminmodel->getThemeImg($field['vendor_code']);
                $theme_code = $this->Adminmodel->getSingleColumnName($field['vendor_code'],'vendor_code','theme_code','vendor_theme_details') ;
                $data['othervendorThemelist'] =self::othervendorThemelist($table,$perPage,$start,$cat_id,$field['vendor_code']); 
                $resultTheme = $this->Adminmodel->themeDetail($field['vendor_code'],@$catId);
                if($resultTheme) {
                    $data['resultTheme'] =$resultTheme;
                } else {
                    $data['resultTheme'] =[];
                }
                $result[$key]['themeCount'] = $this->Adminmodel->record_count('vendor_themelist',$search=null,'cat_id');
                $data['themelistcount'] = self::themeListcount($field['vendor_code']);
            }
            $data['categoryList']=$resultCategoryAll;
            $data['cityList']=$resultCities;
            $data['basicsettingsList']=$resultBasicsettings;
            $data['cartCount']=$cartCount; 
            $data['result'] = $result ;
            $this->load->view('user/detail1',$data);
          }
          else{
             $url=base_url();
             redirect($url);
              //$result =array();
             // $data['result'] = $result;
               
              // $this->load->view('user/detail',$data);
          }        
     
      }

      public function themeListcount($vendor_code){
          $vendor_code = $vendor_code;
        $result = $this->Adminmodel->getCategoryName('vendor_themelist','vendor_code',$vendor_code);
        if($result) {
            foreach($result as $key => $field) {
                $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['cat_id'],'id','category_name','keyaan_category') ;
                $result[$key]['themelistCount'] = $this->Adminmodel->theme_count('vendor_themelist',$vendor_code,$field['cat_id']) ;
            }
            $result;
            return $result;
        } else {
            $result=[];
            return $result;
        }
      }

      public function othervendorThemelist($table,$perPage,$start,$catid,$vendorCode){
        $result2 = $this->Adminmodel->otherVendorBycat('vendor_categories','100','0',$catid,$vendorCode);
        if($result2){
           
            foreach ($result2 as $key2 => $field2) {    
                //$result2[$key2]['image'] = base_url() . "uploads/themeimage/".$this->Adminmodel->getThemeImg($field2['theme_code']);
                $state_id=$this->Adminmodel->getSingleColumnName($field2['vendor_code'],'vendor_code',
                'state_id','keyaan_vendor') ;
                $vendor_name=$this->Adminmodel->getSingleColumnName($field2['vendor_code'],'vendor_code',
                'name','keyaan_vendor') ;
                // get the city id from vendor 
                $city_id=$this->Adminmodel->getSingleColumnName($field2['vendor_code'],'vendor_code',
                'city_id','keyaan_vendor') ;
                $result2[$key2]['listingBy']=$vendor_name;
                $result2[$key2]['state'] = $this->Adminmodel->getSingleColumnName($state_id,'id','state_name','keyaan_states') ;
                $result2[$key2]['city']  = $this->Adminmodel->getSingleColumnName($city_id,'id','city_name','keyaan_cities') ;
            }
           return  $result2;
        }
        else{
            $result2=[];
            return $result2 ;
        }
      }
        
     public function booknow(){
        $this->load->library('email'); 
        if($this->input->post('listId')){
            $event_ordcode =rand(1020,0253) ; 
            $originalDate = $this->input->post('dateofevent');
            $newDate = date("Y-m-d", strtotime($originalDate));                       
            $dataBook= array(
                'event_ordcode'=>$event_ordcode,
                'vendor_code' =>$this->input->post('listId'), 
                'amount' =>$this->input->post('amount'),
                'user_email' =>$this->input->post('bemail'), 
                'user_mobile' =>$this->input->post('bmobile'),
                'location' =>$this->input->post('blocation'),
                'user_address' =>$this->input->post('baddress'), 
                'date_of_event' =>$newDate,               
                'user_name' =>$this->input->post('bname'),                
            );  
            $uname = $this->input->post('bname');
            $table = 'event_booking';  
            $result =  $this->Adminmodel->insertRecordQueryList($table,$dataBook);
            $json_data = array();
            if($result){  
         
          $message ="New Event book";
          $message = $uname." Booked an event in akeventz ";
        //                      $this->email->from('info@akevent.com', 'akeventz');
        // $this->email->to('admin@akevent.com');         
        // $this->email->subject($message);
        // $this->email->message($message);
        //         $this->email->send();   
        
                $json_data['status']='TRUE';
                $json_data['responseCode']=0;                    
                $json_data['message']=" sucessfully record inserted in booking  table";
            }else{
                $json_data['status']='FALSE';
                $json_data['responseCode']=10;                    
                $json_data['message']="OPPS SOME DB ERROR";
            }                                    
            $data['json_data']= $json_data;
            $this->load->view('user/ajax_all',$data); 
           
        }else{
            $json_data['status']='FALSE';
            $json_data['responseCode']=3;                    
            $json_data['message']="please enter all the mandatory fields"; 
            $data['json_data']= $json_data;
            $this->load->view('user/ajax_all',$data); 
        }
    }


    public function addToCart(){
       
        if($this->input->post('listId')){
            $event_ordcode =rand(1020,0253) ;           
            $dataCart= array(
                'cart_code'=>$event_ordcode,
                'item_id' =>$this->input->post('listId'), 
                'price' =>$this->input->post('amount'),
                'user_id' =>$this->input->post('userId'),          
            );              
            $table = 'keyaan_cart';  
            $result =  $this->Adminmodel->insertRecordQueryList($table,$dataCart);
            $json_data = array();
            if($result){  
                $json_data['status']='TRUE';
                $json_data['responseCode']=0;                    
                $json_data['message']=" sucessfully record inserted in booking  table";
            }else{
                $json_data['status']='FALSE';
                $json_data['responseCode']=10;                    
                $json_data['message']="OPPS SOME DB ERROR";
            }                                    
            $data['json_data']= $json_data;
            $this->load->view('user/ajax_all',$data); 
           
        }else{
            $json_data['status']='FALSE';
            $json_data['responseCode']=3;                    
            $json_data['message']="please enter all the mandatory fields"; 
            $data['json_data']= $json_data;
            $this->load->view('user/ajax_all',$data); 
        }
    }

    public function getCategoryPackList(){

        $result = $this->Adminmodel->getCategoryPackList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryPackList($field['id']); 
        } 
        return $result ;   
    }
    public function getSubCategoryPackList($catId){  
        $result = $this->Adminmodel->getSubCategoryPackList($catId);
        return $result ;   
    }




}
?>