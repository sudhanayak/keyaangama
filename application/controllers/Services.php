<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
    }
    public function index() { 
        redirect('viewService');  
    }
    //For Add service
    public function addService() {
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $min='1452';
        $max='8569';
        $serviceCode =rand($min,$max);
        $dataBefore =[];
        $vendor_code = $this->session->userdata('vendorCode');
        $resultCategory = $this->Adminmodel->getAjaxdataCountry('keyaan_category');
        $resultCity = $this->Adminmodel->getAjaxdataCountry('keyaan_cities');
        $dataBefore['city'] = $resultCity;
        $dataBefore['category'] = $resultCategory;  
        $subsubcat_id = $this->input->post('subsubcat_id'); 
        if($subsubcat_id!='' && $vendor_code!=''){ 
            $check_data = array(
            "sub_sub_category_id" => $subsubcat_id,
            "vendor_code" => $vendor_code   
            );
            $tablename = "vendor_sub_sub_categories";
            $checkData = $this->Adminmodel->existData($check_data,$tablename);
            $vendor_code = $this->session->userdata('vendorCode');
            $added_by = $vendor_code!='' ? $vendor_code:'vendor' ;           
            $date     = date("Y-m-d H:i:s");
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Service already exist</div>') ;
                $this->load->view('vendor/add_services',$dataBefore);
            }else{
                $cat_id = $this->input->post('cat_id') =="" ? "":$this->input->post('cat_id');
                $subcat_id = $this->input->post('subcat_id') =="" ? "":$this->input->post('subcat_id');
                $subsubcat_id = $this->input->post('subsubcat_id') =="" ? "":$this->input->post('subsubcat_id');
                $city_id = $this->input->post('city_id') =="" ? "":$this->input->post('city_id');
                //FOR Categories
                $data = array(
                   'vendor_code'=> $vendor_code ,
                   'service_code'=> $serviceCode ,
                   'category_id' => $cat_id,
               );
               $result = $this->Adminmodel->insertRecordQueryList('vendor_categories',$data);
               //FOR Sub Categories
               $data = array(
                   'vendor_code'=>  $vendor_code ,
                   'service_code'=> $serviceCode ,
                   'category_id' => $cat_id,
                   'sub_category_id' => $subcat_id,
               );
               $result = $this->Adminmodel->insertRecordQueryList('vendor_sub_categories',$data);
               //FOR Sub Sub Categories
               $data = array(
                   'vendor_code'=>  $vendor_code ,
                   'service_code'=> $serviceCode ,
                   'category_id' => $cat_id,
                   'sub_category_id' => $subcat_id,
                   'sub_sub_category_id' => $subsubcat_id,
               );
               $result = $this->Adminmodel->insertRecordQueryList('vendor_sub_sub_categories',$data);
               //FOR Sub Sub Categories
               $cityData = array(
                'vendor_code'=>  $vendor_code ,
                'service_code'=> $serviceCode ,
                'city_id' => $city_id,
            );
            $result = $this->Adminmodel->insertRecordQueryList('vendor_cities',$cityData);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss"> Service Inserted</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Service not inserted</div>') ;
                }
                $this->load->view('vendor/add_services',$dataBefore);   
            }
        }else {
            //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
            $this->load->view('vendor/add_services',$dataBefore);    
        } 
    }
    //Edit Service
    public function editService() {   
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $dataBefore =[]; 
        $vendor_code = $this->session->userdata('vendorCode'); 
        $service_code =$this->uri->segment(3);
        $resultCategory = $this->Adminmodel->getAjaxdataCountry('keyaan_category');
        $resultCity = $this->Adminmodel->getAjaxdataCountry('keyaan_cities');
        $dataBefore['city'] = $resultCity;
        $added_by = $vendor_code!='' ? $vendor_code:'vendor' ;           
        $date     = date("Y-m-d H:i:s");
        $table = 'vendor_sub_sub_categories';
        if(!empty($service_code) && !empty($vendor_code)){
            $result = $this->Adminmodel->get_service_details($table,$vendor_code,$service_code);
            if($result){
                foreach ($result as $key => $field) {
                $result['category'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','keyaan_category');
                $result['subcategory'] = $this->Adminmodel->getSingleColumnName($field['sub_category_id'],'id','subcategory_name','keyaan_subcategory');
                $result['subsubcategory'] = $this->Adminmodel->getSingleColumnName($field['sub_sub_category_id'],'id','subsubcat_name','keyaan_subsubcategory');
                //$city_id= $this->Adminmodel->singleRecordData($field['city_id'],'city_id','vendor_cities');
                $city_id = $this->Adminmodel->getSingleColumnName($field['service_code'],'service_code','city_id','vendor_cities');
                $result['city'] = $this->Adminmodel->getSingleColumnName($city_id,'id','city_name','keyaan_cities');
                $result['city_id'] = $city_id ;
                $result['cat_id'] = $field['category_id'] ;
                $result['subcat_id'] = $field['sub_category_id'] ;
                $result['subsubcat_id'] = $field['sub_sub_category_id'] ;
                $result['service_code'] = $service_code;
                }
                $dataBefore['result'] = replace_empty_values($result); 
                $dataBefore['category'] = $resultCategory;
                $this->load->view('vendor/edit_services',$dataBefore);
            } else {
                $url='viewService';
                redirect($url);
            }
        }else{
            redirect('Mastervendor');
        }
    }
    //Update Service
    public  function updateService(){
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $vendor_code = $this->session->userdata('vendorCode');
        $added_by = $vendor_code!='' ? $vendor_code:'vendor' ;
        $service_code = $this->input->post('service_code');           
        $date     = date("Y-m-d H:i:s");
        $subsubcat_id = $this->input->post('subsubcat_id');
        if(!empty($service_code) && !empty($vendor_code) && !empty($subsubcat_id)){
            $check_data = array(
            "sub_sub_category_id" => $this->input->post('subsubcat_id'),
            "vendor_code" => $vendor_code,
            "service_code != " => $service_code 
            );
            $tablename = "vendor_sub_sub_categories";
            $checkData = $this->Adminmodel->existData($check_data,$tablename);
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Service already exist</div>') ;
                $url='Services/editService/'.$service_code;
                redirect($url);
            }else{
            $cat_id = $this->input->post('cat_id') =="" ? "":$this->input->post('cat_id');
            $subcat_id = $this->input->post('subcat_id') =="" ? "":$this->input->post('subcat_id');
            $subsubcat_id =$this->input->post('subsubcat_id') =="" ? "":$this->input->post('subsubcat_id');
            $city_id = $this->input->post('city_id') =="" ? "":$this->input->post('city_id');
            $created_at = date("Y-m-d H:i:s");
            $updated_at = date("Y-m-d H:i:s");
            $created_by = 1;
            $updated_by = 1;
            //FOR Categories
            $catData = array(
                'category_id' => $cat_id,
                "updated_by"=>$added_by,
                "updated_date"=>$date  
            );   
            $where = array(
                "vendor_code"=>$vendor_code,
                "service_code"=>$service_code,
            );
            $resultUpdate = $this->Adminmodel->updateRecordQueryList2('vendor_categories',$catData,$where);
            //FOR Sub Categories
            $subcatData = array(
              'category_id' => $cat_id,
              'sub_category_id' => $subcat_id,
              "updated_by"=>$added_by,
              "updated_date"=>$date  
            );   
            $where = array(
                "service_code"=>$service_code,
                "vendor_code"=>$vendor_code,
            );
            $resultUpdate = $this->Adminmodel->updateRecordQueryList2('vendor_sub_categories',$subcatData,$where);
            //FOR Sub Sub Categories
            $subsubcatData = array(
              'category_id' => $cat_id,
              'sub_category_id' => $subcat_id,
              'sub_sub_category_id' => $subsubcat_id,
              "updated_by"=>$added_by,
              "updated_date"=>$date  
            );   
            $where = array(
                "vendor_code"=>$vendor_code,
                "service_code"=>$service_code,
            );
            $resultUpdate = $this->Adminmodel->updateRecordQueryList2('vendor_sub_sub_categories',$subsubcatData,$where);
            //FOR Cities
            $cityData = array(
                'city_id' => $city_id,
                "updated_by"=>$added_by,
                "updated_date"=>$date  
              );   
              $where = array(
                  "vendor_code"=>$vendor_code,
                  "service_code"=>$service_code,
              );
              $resultUpdate = $this->Adminmodel->updateRecordQueryList2('vendor_cities',$cityData,$where);
            if($resultUpdate){
                $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Service Updated Successfully.</div>');
            }else{
                  $url='Services/editService/'.$service_code;
                  redirect($url);
                  $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Service not updated.</div>') ;
                  }
            } 
            $url='Services/editService/'.$service_code;
            redirect($url);    
        }else{
        //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;                   
        $url='Services/editService/'.$service_code;
        redirect($url);
        }
    }
    //For View Service
    public function viewService(){
        if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
        $vendor_code = $this->session->userdata('vendorCode');
        $table="vendor_categories";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Services/viewService";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,'vendor_code',$vendor_code,$search,'');
        if($result){
            foreach ($result as $key => $field) {
                 $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','keyaan_category');
            }
            $data['result'] = $result ;
        }
        else{
            $result = [];
            $data['result'] = $result ;
        }     
        $this->load->view('vendor/view_services',$data);
    }
    public function serviceDetails(){
      if(!is_logged_invendor())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('Mastervendor');
        }
       $start=0;
       $perPage = 100;
       $vendor = $this->session->userdata('vendorCode');
       $service_code = $this->uri->segment('3');
       $table = 'vendor_sub_sub_categories' ; 
      
       $result1 = $this->Adminmodel->get_service_details($table,$vendor,$service_code);
       $result=replace_attr($result1);
       if($result){
            foreach ($result as $key => $field) {
              $result[$key]['category'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','keyaan_category');
              $result[$key]['subcategory'] = $this->Adminmodel->getSingleColumnName($field['sub_category_id'],'id','subcategory_name','keyaan_subcategory');
              $result[$key]['subsubcategory'] = $this->Adminmodel->getSingleColumnName($field['sub_sub_category_id'],'id','subsubcat_name','keyaan_subsubcategory');
           }
           $data['result'] = $result ;
           $this->load->view('vendor/view_servicedetails',$data);
        } else{
          $url='viewService';
          redirect($url);
       }
    }
    function ServiceEnable($service_code) {
        $service_code=$service_code;
        $dataSubcat =array(
            'keyaan_status' =>'0'
        );
        $table="vendor_categories";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'service_code',$service_code);
        $table="vendor_sub_categories";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'service_code',$service_code);
        $table="vendor_sub_sub_categories";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'service_code',$service_code);
        $url='viewService';
            redirect($url);
    }
    function ServiceDisable($service_code) {
        $service_code=$service_code;
        $dataSubcat =array(
            'keyaan_status' =>'1'
        );
        $table="vendor_categories";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'service_code',$service_code);
        $table="vendor_sub_categories";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'service_code',$service_code);
        $table="vendor_sub_sub_categories";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'service_code',$service_code);
        $url='viewService';
        redirect($url);
    }
    function deleteServices($service_code){
        $service_code=$service_code;
        $result = $this->Adminmodel->delmultipletables($service_code,'vendor_categories','service_code');
        $result1 = $this->Adminmodel->delmultipletables($service_code,'vendor_sub_categories','service_code');
        $result2 = $this->Adminmodel->delmultipletables($service_code,'vendor_sub_sub_categories','service_code');
        redirect($_SERVER['HTTP_REFERER']);
    }
}    
?>