<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Allcategory extends CI_Controller
{
	function __construct() {
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->model('Adminmodel');       
        }
	function index()
	{
        if($this->session->userdata('userCCode') !=""){
                $userCode = $this->session->userdata('userCCode');
                $cartCount = $this->Adminmodel->getcartCount($userCode);
        }else{
                $cartCount=0;
        }
        $data['menuCategoryAll'] = self::getCategoryList('keyaan_category');
        $data['menuPackageAll'] = self::getCategoryPackList('package_category');
        $this->session->unset_userdata('catId');
        $this->session->unset_userdata('cityId');
        $this->session->unset_userdata('priceList');                
        $resultCategory = $this->Adminmodel->getAjaxdataCountry('keyaan_category');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');    
        $data['categoryList']=$resultCategory;
        $data['cartCount']=$cartCount;
        $data['basicsettingsList']=$resultBasicsettings;        
        $this->load->view('user/all_categories',$data);
	}
        public function getCategoryList(){

                $result = $this->Adminmodel->getCategoryList();
                foreach ($result as $key => $field) {
                     $result[$key]['subcatList'] = self::getSubCategoryList($field['id']);
                     
                } 
        
                return $result ;   
        
            }
            public function getSubCategoryList($catId){
                  
                $result = $this->Adminmodel->getSubCategoryList($catId);
                foreach ($result as $key => $field) {
                     $result[$key]['subsubcatList'] = $this->Adminmodel->getSubsubCategoryList($field['id']) ;
                } 
        
                return $result ;   
        
            }
            public function getCategoryPackList(){
        
                $result = $this->Adminmodel->getCategoryPackList();
                foreach ($result as $key => $field) {
                     $result[$key]['subcatList'] = self::getSubCategoryPackList($field['id']); 
                } 
                return $result ;   
            }
            public function getSubCategoryPackList($catId){  
                $result = $this->Adminmodel->getSubCategoryPackList($catId);
                return $result ;   
            }
}
?>