<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modalviewdetail extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->library('image_lib');
    }
    public function index() {
        redirect('modal');
    } 
    public function modal(){
        $table="vendor_themelist";
        $id = $this->input->post('id');
        if($id !=""){
            @$column = "id";
            @$value  = $id;
        }
        $start=0;
        $perPage = 100;
        $search ='';
        $result1 = $this->Adminmodel->get_current_page_records($table,$perPage,$start,@$column,@$value,@$search,@$searchColumn);
        $result=replace_attr($result1);
        if($result){
            foreach ($result as $key => $field) {
                $data['themeimglist'] = $this->Adminmodel->getgallery($field['theme_code'],'vendor_theme_details');
            }
            $data['result'] = $result[0] ;
            $this->load->view('user/modal_view_details',$data);
        } else {
            $data['result'] = [];
            $this->load->view('user/modal_view_details',$data);
        }
    }

    public function addToCart(){
        if($this->input->post('vendorId')){
            $event_ordcode =rand(1020,7410) ;           
            $dataCart= array(
                'cart_code'=>$event_ordcode,
                'vendor_id' =>$this->input->post('vendorId'),
                'item_id' =>$this->input->post('themeId'), 
                'price' =>$this->input->post('amount'),
                'user_id' =>$this->input->post('userId'),  
                'card_type'=>'1'    
            );  
            $vendor_id = $this->input->post('vendorId');
            $item_id = $this->input->post('themeId');
            $user_id = $this->input->post('userId');
            $where = array(
                "vendor_id"=>$vendor_id,
                "item_id"=>$item_id,
                "user_id"=>$user_id,
            );         
            $table = 'keyaan_cart';  
            $itemExist = $this->Adminmodel->existData($where,$table);
            $json_data = array();
            if($itemExist > 0) {
                $json_data['status']='TRUE';
                $json_data['responseCode']=11;                    
                $json_data['message']="Item Alerady added";
            } else {
                $result =  $this->Adminmodel->insertRecordQueryList($table,$dataCart);
                if($result){  
                    $json_data['status']='TRUE';
                    $json_data['responseCode']=0;                    
                    $json_data['message']=" sucessfully record inserted in booking  table";
                }else{
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=10;                    
                    $json_data['message']="OPPS SOME DB ERROR";
                }
            }
            $data['json_data']= $json_data;
            $this->load->view('user/ajax_all',$data);
        }else{
            $json_data['status']='FALSE';
            $json_data['responseCode']=3;                    
            $json_data['message']="please enter all the mandatory fields"; 
            $data['json_data']= $json_data;
            $this->load->view('user/ajax_all',$data); 
        }
    }

    public function addFavourate(){
        if($this->input->post('userId')){
            $dataCart= array(
                'list_id' =>$this->input->post('themeId'),
                'user_id' =>$this->input->post('userId'),
            );
            $list_id = $this->input->post('themeId');
            $user_id = $this->input->post('userId');
            $where = array(
                "list_id"=>$list_id,
                "user_id"=>$user_id,
            );
            $table = 'keyaan_favourate';
            $itemExist = $this->Adminmodel->existData($where,$table);
            $json_data = array();
            if($itemExist > 0) {
                $json_data['status']='TRUE';
                $json_data['responseCode']=11;
                $json_data['message']="Item Alerady added";
            } else {
                $result =  $this->Adminmodel->insertRecordQueryList($table,$dataCart);
                if($result){
                    $json_data['status']='TRUE';
                    $json_data['responseCode']=0;
                    $json_data['message']=" sucessfully record inserted in booking  table";
                }else{
                    $json_data['status']='FALSE';
                    $json_data['responseCode']=10;
                    $json_data['message']="OPPS SOME DB ERROR";
                }
            }
            $data['json_data']= $json_data;
            $this->load->view('user/ajax_all',$data);
        }else{
            $json_data['status']='FALSE';
            $json_data['responseCode']=3;
            $json_data['message']="please enter all the mandatory fields";
            $data['json_data']= $json_data;
            $this->load->view('user/ajax_all',$data);
        }
    }

}