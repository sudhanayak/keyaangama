<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Basicsettings extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
    }
     public function index() {
         redirect('editbasicsettings');
        } 
     public function editbasicsettings(){
            $id = 1;
            if($id==''){
                redirect('adminLogin');
            }
            $tablename = "keyaan_basic_settings";
            $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
            $data['result'] = $result ;
            $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
            $data['basicsettingsList']=$resultBasicsettings;
            $this->load->view('admin/basic_settings',$data);
        }
        public function updatebasic_settings(){
            $id = $this->input->post('id');
            if(empty($id)){
                redirect('adminLogin');
            }
             $site_name = $this->input->post('site_name');      
             if($site_name!=''){            
                 $check_data = array(
                 "site_name" => $site_name,
                 "id !=" =>$id   
                 );
                 $tablename = "keyaan_basic_settings";
                 $checkData = $this->Adminmodel->existData($check_data,$tablename) ;

                 if($checkData > 0){
                     $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Details already exist</div>') ;
                 }else{
                         if($_FILES['image']['size'] > 0)
                         {

                             $config_media['upload_path'] = './uploads/site_logo';
                             $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                             $config_media['max_size']  = '1000000000000000'; // whatever you need
                             $this->load->library('upload',$config_media);
                             $error = [];
                             if ( ! $this->upload->do_upload('image'))
                             {
                                 $error[] = array('error_image' => $this->upload->display_errors());    
                             }
                             else
                             {
                                 $data[] = array('upload_image' => $this->upload->data());
                             }      
                             $image = $data[0]['upload_image']['file_name'];
                             if(count($error) >0){
                                 $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                                 redirect(editbasicsettings);
                             }        
                         }
                         if($_FILES['office_image']['size'] > 0)
                         {
                             $config_media['upload_path'] = './uploads/office_image';
                             $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';   
                             $config_media['max_size']  = '1000000000000000'; // whatever you need
                             $this->load->library('upload',$config_media);
                             $error = [];
                             if ( ! $this->upload->do_upload('office_image'))
                             {
                                 $error[] = array('error_image' => $this->upload->display_errors());    
                             }
                             else
                             {
                                 $data[] = array('upload_image' => $this->upload->data());
                             }      
                             $office_image = $data[0]['upload_image']['file_name'];
                             if(count($error) >0){
                                 $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in image uploads</div>') ;
                                 redirect(editbasicsettings);
                             }        
                         }
                             $admin = $this->session->userdata('userCode');
                             $added_by = $admin!='' ? $admin:'admin' ;          
                             $date     = date("Y-m-d H:i:s");
                             $site_email = $this->input->post('site_email')=="" ? "":$this->input->post('site_email');
                             $site_mobile = $this->input->post('site_mobile')=="" ? "":$this->input->post('site_mobile'); 
                             $site_landline = $this->input->post('site_landline')=="" ? "":$this->input->post('site_landline'); 
                             $site_address = $this->input->post('site_address')=="" ? "":$this->input->post('site_address'); 
                             $order_email = $this->input->post('order_email')=="" ? "":$this->input->post('order_email'); 
                             $enquiry_email = $this->input->post('enquiry_email')=="" ? "":$this->input->post('enquiry_email'); 
                             $customercare_number = $this->input->post('customercare_number')=="" ? "":$this->input->post('customercare_number');
                             $order_comission = $this->input->post('order_comission')=="" ? "":$this->input->post('order_comission'); 
                             $tag = $this->input->post('tag')=="" ? "":$this->input->post('tag');
                             $details = $this->input->post('details')=="" ? "":$this->input->post('details'); 
                            $keyword = $this->input->post('keyword')=="" ? "":$this->input->post('keyword');  
                             $site_footertext = $this->input->post('site_footertext')=="" ? "":$this->input->post('site_footertext'); 
                             $id =$this->input->post('id');
                             $data = array(
                                 'site_name'=> $site_name ,
                                 'site_email'  => $site_email,
                                 'site_mobile'  => $site_mobile,                            
                                 'site_landline'  => $site_landline,
                                 'site_address'  => $site_address,
                                 'order_email'  => $order_email,
                                 'enquiry_email'  => $enquiry_email,
                                 'customercare_number'  => $customercare_number,
                                 'order_comission'  => $order_comission,
                                 'site_detail'   =>  $details,
                                 'meta_tag'      =>   $tag,
                                 'meta_keyword'   =>$keyword,
                                 'site_footertext'  => $site_footertext,
                                 'updated_at'     => $date,
                                 'updated_by'     => $added_by
                             );
                             if($image!=""){
                                 $imgArray =array('site_logo'=> $image);
                                $data= array_merge($data,$imgArray);
                             }
                             if($office_image!=""){
                                 $imgArray =array('office_image'=> $office_image);
                                $data= array_merge($data,$imgArray);
                             }
                             $table="keyaan_basic_settings";
                             $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                             if($result){
                                     $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Basic Settings Updated</div>');
                                     $this->load->view('admin/basic_settings',$data);
                             }
                             else{
                                    $url='basicsettings/editbasicsettings/'.$id;
                                     redirect($url);
                                     $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps Some error</div>') ;
                             }  
                 } 
                 redirect(editbasicsettings);
             }
             else
             {   
                redirect(editbasicsettings);    
             }

         }
}
?>