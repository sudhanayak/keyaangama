<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Checkout extends CI_Controller
{
	function __construct() {
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->model('Adminmodel');       
        }
	function index()
	{

        if($this->session->userdata('userCCode') ==""){
            $url=base_url();
            redirect($url);
        }
       $this->session->unset_userdata('catId');
       $this->session->unset_userdata('cityId');
       $this->session->unset_userdata('priceList');
       if($this->session->userdata('userCCode') !=""){
             $userCode = $this->session->userdata('userCCode');
             $cartCount = $this->Adminmodel->getcartCount($userCode);
             $carTotalAmount = $this->Adminmodel->getCartTotalAmount($userCode);
           }else{
             $cartCount=0;
           }
           $data['menuCategoryAll'] = self::getCategoryList('keyaan_category');
           $data['menuPackageAll'] = self::getCategoryPackList('package_category');
       if($cartCount==0) {
          $url=base_url();
          redirect($url);
       }   

        $resultCities = $this->Adminmodel->getAjaxdataCountry('keyaan_cities');
        $resultCategory = $this->Adminmodel->getAjaxdataCountry('keyaan_category');  
        $resultCountry = $this->Adminmodel->getAjaxdataCountry('keyaan_countries');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');  
        $data['categoryList']=$resultCategory;
        $data['cityList']=$resultCities;
        $data['cartCount']=$cartCount; 
        $data['carTotalAmount']=$carTotalAmount; 
        $data['resultCnt'] = $resultCountry;
        $data['basicsettingsList']=$resultBasicsettings;

        $this->load->view('user/checkout',$data);
       
		
	}
    
    public function addOrder(){
       
        if($this->input->post('user_id')){
            $order_id =rand(1020,0253) ; 
            $date=date('Y-m-d H:i:s');
            $total_amount=  $this->input->post('total_amount')!=""?$this->input->post('total_amount'):"0";
            $advance_amount =  $this->input->post('advance_amount')!=""?$this->input->post('advance_amount'):"0";
             $remain_amount =$total_amount-$advance_amount;
            $payment_type =  $this->input->post('payment_type')!=""?$this->input->post('payment_type'):"0";
              $user_id=  $this->input->post('user_id')!=""?$this->input->post('user_id'):"0";      
            $orderData= array(
                'order_id'=>$order_id,
                'user_id' =>$user_id, 
                'total_amount' =>$total_amount,
                ' advance_amount' =>$advance_amount,
                'remain_amount' =>$remain_amount,
                'payment_type' => $payment_type,
                'payment_status'=>0,
                'order_date' =>$date

            );              
            $table = 'users_orders';  
            $result =  $this->Adminmodel->insertRecordQueryList($table,$orderData);
            $json_data = array();
            if($result){

            // stored the data in billing table
            $order_name=  $this->input->post('order_name')!=""?$this->input->post('order_name'):"0";
            $order_email=  $this->input->post('order_email')!=""?$this->input->post('order_email'):"0";
            $order_mobile=  $this->input->post('order_mobile')!=""?$this->input->post('order_mobile'):"0";
            $order_country=  $this->input->post('order_country')!=""?$this->input->post('order_country'):"0";
            $order_city=  $this->input->post('order_city')!=""?$this->input->post('order_city'):"0";
            $order_pincode=  $this->input->post('order_pincode')!=""?$this->input->post('order_pincode'):"0";
            $order_landmark=  $this->input->post('order_landmark')!=""?$this->input->post('order_landmark'):"0";
            $order_message=  $this->input->post('order_mesage')!=""?$this->input->post('order_mesage'):"0";
            $order_state=  $this->input->post('order_state')!=""?$this->input->post('order_state'):"0";
              

              $billingData= array(
                'order_id'=>$order_id,
                'user_id' =>$user_id, 
                'billing_name' =>$order_name,
                'billing_email' =>$order_email,
                'billing_mobile' =>$order_mobile,
                'billing_country' => $order_country,
                'billing_city'=>$order_city,
                'billing_pincode' =>$order_pincode,
                'billing_landmark'=>$order_landmark,
                'billing_message'=>$order_message,
                'billing_state'=>$order_state,

            );              
            $table = 'users_billing_address';  
            $result =  $this->Adminmodel->insertRecordQueryList($table,$billingData);


        $userCode = $this->session->userdata('userCCode');
        $result2 = $this->Adminmodel->orderCart($userCode);
       if($result2){
           foreach ($result2 as $key => $field) {  
            if($field['item_id'] != '0') {
                $card_type = 1;
                $package_id = '0';
                $event_date= date('Y-m-d H:i:s');
                $dataOrder_detail = array(
                'order_id'=>$order_id,
                'user_id' =>$userCode,
                'vendor_id' =>$field['vendor_id'], 
                'item_id' =>$field['item_id'],
                'card_type' =>$card_type,
                'package_id' =>$package_id,
                'price'=>$field['price'],
                'event_date'=>$event_date
                );
                $table22 = 'orders_detail';  
                $result30 =  $this->Adminmodel->insertRecordQueryList($table22,$dataOrder_detail);
            } else  {
                $item_id = '0';
                $card_type = 2;
                $result3 = $this->Adminmodel->packagecartDetail($userCode);
                foreach ($result3 as $key => $field3) { 
                    $event_date= date('Y-m-d H:i:s');
                    $Order_detail = array(
                    'order_id'=>$order_id,
                    'user_id' =>$userCode,
                    'vendor_id' =>$field['vendor_id'], 
                    'item_id' =>$item_id,
                    'card_type' =>$card_type,
                    'package_id' =>$field3['package_id'],
                    'itinerary_id' =>$field3['iteinerary_id'],
                    'price'=>$field3['itinerary_price'],
                    'event_date'=>$event_date
                    ); 
                    
                    $result30 =  $this->Adminmodel->insertRecordQueryList('orders_detail',$Order_detail);


                }
            }      
              
            if($result30){
              // delete items form cart 
              $userCode=$this->session->userdata('userCCode');
              $result = $this->Adminmodel->delCartAll($userCode);
              $resultData=$result;

            }
         } 
      }   

                $json_data['status']='TRUE';
                $json_data['responseCode']=0;                    
                $json_data['message']=" sucessfully record inserted in booking  table";
            }else{
                $json_data['status']='FALSE';
                $json_data['responseCode']=10;                    
                $json_data['message']="OPPS SOME DB ERROR";
            }                                    
           $data['json_data']= $json_data;
           $this->load->view('user/ajax_all',$data);           
        }else{
            $json_data['status']='FALSE';
            $json_data['responseCode']=3;                    
            $json_data['message']="please enter all the mandatory fields"; 
            $data['json_data']= $json_data;
	    $this->load->view('user/ajax_all',$data);      
            }
    }

    function deleteCart($id) {
        $id=$id;
        $userCode=$this->session->userdata('userCCode');
        $result = $this->Adminmodel->delCart($id,$userCode);
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function getCategoryList(){

        $result = $this->Adminmodel->getCategoryList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryList($field['id']);
             
        } 

        return $result ;   

    }
    public function getSubCategoryList($catId){
          
        $result = $this->Adminmodel->getSubCategoryList($catId);
        foreach ($result as $key => $field) {
             $result[$key]['subsubcatList'] = $this->Adminmodel->getSubsubCategoryList($field['id']) ;
        } 

        return $result ;   

    }
    public function getCategoryPackList(){

        $result = $this->Adminmodel->getCategoryPackList();
        foreach ($result as $key => $field) {
             $result[$key]['subcatList'] = self::getSubCategoryPackList($field['id']); 
        } 
        return $result ;   
    }
    public function getSubCategoryPackList($catId){  
        $result = $this->Adminmodel->getSubCategoryPackList($catId);
        return $result ;   
    }

    //Logout Functionality 
    public function logout(){
        $this->session->unset_userdata('isCUserLoggedIn');
        $this->session->unset_userdata('userCId');
        $this->session->unset_userdata('userCCode');
        $url=base_url();
        redirect($url);
    } 



}
?>