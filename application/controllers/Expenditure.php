<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Expenditure extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
    }
    public function index() {
         redirect('viewExpenditure');
        } 
         
    public function addExpenditure(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
        redirect('admin');
        }
        $this->load->library('upload');
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $dataBefore['basicsettingsList']=$resultBasicsettings;
        $expenditure_name =$this->input->post('expenditure_name');
        if($expenditure_name!=''){            
        $check_data = array(
        "expenditure_name" => $this->input->post('expenditure_name')
        );
        $tablename = "keyaan_expenditure";
        $checkData = $this->Adminmodel->existData($check_data,$tablename);
        if($checkData > 0){
            $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Expenditure name already exist</div>') ;
            $this->load->view('admin/add_expenditure',$dataBefore);
        }else{
          if($_FILES['receipt']['size'] > 0) {
                $config_media['upload_path'] = './uploads/receiptimage';
                $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                $config_media['max_size']   = '1000000000000000'; // whatever you need
                // $this->load->library($config_media);
                $this->upload->initialize($config_media);
                $error = [];
                if ( ! $this->upload->do_upload('receipt'))
                {
                    $error[] = array('error_image' => $this->upload->display_errors()); 
                }
                else
                {
                    $data[] = array('upload_image' => $this->upload->data());
                }       
                $receipt_image = $data[0]['upload_image']['file_name'];
                if(count($error) >0){
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Receipt image uploads</div>') ;
                    redirect('addExpenditure');
                   
                }        
            }else{
                $receipt_image = "";
            }
            $admin = $this->session->userdata('userCode');
            $added_by = $admin!='' ? $admin:'admin' ;
            $exp_date= $this->input->post('date')=="" ? "":$this->input->post('date');
            $amount = $this->input->post('amount')=="" ? "":$this->input->post('amount');
            $date     = date("Y-m-d H:i:s");
            if($exp_date!=""){
              $date_array = explode("/",$exp_date); // split the array
              $var_day = $date_array[0]; //day seqment
              $var_month = $date_array[1]; //month segment
              $var_year = $date_array[2]; //year segment
              $date1= "$var_year-$var_month-$var_day"; // join them together
           }
            $data = array(
                'expenditure_name'=> $expenditure_name,
                'date'           => $date1 ,
                'amount'         => $amount,
                'receipt'        => $receipt_image,
                'created_by'     => $added_by ,
                'created_at'     => $date,
                'updated_at'     => $date,
                'updated_by'     => $added_by
            );
            $table="keyaan_expenditure";
            $result = $this->Adminmodel->insertRecordQueryList($table,$data);
                if($result){
                    $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Expenditure Inserted</div>');
                }
                else{
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! Expenditure not inserted</div>') ;
                }
                 $this->load->view('admin/add_expenditure',$dataBefore);   
            }
        }else{
             $this->load->view('admin/add_expenditure',$dataBefore);
            }
        }
                
    public function viewExpenditure(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="keyaan_expenditure";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Expenditure/viewExpenditure";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'expenditure_name');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'expenditure_name');
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
        if($result){
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result ;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_expenditure',$data);
    }    
    public function editExpenditure(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $id = $this->uri->segment('3');
        if($id==''){
            redirect('adminLogin');
        }
        $tablename = "keyaan_expenditure";
        $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        foreach ($result as $key => $field) {
           $result[$key]['date'] = date("d/m/Y", strtotime($field['date']));
        }
        $data['result'] = $result ;
        if($result) {
           $this->load->view('admin/edit_expenditure',$data);
       } else {
           $url='viewExpenditure';
           redirect($url);
       }
            
        }
    public function updateExpenditure(){
       if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $this->load->library('upload');
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('adminLogin');
        }
        $expenditure_name = $this->input->post('expenditure_name');       
        if($expenditure_name!=''){            
            $check_data = array(
                "expenditure_name" => $expenditure_name,
                "id !=" =>$id   
            );
            $tablename = "keyaan_expenditure";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Expenditure name already exist</div>') ;
            }else{
              if($_FILES['receipt']['size'] > 0) {
                    $config_media['upload_path'] = './uploads/receiptimage';
                    $config_media['allowed_types'] = 'gif|jpg|png|mp4|avi|flv|wmv|mpeg|mp3';    
                    $config_media['max_size']   = '1000000000000000'; // whatever you need
                   // $this->load->library($config_media);
                    $this->upload->initialize($config_media);
                    $error = [];
                    if ( ! $this->upload->do_upload('receipt')) {
                        $error[] = array('error_image' => $this->upload->display_errors()); 
                    }
                    else {
                        $data[] = array('upload_image' => $this->upload->data());
                    }       
                    $receipt_image = $data[0]['upload_image']['file_name'];
                    $imgArr = array(
                        "receipt"=>$receipt_image ,
                        "updated_at"=>$date  
                    );
                    $resultUpdate = $this->Adminmodel->updateRecordQueryList('keyaan_expenditure',$imgArr,'id',$id);
                    if(count($error) >0){
                     $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">opp! error in Receipt image uploads</div>') ;
                        $url='Expenditure/editExpenditure/'.$id;
                        redirect($url);
                    }        
                }else{
                    $receipt_image = "";
                }
                $admin = $this->session->userdata('userCode');
                $added_by = $admin!='' ? $admin:'admin' ;
                $exp_date= $this->input->post('date')=="" ? "":$this->input->post('date');
                $amount = $this->input->post('amount')=="" ? "":$this->input->post('amount');
                $date     = date("Y-m-d H:i:s");
                $id =$this->input->post('id');
                if($exp_date!=""){
                   $date_array = explode("/",$exp_date); // split the array
                   $var_day = $date_array[0]; //day seqment
                   $var_month = $date_array[1]; //month segment
                   $var_year = $date_array[2]; //year segment
                   $date1= "$var_year-$var_month-$var_day"; // join them together
                }
                $data = array(
                    'expenditure_name'=> $expenditure_name,
                    'date'           => $date1,
                    'amount'         => $amount,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="keyaan_expenditure";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Expenditure Updated.</div>');
                }else{
                    $url='Expenditure/editExpenditure/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Expenditure not updated.</div>') ;
                }   
            } 
            $url='Expenditure/editExpenditure/'.$id;
            redirect($url);
        }else {   
            $url='Expenditure/editExpenditure/'.$id;
            redirect($url); 
        }
        
    }
        function ExpenditureEnable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'keyaan_status' =>'0'
            );
            $table="keyaan_expenditure";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Expenditure/viewExpenditure';
            redirect($url);
        }      
        function ExpenditureDisable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'keyaan_status' =>'1'
            );
            $table="keyaan_expenditure";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Expenditure/viewExpenditure';
            redirect($url);
        }
        function deleteExpenditure($id) {
        $id=$id;
        $result = $this->Adminmodel->delImage($id,'keyaan_expenditure','receiptimage','receipt');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }
}


