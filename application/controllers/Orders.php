<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Orders extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
    }
    public function index() {
         redirect('viewOrders');
        } 

    public function viewOrders(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="users_orders";
        $search = ($this->input->get("search"))? $this->input->get("search") : "null";
       $config = array();
       $config['reuse_query_string'] = true;
       $config["base_url"] = base_url() . "Orders/viewOrders";
       $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
       $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'order_id');//search
       $config["per_page"] = PERPAGE_LIMIT;
       $config["uri_segment"] = 3;
       $config['full_tag_open'] = "<ul class='pagination'>";
       $config['full_tag_close'] = '</ul>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active"><a href="#">';
       $config['cur_tag_close'] = '</a></li>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';
       $config['prev_link'] = '<i class="zmdi zmdi-chevron-left"></i>';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '<i class="zmdi zmdi-chevron-right"></i>';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $limit =$config["per_page"];
       $start=$page;
       $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,$column=null,$value=null,$search,'order_id');
       $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
       $data['basicsettingsList']=$resultBasicsettings;
        if($result){
            foreach ($result as $key => $field) {
                $result[$key]['user_name'] = $this->Adminmodel->getSingleColumnName($field['user_id'],'id','name','users');
                if($field['payment_status'] == 0) {
                    $result[$key]['payment_status'] = 'Not Paid';
                } elseif($field['payment_status'] == 1) {
                    $result[$key]['payment_status'] = 'Paid';
                }
                if($field['payment_type'] == 0) {
                    $result[$key]['payment_type'] = 'COD';
                } elseif($field['payment_type'] == 1) {
                    $result[$key]['payment_type'] = 'Online';
                }
                if($field['advance_amount'] == 0 || $field['advance_amount'] == '') {
                    $result[$key]['advance_amount'] = '--';
                } else {
                    $result[$key]['advance_amount'] = $field['advance_amount'];
                }
            }
            $data['result'] = $result ;
        } else {
            $result[] = [] ;
            $data['result'] = $result;
        }
        $data['searchVal'] = $search !='null'?$search:"";
        $this->load->view('admin/view_orders',$data);
    }     
    public function orderDetails(){
        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
        {
          redirect('admin');
        }
        $table ="users_orders";
        $order_id = $this->uri->segment('3');
        $user_id = $this->uri->segment('4');
        $result = $this->Adminmodel->orderDetails($table,$order_id,$user_id);
        $resultBasicsettings = $this->Adminmodel->getAjaxdataSettings('keyaan_basic_settings');
        $data['basicsettingsList']=$resultBasicsettings;
        foreach ($result as $key => $field) {
            $result[$key]['user_name'] = $this->Adminmodel->getSingleColumnName($field['user_id'],'id','name','users');
            if($field['payment_status'] == 0) {
                $data['payment_status'] = 'Not Paid';
            } elseif($field['payment_status'] == 1) {
                $data['payment_status'] = 'Paid';
            }
            if($field['payment_type'] == 0) {
                $data['payment_type'] = 'COD';
            } elseif($field['payment_type'] == 1) {
                $data['payment_type'] = 'Online';
            }
            if($field['advance_amount'] == 0 || $field['advance_amount'] == '') {
                $data['advance_amount'] = '--';
            } else {
                $data['advance_amount'] = $field['advance_amount'];
            }
        }
        $eventDetails = $this->Adminmodel->orderEventDetails('orders_detail',$order_id,$user_id);
        foreach ($eventDetails as $key => $field) {
            $eventDetails[$key]['itemName']= $this->Adminmodel->getSingleColumnName($field['item_id'],'id','theme_name','vendor_themelist') ;
        }
        $packageDetails = $this->Adminmodel->orderPackageDetails('orders_detail',$order_id,$user_id);
        foreach ($packageDetails as $key => $field) {
            $packageDetails[$key]['packageName']= $this->Adminmodel->getSingleColumnName($field['package_id'],'package_code','package_name','package') ;
            $packageDetails[$key]['itineraryName']= $this->Adminmodel->getSingleColumnName($field['itinerary_id'],'itinerary_code','itenerary_name','package_detail') ;
        }

        $billingDetails = $this->Adminmodel->orderDetails('users_billing_address',$order_id,$user_id);
        foreach ($billingDetails as $key => $field) {
            $billingDetails[$key]['country']= $this->Adminmodel->getSingleColumnName($field['billing_country'],'id','country_name','keyaan_countries') ;
            $billingDetails[$key]['state']= $this->Adminmodel->getSingleColumnName($field['billing_state'],'id','state_name','keyaan_states') ;
            $billingDetails[$key]['city']= $this->Adminmodel->getSingleColumnName($field['billing_city'],'id','city_name','keyaan_cities') ;
            $billingDetails[$key]['pincode']= $this->Adminmodel->getSingleColumnName($field['billing_pincode'],'id','pincode','keyaan_pincodes') ;
        }
        if($result && $eventDetails || $packageDetails && $billingDetails){
            $data['eventDetails'] = $eventDetails;
            $data['packageDetails'] = $packageDetails;
            $data['billingDetails'] = $billingDetails[0];
            $data['result'] = $result[0] ;
            $this->load->view('admin/order_details',$data);
        } else {
            redirect('viewOrders');
        }
    }
    //For Approve or Disapprove
    function orderApprove($id) {
        $id=$id;
        $dataOrder =array(
            'approve_by_admin' =>'1'
        );
        $table="users_orders";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataOrder,'id',$id);
        $url='viewOrders';
        redirect($url);
    }      
    function orderDisapprove($id) {
        $id=$id;
        $dataOrder =array(
            'approve_by_admin' =>'0'
        );
        $table="users_orders";
        $result = $this->Adminmodel->updateRecordQueryList($table,$dataOrder,'id',$id);
        $url='viewOrders';
        redirect($url);
    }
}
?>
