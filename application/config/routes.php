<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//Dashboard
$route['MyDasboard'] = 'MyDasboard/viewMyProfile';
$route['UpdateMyProfile'] = 'MyDasboard/UpdateMyProfile';
$route['ViewChangePassword'] = 'MyDasboard/ViewChangePassword';

$route['default_controller'] = 'user';
$route['listing'] = 'Listing/themeListing';
$route['listing/:num'] = 'Listing/themeListing';
$route['listing/:any/:any'] = 'Listing/themeListing';
$route['listing/:any/:any/:any'] = 'Listing/themeListing';
$route['detail/:num'] = 'Detail/detailListing';
$route['detail/:any/:any'] = 'Detail/detailListing';
$route['packageDetailList'] = 'Packagedetails/packageDetailList';
$route['venueListing/:any'] = 'Venuelist/venueListing';
$route['venueListing/:any/:any'] = 'Venuelist/venueListing';
$route['venueListing/:any/:any/:any'] = 'Venuelist/venueListing';
$route['venueDetails/:any'] = 'Venuedetail/venueDetails';
$route['venueDetails/:any/:any'] = 'Venuedetail/venueDetails';
$route['packageDetailList/:num'] = 'Packagedetails/packageDetailList';
$route['Custompackage/:num'] = 'Custompackage/packageDetailList';
$route['catering/:any'] = 'CateringListing/cateringList';
$route['catering/:any/:any'] = 'CateringListing/cateringList';
$route['catering/:any/:any/:any'] = 'CateringListing/cateringList';// for package list
$route['packagelist/:num'] = 'Packagelist/packageListing';
$route['Wishlist'] = 'MyDasboard/wishlist';
$route['subcatlist/:any'] = 'Subcatlisting/subcatlist';

$route['admin'] = 'admin';
$route['addVendor'] = 'vendor/addVendor';
$route['viewVendors'] = 'vendor/viewVendors';
$route['vendorDetails'] = 'vendor/VendorDetails';
$route['addCategory'] = 'category/addCategory';
$route['viewCategory'] = 'category/viewCategory';
$route['addservice'] = 'admin/addService';
$route['editCategory'] = 'category/editCategory';
$route['addSubcategory'] = 'subcategory/addSubcategory';
$route['viewSubcategory'] = 'subcategory/viewSubcategory';
$route['editSubcategory'] = 'subcategory/editSubcategory';
$route['addSubsubcategory'] = 'subsubcategory/addSubsubcategory';
$route['viewSubsubcategory'] = 'subsubcategory/viewSubsubcategory';
$route['editSubsubcategory'] = 'subsubcategory/editSubsubcategory';
$route['updateSubsubcategory'] = 'subsubcategory/updateSubsubcategory';
$route['adminLogin'] = 'admin/adminLogin';
$route['logout'] = 'admin/logout';
$route['viewcountry'] = 'country/viewcountry';
$route['addcountry'] = 'country/addcountry';
$route['editcountry'] = 'country/editcountry';
$route['add_state'] = 'state/add_state';
$route['viewstate'] = 'state/viewstate';
$route['updatestate'] = 'state/editstate';
$route['viewcities'] = 'city/viewcities';
$route['add_city'] = 'city/add_city';
$route['editcity'] = 'city/editcity';
$route['addDistrict'] = 'District/addDistrict';
$route['editDistrict'] = 'District/editDistrict';
$route['viewDistrict'] = 'District/viewDistrict';
$route['addpincode'] = 'pincode/addpincode';
$route['editpincode'] = 'pincode/editpincode';
$route['viewpincode'] = 'pincode/viewpincode';
$route['addLocations'] = 'location/addLocations';
$route['editLocation'] = 'location/editLocation';
$route['viewLocations'] = 'location/viewLocations';
$route['addcms'] = 'cms/addcms';
$route['viewcms'] = 'cms/viewcms';
$route['editcms'] = 'cms/editcms';
$route['addSocialSettings'] = 'SocialSettings/addSocialSettings';
$route['viewSocialSettings'] = 'SocialSettings/viewSocialSettings';
$route['editSocialSettings'] = 'SocialSettings/editSocialSettings';
$route['editbasicsettings'] = 'basicsettings/editbasicsettings';
$route['addCareers'] = 'Careers/addCareers';
$route['editCareers'] = 'Careers/editCareers';
$route['viewCareers'] = 'Careers/viewCareers';
$route['careerDetails'] = 'Careers/careerDetails';
$route['addResumes'] = 'Resumes/addResumes';
$route['editResumes'] = 'Resumes/editResumes';
$route['viewResumes'] = 'Resumes/viewResumes';
$route['addDepartment'] = 'Department/addDepartment';
$route['editDepartment'] = 'Department/editDepartment';
$route['viewDepartment'] = 'Department/viewDepartment';
$route['addMasterdegree'] = 'Masterdegree/addMasterdegree';
$route['editMasterdegree'] = 'Masterdegree/editMasterdegree';
$route['viewMasterdegree'] = 'Masterdegree/viewMasterdegree';
$route['addPaymentsetting'] = 'Paymentsetting/addPaymentsetting';
$route['editPaymentsetting'] = 'Paymentsetting/editPaymentsetting';
$route['viewPaymentsetting'] = 'Paymentsetting/viewPaymentsetting';
$route['addExpenditure'] = 'Expenditure/addExpenditure';
$route['editExpenditure'] = 'Expenditure/editExpenditure';
$route['viewExpenditure'] = 'Expenditure/viewExpenditure';
$route['addBanners'] = 'Banners/addBanners';
$route['editBanners'] = 'Banners/editBanners';
$route['viewBanners'] = 'Banners/viewBanners';
$route['addPortfolio'] = 'Portfolio/addPortfolio';
$route['editPortfolio'] = 'Portfolio/editPortfolio';
$route['viewPortfolio'] = 'Portfolio/viewPortfolio';
$route['addTestimonials'] = 'Testimonials/addTestimonials';
$route['editTestimonials'] = 'Testimonials/editTestimonials';
$route['viewTestimonials'] = 'Testimonials/viewTestimonials';
$route['addChooseus'] = 'Chooseus/addChooseus';
$route['editChooseus'] = 'Chooseus/editChooseus';
$route['viewChooseus'] = 'Chooseus/viewChooseus';
$route['addNews'] = 'News/addNews';
$route['editNews'] = 'News/editNews';
$route['viewNews'] = 'News/viewNews';
$route['Eventbooking'] = 'Eventbooking/viewEventbooking';
$route['Eventenquiry'] = 'Eventenquiry/viewEventenquiry';
$route['addEmployee'] = 'Employee/addEmployee';
$route['editEmployee'] = 'Employee/editEmployee';
$route['viewEmployee'] = 'Employee/viewEmployee';
//For Packages
$route['addPackagecategory'] = 'Packagecategory/addPackagecategory';
$route['editPackagecategory'] = 'Packagecategory/editPackagecategory';
$route['viewPackagecategory'] = 'Packagecategory/viewPackagecategory';
$route['addPackageitinerary'] = 'Packageitinerary/addPackageitinerary';
$route['editPackageitinerary'] = 'Packageitinerary/editPackageitinerary';
$route['viewPackageitinerary'] = 'Packageitinerary/viewPackageitinerary';
$route['addPackage'] = 'Package/addPackage';
$route['editPackage'] = 'Package/editPackage';
$route['viewPackage'] = 'Package/viewPackage';
$route['addItinerary'] = 'Itinerary/addItinerary';
$route['addPackagesubcategory'] = 'Packagesubcategory/addPackagesubcategory';
$route['editPackagesubcategory'] = 'Packagesubcategory/editPackagesubcategory';
$route['viewPackagesubcategory'] = 'Packagesubcategory/viewPackagesubcategory';
//for vendor package
$route['addVendorpackage'] = 'Vendorpackage/addVendorpackage';
$route['editVendorpackage'] = 'Vendorpackage/editVendorpackage';
$route['viewVendorpackage'] = 'Vendorpackage/viewVendorpackage';
//For upgrade Vendor
$route['addUpgradevendor/:any'] = 'Upgradevendor/addUpgradevendor';
$route['viewUpgradevendor/:any'] = 'Upgradevendor/viewUpgradevendor';
$route['UpgradevendorDetails/:any/:any'] = 'Upgradevendor/UpgradevendorDetails';
//for catering category 
$route['addCateringcategory'] = 'Cateringcategory/addCateringcategory';
$route['editCateringcategory'] = 'Cateringcategory/editCateringcategory';
$route['viewCateringcategory'] = 'Cateringcategory/viewCateringcategory';
//for catering subcategory 
$route['addCateringsubcategory'] = 'Cateringsubcategory/addCateringsubcategory';
$route['editCateringsubcategory'] = 'Cateringsubcategory/editCateringsubcategory';
$route['viewCateringsubcategory'] = 'Cateringsubcategory/viewCateringsubcategory';
// For Cusine Types
$route['addCusinetype'] = 'Cusinetypes/addCusinetype';
$route['editCusinetype'] = 'Cusinetypes/editCusinetype';
$route['viewCusinetype'] = 'Cusinetypes/viewCusinetype';
//For Product Weights
$route['addProductweights'] = 'Productweights/addProductweights';
$route['editProductweights'] = 'Productweights/editProductweights';
$route['viewProductweights'] = 'Productweights/viewProductweights';
//For Products
$route['addProducts'] = 'Products/addProducts';
$route['editProducts'] = 'Products/editProducts';
$route['viewProducts'] = 'Products/viewProducts';
// For Product Price 
$route['addProductprice'] = 'Productprice/addProductprice';
$route['editProductprice'] = 'Productprice/editProductprice';
$route['viewProductprice'] = 'Productprice/viewProductprice';

$route['viewVenueTypes'] = 'VenueTypes/viewVenueTypes/';
$route['addVenueTypes'] = 'VenueTypes/addVenueTypes/';

$route['viewFacilities'] = 'Facilities/viewFacilities/';
$route['addFacility'] = 'Facilities/addFacility/';

$route['Mastervendor'] = 'Mastervendor';
$route['logoutvendor'] = 'Mastervendor/logout';
$route['vendorLogin'] = 'Mastervendor/vendorLogin';
$route['addTheme'] = 'Theme/addTheme';
$route['editTheme'] = 'Theme/editTheme';
$route['viewTheme'] = 'Theme/viewTheme';
$route['masterDetails'] = 'Viewmasterdetails/masterDetails';
$route['addService'] = 'Services/addService';
$route['editService'] = 'Services/editService';
$route['viewService'] = 'Services/viewService';
$route['serviceDetails'] = 'Services/serviceDetails';
$route['viewEventbooking'] = 'Eventbooking/viewEventbooking';
$route['viewVendoreventbooking'] = 'Vendoreventbooking/viewVendoreventbooking';
$route['searchList'] = 'Search/searchList';
$route['addVendors'] = 'Uservendor/addVendors';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//For Orders
$route['viewOrders'] = 'Orders/viewOrders';
$route['orderDetails'] = 'Orders/orderDetails/';
//For Vendororders
$route['viewVendororders'] = 'Vendororders/viewVendororders';
$route['vendorOrderDetails'] = 'Vendororders/vendorOrderDetails/';
//For vendor Products
$route['addVendorproducts'] = 'Vendorproducts/addVendorproducts';
$route['editVendorproducts'] = 'Vendorproducts/editVendorproducts';
$route['viewVendorproducts'] = 'Vendorproducts/viewVendorproducts';
// For Vendor product Price
$route['addVendorproductprice'] = 'Vendorproductprice/addVendorproductprice';
$route['editVendorproductprice'] = 'Vendorproductprice/editVendorproductprice';
$route['viewVendorproductprice'] = 'Vendorproductprice/viewVendorproductprice';
// For venue
$route['addVenue'] = 'Venue/addVenue';
$route['editVenue'] = 'Venue/editVenue';
$route['viewVenue'] = 'Venue/viewVenue';
