<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Adminmodel extends CI_Model{
    function __construct() {
        $this->userTbl = 'gstadmin';
        $this->empTable='employee';
    }
    //admin login
    
    function login($where){
        $this->db->where($where); // $where is an array of condtions .
        $query =$this->db->get('master_login');
        $rows=$query->num_rows();
        if($rows>0)
        {           
            $result = $query->result_array() ;
            return  $result ;             
        }   
        else
        {
            return false;
        }        
    }

    //For Vendor Login
    function vendorlogin($where){
        $this->db->where($where); // $where is an array of condtions .
        $query =$this->db->get('vendor_login');
        $rows=$query->num_rows();
        if($rows>0)
        {
            $result = $query->result_array() ;
            return  $result ;
        }
        else
        {
            return false;
        }
    }
    public function existData($where,$tbale){
        
        $this->db->where($where);
        $userExist=$this->db->count_all_results($tbale);
        if($userExist > 0){
            return 1 ;
        }
        else{
            return 0;
        }    
    }   
    public function insertRecordQueryList($table,$data){
        $result = $this->db->insert($table,$data);
        if($result){
            return true ;
        }
        else{
            return false;
        }
    }
    public function insertRecordQueryListReg($table,$data){
        $result = $this->db->insert($table,$data);
        if($result){
            $lastId =$this->db->insert_id();
            $this->db->where('id',$lastId); // $where is an array of condtions .
            $query =$this->db->get('users');
            $rows=$query->num_rows();
            if($rows>0)
            {
             $result = $query->result_array();  
              return $result[0];
               
            } else{
                return array();
            }
        }
        else{
            return false;
        }
    }
    //For Vendor Registration
    public function insertVendorRecord($table,$data){
        $result = $this->db->insert($table,$data);
        if($result){
            $lastId =$this->db->insert_id();
            $this->db->where('id',$lastId); // $where is an array of condtions .
            $query =$this->db->get('keyaan_vendor');
            $rows=$query->num_rows();
            if($rows>0)
            {
             $result = $query->result_array();  
              return $result[0];
            } else{
                return array();
            }
        }
        else{
            return false;
        }
    }
    //
     public function updateRecordQueryList($table,$data,$id,$value){        
        $this->db->where($id,$value) ;
        $result=$this->db->count_all_results($table);
        if($result > 0){
        $this->db->where($id,$value) ;
        $this->db->update($table,$data);
            return true ;
        } 
        else{
            return false;
        }
    }
     public function updateRecordQueryList2($table,$data,$where){        
        $this->db->where($where) ;
        $result=$this->db->count_all_results($table);
        if($result > 0){
        $this->db->where($where) ;
        $this->db->update($table,$data);
            return true ;
        } 
        else{
            return false;
        }
    }
    public function singleRecordData($column,$value,$table){
        $this->db->where($column,$value);
        $this->db->from($table);
        $count = $this->db->count_all_results();
        if($count>0){
            $this->db->where($column,$value);
            $query = $this->db->get($table);
            $result = $query->result_array();
            return $result ;
        }
        else{
            return 0;
        }
    }
     public function get_current_page_records($table,$limit, $start,$column=null,$value=null,$search=null,$searchColumn=null)
   {
       if($search!='null' && $search!='' ){
           $this->db->like($searchColumn,$search);
       }
       if($column!=null && $value!=null){
           $this->db->where($column,$value);
       }
       $this->db->limit($limit, $start);
       $query = $this->db->get($table);

       if ($query->num_rows() > 0)
       {
//            foreach ($query->result() as $row)
//            {
//                $data[] = $row;
//            }
            $data = $query->result_array();
           return $data;
       }

       return false;
   }
    function userLogin($where){
        $username = $where['0'];
        $password = $where['1'];
        $where = "password='$password' AND username='$username' OR email='$username'";
        $this->db->where($where); // $where is an array of condtions .
        $query =$this->db->get('user');
        $rows=$query->num_rows();
        if($rows>0)
        {
          return $query->result_array();   
           
        }   
        else
        {
            return false;
        }
        
    }
    //get all records from user table
    function getAllrecords(){
        $count = $this->db->count_all_results('user');
        if($count > 0) {
            $query  = $this->db->get("user"); 
            $result = $query->result();
            return $result ;
        }
        else{
           return '0' ;  
        }

    }
    // for user register
    public function regUser($data){
        $result = $this->db->insert('user',$data);
        if($result){
            $id = $this->db->insert_id();
            return $id ;
        }
        else{
            return false;
        }

    }
   
    public function checkPassword($data,$data1){
    $this->db->where($data);
    $result=$this->db->count_all_results('user');
    if($result > 0){
        $this->db->where($data);
        $this->db->update('user',$data1);
        return $result ;
    } 
    else
     return false;         
    }
    public function get_current_page_recordsVendor($table,$limit, $start,$catid=null,$city=null,$basePrice=null) 
    {
        $this->db->limit($limit, $start);
        if($catid!=null){
            $this->db->where('cat_id',$catid);
        }
        if($city!=null){
            $this->db->where('city_id',$city);
        }
        if($basePrice!=null){
            if($basePrice==1){
                $this->db->where('category_base_price BETWEEN 0 and 5000', '',false);
            }
            if($basePrice==2){
                $this->db->where('category_base_price BETWEEN 5001 and 10000', '',false);
            }
            if($basePrice==3){
                $this->db->where('category_base_price BETWEEN 10001 and 50000', '',false);
            }
            if($basePrice==4){
                $this->db->where('category_base_price BETWEEN 50001 and 100000', '',false);
            }
            if($basePrice==5){
                $this->db->where('category_base_price BETWEEN 100001 and 500000', '',false);
            }
        }
        $query = $this->db->get($table);
 
        if ($query->num_rows() > 0) 
        {
//            foreach ($query->result() as $row) 
//            {
//                $data[] = $row;
//            }
             $data = $query->result_array();
            return $data;
        }
        else{  
        return false;
        }
    }
    public function record_count($table,$search=null,$searchColumn) {

       if($search!='null'){
           return $this->db
           ->like($searchColumn,$search)
           ->count_all_results($table);
       }else{
           return $this->db->count_all($table);
       }


  }

  //For Cat Theme Count
  public function theme_count($table,$vendor_code,$catid) {
    return $this->db
        ->where('vendor_code',$vendor_code)
        ->where('cat_id',$catid)
        ->count_all_results($table);
}
   // for delete image
   public function delImage($id,$table,$folder,$selectColumn){      
           $this->db->select($selectColumn);
           $this->db->where('id',$id);
           $query = $this->db->get($table);
           $row  = $query->row_array();
           $file = $row[$selectColumn];
           $url= FCPATH;                      
           unlink($url."uploads/".$folder."/".$file);              
           $this->db->where('id',$id);
           $result = $this->db->delete($table);
           if($result){
               return 1 ;
           }
           else{
               return 0;
           }
   }
   
public function getSingleColumnName($value,$column,$expColumn,$table){
    $this->db->select($expColumn);
    $this->db->where($column,$value);
    $query = $this->db->get($table);
    $result = $query->row();
    if($result){
        return $result->$expColumn ;
    }
    else{
        return "" ;
    }
} 
    
private function set_upload_options() {   
    //upload an image options
    $config = array();
    $config['upload_path'] = base_url()."uploads/vendorimage/";
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size']      = '0';
    $config['overwrite']     = FALSE;
    return $config;
}
public function getAjaxdataCountry($table){
    //$id =$this->input->post('id');
    $result1=[];
    $this->db->where('keyaan_status','0');
    $query = $this->db->get($table);
    $result = $query->result_array();
    if($result){
        $result1 =$result;
        return  $result1 ;
    }         
    else{
        return $result1 ;
    }
}
public function getAjaxdata($column,$value,$table){
    $result1 =[];    
    //$id =$this->input->post('id');
    $this->db->where($column,$value);
    $query = $this->db->get($table);
    $result = $query->result_array();
    if($result){
        $result1 =$result;
        return $result1 ;
    } else{
        return $result1 ;
    }
}
public function getgallery($id,$table){
    if($table == 'vendor_image_details') {
        $this->db->select('id,vendor_code,image,media_type');
    } elseif($table == 'vendor_theme_details') {
        $this->db->select('id,vendor_code,theme_image,media_type');
    } elseif($table == 'emp_document_detail') {
        $this->db->select('id,emp_code,doc_img,doc_type');
    } elseif($table == 'venue_image_details') {
        $this->db->select('id,venue_code,venue_image,media_type');
    }
    $this->db->where('keyaan_status',0);
    if($table == 'vendor_image_details') {
        $this->db->where('vendor_code',$id);
    } elseif($table == 'vendor_theme_details') {
        $this->db->where('theme_code',$id);
    } elseif($table == 'emp_document_detail') {
        $this->db->where('emp_code',$id);
    } elseif($table == 'venue_image_details') {
        $this->db->where('venue_code',$id);
    }
 
    $query = $this->db->get($table);
    $result = $query->result_array();
    if($result){
        foreach ($result as $key => $field) {
            if($table == 'vendor_image_details') {
                if($field['media_type']==1){
                    $pathVen =base_url()."uploads/vendorimage/";
                }else{
                    $pathVen =base_url()."uploads/vendorvideo/";
                }
                $result[$key]['image'] = $pathVen.$field['image'];
            } elseif($table == 'vendor_theme_details') {
                if($field['media_type']==1){
                    $paththeme =base_url()."uploads/themeimage/";
                    $paththeme1 =base_url()."uploads/thumbnail/themeimage/";
                }else{
                    $paththeme =base_url()."uploads/themevideo/";
                }
                $result[$key]['theme_image'] = $paththeme.$field['theme_image'];
                $result[$key]['theme_image1'] = $paththeme1.$field['theme_image'];
            } elseif($table == 'emp_document_detail') {
                $result[$key]['doc_img'] = "uploads/employeeDocuments/".$field['doc_img'];
            }
            elseif($table == 'venue_image_details') {
                if($field['media_type']==1){
                    $pathvenue =base_url()."uploads/venue_gallery/";
                }else{
                    $pathvenue =base_url()."uploads/venuevideo/";
                }
                $result[$key]['venue_image'] = $pathvenue.$field['venue_image'];
            }
       }
       return $result;
    }
    else{
       return "" ;
    }
 } 
 	public function getId($id,$table) {
        $this->db->where('keyaan_status',0);
        $this->db->where('vendor_code',$id);
        $query = $this->db->get($table);
        $result = $query->result_array();
        if($result){
            return $result;
        } else {
            return "" ;
        }
    }
    //for vendor theme images
    public function getThemeImg($vendor_code){
        $this->db->select('id,vendor_code,theme_banner');
        $this->db->where('vendor_code',$vendor_code);
        $this->db->where('keyaan_status',0);
        $query = $this->db->get('vendor_themelist');
        $result = $query->result_array();
        if($result){
            return $result;
        }
        else{
            return "" ;
        }
    }
    
    // public function getSubCategoryList($catid){
    //     $result1 =[];    
    //     //$id =$this->input->post('id');
    //     $this->db->select('id,cat_id,subcategory_name');
    //     $this->db->where('cat_id',$catid);
    //     $query = $this->db->get('keyaan_subcategory');
    //     $result = $query->result_array();
    //     if($result){
    //         $result1 =$result;
    //         return $result1 ;
    //     } else{
    //         return $result1 ;
    //     }
    // } 
    public function themeListingDetail($table,$limit, $start,$listid){
        $this->db->where('id',$listid);
        $this->db->select('id,vendor_code,category_base_price,vendor_detail,name,state_id,city_id,basic_image,logo,about_vendor,default_theme');
        $this->db->limit($limit, $start);
        $query = $this->db->get($table);
 
        if ($query->num_rows() > 0) 
        {
            $data = $query->result_array();
            return $data;
        }
        else{  
        return false;
        }
    }

    public function otherThemeListing($table,$limit, $start,$vendor_code,$id){
        $this->db->where('vendor_code',$vendor_code);
        $this->db->where('id !=',$id);
        $this->db->where('keyaan_status',0);
        $this->db->select('id,vendor_code,theme_code,price,theme_detail,theme_name');
        $this->db->limit($limit, $start);
        $query = $this->db->get($table);
 
        if ($query->num_rows() > 0) 
        {
            $data = $query->result_array();
            return $data;
        }
        else{  
        return false;
        }
    }
   
    // For Category Name from vendor tables
    public function getCategoryName($table,$column,$value) {
        $result1 =[];
        $this->db->where('keyaan_status','0');
        $this->db->where($column,$value);
        $query = $this->db->get($table);
        $result = $query->result_array();
        if($result){
            $result1 =$result;
            return $result1 ;
        } else{
            return $result1 ;
        }
    }
    //For Service Details
    public function get_service_details($table,$vendorcode,$service_code){
        $this->db->where('service_code',$service_code);
        $this->db->where('vendor_code',$vendorcode);
        $query =$this->db->get($table);
        $rows=$query->num_rows();
        if($rows>0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    // for delete
   public function delRow($id,$table){             
        $this->db->where('id',$id);
        $result = $this->db->delete($table);
        if($result){
            return true ;
        }
        else{
            return false;
        }
    }
    //for delete multiple tables
   public function delmultipletables($code,$table,$column){             
        $this->db->where($column,$code);
        $result = $this->db->delete($table);
        if($result){
            return true ;
        }
        else{
            return false;
        }
    }
    //for delete multiple images
   public function delmultipleImage($code,$table,$folder,$folder1,$folder2,$folder3,$selectColumn,$selectColumn1,$selectColumn2,$selectColumn3,$selectColumn4){      
       $this->db->select($selectColumn);
       $this->db->select($selectColumn1);
       $this->db->select($selectColumn2);
       $this->db->select($selectColumn3);
       $this->db->where($selectColumn4,$code);
       $query = $this->db->get($table);
       $row  = $query->row_array();
       $url= FCPATH;
       if($selectColumn != '' || $selectColumn != null){
          $file = $row[$selectColumn];
          unlink($url."uploads/".$folder."/".$file);
       }if($selectColumn1 != '' || $selectColumn1 != null){
           $file1 = $row[$selectColumn1];
            unlink($url."uploads/".$folder1."/".$file1);
        }if($selectColumn2 != '' || $selectColumn2 != null){
           $file2 = $row[$selectColumn2];
           unlink($url."uploads/".$folder2."/".$file2);
       }if($selectColumn3 != '' || $selectColumn3 != null){
           $file3 = $row[$selectColumn3];
            unlink($url."uploads/".$folder3."/".$file3);
       }
       $this->db->where($selectColumn4,$code);
       $result = $this->db->delete($table);
       if($result){
           return true ;
       }
       else{
           return false;
       }
       
   }
   //delete multiple image folder
    public function delmultipleImagefile($code,$table,$folder,$folder1,$selectColumn,$selectColumn1,$selectColumn3){
        $this->db->select($selectColumn); //for image
        $this->db->select($selectColumn1); //media type
        $this->db->where($selectColumn3,$code);
        $query = $this->db->get($table);
        $row  = $query->result_array();
        $url= FCPATH; 
        $count = $query->num_rows();
        if($count > 0) { 
            foreach ($row as $key => $value) {
                $file = $value[$selectColumn];
                $media_type = $value[$selectColumn1];
                if($media_type == 1){
                   unlink($url."uploads/".$folder."/".$file);
                    }elseif($media_type == 2){
                   unlink($url."uploads/".$folder1."/".$file);
                 }          
               $this->db->where($selectColumn3,$code);
               $result = $this->db->delete($table);          
            }
        } else {
            $result = "";
        } 
        if($result){
           return true;
        }
        else{
           return false;
        }

    }


// for login 
    function loginCustomer($where1){
        $username = $where1['username'];
        $password = $where1['password'];
        $where = "keyaan_status='0' AND password='$password' AND (mobile='$username' OR email='$username')";
        $this->db->where($where); // $where is an array of condtions .
        $query =$this->db->get('users');
        $rows=$query->num_rows();
        if($rows > 0)
        {           
            $result = $query->result_array() ;
            return  $result[0] ;             
        }   
        else
        {
            return false;
        }        
    }
    function existCustomer($where1){
        $email = $where1['email'];
        $mobile = $where1['mobile'];
        $whereEmail = "email='$email'";
        $whereMobile = "mobile='$mobile'";
        $this->db->where($whereEmail); // $where is an array of condtions .
        $query =$this->db->get('users');
        $rows=$query->num_rows();
        if($rows > 0)
        {           
            $result = 1; // for email exist
            return $result ;                      
        }   
        else 
        {
            $this->db->where($whereMobile); // $where is an array of condtions .
            $query =$this->db->get('users');
            $rows2=$query->num_rows();
            if($rows2 > 0)
            {           
                $result = 2; // for mobile exist
                return $result ;                        
            }else{
                $result = 0; // not exist
                return $result ;
            }
            
        }        
    }



    function existCustomer2($where1){
        $mobile = $where1['mobile'];      
        $whereMobile = "mobile='$mobile'";
        $this->db->where($whereMobile); // $where is an array of condtions .
        $query =$this->db->get('users');
        $rows=$query->num_rows();
        if($rows > 0)
        {           
            $result = 1; // for email exist
            return $result ;                      
        }   
        else 
        {           
            $result = 0; // not exist
            return $result ;
           
        }        
    }

    //For Exist Vendor
    function existVendor($where1){
        $email = $where1['email'];
        $mobile = $where1['mobile'];
        $whereEmail = "email='$email'";
        $whereMobile = "mobile='$mobile'";
        $this->db->where($whereEmail); // $where is an array of condtions .
        $query =$this->db->get('keyaan_vendor');
        $rows=$query->num_rows();
        if($rows > 0)
        {           
            $result = 1; // for email exist
            return $result ;                      
        }   
        else 
        {
            $this->db->where($whereMobile); // $where is an array of condtions .
            $query =$this->db->get('keyaan_vendor');
            $rows2=$query->num_rows();
            if($rows2 > 0)
            {           
                $result = 2; // for mobile exist
                return $result ;                        
            }else{
                $result = 0; // not exist
                return $result ;
            }
            
        }        
    }
    public function themeListing($table,$limit,$start,$catid=null,$city=null,$basePrice=null,
        $subcatid=null,$rating_filter=null,$sorting=null)
   {

       $start = max(0, ($start-1 ) * $limit);
      // $this->db->where('keyaan_status', 0);
       $this->db->select('a.id,a.vendor_code,
        a.category_base_price,a.vendor_detail,a.name,a.state_id,a.district_id,a.city_id,a.basic_image,a.logo,a.vendor_type');
       
       $this->db->from('keyaan_vendor a');
       $this->db->join('vendor_themelist b', 'a.vendor_code =b.vendor_code','left');
       if($catid!=null){
            $this->db->where("b.cat_id",$catid);
        }
        if($subcatid!=null){
             
             $imp ="'" . implode( "','",$subcatid) . "'";
             
            $this->db->where("b.subcat_id IN ($imp) ");
        }
        if($rating_filter!=null){
             
              $imp ="'" . implode( "','",$rating_filter) . "'";
               $this->db->where("a.rating_avg IN ($imp) ");
        }
       if($city!=null){
            $sql = "select vendor_code from vendor_cities where city_id='$city'";
            $this->db->where("a.vendor_code IN ($sql) ");
        }

       if($basePrice!=null){          
            if($basePrice==1){
            $this->db->where('b.price BETWEEN 0 and 5000', '',false);
            }
            if($basePrice==2){
                $this->db->where('b.price BETWEEN 5001 and 10000', '',false);
            }
            if($basePrice==3){
                $this->db->where('b.price BETWEEN 10001 and 50000', '',false);
            }
            if($basePrice==4){
                $this->db->where('b.price BETWEEN 50001 and 100000', '',false);
            }
            if($basePrice==5){
                $this->db->where('b.price BETWEEN 100001 and 500000', '',false);
            }
        }
       $this->db->group_by('a.vendor_code'); 

       $this->db->order_by("a.vendor_type", "DESC");
       if($sorting !=null){
           if($sorting ==1){
            $this->db->order_by("a.rating_avg", "DESC");
           }
           if($sorting ==2){
            $this->db->order_by("b.price", "ASC");
           }
           if($sorting ==3){
            $this->db->order_by("b.price", "DESC");
           }

       }
       $this->db->limit($limit,$start);
      // $this->db->offset($start);
       

       $query = $this->db->get();
       //echo $this->db->last_query(); die;
       if ($query->num_rows() > 0)
       {
//            foreach ($query->result() as $row)
//            {
//                $data[] = $row;
//            }
            $data = $query->result_array();
           return $data;
       }
       else{
       return false;
       }
   }

   //Venue Listing 
   public function venueListing($table,$limit,$start,$catid=null,$city=null,$basePrice=null,$venue_type=null,$audience=null,$rating=null,$facility=null,$sorting=null) {
    
        $start = max(0, ($start-1 ) * $limit);
        $this->db->where('keyaan_status', 0);
        
        $this->db->select('id,vendor_code,venue_code,venue_name,venue_type,venue_detail,address,price_start,image');

        if($catid!=null){
            $sql = "select vendor_code from vendor_categories where category_id='$catid'";
            $this->db->where("vendor_code IN ($sql) ");
        }

        if($city!=null){
            $sql = "select vendor_code from vendor_cities where city_id='$city'";
            $this->db->where("vendor_code IN ($sql) ");
        }

        if($basePrice!=null){          
            if($basePrice==1){
            $this->db->where('price_start BETWEEN 0 and 5000', '',false);
            }
            if($basePrice==2){
                $this->db->where('price_start BETWEEN 5001 and 10000', '',false);
            }
            if($basePrice==3){
                $this->db->where('price_start BETWEEN 10001 and 50000', '',false);
            }
            if($basePrice==4){
                $this->db->where('price_start BETWEEN 50001 and 100000', '',false);
            }
            if($basePrice==5){
                $this->db->where('price_start BETWEEN 100001 and 500000', '',false);
            }
        }

        if($venue_type!=null){
            $imp ="'" . implode( "','",$venue_type) . "'";
            $this->db->where("venue_type IN ($imp) ");
        }
        if($rating!=null){
            $imp ="'" . implode( "','",$rating) . "'";
            $this->db->where("rating_avg IN ($imp) ");
        }
        if($facility!=null){
            $sql = "select vendor_code from venue_facilities where facility_id IN ($facility) ";
            print_r($sql);exit;
            $this->db->where("vendor_code IN ($sql) ");
        }
        if($sorting !=null){
            if($sorting ==1){
            $this->db->order_by("rating_avg", "DESC");
            }
            if($sorting ==2){
            $this->db->order_by("category_base_price", "ASC");
            }
            if($sorting ==3){
            $this->db->order_by("category_base_price", "DESC");
            }

        }
        $this->db->limit($limit,$start);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
            return $data;
        } else{
            return false;
        }
    }

    //Catering Listing
    public function cateringListing($table,$limit,$start,$catid=null,$city=null,$basePrice=null,$sorting=null) {

        $start = max(0, ($start-1 ) * $limit);
        // $this->db->where('keyaan_status', 0);
        $this->db->select('id,vendor_code,category_base_price,vendor_detail,name,state_id,district_id,city_id,basic_image,logo,address');
        $this->db->from('keyaan_vendor');
        if($catid!=null){
            $sql = "select vendor_code from vendor_categories where category_id='$catid'";
            $this->db->where("vendor_code IN ($sql) ");
        }
        if($city!=null){
            $sql = "select vendor_code from vendor_cities where city_id='$city'";
            $this->db->where("a.vendor_code IN ($sql) ");
        }
        if($basePrice!=null){          
            if($basePrice==1){
            $this->db->where('category_base_price BETWEEN 0 and 5000', '',false);
            }
            if($basePrice==2){
                $this->db->where('category_base_price BETWEEN 5001 and 10000', '',false);
            }
            if($basePrice==3){
                $this->db->where('category_base_price BETWEEN 10001 and 50000', '',false);
            }
            if($basePrice==4){
                $this->db->where('category_base_price BETWEEN 50001 and 100000', '',false);
            }
            if($basePrice==5){
                $this->db->where('category_base_price BETWEEN 100001 and 500000', '',false);
            }
        }
        $this->db->group_by('vendor_code'); 

        $this->db->order_by("vendor_status", "DESC");
        if($sorting !=null){
            if($sorting ==1){
            $this->db->order_by("rating_avg", "DESC");
            }
            if($sorting ==2){
            $this->db->order_by("category_base_price", "ASC");
            }
            if($sorting ==3){
            $this->db->order_by("category_base_price", "DESC");
            }

        }
        $this->db->limit($limit,$start);
        // $this->db->offset($start);
        

        $query = $this->db->get();
        //echo $this->db->last_query(); die;
        if ($query->num_rows() > 0)
        {
    //            foreach ($query->result() as $row)
    //            {
    //                $data[] = $row;
    //            }
            $data = $query->result_array();
            return $data;
        }
        else{
        return false;
        }
    }

    //Sub Category Listing
    public function subcatListing($table,$limit,$start,$subcatid=null,$price=null,$rating=null,$sorting=null) {

        $start = max(0, ($start-1 ) * $limit);
        // $this->db->where('keyaan_status', 0);
        $this->db->select('a.id,a.vendor_code,
            a.category_base_price,a.vendor_detail,a.name,a.state_id,a.district_id,a.city_id,a.basic_image,a.logo');
        $this->db->from('keyaan_vendor a');
        $this->db->join('vendor_themelist b', 'a.vendor_code =b.vendor_code','left');
        if($subcatid!=null){
            $this->db->where("b.subcat_id",$subcatid);
        }
        if($price!=null){          
            if($price==1){
            $this->db->where('category_base_price BETWEEN 0 and 5000', '',false);
            }
            if($price==2){
                $this->db->where('category_base_price BETWEEN 5001 and 10000', '',false);
            }
            if($price==3){
                $this->db->where('category_base_price BETWEEN 10001 and 50000', '',false);
            }
            if($price==4){
                $this->db->where('category_base_price BETWEEN 50001 and 100000', '',false);
            }
            if($price==5){
                $this->db->where('category_base_price BETWEEN 100001 and 500000', '',false);
            }
        }
        if($sorting !=null){
            if($sorting ==1){
            $this->db->order_by("rating_avg", "DESC");
            }
            if($sorting ==2){
            $this->db->order_by("category_base_price", "ASC");
            }
            if($sorting ==3){
            $this->db->order_by("category_base_price", "DESC");
            }
        }
        if($rating!=null){
             
            $imp ="'" . implode( "','",$rating) . "'";
             $this->db->where("a.rating_avg IN ($imp) ");
      }
        $this->db->group_by('a.vendor_code'); 
        $this->db->order_by("a.vendor_type", "DESC");
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        return $data;
        } else{
            return false;
        }
    }

public function themeListingCount($table,$limit, $start,$catid=null,$city=null,$basePrice=null,
        $subcatid=null,$rating_filter=null)
   {

       $this->db->select('a.id');
       $this->db->from('keyaan_vendor a');
       $this->db->join('vendor_themelist b', 'a.vendor_code =b.vendor_code','left');
       if($catid!=null){
            $this->db->where("b.cat_id",$catid);
        }
        if($subcatid!=null){
             
             $imp ="'" . implode( "','",$subcatid) . "'";
             
            $this->db->where("b.subcat_id IN ($imp) ");
        }
        if($rating_filter!=null){
             
              $imp ="'" . implode( "','",$rating_filter) . "'";
               $this->db->where("a.rating_avg IN ($imp) ");
        }
       if($city!=null){
            $sql = "select vendor_code from vendor_cities where city_id='$city'";
            $this->db->where("a.vendor_code IN ($sql) ");
        }

       if($basePrice!=null){          
            if($basePrice==1){
            $this->db->where('b.price BETWEEN 0 and 5000', '',false);
            }
            if($basePrice==2){
                $this->db->where('b.price BETWEEN 5001 and 10000', '',false);
            }
            if($basePrice==3){
                $this->db->where('b.price BETWEEN 10001 and 50000', '',false);
            }
            if($basePrice==4){
                $this->db->where('b.price BETWEEN 50001 and 100000', '',false);
            }
            if($basePrice==5){
                $this->db->where('b.price BETWEEN 100001 and 500000', '',false);
            }
        }
       $this->db->where('a.keyaan_status', 0);
       $this->db->where('b.keyaan_status', 0);
       $this->db->group_by('a.vendor_code'); 
       
       $query = $this->db->get();

      // echo $this->db->last_query(); die;
       if ($query->num_rows() > 0)
       {
//           
            $data = $query->num_rows();
           return $data;
       }
       else{
       return 0;
       }
   }






















   
    public function vendorCodeList($basePrice){
        $vendor_code=[];
        $this->db->select('id,vendor_code');
        if($basePrice==1){
            $this->db->where('price BETWEEN 0 and 5000', '',false);
        }
        if($basePrice==2){
            $this->db->where('price BETWEEN 5001 and 10000', '',false);
        }
        if($basePrice==3){
            $this->db->where('price BETWEEN 10001 and 50000', '',false);
        }
        if($basePrice==4){
            $this->db->where('price BETWEEN 50001 and 100000', '',false);
        }
        if($basePrice==5){
            $this->db->where('price BETWEEN 100001 and 500000', '',false);
        }
        //based on price get all vendor
        $query = $this->db->get('vendor_themelist');
        if ($query->num_rows() > 0)
        {
        foreach ($query->result() as $row)
        {
            $vendor_code[] = $row->vendor_code;
        }
        return   $vendor_code;
        }else {
            return   $vendor_code;
        }
    }

    //For Cat Search
    public function vendorList($catid){
        $vendor_code=[];
        $this->db->select('id,vendor_code');
        $this->db->where('cat_id',$catid);
        //based on cat get all vendor
        $query = $this->db->get('vendor_themelist');
        if ($query->num_rows() > 0)
        {
        foreach ($query->result() as $row)
        {
            $vendor_code[] = $row->vendor_code;
        }
        return   $vendor_code;
        }else {
            return   $vendor_code;
        }
    }
    // vendor code based on subcat
     public function vendorListsub($subcatid){
        $vendor_code=[];
        $this->db->select('id,vendor_code');
        $this->db->where('sub_category_id',$catid);
        //based on cat get all vendor
        $query = $this->db->get('vendor_themelist');
        if ($query->num_rows() > 0)
        {
        foreach ($query->result() as $row)
        {
            $vendor_code[] = $row->vendor_code;
        }
        return   $vendor_code;
        }else {
            return   $vendor_code;
        }
    }

    //For Cat Search
    public function vendorListonCities($city){
        $vendor_code=[];
        $this->db->select('c.vendor_code');
         $this->db->from('vendor_cities c');
        $this->db->where('c.city_id',$city);
        //based on catcity get all vendor
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
        foreach ($query->result() as $row)
        {
            $vendor_code[] = $row->vendor_code;
        }
        return   $vendor_code;
        }else {
            return   $vendor_code;
        }
    }

   //For Search
    public function vendorSearching($table,$limit, $start,$searchTxt=null,$city=null) {
        if($city!=null){
           $this->db->where('city_id',$city);
        }
       if($searchTxt!=null){
            $this->db->like('search_text',$searchTxt);
        }
       $this->db->select('id,vendor_code,category_base_price,vendor_detail,name,state_id,district_id,city_id,basic_image,logo');
       $this->db->limit($limit, $start);
       $query = $this->db->get($table);

       if ($query->num_rows() > 0)
       {
//            foreach ($query->result() as $row)
//            {
//                $data[] = $row;
//            }
            $data = $query->result_array();
           return $data;
       }
       else{
       return false;
       }
   }
   
   public function themeDetail($vendor_code,$cat_id){
        $this->db->where('vendor_code',$vendor_code);
        if($cat_id != '') {
            $this->db->where('cat_id',$cat_id);
        }
        $this->db->select('id,vendor_code,theme_code,theme_name,theme_banner,theme_detail,price');
        $query = $this->db->get('vendor_themelist');

        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
            return $data;
        }
        else{
        return false;
        }
    }
   public function otherVendorBycat($table,$limit,$start,$catid,$vendorCode){
       $this->db->where('vendor_code !=',$vendorCode);
       $this->db->where('keyaan_status',0);
       $this->db->select('id,vendor_code,name,category_base_price');
       $this->db->limit($limit, $start);
       $query = $this->db->get('keyaan_vendor');
       if($query->num_rows() > 0)
       {
           $data = $query->result_array();
           return $data;
       }
       else{
           return false;
       }
   }


//For Rated Vendors
   public function ratedVendors() {
       $result1 =[];
       $this->db->select('id,vendor_code,category_base_price,vendor_detail,name,state_id,district_id,city_id,basic_image,logo');
       $this->db->where('keyaan_status',0);
       $query = $this->db->get('keyaan_vendor');
       $result = $query->result_array();
       if($result){
           $result1 =$result;
           return $result1 ;
       } else{
           return $result1 ;
       }
   }
// for cart Count
public function getcartCount($userCode){
    $this->db->select('id,user_id');
   $this->db->where('user_id',$userCode);
   $this->db->group_by('item_id'); 
   $this->db->group_by('package_id');
   //based on city get all vendor
   $query = $this->db->get('keyaan_cart');
   if ($query->num_rows() > 0)
   {
    $result=$query->num_rows();
   }else{
    $result=0;
   }
   return $result ;
}  
    //To Display Cart
    public function cartDetail($userCode){
        $this->db->select('id,user_id,price,cart_code,item_id,vendor_id,card_type');
       $this->db->where('user_id',$userCode);
       $this->db->where('card_type',1);
       $this->db->group_by('item_id'); 
       //based on city get all vendor
       $query = $this->db->get('keyaan_cart');
       if ($query->num_rows() > 0)
       {
        $result= $query->result_array();
       }else{
        $result=array();
       }
       return $result ;
    }
    //for Package cart
    public function packagecartDetail($userCode){
        $this->db->select('id,user_id,itinerary_price,cart_code,package_id,iteinerary_id');
       $this->db->where('user_id',$userCode);
       $this->db->group_by('iteinerary_id'); 
       //based on city get all vendor
       $query = $this->db->get('cart_details');
       if ($query->num_rows() > 0)
       {
        $result= $query->result_array();
       }else{
        $result=array();
       }
       return $result ;
    } 
    //For Orders
    public function orderCart($userCode){
        $this->db->select('id,user_id,price,cart_code,item_id,vendor_id,card_type');
       $this->db->where('user_id',$userCode);
       $this->db->group_by('item_id'); 
       //based on city get all vendor
       $query = $this->db->get('keyaan_cart');
       if ($query->num_rows() > 0)
       {
        $result= $query->result_array();
       }else{
        $result=array();
       }
       return $result ;
    }
    //Delete Theme Cart
    public function delCart($id,$userCode) {
        $this->db->where('item_id',$id);
        $this->db->where('user_id',$userCode);
           $result = $this->db->delete('keyaan_cart');
           if($result){
               return true ;
           }
           else{
               return false;
           }
    }
    //Delete Package Cart
    public function delPackageCart($id,$userCode) {
        $this->db->where('iteinerary_id',$id);
        $this->db->where('user_id',$userCode);
        $result = $this->db->delete('cart_details');
        if($result){
            return true ;
        }
        else{
            return false;
        }
    }
    // For Event booking
   public function getEventbooking($table,$column,$value) {
       $result1 =[];
       $this->db->where('approve_admin','0');
       $this->db->where($column,$value);
       $query = $this->db->get($table);
       $result = $query->result_array();
       if($result){
           $result1 =$result;
           return $result1 ;
       } else{
           return $result1 ;
       }
   }
public function getCartTotalAmount($userCode){
        $this->db->select_sum('price');
       $this->db->where('user_id',$userCode);
       $this->db->group_by('item_id'); 
       //based on city get all vendor
       $query = $this->db->get('keyaan_cart');
       if ($query->num_rows() > 0)
       {
        $amount=0;
        foreach ($query->result() as $row)
              {
               $amount= $amount+$row->price;
              }
              
        return $amount;

       }else{
        $result=0;
       }
       return $result ;

    }
public function delCartAll($userCode) {        
        $this->db->where('user_id',$userCode);
           $result = $this->db->delete('keyaan_cart');
           if($result){
            $this->db->where('user_id',$userCode);
            $result = $this->db->delete('cart_details');
            

               return true ;
           }
           else{
               return false;
           }
    }
    
    //For Order Details
    public function orderDetails($table,$order_id,$user_id) {
        $result1 =[];
        $this->db->where('order_id',$order_id);
        $this->db->where('user_id',$user_id);
        $query = $this->db->get($table);
        $result = $query->result_array();
       if($result){
           $result1 =$result;
           return $result1 ;
       } else{
           return $result1 ;
       }
    }

    //For Vendor Orders
    public function orderEventDetails($table,$order_id,$user_id) {
        $result1 =[];
        $this->db->where('order_id',$order_id);
        $this->db->where('user_id',$user_id);
        $this->db->where('card_type',1);
        $query = $this->db->get($table);
        $result = $query->result_array();
       if($result){
           $result1 =$result;
           return $result1 ;
       } else{
           return $result1 ;
       }
    }

    //For Package Orders
    public function orderPackageDetails($table,$order_id,$user_id) {
        $result1 =[];
        $this->db->where('order_id',$order_id);
        $this->db->where('user_id',$user_id);
        $this->db->where('card_type',2);
        $query = $this->db->get($table);
        $result = $query->result_array();
       if($result){
           $result1 =$result;
           return $result1 ;
       } else{
           return $result1 ;
       }
    }
    
    // For Venodr Orders
    public function getVendorOrders($table,$limit,$start,$column,$value) {
        $result1 =[];
        $this->db->where('approve_by_admin','1');
        $this->db->where($column,$value);
        $this->db->limit($limit, $start);
        $query = $this->db->get($table);
        $result = $query->result_array();
        if($result){
            $result1 =$result;
            return $result1 ;
        } else{
            return $result1 ;
        }
    }
    //For Package
    public function packages($packageCode=null) {
        $result1 =[];
        if($packageCode !=null){
            $this->db->where('package_code',$packageCode);
        }
        $this->db->select('id,package_code,package_name,price,image,package_detail');
        $this->db->where('keyaan_status',0);
        $query = $this->db->get('package');
        $result = $query->result_array();
        if($result){
            $result1 =$result;
            return $result1 ;
        } else{
            return $result1 ;
        }
    }
    //package days packagesDays
    public function packagesDays($packageCode) {
        $result1 =[];
        if($packageCode !=null){
            $this->db->where('package_code',$packageCode);
        }

        $this->db->select('id,event_day,package_code');
        $this->db->where('keyaan_status',0);
        $this->db->group_by('event_day'); 
        $this->db->order_by("event_day", "ASC");
        $query = $this->db->get('package_detail');
        $result = $query->result_array();
        if($result){
            $result1 =$result;
            return $result1 ;

        } else{

            $arr = array();
            return $arr ;
        }
    }
    //for itinary list 
    public function itineraryList($packageCode,$event_day) {
        $result1 =[];
        
        $this->db->where('package_code',$packageCode);
        $this->db->where('event_day',$event_day);
        $this->db->select('id,package_code,itenerary_id,itenerary_name,itinerary_code,event_day');
        //$this->db->where('keyaan_status',0);
        $this->db->group_by('itenerary_id'); 
        $query = $this->db->get('package_detail');
        $result = $query->result_array();
        if($result){
            $result1 =$result;
            return $result1 ;

        } else{

            $arr = array();
            return $arr ;
        }
    }
    //for itineraryDetail sn
    public function itineraryDetail($packageCode,$itinerary_id,$event_day,$make_it_default) {
        $result1 =[];
        if($packageCode !=null){
            $this->db->where('package_code',$packageCode);
        }
        if($event_day !=null){
            $this->db->where('event_day',$event_day);
        }
        if($itinerary_id !=null){
            $this->db->where('itenerary_id',$itinerary_id);
        }
        //$this->db->where('itenerary_id',$itinerary_code);         
        $this->db->select('id,package_code,itenerary_id,itenerary_name,event_day,details,itinerary_code,itinerary_price');
        $this->db->where('keyaan_status',0);
        if($make_it_default == 0){
            $this->db->where('make_it_default',0);
        }
        $query = $this->db->get('package_detail');
        $result = $query->result_array();
        if($result){
            $result1 =$result;
            return $result1 ;

        } else{

            $arr = array();
            return $arr ;
        }
    }
    //For Itinerary Count
    public function itinerary_count($table,$column,$value) {
        $this->db->where($column,$value);
         return $this->db->count_all_results($table);
    }
    //For Itinerary Gallery
    public function getpackagegallery($itinerary_code,$table){
        $this->db->select('id,package_code,itinerary_code,event_day,images,media_type');
        $this->db->where('keyaan_status',0);
        $this->db->where('itinerary_code',$itinerary_code);
        $query = $this->db->get($table);
        $result = $query->result_array();
        if($result){
           return $result;
        }
        else{
           return "" ;
        }
    }

    //For Package Gallery
    public function getpackageImages($package_code,$table){
        $this->db->select('id,package_code,image,media_type');
        //$this->db->order_by('id',DESC); 
        $this->db->where('keyaan_status',0);
        $this->db->where('package_code',$package_code);
        $query = $this->db->get($table);
        $result = $query->result_array();
        if($result){
           return $result;
        }
        else{
           return "" ;
        }
    }

    public function getCategoryList(){
        $result1 =[];    
        //$id =$this->input->post('id');
        $this->db->select('id,category_name');
        $this->db->where('  keyaan_status',0);
        $query = $this->db->get('keyaan_category');
        $result = $query->result_array();
        if($result){
            $result1 =$result;
            return $result1 ;
        } else{
            return $result1 ;
        }
    } 
    public function getSubCategoryList($catid){
        $result1 =[];    
        //$id =$this->input->post('id');
        $this->db->select('id,cat_id,subcategory_name');
        $this->db->where('cat_id',$catid);
        $this->db->where('  keyaan_status',0);
        $query = $this->db->get('keyaan_subcategory');
        $result = $query->result_array();
        if($result){
            $result1 =$result;
            return $result1 ;
        } else{
            return $result1 ;
        }
    } 

    public function getSubsubCategoryList($subcatid){
        $result1 =[];    
        //$id =$this->input->post('id');
        $this->db->select('id,cat_id,subcat_id,subsubcat_name');
        $this->db->where('subcat_id',$subcatid);
        $this->db->where('keyaan_status',0);
        $query = $this->db->get('keyaan_subsubcategory');
        $result = $query->result_array();
        if($result){
            $result1 =$result;
            return $result1 ;
        } else{
            return $result1 ;
        }
    }

    //For Upgradevendor details
    public function get_upgradevendor_details($table,$vendorcode,$order_id){
        $this->db->where('order_id',$order_id);
        $this->db->where('vendor_code',$vendorcode);
        $query =$this->db->get($table);
        $rows=$query->num_rows();
        if($rows>0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //Get Facilities of Venues
    public function facilityList($table,$vendor_code,$venue_code) {
        $this->db->where('vendor_code',$vendor_code);
        $this->db->where('venue_code',$venue_code);
        $query =$this->db->get($table);
        $rows=$query->num_rows();
        if($rows>0) {
            return $query->result_array();
        } else {
            $arr = [];
            return $arr;
        }
    }

    //For Facilities
    public function getSelectedfacilities($table,$venue_code){
        //$id =$this->input->post('id');
        $result1=[];
        $data = array();
         $this->db->select('facility_id');
        $this->db->where('keyaan_status','0');
        $this->db->where('venue_code',$venue_code);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0)
           {
               foreach ($query->result() as $row)
               {
                   $data[] = $row->facility_id;
               }
     
               return $data;
           }
     
        else{
            return $result1 ;
        }
    }
    //for delete multiple dropdown values
    public function delselectedvalue($table,$venueCode,$vendor_code){
        $this->db->where('venue_code',$venueCode);
        $this->db->where('vendor_code',$vendor_code);
        $result = $this->db->delete($table);
        if($result){
            return true ;
        }
        else{
            return false;
        }
    }
    //For Package Highlights
    public function getpackageHighlights($package_code,$table){
        $this->db->select('id,package_code,highlights');
        //$this->db->order_by('id',DESC);
        $this->db->where('keyaan_status',0);
        $this->db->where('package_code',$package_code);
        $query = $this->db->get($table);
        $result = $query->result_array();
        if($result){
        return $result;
        }
        else{
            $arr = [];
            return $arr;
        }
    }
    public function  allItinaryPrice($table,$packageCode){
        $this->db->select('SUM(itinerary_price) as amount');
        $this->db->where('package_code',$packageCode);
        $query =$this->db->get($table);
        $rows=$query->num_rows();
        if($rows> 0 ) {
            $result = $query->row();
            return $result->amount ;
        } else {
            return 0;
        }

    }
public function packagesAll($catId) {
        $result1 =[];
        $this->db->select('id,package_code,package_name,price,image,package_detail');
        $this->db->where('category_id',$catId);
        $this->db->where('keyaan_status',0);
        $query = $this->db->get('package');
        $result = $query->result_array();
        if($result){
            $result1 =$result;
            return $result1 ;
        } else{
            return $result1 ;
        }
    }
    public function getSubCategoryListPackage($catid){
        $result1 =[];    
        //$id =$this->input->post('id');
        $this->db->select('id,category_id,subcategory_name');
        $this->db->where('category_id',$catid);
        $this->db->where('keyaan_status',0);
        $query = $this->db->get('package_subcategory');
        $result = $query->result_array();
        if($result){
            $result1 =$result;
            return $result1 ;
        } else{
            return $result1 ;
        }
    }
    public function getCategoryPackList(){
        $result1 =[];    
        //$id =$this->input->post('id');
        $this->db->select('id,category_name');
        $this->db->where('  keyaan_status',0);
        $query = $this->db->get('package_category');
        $result = $query->result_array();
        if($result){
            $result1 =$result;
            return $result1 ;
        } else{
            return $result1 ;
        }
    } 
    public function getSubCategoryPackList($catid){
        $result1 =[];    
        //$id =$this->input->post('id');
        $this->db->select('id,category_id,subcategory_name');
        $this->db->where('category_id',$catid);
        $this->db->where('keyaan_status',0);
        $query = $this->db->get('package_subcategory');
        $result = $query->result_array();
        if($result){
            $result1 =$result;
            return $result1 ;
        } else{
            return $result1 ;
        }
    }
    public function packagescategory() {
        $result1 =[];
 
        $this->db->select('id,category_name,image,base_price');
        $this->db->where('keyaan_status',0);
        $query = $this->db->get('package_category');
        $result = $query->result_array();
        if($result){
            $result1 =$result;
            return $result1 ;
        } else{
            return $result1 ;
        }
    }
    public function myFav($usercode){
        $this->db->select('a.id,a.vendor_code,
        a.category_base_price,a.vendor_detail,a.name,a.state_id,a.district_id,a.city_id,a.basic_image,a.logo,a.vendor_detail');
        $this->db->where("a.id IN ( SELECT list_id  from keyaan_favourate where user_id ='$usercode' )");
        $this->db->from('keyaan_vendor a');
        $query = $this->db->get();
       //echo $this->db->last_query(); die;
       if ($query->num_rows() > 0)
       {
           $data = $query->result_array();
           return $data;
       }
       else{
        $ar = array();
       return $ar;
       }
    }
    public function getAjaxdataSettings($table){
        $result1=[];
        $query = $this->db->get($table);
        $result = $query->result_array();
        
        if($result){
            $result1 =$result;
            return  $result1 ;
        }         
        else{
            return $result1 ;
        }
    } 
}
?>