<?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Add Theme
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>addTheme" 
                            method="POST" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?> 
                              
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="theme_name"class="form-control input-sm" placeholder="Theme Name" required>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select  required class="form-control input-sm"  name="cat_id" id="catId">
                                                    <option value="">Select Category </option>
                                                    <?php 
                                                    foreach($category as $val)
                                                    { 
                                                        echo '<option value="'.$val['id'].'">'.$val['category_name'].'</option>';
                                                    }
                                                    ?> 
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select  required class="form-control input-sm" id="subCatId"  name="subcat_id">
                                                    <option value="">Select SubCategory </option>
                                                    
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control input-sm"  id="subsubCatId" class="form-control" name="subsubcat_id">
                                                    <option value="">Select SubsubCategory </option>
                                                    
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select  required class="form-control input-sm">
                                                    <option value="">Select City </option>
                                                    <?php 
                                                    foreach($city as $val)
                                                    { 
                                                        echo '<option value="'.$val['id'].'">'.$val['city_name'].'</option>';
                                                    }
                                                    ?> 
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="number" name="price" class="form-control input-sm" min="1" placeholder="Price">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        Inclusions
                                            <textarea rows="5" class="form-control input-sm html-editor" name="inclusions"  placeholder="Inclusions"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        Pre requisites
                                        <textarea rows="5" class="form-control input-sm html-editor" name="pre_requisites"  placeholder="Pre requisites"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        Facts
                                            <textarea rows="5" class="form-control input-sm html-editor" name="facts"  placeholder="Facts"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        Terms and Conditions
                                            <textarea rows="5" class="form-control input-sm html-editor" name="terms_and_conditins"  placeholder="Terms and Conditions"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        Theme Details
                                            <textarea rows="5" class="form-control input-sm html-editor" name="theme_detail"  placeholder="Theme Details"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        Theme Note
                                            <textarea rows="5" class="form-control input-sm html-editor" name="theme_note"  placeholder="Theme Note"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                         <div class="clearfix"></div>
			<div class="row">
				<div class="fileinput fileinput-new" data-provides="fileinput">
					<div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
					<div>
						<span class="btn btn-info btn-file">
							<span class="fileinput-new">Select Banner Image[1349x466]</span>
										
							<input type="file" name="theme_banner">
						</span>
								   
					</div>
				</div>
			</div>
                       
                            <div class="row" style='margin-left:2px;'>
                                <div class="col-sm-6">
                                    <div class="theme-text-box">                                                            
                                        <div class="row newImg">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                                    <img id="themefalseinput0" height="150" width="150">
                                                </div>
                                                <div>
                                                    <span class="btn btn-info btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input onchange="readURL(this,'themefalseinput0','themebaseInput0')" type="file" name="theme_image[]">
                                                    </span>                                                                        
                                                    <a style="cursor:pointer" class="btn btn-danger fileinput-exists removeThemeImg"
                                                        data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                        <a class="add-box btn btn-danger fileinput-exists" id="addmorethemes" style="cursor:pointer;margin-left:-12px;"> Add More + </a>
                                </div>             
                                <div class="col-sm-5 ">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select Video</span>
                                                
                                                <input class='allVideo' type="file" name="theme_video">
                                            </span>
                                            <a href="#" class="btn btn-danger fileinput-exists"
                                                data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>