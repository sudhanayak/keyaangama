<?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Edit Venue
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>Venue/updateVenue" 
                            method="POST" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?> 
                              
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="venue_name"class="form-control input-sm" placeholder="Venue Name" value="<?php echo $result[0]['venue_name']; ?>"required>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select required class="form-control input-sm"  name="venue_type" id="venueType">
                                                     <option value="<?php echo $result[0]['venue_type']; ?>"><?php echo $result[0]['venuetype']; ?> </option>
                                                    <option value="">Select Venue Type </option>
                                                    <?php 
                                                    foreach($resultvenue as $val)
                                                    { 
                                                        echo '<option value="'.$val['id'].'">'.$val['venue_type'].'</option>';
                                                    }
                                                    ?> 
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                   
                                                    <select class="form-control input-sm"  name="facility_id[]" multiple="true" id="Facility">
                                                     <option value="">select Facilities</option>
                                                     <?php 

                                                    foreach($result[0]['facilitylist'] as $val) { 
                                                      
                                                        if(in_array($val['id'], $facility)){

                                                            echo '<option selected="selected" value="'.$val['id'].'">'.$val['faciility_name'].'</option>';
                                                        }else{
                                                            echo '<option  value="'.$val['id'].'">'.$val['faciility_name'].'</option>';
                                                        }
                                                        
                                                    }
                                                    ?> 
                                                    </select>                      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select  required class="form-control input-sm"  name="capacity_id">
                                                    <option value="<?php echo $result[0]['capacity_id']; ?>"><?php echo $result[0]['capacity']; ?> </option>
                                                    <option value="">Select capacity</option>
                                                    <?php 
                                                    foreach($rescapacity as $val)
                                                    { 
                                                        echo '<option value="'.$val['id'].'">'.$val['capacity'].'</option>';
                                                    }
                                                    ?> 
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="number" name="price_start" class="form-control input-sm" min="1" placeholder="Enter Price" value="<?php echo $result[0]['price_start']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" class="form-control input-sm html-editor" name="venue_detail" placeholder="Enter venue details"><?php echo $result[0]['venue_detail']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        <textarea rows="5" class="form-control input-sm" name="address" placeholder="Enter address"><?php echo $result[0]['address']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         <div class="clearfix"></div>
                        <div class="row">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                    <img src='<?php echo $result[0]['image']; ?>' width="250" height="250" >
                                </div>
                                <div>
                                    <span class="btn btn-info btn-file">
                                        <span class="fileinput-new">Select Image</span>
                                                    
                                        <input type="file" name="image">
                                    </span>
                                               
                                </div>
                            </div>
                        </div>
                            <div class="row" style='margin-left:2px;'>
                                <div class="col-sm-6">
                                    <?php
                                    if($result['venueimglist'] != "") { 
                                        foreach ($result['venueimglist'] as $key => $venue_image) {
                                            if($venue_image['media_type'] == 1) { 
                                    ?>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                            <img  src='<?php echo ($venue_image['venue_image']); ?>' height="150" width="150">
                                        </div>
                                        <div>                              
                                        <a imgId='<?php echo $venue_image['id'] ?>' style="cursor:pointer" class="btn btn-danger removeVenueImg venueImgs"
                                                data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                    <?php } } } ?>  
                                    <div class="venue-text-box">                               
                                        <div class="row newImg">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                                    <img id="venuefalseinput0" height="150" width="150">
                                                </div>
                                                <div>
                                                    <span class="btn btn-info btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input onchange="readURL(this,'venuefalseinput0','venuebaseInput0')" type="file" name="venue_image[]">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <a class="add-box btn btn-danger fileinput-exists" id="addmorevenues" style="cursor:pointer;margin-left:-12px;"> Add More + </a>
                                </div>             
                                <div class="col-sm-5 ">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                            <?php
                                            if($result['venueimglist'] != "") { 
                                                foreach ($result['venueimglist'] as $key => $venue_image) {
                                                    if($venue_image['media_type'] == 2) { 
                                            ?><?php echo $venue_image['venue_image']; ?>
                                            <?php } } } ?>
                                        </div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select Video</span>
                                                
                                                <input class='allVideo' type="file" name="video">
                                            </span>
                                            <a href="#" class="btn btn-danger fileinput-exists"
                                                data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="hidden"  name="id" value="<?php echo $result[0]['id']; ?>">
                                    <input type="hidden" class="form-control" name="venue_code" value="<?php echo $result[0]['venue_code']; ?>">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>