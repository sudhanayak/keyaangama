<?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Edit Theme
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>Theme/updateTheme" 
                            method="POST" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?> 
                              
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="theme_name"class="form-control input-sm" placeholder="Theme Name" required value="<?php echo $result[0]['theme_name']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select  required class="form-control input-sm"  name="cat_id" id="catId">
                                                    <option value="<?php echo $result[0]['cat_id']; ?>"><?php echo $result[0]['category']; ?> </option>
                                                    <option value="">Select Category </option>
                                                    <?php 
                                                    foreach($category as $val)
                                                    { 
                                                        echo '<option value="'.$val['id'].'">'.$val['category_name'].'</option>';
                                                    }
                                                    ?> 
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select  required class="form-control input-sm" id="subCatId"  name="subcat_id">
                                                    <option value="<?php echo $result[0]['subcat_id']; ?>"><?php echo $result[0]['subcategory']; ?> </option>
                                                    <option value="">Select SubCategory </option>
                                                    
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select  required class="form-control input-sm"  id="subsubCatId" class="form-control" name="subsubcat_id">
                                                <option value="<?php echo $result[0]['subsubcat_id']; ?>"><?php echo $result[0]['subsubcategory']; ?> </option>
                                                    <option value="">Select SubsubCategory </option>
                                                    
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="number" name="price" class="form-control input-sm" min="1" placeholder="Price" value="<?php echo $result[0]['price']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        Inclusions
                                            <textarea rows="5" class="form-control input-sm html-editor" name="inclusions"  placeholder="Inclusions"><?php echo $result[0]['inclusions']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        Pre requisites
                                        <textarea rows="5" class="form-control input-sm html-editor" name="pre_requisites"  placeholder="Pre requisites"><?php echo $result[0]['pre_requisites']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        Facts
                                            <textarea rows="5" class="form-control input-sm html-editor" name="facts"  placeholder="Facts"><?php echo $result[0]['facts']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        Terms and Conditions
                                            <textarea rows="5" class="form-control input-sm html-editor" name="terms_and_conditins"  placeholder="Terms and Conditions"><?php echo $result[0]['terms_and_conditins']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        Theme Details
                                            <textarea rows="5" class="form-control input-sm html-editor" name="theme_detail"  placeholder="Theme Details"><?php echo $result[0]['theme_detail']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                           <div class="clearfix"></div>
                        <div class="row">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"><img src='<?php echo $result[0]['theme_banner']; ?>' width="250" height="250" ></div>
                                <div>
                                    <span class="btn btn-info btn-file">
                                        <span class="fileinput-new">Select Banner Image [1349x466]</span>
                                        <input type="file" name="theme_banner">
                                    </span>
                                </div>
                            </div>
                        </div>


                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        Theme Note
                                            <textarea rows="5" class="form-control input-sm html-editor" name="theme_note"  placeholder="Theme Note"><?php echo $result[0]['theme_note']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style='margin-left:2px;'>
                                <div class="col-sm-6">
                                    <?php
                                    if($result['themeimglist'] != "") { 
                                        foreach ($result['themeimglist'] as $key => $theme_image) {
                                            if($theme_image['media_type'] == 1) { 
                                    ?>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                            <img  src='<?php echo ($theme_image['theme_image']); ?>' height="150" width="150">
                                        </div>
                                        <div>                              
                                        <a imgId='<?php echo $theme_image['id'] ?>' style="cursor:pointer" class="btn btn-danger removeThemeImg themeGalImg"
                                                data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                    <?php } } } ?>  
                                    <div class="theme-text-box">
                                        <div class="row newImg">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                                    <img id="themefalseinput0" height="150" width="150">
                                                </div>
                                                <div>
                                                    <span class="btn btn-info btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input onchange="readURL(this,'themefalseinput0','themebaseInput0')" type="file" name="theme_image[]">
                                                    </span>                                                                        
                                                    <a style="cursor:pointer" class="btn btn-danger fileinput-exists removeThemeImg"
                                                        data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                        <a class="add-box btn btn-danger fileinput-exists" id="addmorethemes" style="cursor:pointer;margin-left:-12px;"> Add More + </a>
                                </div>             
                                <div class="col-sm-5 ">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                        <?php
                                            if($result['themeimglist'] != "") { 
                                                foreach ($result['themeimglist'] as $key => $theme_image) {
                                                    if($theme_image['media_type'] == 2) { 
                                            ?><?php echo $theme_image['theme_image']; ?>
                                                <!-- <video width="100px" height="100px"/><source src="<?php echo $theme_image['theme_image']; ?>"></video> -->
                                            <?php } } } ?>
                                        </div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select Video</span>
                                                
                                                <input class='allVideo' type="file" name="theme_video">
                                            </span>
                                            <a href="#" class="btn btn-danger fileinput-exists"
                                                data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="hidden"  name="id" value="<?php echo $result[0]['id']; ?>">
                                    <input type="hidden" class="form-control" name="theme_code" placeholder="Enter Your Theme code" value="<?php echo $result[0]['theme_code']; ?>">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>