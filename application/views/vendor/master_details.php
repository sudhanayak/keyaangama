       <?php include_once'header.php'; ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container container-alt">

                    <div class="block-header">
                        <h2>
                            <?php echo $result[0]['name']; ?>
                        </h2>
                    </div>

                    <div class="card" id="profile-main" style="min-height:800px">
                        <div class="pm-overview c-overflow">
                            <div class="pmo-pic">
                                <div class="p-relative">
                                     <img class="img-responsive" src="<?php echo $result[0]['logo']; ?>" alt="">
                                </div>
                                <!-- <div class="pmo-stat">
                                    <h2 class="m-0 c-white">1562</h2>
                                    Total Connections
                                </div> -->
                            </div>

                            <div class="pmo-block pmo-contact hidden-xs">
                                <h2>Contact</h2>
                                <ul>
                                    <li><i class="zmdi zmdi-phone"></i><?php echo $result[0]['mobile']; ?></li>
                                    <li><i class="zmdi zmdi-email"></i><?php echo $result[0]['email']; ?></li>
                                    <li ng-if='resultList.alt_mobile'><i class="zmdi zmdi-facebook-box"></i><?php echo $result[0]['fb_link']; ?></li>
                                    <li><i class="zmdi zmdi-twitter"></i><?php echo $result[0]['twitter_link']; ?></li>
                                    <li>
                                        <i class="zmdi zmdi-pin"></i>
                                        <address class="m-b-0 ng-binding">
                                            <?php echo $result[0]['address']; ?>
                                        </address>
                                    </li>
                                </ul>
                            </div>

                            <!-- <div class="pmo-block pmo-items hidden-xs">
                                <h2>Connections</h2>

                                <div class="pmob-body">
                                    <div class="row">
                                        <a class="col-xs-2">
                                            <img class="img-circle" src="assets/img/demo/profile-pics/1.jpg" alt="">
                                        </a>
                                        <a class="col-xs-2">
                                            <img class="img-circle" src="assets/img/demo/profile-pics/2.jpg" alt="">
                                        </a>
                                        <a  class="col-xs-2">
                                            <img class="img-circle" src="assets/img/demo/profile-pics/3.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        
                        <div class="pm-body clearfix">
                            <ul class="tab-nav tn-justified">
                                <li class="active"><a style="cursor: pointer;" class="vendorDetails">About</a></li>
                                <li><a style="cursor: pointer;" class="vendorPhotos">Photos</a></li>
                                <li><a>Timeline</a></li>
                                <li><a>Connections</a></li>
                            </ul>
                            <div  class="basic_profile">
                                <div class="pmb-block">
                                    <div class="pmbb-header">
                                        <h2><i class="zmdi zmdi-equalizer m-r-10"></i> Summary</h2>
                                    </div>
                                    <div class="pmbb-body p-l-30">
                                        <div class="pmbb-view">
                                           Vendor Details
                                        </div>
                                    </div>
                                </div>                    
                                <div class="pmb-block">
                                    <div class="pmbb-header">
                                        <h2><i class="zmdi zmdi-account m-r-10"></i> Basic Information</h2>
                                    </div>
                                    <div class="pmbb-body p-l-30">
                                        <div class="pmbb-view">
                                            <dl class="dl-horizontal">
                                                <dt>Full Name</dt>
                                                <dd><?php echo $result[0]['name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Email</dt>
                                                <dd><?php echo $result[0]['email']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Mobile</dt>
                                                <dd><?php echo $result[0]['mobile']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Alternate Mobile</dt>
                                                <dd><?php echo $result[0]['alt_mobile']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Address</dt>
                                                <dd><?php echo $result[0]['address']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Proof_type</dt>
                                                <dd><?php if($result[0]['proof_type'] == 1) { echo "Pan Card"; } elseif($result[0]['proof_type'] == 2) { echo "Aadhaar Number"; } elseif($result[0]['proof_type'] == 3) { echo "Driving License"; } elseif($result[0]['proof_type'] == 4) { echo "Voter Id"; } ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal" ng-if="resultList.proof_number">
                                                <dt>Proof Number</dt>
                                                <dd><?php echo $result[0]['proof_number']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal" ng-if="resultList.proof_number">
                                                <dt>Basic Image</dt>
                                                <dd><img class="img-responsive" src="<?php echo $result[0]['basic_image']; ?>" width="150px" height='150px' alt=""></dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
                                    <div class="pmb-block">
                                        <div class="pmbb-header">
                                            <h2><i class="zmdi zmdi-phone m-r-10"></i> BUSINESS DETAILS</h2>
                                        </div>
                                        <div class="pmbb-body p-l-30">
                                            <div class="pmbb-view">
                                              <div class="col-sm-6"> 

                                                <dl class="dl-horizontal">
                                                    <dt>Company Name</dt>
                                                    <dd><?php echo $result[0]['company_name']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal" ng-if="resultList.business_name">
                                                    <dt>Business Name</dt>
                                                    <dd><?php echo $result[0]['business_name']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal" ng-if="resultList.business_email">
                                                    <dt>Email</dt>
                                                    <dd><?php echo $result[0]['business_email']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal" ng-if="resultList.country">
                                                    <dt>Country</dt>
                                                    <dd><?php echo $result[0]['country']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal" ng-if="resultList.state">
                                                    <dt>State</dt>
                                                    <dd><?php echo $result[0]['state']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal" ng-if="resultList.city">
                                                    <dt>City</dt>
                                                    <dd><?php echo $result[0]['city']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal" ng-if="resultList.business_mobnumber">
                                                    <dt>Mobile Number</dt>
                                                    <dd><?php echo $result[0]['business_mobile_code']; ?>-<?php echo $result[0]['business_mobnumber']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal" ng-if="resultList.landline_number">
                                                    <dt>Land line Number</dt>
                                                    <dd><?php echo $result[0]['landline_code']; ?> - <?php echo $result[0]['landline_number']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal" ng-if="resultList.business_pancard">
                                                    <dt>Pan card</dt>
                                                    <dd><?php echo $result[0]['business_pancard']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal" ng-if="resultList.proofImg">
                                                    <dt>Proof Image</dt>
                                                    <dd><img class="img-responsive" src="<?php echo base_url()."/uploads/proofimage/".$result[0]['proofImg']; ?>" width="60" height="60" alt=""></dd>
                                                </dl>
                                            </div> 
                                            <div class="col-sm-6">  
                                                <dl class="dl-horizontal" ng-if="resultList.business_address">
                                                    <dt>Address</dt>
                                                    <dd><?php echo $result[0]['business_address']; ?></dd>
                                                </dl>
                                                 <dl class="dl-horizontal">
                                                    <dt>Company Register Number</dt>
                                                    <dd><?php echo $result[0]['company_reg_number']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Website Link </dt>
                                                    <dd><?php echo $result[0]['website_link']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Category</dt>
                                                    <dd><?php echo $result[0]['category']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Category Base Price</dt>
                                                    <dd><?php echo $result[0]['category_base_price']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Sub Category</dt>
                                                    <dd><?php echo $result[0]['subcategory']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Sub Category Base Price</dt>
                                                    <dd><?php echo $result[0]['subcat_base_price']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Sub Sub Category</dt>
                                                    <dd><?php echo $result[0]['subsubcategory']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Sub Sub Category Base Price</dt>
                                                    <dd><?php echo $result[0]['sub_sub_cat_base_price']; ?></dd>
                                                </dl>
                                             </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pmb-block">
                                    <div class="pmbb-header">
                                        <h2>Bank Details</h2>
                                    </div>
                                    <div class="pmbb-body p-l-30">
                                        <div class="pmbb-view">
                                            <dl class="dl-horizontal">
                                                <dt>Bank Name</dt>
                                                <dd><?php echo $result[0]['bank_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Bank Account Number</dt>
                                                <dd><?php echo $result[0]['bank_account_no']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Branch</dt>
                                                <dd><?php echo $result[0]['bank_branch']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>IFSC Code</dt>
                                                <dd><?php echo $result[0]['bank_ifsc']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Bank Account Name</dt>
                                                <dd><?php echo $result[0]['bank_account_name']; ?></dd>
                                            </dl>
                                            <?php
                                            if($result[0]['bank_upload_cancel_cheque'] !="" && $result[0]['bank_upload_cancel_cheque'] !="0"){
                                            ?>
                                            <dl class="dl-horizontal" ng-if="resultList.business_pancard">
                                                <dt>Cancel Check Image</dt>
                                                <dd><img class="img-responsive" width="150" height="150" src="<?php echo $result[0]['bank_upload_cancel_cheque']; ?>" alt="No Cancel Image"></dd>
                                            </dl>
                                            <?php
                                        }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                    <div class="pmb-block">
                                        <div class="pmbb-header">
                                            <h2><i class="zmdi zmdi-share"></i> SOCIAL MEDIA </h2>
                                        </div>
                                        <div class="pmbb-body p-l-30">
                                            <div class="pmbb-view">
                                              <div class="col-sm-12"> 
                                                <dl class="dl-horizontal" ng-if="resultList.fb_link">
                                                    <dt><i class="zmdi zmdi-facebook-box"></i></dt>
                                                    <dd><?php echo $result[0]['fb_link']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal" ng-if="resultList.twitter_link">
                                                    <dt><i class="zmdi zmdi-twitter"></i></dt>
                                                    <dd><?php echo $result[0]['twitter_link']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal" ng-if="resultList.google_plus_link">
                                                    <dt><i class="zmdi zmdi-linkedin"></i></dt>
                                                    <dd><?php echo $result[0]['linkedln_link']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal" ng-if="resultList.linkedln_link">
                                                    <dt><i class="zmdi zmdi-google-plus-box"></i></dt>
                                                    <dd><?php echo $result[0]['google_plus_link']; ?></dd>
                                                </dl>
                                            </div> 
                                            </div>
                                        </div>
                                    </div>                            
                            </div>                           
                          <!--
                          first tab end here
                          -->
                        <div   class="m-t-25 photo_profile" >
                            <div class="row">
                                <h3><center>Vendor Images.</center></h3>
                                <div class="lightbox">
                                    <?php 
                                    if($result['imglist'] != "") { 
                                        foreach ($result['imglist'] as $key => $img) {
                                            if($img['media_type'] == 1) { 
                                         ?>
                                        <div data-src="<?php echo ($img['image']); ?>" class="col-md-3 col-sm-4 col-xs-6">
                                            <div class="lightbox-item p-item">
                                                <img src="<?php echo ($img['image']); ?>" alt="" width="100px" height="100px"/>
                                            </div>
                                        </div>
                                        <?php
                                            }
                                        }
                                    } else {
                                        echo "<h4><center>No Vendor Images.</center></h4>";
                                    }
                                    ?>    
                                </div>
                            </div>
                            </div>                                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>