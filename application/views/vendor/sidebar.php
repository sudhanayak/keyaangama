<div class="s-profile">
<?php $vendor = $this->session->userdata('vendorCode');
  $logo=$this->Adminmodel->getSingleColumnName($vendor,'vendor_code',
                  'logo','keyaan_vendor'); ?>
                    <a href="#" data-ma-action="profile-menu-toggle">
                        <div class="sp-pic">
                            <img src="<?php echo base_url(); ?>uploads/vendorlogo/<?php echo $logo ; ?>" alt="">
                        </div>

                        <div class="sp-info">
                            <?php
                            
                            echo ucwords($this->session->userdata("vendorId"));
                            ?>

                            <i class="zmdi zmdi-caret-down"></i>
                        </div>
                    </a>

                    <ul class="main-menu">
<!--                        <li>
                            <a href="profile-about.html"><i class="zmdi zmdi-account"></i> View Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="zmdi zmdi-input-antenna"></i> Privacy Settings</a>
                        </li>
                        <li>
                            <a href="#"><i class="zmdi zmdi-settings"></i> Settings</a>
                        </li>-->
                        <li>
                            <a href="<?php echo base_url() ?>logoutvendor"><i class="zmdi zmdi-time-restore"></i> Logout</a>
                        </li>
                    </ul>
                    <ul class="main-menu">
<!--                        <li>
                            <a href="profile-about.html"><i class="zmdi zmdi-account"></i> View Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="zmdi zmdi-input-antenna"></i> Privacy Settings</a>
                        </li>
                        <li>
                            <a href="#"><i class="zmdi zmdi-settings"></i> Settings</a>
                        </li>-->
                        <li>
                            <a href="<?php echo base_url() ?>logout"><i class="zmdi zmdi-time-restore"></i> Logout</a>
                        </li>
                    </ul>
                </div>

                <ul class="main-menu">
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Theme</a>
                       <ul>
                           <li><a href="<?php echo base_url() ?>viewTheme">View Theme</a></li>
                           <li><a href="<?php echo base_url() ?>addTheme">Add Theme</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Service</a>
                       <ul>
                           <li><a href="<?php echo base_url() ?>viewService">View Service</a></li>
                           <li><a href="<?php echo base_url() ?>addService">Add Service</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Event Booking</a>
                       <ul>
                           <li><a href="<?php echo base_url() ?>viewVendoreventbooking">View Vendor Eventbooking </a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Orders</a>
                       <ul>
                           <li><a href="<?php echo base_url() ?>viewVendororders">View Orders</a></li>
                       </ul>
                   </li>
                   <?php //if($catData != 1) { ?>
                       <li class="sub-menu">
                            <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Venues</a>
                            <ul>
                                <li><a href="<?php echo base_url() ?>viewVenue">View Venues</a></li>
                                <li><a href="<?php echo base_url() ?>addVenue">Add Venues</a></li>
                            </ul>
                        </li>
                   <?php //} ?>
                   
                  <li class="sub-menu">
                     <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Vendor Products</a>
                     <ul>
                         <li><a href="<?php echo base_url() ?>viewVendorproducts">View Vendor Products</a></li>
                         <li><a href="<?php echo base_url() ?>addVendorproducts">Add Vendor Products</a></li>
                     </ul>
                 </li>
                 <li class="sub-menu">
                     <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Vendor Product Price</a>
                     <ul>
                         <li><a href="<?php echo base_url() ?>viewVendorproductprice">View Vendor Product Price</a></li>
                         <li><a href="<?php echo base_url() ?>addVendorproductprice">Add Vendor Product Price</a></li>
                     </ul>
                 </li>
                </ul>
                </div>

                