<?php include_once'header.php'; ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
               <div class="container container-alt">



                   <div class="card" id="profile-main">


                       <div class="pm-body clearfix" style="padding-left: 0px !important;">
                           <ul class="tab-nav tn-justified">
                               <li class="active"><a href="#" style="text-align: left;"><?php echo $result[0]['theme_name']; ?></a></li>

                           </ul>


                           <div class="pmb-block">
                               <div class="pmbb-header">
                                   <h2><i class="zmdi zmdi-equalizer m-r-10"></i> Summary</h2>

                                   <ul class="actions">
                                       <li class="dropdown">
                                           <a href="" data-toggle="dropdown">
                                               <i class="zmdi zmdi-more-vert"></i>
                                           </a>

                                           <ul class="dropdown-menu dropdown-menu-right">
                                               <li>
                                                   <a href="<?php echo base_url(); ?>Theme/editTheme/<?php echo $result[0]['id'] ?>">Edit</a>
                                               </li>
                                           </ul>
                                       </li>
                                   </ul>
                               </div>
                               <div class="pmbb-body p-l-30">
                                   <div class="pmbb-view">
                                       <dl class="dl-horizontal">
                                            <dt>Theme Name</dt>
                                            <dd><?php echo $result[0]['theme_name']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Category</dt>
                                            <dd><?php echo $result[0]['category']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Sub Category</dt>
                                            <dd><?php echo $result[0]['subcategory']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Sub Sub Category</dt>
                                            <dd><?php echo $result[0]['subsubcategory']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Inclusions</dt>
                                            <dd><?php echo $result[0]['inclusions']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Pre Requisites</dt>
                                            <dd><?php echo $result[0]['pre_requisites']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Facts</dt>
                                            <dd><?php echo $result[0]['facts']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Terms & Conditins</dt>
                                            <dd><?php echo $result[0]['terms_and_conditins']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Theme Details</dt>
                                            <dd><?php echo $result[0]['theme_detail']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Theme Note</dt>
                                            <dd><?php echo $result[0]['theme_note']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Theme Banner </dt>
                                            <dd>
                                                 <img class="img-responsive" 
                                                      src="<?php echo $result[0]['theme_banner']; ?>" 
                                                      alt="" width="100px" height="100px"/>
                                            </dd>
                                        </dl>
                                   </div>
                               </div>
                               <div class="pmbb-body p-l-30">
                                   <div class="pmbb-view">
                                   <?php 
                                    if($result['themeimglist'] != "") { 
                                        foreach ($result['themeimglist'] as $key => $img) {
                                            if($img['media_type'] == 1) { 
                                    ?>
                                       <div class="col-md-3" style="margin-bottom: 25px;">
                                           <img class="img-responsive" src="<?php echo ($img['theme_image']); ?>" alt="" width="100px" height="100px"/>
                                       </div>
                                       <?php } } } else { echo "<h4><center>No Theme Images.</center></h4>"; } ?>
                                   </div>
                               </div>
                               <div class="pmbb-body p-l-30">
                                   <div class="pmbb-view">
                                   <?php 
                                    if($result['themeimglist'] != "") { 
                                        foreach ($result['themeimglist'] as $key => $img) {
                                            if($img['media_type'] == 2) { 
                                    ?>
                                       <div class="col-md-3" style="margin-bottom: 25px;">
                                                <video width="100px" height="100px"/><source src="<?php echo ($img['theme_image']); ?>"></video>
                                       </div>
                                       <?php } } } else { echo "<h4><center>No Theme Video.</center></h4>"; } ?>
                                   </div>
                               </div>
                           </div>


                       </div>
                   </div>
               </div>
           </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>