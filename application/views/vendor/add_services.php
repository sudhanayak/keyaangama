<?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Add Services
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>addService" 
                            method="POST" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select  required class="form-control input-sm"  name="cat_id" id="catId">
                                                    <option value="">Select Category </option>
                                                    <?php 
                                                    foreach($category as $val)
                                                    { 
                                                        echo '<option value="'.$val['id'].'">'.$val['category_name'].'</option>';
                                                    }
                                                    ?> 
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select  required class="form-control input-sm" id="subCatId"  name="subcat_id">
                                                    <option value="">Select SubCategory </option>
                                                    
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>                                                              
                            </div> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select  required class="form-control input-sm"  id="subsubCatId" class="form-control" name="subsubcat_id">
                                                    <option value="">Select SubsubCategory </option>
                                                    
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select  required class="form-control input-sm"  name="city_id">
                                                    <option value="">Select City </option>
                                                    <?php 
                                                    foreach($city as $val)
                                                    { 
                                                        echo '<option value="'.$val['id'].'">'.$val['city_name'].'</option>';
                                                    }
                                                    ?> 
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>