<?php include_once'header.php'; ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
               <div class="container container-alt">
                   <div class="card" id="profile-main">
                       <div class="pm-body clearfix" style="padding-left: 0px !important;">

                           <div class="pmb-block">
                               <div class="pmbb-header">
                                   <h2><i class="zmdi zmdi-equalizer m-r-10"></i> Summary</h2>

                                   <ul class="actions">
                                       <li class="dropdown">
                                           <a href="" data-toggle="dropdown">
                                               <i class="zmdi zmdi-more-vert"></i>
                                           </a>

                                           <ul class="dropdown-menu dropdown-menu-right">
                                               <li>
                                                   <a href="<?php echo base_url(); ?>Services/editService/<?php echo $result[0]['service_code'] ?>">Edit</a>
                                               </li>
                                           </ul>
                                       </li>
                                   </ul>
                               </div>
                               <div class="pmbb-body p-l-30">
                                   <div class="pmbb-view">
                                       <dl class="dl-horizontal">
                                            <dt>Category</dt>
                                            <dd><?php echo $result[0]['category']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Subcategory</dt>
                                            <dd><?php echo $result[0]['subcategory']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Subsubcategory</dt>
                                            <dd><?php echo $result[0]['subsubcategory']; ?></dd>
                                        </dl>
                                   </div>
                               </div>
                       </div>
                   </div>
               </div>
           </div>
           </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>