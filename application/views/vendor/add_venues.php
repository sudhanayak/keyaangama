<?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Add Venue
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>addVenue" 
                            method="POST" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?> 
                              
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="venue_name"class="form-control input-sm" placeholder="Venue Name" required>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select required class="form-control input-sm"  name="venue_type" id="venueType">
                                                    <option value="">Select Venue Type </option>
                                                    <?php 
                                                    foreach($resultvenue as $val)
                                                    { 
                                                        echo '<option value="'.$val['id'].'">'.$val['venue_type'].'</option>';
                                                    }
                                                    ?> 
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm"  name="facility_id[]" multiple="true" id="Facility">
                                                     <option value="">select Facilities</option>
                                                    </select>                      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select  required class="form-control input-sm"  name="capacity_id">
                                                    <option value="">Select capacity</option>
                                                    <?php 
                                                    foreach($rescapacity as $val)
                                                    { 
                                                        echo '<option value="'.$val['id'].'">'.$val['capacity'].'</option>';
                                                    }
                                                    ?> 
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="number" name="price_start" class="form-control input-sm" min="1" placeholder="Enter Price">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" class="form-control input-sm html-editor" name="venue_detail"  placeholder="Enter venue details"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        <textarea rows="5" class="form-control input-sm" name="address" placeholder="Enter address"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         <div class="clearfix"></div>
                        <div class="row">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                <div>
                                    <span class="btn btn-info btn-file">
                                        <span class="fileinput-new">Select Image</span>
                                                    
                                        <input type="file" name="image">
                                    </span>
                                               
                                </div>
                            </div>
                        </div>
                            <div class="row" style='margin-left:2px;'>
                                <div class="col-sm-6">
                                    <div class="venue-text-box">                                                            
                                        <div class="row newImg">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                                    <img id="venuefalseinput0" height="150" width="150">
                                                </div>
                                                <div>
                                                    <span class="btn btn-info btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input onchange="readURL(this,'venuefalseinput0','venuebaseInput0')" type="file" name="venue_image[]">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                        <a class="add-box btn btn-danger fileinput-exists" id="addmorevenues" style="cursor:pointer;margin-left:-12px;"> Add More + </a>
                                </div>             
                                <div class="col-sm-5 ">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select Video</span>
                                                
                                                <input class='allVideo' type="file" name="video">
                                            </span>
                                            <a href="#" class="btn btn-danger fileinput-exists"
                                                data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>