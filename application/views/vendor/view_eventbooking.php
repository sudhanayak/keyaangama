       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
               <div class="container">                  
                   <div class="card">
                       <div class="card-header">
                            <div class="row">   
                                <div class="col-sm-6">   
                                    <h2>
                                    View Vendor Event Booking List
                                    </h2>
                                </div>
                                <div class="col-sm-3 ">  </div>
                                <!-- <div class="col-sm-3 ">  
                                    <div class="form-group">
                                        <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                    </div>
                                </div> -->
                            </div>
                       </div>
                       
                       <div class="card-body table-responsive">
                           <!-- <div class="alert alert-success" role="alert">
                               Successfully status Updated
                            </div>
                            <div class="alert alert-danger" role="alert">
                               Error
                            </div> -->
                           <table class="table table-striped">
                               <thead class="black white-text">
                               <tr>
                                <th>Sno</th>
                                <th>Category</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile Number</th>
                                <th>Action</th>
                               </tr>
                               </thead>
                               <tbody>
                                   <?php
                                    $i =$this->uri->segment(3)+1;
                                    $count = count(array_filter($result));
                                    if($count > 0) {
                                        foreach($result as $key => $row){
                                    ?>
                                   <tr class="zebra-striping">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row['category']; ?></td>
                                        <td><?php echo $row['user_name']; ?></td>
                                        <td><?php echo $row['user_email']; ?></td>
                                        <td><?php echo $row['user_mobile']; ?></td>
                                        <td><a data-toggle="tooltip"data-placement="bottom" title="View Details" href="<?php echo base_url(); ?>Vendoreventbooking/VendoreventbookingDetails/<?php echo $row['id'] ?>"><i class="zmdi zmdi-eye"></i></a>
                                    </tr>
                                    <?php
                                       $i++; }
                                    }else{
                                        ?>
                                    <tr><td colspan="7">No Eventbooking Found</td></tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                           </table>
                       </div>
                   </div>
               </div>                
           </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>