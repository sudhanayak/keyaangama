<?php include_once'header.php'; ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
               <div class="container container-alt">



                   <div class="card" id="profile-main">


                       <div class="pm-body clearfix" style="padding-left: 0px !important;">
                           <ul class="tab-nav tn-justified">
                               <li class="active"><a href="#" style="text-align: left;"><?php echo $result[0]['venue_name']; ?></a></li>

                           </ul>


                           <div class="pmb-block">
                               <div class="pmbb-header">
                                   <h2><i class="zmdi zmdi-equalizer m-r-10"></i> Summary</h2>

                                   <ul class="actions">
                                       <li class="dropdown">
                                           <a href="" data-toggle="dropdown">
                                               <i class="zmdi zmdi-more-vert"></i>
                                           </a>

                                           <ul class="dropdown-menu dropdown-menu-right">
                                               <li>
                                                   <a href="<?php echo base_url(); ?>Venue/editVenue/<?php echo $result[0]['id'] ?>">Edit</a>
                                               </li>
                                           </ul>
                                       </li>
                                   </ul>
                               </div>
                               <div class="pmbb-body p-l-30">
                                   <div class="pmbb-view">
                                       <dl class="dl-horizontal">
                                            <dt>Venue Name</dt>
                                            <dd><?php echo $result[0]['venue_name']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Venue Type</dt>
                                            <dd><?php echo $result[0]['venuetype']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Capacity</dt>
                                            <dd><?php echo $result[0]['capacity']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Price</dt>
                                            <dd><?php echo $result[0]['price_start']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Venue Details</dt>
                                            <dd><?php echo $result[0]['venue_detail']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Address</dt>
                                            <dd><?php echo $result[0]['address']; ?></dd>
                                        </dl>
                                         <dl class="dl-horizontal">
                                             <dt>Venue facilities</dt>
                                             <dd>
                                               <?php 
                                                if($result[0]['facilitylist'] != "") { 
                                                    foreach ($result[0]['facilitylist'] as $key => $val) {
                                                        if(in_array($val['id'], $facility)){
                                                          echo $val['faciility_name'];
                                                ?>
                                                   <?php } } } else { echo "<h4><center>No Venue Facilities.</center></h4>"; } ?>
                                           </dd>
                                         </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Venue Image</dt>
                                            <dd>
                                                 <img class="img-responsive" 
                                                      src="<?php echo $result[0]['image']; ?>" 
                                                      alt="" width="100px" height="100px"/>
                                            </dd>
                                        </dl>
                                   </div>
                               </div>
                               <div class="pmbb-body p-l-30">
                                   <div class="pmbb-view">
                                   <?php 
                                    if($result['venueimglist'] != "") { 
                                        foreach ($result['venueimglist'] as $key => $img) {
                                            if($img['media_type'] == 1) { 
                                    ?>
                                       <div class="col-md-3" style="margin-bottom: 25px;">
                                           <img class="img-responsive" src="<?php echo ($img['venue_image']); ?>" alt="" width="100px" height="100px"/>
                                       </div>
                                       <?php } } } else { echo "<h4><center>No Venue Images.</center></h4>"; } ?>
                                   </div>
                               </div>
                               <div class="pmbb-body p-l-30">
                                   <div class="pmbb-view">
                                   <?php 
                                    if($result['venueimglist'] != "") { 
                                        foreach ($result['venueimglist'] as $key => $img) {
                                            if($img['media_type'] == 2) { 
                                    ?>
                                       <div class="col-md-3" style="margin-bottom: 25px;">
                                                <video width="100px" height="100px"/><source src="<?php echo ($img['venue_image']); ?>"></video>
                                       </div>
                                       <?php } } } else { echo "<h4><center>No Venue Video.</center></h4>"; } ?>
                                   </div>
                               </div>
                           </div>


                       </div>
                   </div>
               </div>
           </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>