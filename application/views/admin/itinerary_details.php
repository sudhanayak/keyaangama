<?php include_once'header.php'; ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
               <div class="container container-alt">
                   <div class="card" id="profile-main">
                       <div class="pm-body clearfix" style="padding-left: 0px !important;">
                           <div class="pmb-block">
                               <div class="clearfix"></div>
                               <div class="pmbb-header">
                                   <h2><i class="zmdi zmdi-equalizer m-r-10"></i>Itinerary Details</h2>
                               </div>
                               <div class="pmbb-body p-l-30">
                                   <div class="pmbb-view">
                                        <div class="col-sm-6">
                                            <?php
                                            $i =1;
                                            $count = count(array_filter($Iteneraryname));
                                            if($count > 0) {
                                                foreach($Iteneraryname as $key => $row){
                                            ?>
                                            <dl class="dl-horizontal">
                                                <dt>Package</dt>
                                                <dd><?php echo $row['package_name']; ?></dd>
                                            </dl>
                                             <dl class="dl-horizontal">
                                                <dt>Itinerary</dt>
                                                <dd><?php echo $row['itenerary']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Itinerary Name</dt>
                                                <dd><?php echo $row['itenerary_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                               <dt>Itinerary Price</dt>
                                               <dd><?php echo $row['itinerary_price']; ?></dd>
                                           </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Itinerary Type</dt>
                                                <dd><?php if($row['itinerary_type'] == 1) { echo "Image"; } elseif($row['itinerary_type'] == 2) { echo "Description"; } elseif($row['itinerary_type'] == 3) { echo "Images and Description"; } ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Event Day</dt>
                                                <dd><?php echo "Day".$row['event_day']; ?></dd>
                                            </dl>
                                            <?php if($row['itinerary_type'] == 2 || $row['itinerary_type'] == 3) { ?>
                                            <dl class="dl-horizontal">
                                                <dt>Itinerary Details</dt>
                                                <dd><?php echo $row['details']; ?></dd>
                                            </dl>
                                            <?php } ?>
                                            <?php if($row['itinerary_type'] == 1 || $row['itinerary_type'] == 3) { ?>
                                            <dl class="dl-horizontal">
                                                <dt>Itinerary Images</dt>
                                                <?php 
                                                if($row['itineraryImg'] != "") { 
                                                    foreach ($row['itineraryImg'] as $key => $img) {
                                                        if($img['media_type'] == 1) { 
                                                ?>
                                                    <dd><img src="<?php echo base_url()."uploads/itineraryimg/".$img['images']; ?>" style="height:100px;width: 100px"></dd>
                                            <?php } } } else { echo "<h4><center>No Itinerary Images.</center></h4>"; } ?> 
                                            </dl> 
                                            <?php }
                                            $i++; ?>
                                            <dl class="dl-horizontal">
                                              <dt></dt>
                                              <dd>
                                              <a href="<?php echo base_url()?>Itinerary/editItinerary/<?php echo $row['package_code'];?>/<?php echo $row['itinerary_code'];?>"><button type="submit" class="btn  btn-fill btn-primary btn-wd waves-effect">Edit Itinerary</button></a>
                                              </dd>
                                            </dl> 
                                          <?php }
                                            
                                            }else{
                                             }
                                        ?>
                                        </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>