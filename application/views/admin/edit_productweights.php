       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Edit Product Weights
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>Productweights/updateProductweights" method="POST" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?> 
                                
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm"  name="master_category_id" id="masterCategory">
                                                    <option value="<?php echo $result['master_category_id']; ?>"><?php echo $result['mastercategory']; ?> </option> 
                                                    <option value="">Select Master Category</option>
                                                    <?php                    
                                                        $count = count(array_filter($resultCat));
                                                        if($count > 0) {
                                                        $i=0;
                                                        foreach($resultCat as $key => $row){
                                                        ?>
                                                            <option value="<?php echo  $row['id'] ?>"><?php echo  $row['master_category_name'] ?></option>
                                                        <?php
                                                        }
                                                        }
                                                    ?>
                                                  </select>                               
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div> 
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm"  name="category_id" id="cateringcatId">
                                                    <option value="<?php echo $result['category_id']; ?>"><?php echo $result['category']; ?> </option> 
                                                     <option value="">Select  Category</option>
                                                    </select>                      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div> 
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm"  name="subcategory_id" id="cateringsubCatId">
                                                    <option value="<?php echo $result['subcategory_id']; ?>"><?php echo $result['subcategory']; ?> </option>
                                                     <option value="">Select  Subcategory</option>
                                                    </select>                      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="product_weight" class="form-control input-sm" placeholder="Enter product weight" value="<?php echo $result['product_weight']; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                     <input type="hidden"  name="id" value="<?php echo $result['id']; ?>">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>