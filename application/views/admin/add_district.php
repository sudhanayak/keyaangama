       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Add District
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>district/addDistrict" method="POST" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select id="countryId" class="form-control input-sm" name="country_id">
                                                    <option value="">Select Country</option>
                                                    <?php
                                                        
                                                        $count = count(array_filter($resultCnt));
                                                        if($count > 0) {
                                                        $i=0;
                                                        foreach($resultCnt as $key => $row){
                                                        ?>
                                                            <option value="<?php echo  $row['id'] ?>"><?php echo  $row['country_name'] ?></option>
                                                        <?php
                                                        }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select id="stateId" class="form-control input-sm" name="state_id">
                                                    <option>Select State</option>
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="district_name" class="form-control input-sm" placeholder="District Name" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>