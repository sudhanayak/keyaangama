       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
               <div class="container">                  
                   <div class="card">
                       <div class="card-header">
                            <div class="row">   
                                <div class="col-sm-6">   
                                    <h2>
                                    Orders List
                                    </h2>
                                </div>
                                <div class="col-sm-6">
                                   <div class='row'>
                                       <form action='<?php echo base_url() ?>viewOrders' method='GET'>
                                           <div class="col-sm-6 ">
                                               <div class="form-group pull-right">
                                                   <input type="text" name='search' value="<?php echo $searchVal; ?>" ng-model="search" class="form-control" placeholder="Search">
                                               </div>
                                           </div>
                                           <div class="col-sm-6 ">
                                               <div class="form-group pull-right">
                                                   <button type="submit" class="btn  btn-fill btn-primary btn-wd waves-effect">Search</button>
                                               </div>
                                           </div>
                                       </form>
                                   </div>
                               </div>
                                <div class="col-sm-3 ">  </div>
                                
                            </div>
                       </div>
                       
                       <div class="card-body table-responsive">
                          <?php
                                //echo $this->session->flashdata('msg');
                            ?>
                           <table class="table table-striped">
                               <thead class="black white-text">
                               <tr>
                                <th>Sno</th>
                                <th>User Name</th>
                                <th>Order Id</th>
                                <th>Total Amount</th>
                                <th>Advance Amount</th>
                                <!-- <th>Payment Status</th>
                                <th>Payment Method</th> -->
                                <th>Action</th>
                               </tr>
                               </thead>
                               <tbody>
                                   <?php
                                    $i =$this->uri->segment(3)+1;
                                    $count = count(array_filter($result));
                                    if($count > 0) {
                                        foreach($result as $key => $row){
                                    ?>
                                   <tr class="zebra-striping">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row['user_name']; ?></td>
                                        <td><?php echo $row['order_id']; ?></td>
                                        <td><?php echo $row['total_amount']; ?></td>
                                        <td><?php echo $row['advance_amount']; ?></td>
                                        <!-- <td><?php echo $payment_status; ?></td>
                                        <td><?php echo $payment_type; ?></td> -->
                                        <td><a data-toggle="tooltip"data-placement="bottom" title="View Details" href="<?php echo base_url()?>Orders/orderDetails/<?php echo $row['order_id'];?>/<?php echo $row['user_id'];?>" ><i class="zmdi zmdi-eye"></i></a>
                                        <?php
                                            if($row['approve_by_admin']==0){
                                            ?>
                                            <a data-toggle="tooltip"data-placement="bottom" title="Disapprove Admin" onclick="return confirm('Confirm to approve?');" href="<?php echo base_url()?>Orders/orderApprove/<?php echo $row['id'];?>"> <i class="zmdi zmdi-close-circle-o"></i></a> 
                                           <?php
                                            }else{
                                           ?>
                                            <a data-toggle="tooltip"data-placement="bottom" title="Approve Admin" onclick="return confirm('Confirm to disapprove?');" href="<?php echo base_url()?>Orders/orderDisapprove/<?php echo $row['id'];?>" style="cursor:pointer"><i class="zmdi zmdi-check"></i></a>
                                            <?php
                                            }
                                        ?>
                                        </td>
                                    </tr>
                                    <?php
                                       $i++; }
                                    }else{
                                        ?>
                                    <tr><td colspan="7">No Orders Found</td></tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                           </table>
                           <div class='col-md-12'>
                                <div class='pull-right'><?php echo $links; ?></div>
                            </div>
                            <!--
                            //sample data start here
                            -->
                            
                       </div>
                   </div>
               </div>                
           </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>