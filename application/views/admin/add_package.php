       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Add Package
                            </h2>
                        </div>
                        <form method="POST"  action="<?php echo base_url() ?>addPackage" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?>    
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="package_name" class="form-control input-sm" placeholder="Package Name" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm packagecategory"  name="category_id" id="catmultiple">
                                                     <option value="">Select Category </option>
                                                     <?php 
                                                        foreach($category as $val)
                                                        { 
                                                            echo '<option value="'.$val['id'].'">'.$val['category_name'].'</option>';
                                                        }
                                                     ?> 
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm packagesubcategory"  name="sub_category_id">
                                                     <option value="">select Subcategory </option>
                                                    </select>                      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div id="cat_type" style="display: none"> 
                                <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="fg-line">
                                                    <input type="number" name="number_of_days" class="form-control input-sm" placeholder="Enter number of days">
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="number" name="price" class="form-control input-sm" placeholder="Enter Price" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            Details
                                            <textarea rows="5" name="package_detail" class="form-control input-sm html-editor"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="package-image">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"><img id="falseinput0" height="150" width="150"></div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select Package image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input onchange="readURL(this,'falseinput0','baseInput0')" type="file" name="image[]" required>
                                            </span>
                                            <a class="add-box btn btn-danger fileinput-exists" id="addmorepacakges" style="cursor:pointer"> Add More + </a>
                                        </div>
                                    </div>
                                </div>
                            </div></div>
                            <div class="package-highlights">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="highlights[]" class="form-control input-sm" placeholder="Enter package highlights" id="Packagehighlights">
                                            </div>
                                            <a class="add-box btn btn-danger fileinput-exists" id="addmorepackagehighlights" style="cursor:pointer"> Add More + </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <button  type='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>