       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Edit Resumes
                            </h2>
                        </div>
                        <form method="POST"  action="<?php echo base_url() ?>Resumes/updateResumes" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?>    
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="name" class="form-control input-sm" placeholder="Name" required value="<?php echo $result[0]['name']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        <input type="text" name="email" class="form-control input-sm" placeholder="Email" required value="<?php echo $result[0]['email']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        <input type="text" name="mobile_number" class="form-control input-sm" placeholder="Mobile Number" required value="<?php echo $result[0]['mobile_number']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <Select class="form-control" name="experience_years">
                                                    <option value="<?php echo $result[0]['experience_years']; ?>"><?php if($result[0]['experience_years'] == 1) { echo "1 Year" ;} elseif($result[0]['experience_years'] == 2) { echo "2 Years" ;} elseif($result[0]['experience_years'] == 3) { echo "3 Years" ;} elseif($result[0]['experience_years'] == 4) { echo "4 Years" ;} elseif($result[0]['experience_years'] == 5) { echo "5 Years" ;} elseif($result[0]['experience_years'] == 6) { echo "6 Years" ;} elseif($result[0]['experience_years'] == 7) { echo "7 Years" ;} elseif($result[0]['experience_years'] == 8) { echo "8 Years" ;} elseif($result[0]['experience_years'] == 9) { echo "9 Years" ;} elseif($result[0]['experience_years'] == 10) { echo "10 Years" ;}  ?> </option>
                                                    <option>Select Experience in Years</option>
                                                    <option value="1">1 Year</option>
                                                    <option value="2">2 Years</option>
                                                    <option value="3">3 Years</option>
                                                    <option value="4">4 Years</option>
                                                    <option value="5">5 Years</option>
                                                    <option value="6">6 Years</option>
                                                    <option value="7">7 Years</option>
                                                    <option value="8">8 Years</option>
                                                    <option value="9">9 Years</option>
                                                    <option value="10">10 Years</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <Select class="form-control" name="experience_months">
                                                    <option value="<?php echo $result[0]['experience_months']; ?>"><?php if($result[0]['experience_months'] == 1) { echo "1 Month" ;} elseif($result[0]['experience_months'] == 2) { echo "2 Months" ;} elseif($result[0]['experience_months'] == 3) { echo "3 Months" ;} elseif($result[0]['experience_months'] == 4) { echo "4 Months" ;} elseif($result[0]['experience_months'] == 5) { echo "5 Months" ;} elseif($result[0]['experience_months'] == 6) { echo "6 Months" ;} elseif($result[0]['experience_months'] == 7) { echo "7 Months" ;} elseif($result[0]['experience_months'] == 8) { echo "8 Months" ;} elseif($result[0]['experience_months'] == 9) { echo "9 Months" ;} elseif($result[0]['experience_months'] == 10) { echo "10 Months" ;}  ?> </option>
                                                    <option>Select Experience in Months</option>
                                                    <option value="1">1 Month</option>
                                                    <option value="2">2 Months</option>
                                                    <option value="3">3 Months</option>
                                                    <option value="4">4 Months</option>
                                                    <option value="5">5 Months</option>
                                                    <option value="6">6 Months</option>
                                                    <option value="7">7 Months</option>
                                                    <option value="8">8 Months</option>
                                                    <option value="9">9 Months</option>
                                                    <option value="10">10 Months</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        <input type="text" name="ctc" class="form-control input-sm" placeholder="ctc" required value="<?php echo $result[0]['ctc']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        <input type="text" name="ectc" class="form-control input-sm" placeholder="Expected CTC" required value="<?php echo $result[0]['ectc']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        <input type="text" name="notice_period" class="form-control input-sm" placeholder="Notice Period" required value="<?php echo $result[0]['notice_period']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                       <?php echo $result[0]['resume']; ?></div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select Resumes</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="resume">
                                            </span>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="hidden" name="id" class="form-control input-sm" placeholder="id" required value="<?php echo $result[0]['id']; ?>">
                                    <button  type='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>