<?php include_once'header.php'; ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
               <div class="container container-alt">
                   <div class="card" id="profile-main">
                       <div class="pm-body clearfix" style="padding-left: 0px !important;">
                           <div class="pmb-block">
                               <div class="pmbb-header">
                                   <h2><i class="zmdi zmdi-equalizer m-r-10"></i>Package Summary</h2>
                               </div>
                               <div class="pmbb-body p-l-30">
                                   <div class="pmbb-view">
                                        <div class="col-sm-6">
                                            <dl class="dl-horizontal">
                                                <dt>Package Name</dt>
                                                <dd><?php echo $result['package_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                               <dt>Price</dt>
                                               <dd><?php echo $result['price']; ?></dd>
                                           </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Category</dt>
                                                <dd><?php echo $result['category']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Number of Days</dt>
                                                <dd><?php echo $result['number_of_days']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Details</dt>
                                                <dd><?php echo $result['package_detail']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Package Images</dt>
                                                <?php 
                                                if($result['packageimage'] != "") { 
                                                    foreach ($result['packageimage'] as $key => $img) {
                                                        if($img['media_type'] == 1) { 
                                                ?>
                                                    <dd><img src="<?php echo base_url()."uploads/packageimg/".$img['image']; ?>" style="height:100px;width: 100px"></dd>
                                            <?php } } } else { echo "<h4><center>No Packages Images.</center></h4>"; } ?>
                                            </dl>
                                            <dl class="dl-horizontal">
                                               <dt>Package Highlights</dt>
                                               <?php
                                               if($result['packagehighlights'] != "") {
                                                   foreach ($result['packagehighlights'] as $key => $highlights) {
                                               ?>
                                                   <dd><?php echo $highlights['highlights']; ?></dd>
                                           <?php } } else { echo "<h4><center>No Package Highlights.</center></h4>"; } ?>
                                           </dl>
                                        </div>
                                   </div>
                               </div>
                               <div class="clearfix"></div>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>