       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Edit Paymentsetting
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>Paymentsetting/updatePaymentsetting" method="POST" enctype="multipart/form-data" >
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                               <select required class="form-control input-sm" id="payment_mode" name="payment_mode"> 
                                                <option value="<?php echo $result[0]['payment_mode']; ?>">
                                                    <?php if($result[0]['payment_mode'] == 1){ echo 'COD'; } 
                                                     elseif($result[0]['payment_mode'] == 2){ echo 'Online'; } ?> 
                                                </option>
                                                <option value="">Select Payment mode</option>
                                                    <option value="1">COD</option>
                                                    <option value="2">Online</option>
                                                </select>                  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if($result[0]['payment_mode'] == 2){ ?><div id="payment_cod"> <?php } else { ?><div id="payment_cod" style="display: none"> <?php } ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                               <select class="form-control input-sm" id="payment_gateway" name="payment_gateway"> 
                                                <option value="<?php echo $result[0]['payment_gateway']; ?>">
                                                    <?php if($result[0]['payment_gateway'] == 1){ echo 'Paytm'; }
                                                    elseif($result[0]['payment_gateway'] == 2){ echo 'Payumoney'; } 
                                                    elseif($result[0]['payment_gateway'] == 3){ echo 'ccavenue'; }
                                                    elseif($result[0]['payment_gateway'] == 0 || $result[0]['payment_gateway'] == ''){ echo 'Select Payment gateway'; } ?>  
                                                </option>
                                                <option value="">Select Payment gateway</option>
                                                    <option value="1">Paytm</option>
                                                    <option value="2">Payumoney</option>
                                                    <option value="3">ccavenue</option>
                                                </select>                  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select id="payment_status" class="form-control input-sm" name="payment_status">
                                                    <option value="<?php echo $result[0]['payment_mode']; ?>">
                                                    <?php if($result[0]['payment_status'] == 1){ echo 'Sand box'; } 
                                                    elseif($result[0]['payment_status'] == 2){ echo 'Live'; }
                                                    elseif($result[0]['payment_status'] == 0 || $result[0]['payment_status'] == ''){ echo 'Select Payment status'; } ?> 
                                                    </option>
                                                    <option value="">Select Payment status</option>
                                                    <option value="1">Sand box</option>
                                                    <option value="2">Live</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="merchant_keys"class="form-control input-sm" placeholder="Merchant keys"
                                            value="<?php echo $result[0]['merchant_keys']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="salt_keys"class="form-control input-sm" placeholder="Salt keys"
                                            value="<?php echo $result[0]['salt_keys']; ?>"
                                             >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="hidden"  name="id" value="<?php echo $result[0]['id']; ?>">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>