       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Basic Settings
                            </h2>
                        </div>
                        <form enctype="multipart/form-data" method="POST" action="<?php echo base_url() ?>/basicsettings/updatebasic_settings">   
                            <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="site_name" class="form-control input-sm" placeholder="Website Name" value="<?php echo $result[0]['site_name']; ?>"  required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="email" name="site_email" class="form-control input-sm" placeholder="Contact Email" value="<?php echo $result[0]['site_email']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="site_mobile" pattern="[0-9]{10}" class="form-control input-sm" placeholder="Contact Mobile" value="<?php echo $result[0]['site_mobile']; ?>"  required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="site_landline" class="form-control input-sm" placeholder="Contact Number" value="<?php echo $result[0]['site_landline']; ?>"  required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                         <img src='<?php echo base_url() ?>uploads/site_logo/<?php echo $result[0]['site_logo']; ?>' width="250" height="250" >
                                        </div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="image">
                                            </span>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="order_email" class="form-control input-sm" placeholder="Order Email" value="<?php echo $result[0]['order_email']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="enquiry_email" class="form-control input-sm" placeholder="Enquiry Email" value="<?php echo $result[0]['enquiry_email']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="customercare_number" pattern="[0-9]{10}" class="form-control input-sm" placeholder="customer care number" value="<?php echo $result[0]['customercare_number']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="order_comission" class="form-control input-sm" placeholder="order comission" value="<?php echo $result[0]['order_comission']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="details" class="form-control input-sm" placeholder="site details"><?php echo $result[0]['site_detail']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="tag" class="form-control input-sm" placeholder="tag"><?php echo $result[0]['meta_tag']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="keyword" class="form-control input-sm" placeholder="keyword"><?php echo $result[0]['meta_keyword']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="site_address" class="form-control input-sm" placeholder="Description"><?php echo $result[0]['site_address']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="site_footertext" class="form-control input-sm" placeholder="Description"><?php echo $result[0]['site_footertext']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                         <img src='<?php echo base_url() ?>uploads/office_image/<?php echo $result[0]['office_image']; ?>' width="250" height="250">
                                        </div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="office_image">
                                            </span>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="hidden" value="<?php echo $result[0]['id']; ?>" name="id">
                                    <button  type='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>