<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Add Employee
                            </h2>
                        </div>
                        <form method="POST"  action="<?php echo base_url() ?>Employee/addEmployee" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" placeholder="Enter First Name" name="emp_fname">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                    <input type="text" class="form-control" placeholder="Enter Last Name" name="emp_lname">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                    <input type="email" class="form-control" placeholder="Email" name="emp_email">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="emp_mobile" placeholder="Mobile Number">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control date-picker" name="emp_dob" placeholder="Date Of Birth">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                    <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="emp_age" placeholder="Age">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control" name="emp_marital_status">
                                            <option value="">Select Marital Status</option>
                                            <option value="1">Yes</option>
                                            <option value="2">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                    <input type="text" class="form-control" name="emp_designation" placeholder="Designation">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control" name="any_disability">
                                            <option value="">Disability</option>
                                            <option value="1">Yes</option>
                                            <option value="2">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1" style="display:none">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="disability_detail" placeholder="Disability Detail">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="master_degree" placeholder="Master Degree">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="master_passout" placeholder="Master Passout Year">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="master_spl" placeholder="Master Specialization">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="master_intitutes" placeholder="Master Institutes">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="graduation" placeholder="Graduation">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="graduation_passout" placeholder="Graduation Passout Year">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="graduation_spl" placeholder="Graduation Specialization">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="graduation_intitutes" placeholder="Graduation Institutes">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="inter" placeholder="Inter">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="inter_passout" placeholder="Inter Passout Year">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="inter_spl" placeholder="Inter Specialization">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="inter_intitutes" placeholder="Inter Institutes">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="father_name" placeholder="Father Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="mother_name" placeholder="Mother Name">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="countryId" class="form-control" name="per_country">
                                                <option value="">Select Country</option>
                                                <?php
                                                    $count = count(array_filter($resultCnt));
                                                    if($count > 0) {
                                                    $i=0;
                                                    foreach($resultCnt as $key => $row){
                                                    ?>
                                                        <option value="<?php echo  $row['id'] ?>"><?php echo  $row['country_name'] ?></option>
                                                    <?php
                                                    }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="stateId" class="form-control" name="per_state">
                                                <option>Select State</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="districtId" class="form-control" name="per_district">
                                                <option>Select District</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="cityId" class="form-control" name="per_city">
                                                <option>Select City</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="pincodeId" class="form-control" name="per_pincode">
                                                <option>Select Pincode</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="locationId" class="form-control" name="per_address">
                                                <option>Select Location</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="countryId1" class="form-control" name="res_country">
                                                <option value="">Select Country</option>
                                                <?php
                                                    $count = count(array_filter($resultCnt1));
                                                    if($count > 0) {
                                                    $i=0;
                                                    foreach($resultCnt as $key => $row){
                                                    ?>
                                                        <option value="<?php echo  $row['id'] ?>"><?php echo  $row['country_name'] ?></option>
                                                    <?php
                                                    }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="stateId1" class="form-control" name="res_state">
                                                <option>Select State</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="districtId1" class="form-control" name="res_district">
                                                <option>Select District</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="cityId1" class="form-control" name="res_city">
                                                <option>Select City</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="pincodeId1" class="form-control" name="res_pincode">
                                                <option>Select Pincode</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="locationId1" class="form-control" name="res_address">
                                                <option>Select Location</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control" name="grade_type">
                                                <option value="">Select Grade Type</option>
                                                <option value="1">Label 1</option>
                                                <option value="2">Label 2</option>
                                                <option value="3">Label 3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="total_salary" placeholder="Total Salary">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="take_home" placeholder="Take Home">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="pf" placeholder="PF">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="pf_amount" placeholder="PF Amount">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="bank_name" placeholder="Bank Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="account_number" placeholder="Account Number">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="designation" placeholder="Designation">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="company_name" placeholder="Company Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="emp_mode" placeholder="Mode of Employee">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control date-picker" name="from_date" placeholder="From Date">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control date-picker" name="to_date" placeholder="To Date">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                    <div>
                                        <span class="btn btn-info btn-file">
                                            <span class="fileinput-new">Select Profile</span>
                                            <input type="file" name="emp_profile_img">
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                    <div>
                                        <span class="btn btn-info btn-file">
                                            <span class="fileinput-new">Select Resume</span>
                                            <input type="file" name="resume">
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row text-box">
                                <div class="col-sm-3 col-sm-offset-1">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" class="form-control" name="doc_type" placeholder="Type of Document">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-sm-offset-1">
                                    <div class="">           
                                        <div class="row newImg">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                                    <img id="falseinput0" height="150" width="150">
                                                </div>
                                                <div>
                                                    <span class="btn btn-info btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input onchange="readURL(this,'falseinput0','baseInput0')" type="file" name="doc_img[]">
                                                    </span>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-sm-offset-1">
                                    <a style="cursor:pointer" class="btn btn-danger fileinput-exists removeEmployeeImg" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                            <a class="add-box btn btn-danger fileinput-exists" id="addmoredoc" style="cursor:pointer"> Add More + </a>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-sm-5 col-sm-offset-1">
                                    <button  type='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>