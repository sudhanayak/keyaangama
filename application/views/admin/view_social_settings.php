       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
               <div class="container">                  
                   <div class="card">
                       <div class="card-header">
                            <div class="row">   
                                <div class="col-sm-6">   
                                    <h2>
                                    Social Settings List
                                    </h2>
                                </div>
                                <div class="col-sm-3 ">  </div>
                            </div>
                       </div>
                       <div class="card-body table-responsive">
                           <table class="table table-striped">
                               <thead class="black white-text">
                               <tr>
                                <th>Sno</th>
                                <th>Type</th>
                                <th>Link</th>
                                <th>Action</th>
                               </tr>
                               </thead>
                               <tbody>
                                   <?php
                                   $i =$this->uri->segment(3)+1;
                                    $count = count(array_filter($result));
                                    if($count > 0) {
                                        foreach($result as $key => $row){
                                    ?>
                                   <tr class="zebra-striping">
                                        <td><?php echo $i; ?></td>
                                        <td><?php if($row['type'] == 1) { echo "Facebook"; } elseif($row['type'] == 2) { echo "Googleplus"; } elseif($row['type'] == 3) { echo "Twitter"; } elseif($row['type'] == 4) { echo "Linkedin"; } ;?></td>
                                        <td><?php echo $row['link'];?></td>
                                        <td>
                                            <a href="<?php echo base_url(); ?>SocialSettings/editSocialSettings/<?php echo $row['id'] ?>"><i class="zmdi zmdi-edit"></i></a>
                                            <?php
                                            if($row['keyaan_status']==1){
                                            ?>
                                            <a onclick="return confirm('Confirm to Enable?');" href="<?php echo base_url()?>SocialSettings/SocialSettingsEnable/<?php echo $row['id'];?>"> <i class="zmdi zmdi-close-circle-o"></i></a> 
                                           <?php
                                            }else{
                                           ?>
                                            <a onclick="return confirm('Confirm to Disable?');" href="<?php echo base_url()?>SocialSettings/SocialSettingsDisable/<?php echo $row['id'];?>" style="cursor:pointer"><i class="zmdi zmdi-check"></i></a>
                                            <?php
                                            }
                                            ?> 
                                            <a onclick="return confirm('Are You Sure to Delete ?');" href="<?php echo base_url()?>SocialSettings/deleteSocialSettings/<?php echo $row['id'];?>" style="cursor:pointer"><i class="zmdi zmdi-delete"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                       $i++; }
                                    }else{
                                        ?>
                                    <tr><td colspan="7">No Social Settings Found</td></tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                           </table>
                           <div class='col-md-12'>
                                <div class='pull-right'><?php echo $links; ?></div>
                            </div>
                       </div>
                   </div>
               </div>                
           </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>