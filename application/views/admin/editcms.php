       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Edit CMS
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>cms/updatecms" method="POST" enctype="multipart/form-data" >
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?>
                          <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                               <select class="form-control input-sm"  name="cms_id"> 
                                                <option value="<?php echo $result[0]['cms_id']; ?>">
                                                    <?php if($result[0]['cms_id'] == 1){ echo 'About Us'; } ?>
                                                    <?php if($result[0]['cms_id'] == 2){ echo 'Terms and Conditions'; } ?>   
                                                    <?php if($result[0]['cms_id'] == 3){ echo 'Privacy policy'; } ?>
                                                </option>
                                                <option value="">Select CMS</option>
                                                <option value="1">About Us</option>
                                                <option value="2">Terms and Conditions</option>
                                                <option value="3">Privacy policy</option>
                                                </select>                  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="title" class="form-control input-sm" placeholder="Title" value="<?php echo $result[0]['title']; ?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="description" class="form-control input-sm" placeholder="Description"><?php echo $result[0]['description']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="hidden"  name="id" value="<?php echo $result[0]['id']; ?>">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>