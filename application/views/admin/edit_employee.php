<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Edit Employee
                            </h2>
                        </div>
                        <form method="POST"  action="<?php echo base_url() ?>Employee/updateEmployee" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" placeholder="Enter First Name" name="emp_fname" value="<?php echo $result[0]['emp_fname']; ?>" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                    <input type="text" class="form-control" placeholder="Enter Last Name" name="emp_lname" value="<?php echo $result[0]['emp_lname']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                    <input type="email" class="form-control" placeholder="Email" name="emp_email" value="<?php echo $result[0]['emp_email']; ?>" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="emp_mobile" placeholder="Mobile Number" value="<?php echo $result[0]['emp_mobile']; ?>" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control date-picker" name="emp_dob" placeholder="Date Of Birth" value="<?php echo $result[0]['dob']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                    <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="emp_age" placeholder="Age" value="<?php echo $result[0]['emp_age']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control" name="emp_marital_status">
                                                <option selected="selected" value="<?php echo $result[0]['emp_marital_status']; ?>"><?php if($result[0]['emp_marital_status'] == "") { echo "Select Marital Status"; } elseif($result[0]['emp_marital_status'] == "1") { echo "Yes"; } elseif($result[0]['emp_marital_status'] == "2") { echo "No"; } ?> </option>
                                                <option value="">Select Marital Status</option>
                                                <option value="1">Yes</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                    <input type="text" class="form-control" name="emp_designation" placeholder="Designation" value="<?php echo $result[0]['emp_designation']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control" name="any_disability">
                                                <option selected="selected" value="<?php echo $result[0]['any_disability']; ?>"><?php if($result[0]['any_disability'] == "") { echo "Disability"; } elseif($result[0]['any_disability'] == "1") { echo "Yes"; } elseif($result[0]['any_disability'] == "2") { echo "No"; } ?> </option>
                                                <option value="">Disability</option>
                                                <option value="1">Yes</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1" style="display:none">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="disability_detail" placeholder="Disability Detail" value="<?php echo $result[0]['disability_detail']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="master_degree" placeholder="Master Degree" value="<?php echo $result2[0]['master_degree']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="master_passout" placeholder="Master Passout Year" value="<?php echo $result2[0]['master_passout']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="master_spl" placeholder="Master Specialization" value="<?php echo $result2[0]['master_spl']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="master_intitutes" placeholder="Master Institutes" value="<?php echo $result2[0]['master_intitutes']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="graduation" placeholder="Graduation" value="<?php echo $result2[0]['graduation']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="graduation_passout" placeholder="Graduation Passout Year" value="<?php echo $result2[0]['graduation_passout']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="graduation_spl" placeholder="Graduation Specialization" value="<?php echo $result2[0]['graduation_spl']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="graduation_intitutes" placeholder="Graduation Institutes" value="<?php echo $result2[0]['graduation_intitutes']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="inter" placeholder="Inter" value="<?php echo $result2[0]['inter']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="inter_passout" placeholder="Inter Passout Year" value="<?php echo $result2[0]['inter_passout']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="inter_spl" placeholder="Inter Specialization" value="<?php echo $result2[0]['inter_spl']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="inter_intitutes" placeholder="Inter Institutes" value="<?php echo $result2[0]['inter_intitutes']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="father_name" placeholder="Father Name" value="<?php echo $result3[0]['father_name']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="mother_name" placeholder="Mother Name" value="<?php echo $result3[0]['mother_name']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="countryId" class="form-control" name="per_country">
                                                <option selected="selected" value="<?php echo $result3[0]['per_country']; ?>"><?php if($result3[0]['per_country'] == "") { echo "Select Country"; } else { echo $result3[0]['per_country_name']; } ?> </option>
                                                <option value="">Select Country</option>
                                                <?php
                                                    $count = count(array_filter($resultCnt));
                                                    if($count > 0) {
                                                    $i=0;
                                                    foreach($resultCnt as $key => $row){
                                                    ?>
                                                        <option value="<?php echo  $row['id'] ?>"><?php echo  $row['country_name'] ?></option>
                                                    <?php
                                                    }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="stateId" class="form-control" name="per_state">
                                                <option selected="selected" value="<?php echo $result3[0]['per_state']; ?>"><?php if($result3[0]['per_state'] == "") { echo "Select State"; } else { echo $result3[0]['per_state_name']; } ?> </option>
                                                <option>Select State</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="districtId" class="form-control" name="per_district">
                                                <option selected="selected" value="<?php echo $result3[0]['per_district']; ?>"><?php if($result3[0]['per_district'] == "") { echo "Select District"; } else { echo $result3[0]['per_district_name']; } ?> </option>
                                                <option>Select District</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="cityId" class="form-control" name="per_city">
                                                <option selected="selected" value="<?php echo $result3[0]['per_city']; ?>"><?php if($result3[0]['per_city'] == "") { echo "Select City"; } else { echo $result3[0]['per_city_name']; } ?> </option>
                                                <option>Select City</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="pincodeId" class="form-control" name="per_pincode">
                                                <option selected="selected" value="<?php echo $result3[0]['per_pincode']; ?>"><?php if($result3[0]['per_pincode'] == "") { echo "Select Pincode"; } else { echo $result3[0]['per_pincode_name']; } ?> </option>
                                                <option>Select Pincode</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="locationId" class="form-control" name="per_address">
                                                <option selected="selected" value="<?php echo $result3[0]['per_address']; ?>"><?php if($result3[0]['per_address'] == "") { echo "Select Location"; } else { echo $result3[0]['per_address_name']; } ?> </option>
                                                <option>Select Location</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="countryId1" class="form-control" name="res_country">
                                                <option selected="selected" value="<?php echo $result3[0]['res_country']; ?>"><?php if($result3[0]['res_country'] == "") { echo "Select Country"; } else { echo $result3[0]['res_country_name']; } ?> </option>
                                                <option value="">Select Country</option>
                                                <?php
                                                    $count = count(array_filter($resultCnt1));
                                                    if($count > 0) {
                                                    $i=0;
                                                    foreach($resultCnt as $key => $row){
                                                    ?>
                                                        <option value="<?php echo  $row['id'] ?>"><?php echo  $row['country_name'] ?></option>
                                                    <?php
                                                    }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="stateId1" class="form-control" name="res_state">
                                                <option selected="selected" value="<?php echo $result3[0]['res_state']; ?>"><?php if($result3[0]['res_state'] == "") { echo "Select State"; } else { echo $result3[0]['res_state_name']; } ?> </option>
                                                <option>Select State</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="districtId1" class="form-control" name="res_district">
                                                <option selected="selected" value="<?php echo $result3[0]['res_district']; ?>"><?php if($result3[0]['res_district'] == "") { echo "Select District"; } else { echo $result3[0]['res_district_name']; } ?> </option>
                                                <option>Select District</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="cityId1" class="form-control" name="res_city">
                                                <option selected="selected" value="<?php echo $result3[0]['res_city']; ?>"><?php if($result3[0]['res_city'] == "") { echo "Select City"; } else { echo $result3[0]['res_city_name']; } ?> </option>
                                                <option>Select City</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="pincodeId1" class="form-control" name="res_pincode">
                                                <option selected="selected" value="<?php echo $result3[0]['res_pincode']; ?>"><?php if($result3[0]['res_pincode'] == "") { echo "Select Pincode"; } else { echo $result3[0]['res_pincode_name']; } ?> </option>
                                                <option>Select Pincode</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="locationId1" class="form-control" name="res_address">
                                                <option selected="selected" value="<?php echo $result3[0]['res_address']; ?>"><?php if($result3[0]['res_address'] == "") { echo "Select Location"; } else { echo $result3[0]['res_address_name']; } ?> </option>
                                                <option>Select Location</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control" name="grade_type">
                                                <option selected="selected" value="<?php echo $result4[0]['grade_type']; ?>"><?php if($result4[0]['grade_type'] == "") { echo "Select Grade Type"; } elseif($result4[0]['grade_type'] == "1") { echo "Label 1"; } elseif($result4[0]['grade_type'] == "2") { echo "Label 2"; } elseif($result4[0]['grade_type'] == "3") { echo "Label 3"; } ?> </option>
                                                <option value="">Select Grade Type</option>
                                                <option value="1">Label 1</option>
                                                <option value="2">Label 2</option>
                                                <option value="3">Label 3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="total_salary" placeholder="Total Salary" value="<?php echo $result4[0]['total_salary']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="take_home" placeholder="Take Home" value="<?php echo $result4[0]['take_home']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="pf" placeholder="PF" value="<?php echo $result4[0]['pf']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="pf_amount" placeholder="PF Amount" value="<?php echo $result4[0]['pf_amount']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="bank_name" placeholder="Bank Name" value="<?php echo $result4[0]['bank_name']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="account_number" placeholder="Account Number" value="<?php echo $result4[0]['account_number']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="designation" placeholder="Designation" value="<?php echo $result5[0]['designation']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="company_name" placeholder="Company Name" value="<?php echo $result5[0]['company_name']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="emp_mode" placeholder="Mode of Employee" value="<?php echo $result5[0]['emp_mode']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control date-picker" name="from_date" placeholder="From Date" value="<?php echo $result[0]['fromDate']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" class="form-control date-picker" name="to_date" placeholder="To Date" value="<?php echo $result[0]['toDate']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput"><img src='<?php echo $result[0]['emp_profile_img']; ?>' width="250" height="250" ></div>
                                    <div>
                                        <span class="btn btn-info btn-file">
                                            <span class="fileinput-new">Select Profile</span>
                                            <input type="file" name="emp_profile_img">
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5 col-sm-offset-1">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                       <?php echo $result[0]['resume']; ?></div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select Resumes</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="resume">
                                            </span>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <?php
                                if($result1['imglist'] != "") { 
                                    foreach ($result1['imglist'] as $key => $img) {
                            ?>
                            <div class="row">
                                <div class="col-sm-3 col-sm-offset-1">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" class="form-control" name="doc_type" placeholder="Type of Document" value="<?php echo $result1[0]['doc_type']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-sm-offset-1">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                            <img  src='<?php echo base_url() ?><?php echo ($img['doc_img']); ?>' height="150" width="150">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-sm-offset-1">
                                    <div>                              
                                        <a  imgId='<?php echo $img['id'] ?>' style="cursor:pointer" class="btn btn-danger removeEmployeeImg removedoc" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                            </div>
                            <?php } } ?>
                            <div class="row text-box">
                                <div class="col-sm-3 col-sm-offset-1">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" class="form-control" name="doc_type" placeholder="Type of Document">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-sm-offset-1">
                                    <div class="">           
                                        <div class="row newImg">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                                    <img id="falseinput0" height="150" width="150">
                                                </div>
                                                <div>
                                                    <span class="btn btn-info btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input onchange="readURL(this,'falseinput0','baseInput0')" type="file" name="doc_img[]">
                                                    </span>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-sm-offset-1">
                                    <a style="cursor:pointer" class="btn btn-danger fileinput-exists removeEmployeeImg" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                            <a class="add-box btn btn-danger fileinput-exists" id="addmoredoc" style="cursor:pointer"> Add More + </a>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-sm-5 col-sm-offset-1">
                                    <input type="hidden" class="form-control" name="emp_code" placeholder="emp_code" value="<?php echo $result[0]['emp_code']; ?>">
                                    <button  type='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>