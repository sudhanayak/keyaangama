       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
               <div class="container">                  
                   <div class="card">
                       <div class="card-header">
                            <div class="row">   
                                <div class="col-sm-6">   
                                    <h2>
                                    Portfolio List
                                    </h2>
                                </div>
                                <div class="col-sm-3 ">  </div>
                            </div>
                       </div>
                       <div class="card-body table-responsive">
                           <table class="table table-striped">
                               <thead class="black white-text">
                               <tr>
                                <th>Sno</th>
                                <th>Title</th>
                                <th>City</th>
                                <th>Action</th>
                               </tr>
                               </thead>
                               <tbody>
                                   <?php
                                   $i =$this->uri->segment(3)+1;
                                    $count = count(array_filter($result));
                                    if($count > 0) {
                                        foreach($result as $key => $row){
                                    ?>
                                   <tr class="zebra-striping">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row['title'];?></td>
                                        <td><?php echo $row['city'];?></td>
                                        <td>
                                            <a data-toggle="tooltip"data-placement="bottom" title="Edit Protfolio" href="<?php echo base_url(); ?>Portfolio/editPortfolio/<?php echo $row['id'] ?>"><i class="zmdi zmdi-edit"></i></a>
                                            <?php
                                            if($row['keyaan_status']==1){
                                            ?>
                                            <a data-toggle="tooltip"data-placement="bottom" title="Disable" onclick="return confirm('Confirm to Enable?');" href="<?php echo base_url()?>Portfolio/PortfolioEnable/<?php echo $row['id'];?>"> <i class="zmdi zmdi-close-circle-o"></i></a> 
                                           <?php
                                            }else{
                                           ?>
                                            <a data-toggle="tooltip"data-placement="bottom" title="Enable" onclick="return confirm('Confirm to Disable?');" href="<?php echo base_url()?>Portfolio/PortfolioDisable/<?php echo $row['id'];?>" style="cursor:pointer"><i class="zmdi zmdi-check"></i></a>
                                            <?php
                                            }
                                            ?>
                                            <a data-toggle="modal" title="Image" 
                                               data-target="#myModal<?php echo $i; ?>">
                                                <i class="zmdi zmdi-collection-folder-image">                                              
                                                </i>
                                            </a>
                                            <div class="modal fade"
                                            id="myModal<?php echo $i; ?>" 
                                            tabindex="-1" 
                                            role="dialog" 
                                            aria-labelledby="myModalLabel<?php echo $i; ?>">
                                            <div class="modal-dialog" role="document">
                                               <div class="modal-content">
                                                 <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                   <h4 class="modal-title">Portfolio Detail</h4>
                                                 </div>
                                                 <div class="modal-body">
                                                     <p><img src='<?php echo base_url() ?>uploads/portfolio_image/<?php echo $row['image']; ?>' width="550" height="550"></p>
                                                 </div>

                                               </div><!-- /.modal-content -->
                                               </div><!-- /.modal-content -->
                                            </div><!-- /.modal-content --> 
                                        </td>
                                    </tr>
                                    <?php
                                       $i++; }
                                    }else{
                                        ?>
                                    <tr><td colspan="7">No Portfolio Found</td></tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                           </table>
                       </div>
                   </div>
               </div>                
           </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>