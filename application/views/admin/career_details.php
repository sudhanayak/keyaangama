<?php include_once'header.php'; ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
               <div class="container container-alt">
                   <div class="card" id="profile-main">
                       <div class="pm-body clearfix" style="padding-left: 0px !important;">
                           <ul class="tab-nav tn-justified">
                               <li class="active"><a href="#" style="text-align: left;"><?php echo $result[0]['job_title']; ?></a></li>
                           </ul>
                           <div class="pmb-block">
                               <div class="pmbb-header">
                                   <h2><i class="zmdi zmdi-equalizer m-r-10"></i> Summary</h2>
                                   <ul class="actions">
                                       <li class="dropdown">
                                           <a href="" data-toggle="dropdown">
                                               <i class="zmdi zmdi-more-vert"></i>
                                           </a>
                                           <ul class="dropdown-menu dropdown-menu-right">
                                               <li>
                                                   <a href="<?php echo base_url(); ?>Careers/editCareers/<?php echo $result[0]['id'] ?>">Edit</a>
                                               </li>
                                           </ul>
                                       </li>
                                   </ul>
                               </div>
                               <div class="pmbb-body p-l-30">
                                   <div class="pmbb-view">
                                       <dl class="dl-horizontal">
                                            <dt>Job Title</dt>
                                            <dd><?php echo $result[0]['job_title']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Job Details</dt>
                                            <dd><?php echo $result[0]['job_detail']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Country</dt>
                                            <dd><?php echo $result[0]['country']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>State</dt>
                                            <dd><?php echo $result[0]['state']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>District</dt>
                                            <dd><?php echo $result[0]['district']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>City</dt>
                                            <dd><?php echo $result[0]['city']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Pincode</dt>
                                            <dd><?php echo $result[0]['pincode']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Location</dt>
                                            <dd><?php echo $result[0]['location']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Package</dt>
                                            <dd><?php echo $result[0]['package']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Skills Required</dt>
                                            <dd><?php echo $result[0]['skills_required']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Job Type</dt>
                                            <dd><?php if($result[0]['job_type'] == 1) { echo "Contract Based" ;} elseif($result[0]['job_type'] == 2) { echo "Permanent" ;} ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>No of Positions</dt>
                                            <dd><?php echo $result[0]['no_of_positions']; ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Experience in Years</dt>
                                            <dd><?php if($result[0]['exprience_years'] == 1) { echo "1 Year" ;} elseif($result[0]['exprience_years'] == 2) { echo "2 Years" ;} elseif($result[0]['exprience_years'] == 3) { echo "3 Years" ;} elseif($result[0]['exprience_years'] == 4) { echo "4 Years" ;} elseif($result[0]['exprience_years'] == 5) { echo "5 Years" ;} elseif($result[0]['exprience_years'] == 6) { echo "6 Years" ;} elseif($result[0]['exprience_years'] == 7) { echo "7 Years" ;} elseif($result[0]['exprience_years'] == 8) { echo "8 Years" ;} elseif($result[0]['exprience_years'] == 9) { echo "9 Years" ;} elseif($result[0]['exprience_years'] == 10) { echo "10 Years" ;}  ?> </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Experience in Months</dt>
                                            <dd><?php if($result[0]['exprience_months'] == 1) { echo "1 Month" ;} elseif($result[0]['exprience_months'] == 2) { echo "2 Months" ;} elseif($result[0]['exprience_months'] == 3) { echo "3 Months" ;} elseif($result[0]['exprience_months'] == 4) { echo "4 Months" ;} elseif($result[0]['exprience_months'] == 5) { echo "5 Months" ;} elseif($result[0]['exprience_months'] == 6) { echo "6 Months" ;} elseif($result[0]['exprience_months'] == 7) { echo "7 Months" ;} elseif($result[0]['exprience_months'] == 8) { echo "8 Months" ;} elseif($result[0]['exprience_months'] == 9) { echo "9 Months" ;} elseif($result[0]['exprience_months'] == 10) { echo "10 Months" ;}  ?></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Notice Period</dt>
                                            <dd><?php echo $result[0]['notice_period']; ?></dd>
                                        </dl>
                                   </div>
                               </div>
                           </div>


                       </div>
                   </div>
               </div>
           </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>