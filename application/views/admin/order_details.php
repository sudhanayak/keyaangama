<?php include_once'header.php'; ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
               <div class="container container-alt">



                   <div class="card" id="profile-main">


                       <div class="pm-body clearfix" style="padding-left: 0px !important;">
                           <div class="pmb-block">
                               <div class="pmbb-header">
                                   <h2><i class="zmdi zmdi-equalizer m-r-10"></i> Order Summary</h2>
                               </div>
                               <div class="pmbb-body p-l-30">
                                   <div class="pmbb-view">
                                        <div class="col-sm-6">
                                            <dl class="dl-horizontal">
                                                <dt>Order Id</dt>
                                                <dd><?php echo $result['order_id']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>User Name</dt>
                                                <dd><?php echo $result['user_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Total Amount</dt>
                                                <dd><?php echo $result['total_amount']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Advance Amount</dt>
                                                <dd><?php echo $advance_amount; ?></dd>
                                            </dl>
                                        </div>
                                        <div class="col-sm-6">
                                            <dl class="dl-horizontal">
                                                <dt>Remaining Amount</dt>
                                                <dd><?php echo $result['remain_amount']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Payment Type</dt>
                                                <dd><?php echo $payment_type; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Payment Status</dt>
                                                <dd><?php echo $payment_status; ?></dd>
                                            </dl>
                                        </div>
                                   </div>
                               </div>
                               <div class="clearfix"></div>
                               <?php $i =0;
                                $count = count(array_filter($eventDetails));
                                if($count > 0) { ?>
                               <div class="pmbb-header">
                                   <h2><i class="zmdi zmdi-equalizer m-r-10"></i>Vendor Order Details</h2>
                               </div>
                                <table class="table table-striped">
                                    <thead class="black white-text">
                                        <tr>
                                            <th>Event Name</th>
                                            <th>Event Code</th>
                                            <th>Price</th>
                                            <th>Event Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($eventDetails as $key => $row){
                                        ?>
                                        <tr class="zebra-striping">
                                            <td><?php echo $row['itemName']; ?></td>
                                            <td><?php echo $row['item_id']; ?></td>
                                            <td><?php echo $row['price']; ?></td>
                                            <td><?php echo $row['event_date']; ?></td>
                                        </tr>
                                        <?php
                                            $i++; } ?>
                                    </tbody>
                                </table>
                                <?php } ?>
                                <div class="clearfix"></div>
                                <?php
                                    $i =0;
                                    $count = count(array_filter($packageDetails));
                                    if($count > 0) {
                                ?>
                                <div class="pmbb-header">
                                   <h2><i class="zmdi zmdi-equalizer m-r-10"></i>Package Order Details</h2>
                               </div>
                                <table class="table table-striped">
                                    <thead class="black white-text">
                                        <tr>
                                            <th>Package Name</th>
                                            <th>Itinerary Name</th>
                                            <th>Price</th>
                                            <th>Event Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($packageDetails as $key => $row){
                                        ?>
                                        <tr class="zebra-striping">
                                            <td><?php echo $row['packageName']; ?></td>
                                            <td><?php echo $row['itineraryName']; ?></td>
                                            <td><?php echo $row['price']; ?></td>
                                            <td><?php echo $row['event_date']; ?></td>
                                        </tr>
                                        <?php
                                            $i++; } ?>
                                    </tbody>
                                </table>
                                <?php } ?>
                               <div class="pmbb-header">
                                   <h2><i class="zmdi zmdi-equalizer m-r-10"></i> Billing Address</h2>
                               </div>
                               <div class="pmbb-body p-l-30">
                                   <div class="pmbb-view">
                                        <div class="col-sm-6">
                                            <dl class="dl-horizontal">
                                                <dt>Name</dt>
                                                <dd><?php echo $billingDetails['billing_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Email</dt>
                                                <dd><?php echo $billingDetails['billing_email']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Mobile</dt>
                                                <dd><?php echo $billingDetails['billing_mobile']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Country</dt>
                                                <dd><?php echo $billingDetails['country']; ?></dd>
                                            </dl>
                                        </div>
                                        <div class="col-sm-6">
                                            <dl class="dl-horizontal">
                                                <dt>State</dt>
                                                <dd><?php echo $billingDetails['state']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>City</dt>
                                                <dd><?php echo $billingDetails['city']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Pincode</dt>
                                                <dd><?php echo $billingDetails['pincode']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Landmark</dt>
                                                <dd><?php echo $billingDetails['billing_landmark']; ?></dd>
                                            </dl>
                                        </div>
                                   </div>
                               </div>
                           </div>


                       </div>
                   </div>
               </div>
           </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>