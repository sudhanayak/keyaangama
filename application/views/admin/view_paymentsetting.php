       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
               <div class="container">                  
                   <div class="card">
                       <div class="card-header">
                            <div class="row">   
                                <div class="col-sm-6">   
                                    <h2>
                                    Payment List
                                    </h2>
                                </div>
                                <div class="col-sm-3 ">  </div>
                                
                            </div>
                       </div>
                       
                       <div class="card-body table-responsive">
                          <!-- <?php
                                echo $this->session->flashdata('msg');
                            ?> -->
                           <table class="table table-striped">
                               <thead class="black white-text">
                               <tr>
                                <th>Sno</th>
                                <th>Payment Mode</th>
                                <th>Payment Gateway </th>
                                <th>Action</th>
                               </tr>
                               </thead>
                               <tbody>
                                   <?php
                                    $i =$this->uri->segment(3)+1;
                                    $count = count(array_filter($result));
                                    if($count > 0) {
                                        foreach($result as $key => $row){
                                    ?>
                                   <tr class="zebra-striping">
                                        <td><?php echo $i; ?></td>
                                        <td>
                                            <?php if($row['payment_mode'] == 1){ echo 'COD'; }
                                            elseif($row['payment_mode'] == 2){ echo 'Online'; } ?>
                                        </td>
                                        <?php if($row['payment_mode'] == 2){ ?>
                                        <td>
                                            <?php if($row['payment_gateway'] == 1){ echo 'Paytm'; } 
                                            elseif($row['payment_gateway'] == 2){ echo 'Payumoney'; }
                                            elseif($row['payment_gateway'] == 3){ echo 'ccavenue'; } ?>
                                        </td>
                                      <?php } else { ?>
                                        <td> -- </td>
                                      <?php } ?>
                                        <td>
                                            <a data-toggle="tooltip"data-placement="bottom" title="Edit Payment Settings" href="<?php echo base_url(); ?>Paymentsetting/editPaymentsetting/<?php echo $row['id'] ?>"><i class="zmdi zmdi-edit"></i></a>
                                            <?php
                                            if($row['keyaan_status']==1){
                                            ?>
                                            <a data-toggle="tooltip"data-placement="bottom" title="Disable" onclick="return confirm('Confirm to Enable?');" href="<?php echo base_url()?>Paymentsetting/PaymentsettingEnable/<?php echo $row['id'];?>"> <i class="zmdi zmdi-close-circle-o"></i></a> 
                                           <?php
                                            }else{
                                           ?>
                                            <a data-toggle="tooltip"data-placement="bottom" title="Enable" onclick="return confirm('Confirm to Disable?');" href="<?php echo base_url()?>Paymentsetting/PaymentsettingDisable/<?php echo $row['id'];?>" style="cursor:pointer"><i class="zmdi zmdi-check"></i></a>
                                            <?php
                                            }
                                            ?> 
                                            <a data-toggle="tooltip"data-placement="bottom" title="Delete Payment Settings" onclick="return confirm('Are You Sure to Delete ?');" href="<?php echo base_url()?>Paymentsetting/deletePaymentsettings/<?php echo $row['id'];?>" style="cursor:pointer"><i class="zmdi zmdi-delete"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                       $i++; }
                                    }else{
                                        ?>
                                    <tr><td colspan="7">No Payment GatewayFound</td></tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                           </table>
                           <div class='col-md-12'>
                                <div class='pull-right'><?php echo $links; ?></div>
                            </div>
                            <!--
                            //sample data start here
                            -->
                            
                       </div>
                   </div>
               </div>                
           </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>