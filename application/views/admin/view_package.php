       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
               <div class="container">                  
                   <div class="card">
                       <div class="card-header">
                            <div class="row">   
                                <div class="col-sm-6">   
                                    <h2>
                                    Package List
                                    </h2>
                                </div>
                                <div class="col-sm-6">
                                   <div class='row'>
                                       <form action='<?php echo base_url() ?>Package/viewPackage' method='GET'>
                                           <div class="col-sm-6 ">
                                               <div class="form-group pull-right">
                                                   <input type="text" name='search' value="<?php echo $searchVal; ?>" ng-model="search" class="form-control" placeholder="Search">
                                               </div>
                                           </div>
                                           <div class="col-sm-6 ">
                                               <div class="form-group pull-right">
                                                   <button type="submit" class="btn  btn-fill btn-primary btn-wd waves-effect">Search</button>
                                               </div>
                                           </div>
                                       </form>
                                   </div>
                               </div>
                                <div class="col-sm-3 "></div>
                            </div>
                       </div>
                       
                       <div class="card-body table-responsive">
                           <table class="table table-striped">
                               <thead class="black white-text">
                               <tr>
                                <th>Sno</th>
                                <th>Package Name</th>
                                <th>category</th>
                                <th>Action</th>
                               </tr>
                               </thead>
                               <tbody>
                                   <?php
                                    $i =$this->uri->segment(3)+1;
                                    $count = count(array_filter($result));
                                    if($count > 0) {
                                        foreach($result as $key => $row){
                                    ?>
                                   <tr class="zebra-striping">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row['package_name']; ?></td>
                                        <td><?php echo $row['category']; ?></td>
                                        <td>
                                          <a data-toggle="tooltip" data-placement="bottom" title="" data-original-title="View" href="<?php echo base_url()?>Package/packageDetails/<?php echo $row['id'];?>" ><i class="zmdi zmdi-eye"></i></a>
                                            <a data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Edit" href="<?php echo base_url(); ?>Package/editPackage/<?php echo $row['id'] ?>"><i class="zmdi zmdi-edit"></i></a>
                                            <?php
                                            if($row['keyaan_status']==1){
                                            ?>
                                            <a data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Disable" onclick="return confirm('Confirm to Enable?');" href="<?php echo base_url()?>Package/packageEnable/<?php echo $row['id'];?>"> <i class="zmdi zmdi-close-circle-o"></i></a> 
                                           <?php
                                            }else{
                                           ?>
                                            <a data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Enable" onclick="return confirm('Confirm to Disable?');" href="<?php echo base_url()?>Package/packageDisable/<?php echo $row['id'];?>" style="cursor:pointer"><i class="zmdi zmdi-check"></i></a>
                                            <?php
                                            }
                                            ?>
                                            <a data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete" onclick="return confirm('Are You Sure to Delete ?');" href="<?php echo base_url()?>Package/deletePackage/<?php echo $row['package_code'];?>" style="cursor:pointer"><i class="zmdi zmdi-delete"></i></a>
                                            
                                            <a href="<?php echo base_url(); ?>Itinerary/addItinerary/<?php echo $row['package_code']; ?>">Add Itinerary</a>
                                            <a href="<?php echo base_url(); ?>Itinerary/ItineraryDetails/<?php echo $row['package_code']; ?>">View Itinerary</a>
                                        </td>
                                    </tr>
                                    <?php
                                       $i++; }
                                    }else{
                                        ?>
                                    <tr><td colspan="7">No Package Found</td></tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                           </table>
                           <div class='col-md-12'>
                                <div class='pull-right'><?php echo $links; ?></div>
                            </div>
                       </div>
                   </div>
               </div>                
           </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>