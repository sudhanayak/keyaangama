<div class="s-profile">
                              <?php                                                                    
                                $count_basicsettings = count(array_filter($basicsettingsList));
                                if($count_basicsettings > 0) {
                                foreach($basicsettingsList as $key => $row_basicsettingsList){   
                                    $url = base_url().'uploads/site_logo/'.$row_basicsettingsList['site_logo'];
                              ?>
                              <?php } } ?>
                     <a href="#" data-ma-action="profile-menu-toggle">
                        <center><img src="<?php echo $url?>" style="margin-top:5%;"></center>
                    </a>
                    <ul class="main-menu">
<!--                        <li>
                            <a href="profile-about.html"><i class="zmdi zmdi-account"></i> View Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="zmdi zmdi-input-antenna"></i> Privacy Settings</a>
                        </li>
                        <li>
                            <a href="#"><i class="zmdi zmdi-settings"></i> Settings</a>
                        </li>-->
                        <li>
                            <a href="<?php echo base_url() ?>logout"><i class="zmdi zmdi-time-restore"></i> Logout</a>
                        </li>
                    </ul>
                </div>

                <ul class="main-menu">
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Basic Settings</a>

                        <ul>
                            <li><a href="<?php echo base_url() ?>editbasicsettings">Basic Settings</a></li>
                        </ul>
                    </li>

                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Social Settings</a>

                        <ul>
                            <li><a href="<?php echo base_url() ?>viewSocialSettings">View Social Settings</a></li>
                            <li><a href="<?php echo base_url() ?>addSocialSettings">Add Social Settings</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Payment Settings</a>

                        <ul>
                            <li><a href="<?php echo base_url() ?>viewPaymentsetting">View Payment Settings</a></li>
                            <li><a href="<?php echo base_url() ?>addPaymentsetting">Add Payment Settings</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Category</a>

                        <ul>
                            <li><a href="<?php echo base_url() ?>viewCategory">View Category</a></li>
                            <li><a href="<?php echo base_url() ?>addCategory">Add Category</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Sub Category</a>

                        <ul>
                            <li><a href="<?php echo base_url() ?>viewSubcategory">View Sub Category</a></li>
                            <li><a href="<?php echo base_url() ?>addSubcategory">Add Sub Category</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Sub Sub Category</a>

                        <ul>
                            <li><a href="<?php echo base_url() ?>viewSubsubcategory">View Sub Sub Category</a></li>
                            <li><a href="<?php echo base_url() ?>addSubsubcategory">Add Sub Sub Category</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Vendors</a>

                        <ul>
                            <li><a href="<?php echo base_url() ?>viewVendors">View Vendors</a></li>
                            <li><a href="<?php echo base_url() ?>addVendor">Add Vendors</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Employee</a>

                        <ul>
                            <li><a href="<?php echo base_url() ?>viewEmployee">View Employee</a></li>
                            <li><a href="<?php echo base_url() ?>addEmployee">Add Employee</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Countries</a>
                       <ul>
                           <li><a href="<?php echo base_url() ?>viewcountry">View Country</a></li>
                           <li><a href="<?php echo base_url() ?>addcountry">Add Country</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>States</a>
                       <ul>
                           <li><a href="<?php echo base_url() ?>viewstate">View States</a></li>
                           <li><a href="<?php echo base_url() ?>add_state">Add State</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Districts</a>
                       <ul>
                           <li><a href="<?php echo base_url() ?>viewDistrict">View Districts</a></li>
                           <li><a href="<?php echo base_url() ?>addDistrict">Add Districts</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Cities</a>
                       <ul>
                           <li><a href="<?php echo base_url() ?>viewcities">View City</a></li>
                           <li><a href="<?php echo base_url() ?>add_city">Add City</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Pincodes</a>
                       <ul>
                           <li><a href="<?php echo base_url() ?>viewpincode">View Pincodes</a></li>
                           <li><a href="<?php echo base_url() ?>addpincode">Add Pincode</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Locations</a>
                       <ul>
                           <li><a href="<?php echo base_url() ?>viewLocations">View Locations</a></li>
                           <li><a href="<?php echo base_url() ?>addLocations">Add Locations</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> CMS </a>

                        <ul>
                            <li><a href="<?php echo base_url() ?>viewcms">View CMS</a></li>
                            <li><a href="<?php echo base_url() ?>addcms">Add CMS</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Careers</a>

                        <ul>
                            <li><a href="<?php echo base_url() ?>viewCareers">View Careers</a></li>
                            <li><a href="<?php echo base_url() ?>addCareers">Add Careers</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Resumes</a>

                        <ul>
                            <li><a href="<?php echo base_url() ?>viewResumes">View Resumes</a></li>
                            <li><a href="<?php echo base_url() ?>addResumes">Add Resumes</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Departments</a>

                        <ul>
                            <li><a href="<?php echo base_url() ?>viewDepartment">View Departments</a></li>
                            <li><a href="<?php echo base_url() ?>addDepartment">Add Departments</a></li>
                        </ul>
                    </li>
                     <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Banners</a>
                        <ul>
                            <li><a href="<?php echo base_url() ?>viewBanners">View Banners</a></li>
                            <li><a href="<?php echo base_url() ?>addBanners">Add banners</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Event Bookings</a>
                        <ul>
                            <li><a href="<?php echo base_url() ?>viewEventbooking">View Event Bookings </a></li>
                           
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Orders</a>
                        <ul>
                            <li><a href="<?php echo base_url() ?>viewOrders">View Orders </a></li>
                           
                        </ul>
                    </li> 
                    <!-- <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Portfolio</a>
                        <ul>
                            <li><a href="<?php echo base_url() ?>viewPortfolio">View Portfolio</a></li>
                            <li><a href="<?php echo base_url() ?>addPortfolio">Add Portfolio</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Portfolio</a>
                        <ul>
                            <li><a href="<?php echo base_url() ?>viewPortfolio">View Portfolio</a></li>
                            <li><a href="<?php echo base_url() ?>addPortfolio">Add Portfolio</a></li>
                        </ul>
                    </li> 
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Testimonials</a>
                        <ul>
                            <li><a href="<?php echo base_url() ?>viewTestimonials ">View Testimonials </a></li>
                            <li><a href="<?php echo base_url() ?>addTestimonials">Add Testimonials</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Chooseus</a>
                        <ul>
                            <li><a href="<?php echo base_url() ?>viewChooseus ">View Chooseus </a></li>
                            <li><a href="<?php echo base_url() ?>addChooseus">Add Chooseus</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> News</a>
                        <ul>
                            <li><a href="<?php echo base_url() ?>viewNews">View News </a></li>
                            <li><a href="<?php echo base_url() ?>addNews">Add News</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i> Event Enquiry</a>
                        <ul>
                            <li><a href="<?php echo base_url() ?>Eventenquiry">View Event Enquiry </a></li>
                           
                        </ul>
                    </li>-->
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Expenditures</a>

                        <ul>
                            <li><a href="<?php echo base_url() ?>viewExpenditure">View Expenditures</a></li>
                            <li><a href="<?php echo base_url() ?>addExpenditure">Add Expenditures</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Master Degree</a>

                        <ul>
                            <li><a href="<?php echo base_url() ?>viewMasterdegree">View Master Degree</a></li>
                            <li><a href="<?php echo base_url() ?>addMasterdegree">Add Master Degree</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Package Category</a>

                       <ul>
                           <li><a href="<?php echo base_url() ?>viewPackagecategory">View Package Category</a></li>
                           <li><a href="<?php echo base_url() ?>addPackagecategory">Add Package Category</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                      <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Package Sub Category</a>

                      <ul>
                          <li><a href="<?php echo base_url() ?>viewPackagesubcategory">View Package Sub Category</a></li>
                          <li><a href="<?php echo base_url() ?>addPackagesubcategory">Add Package Sub Category</a></li>
                      </ul>
                  </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Package Itinerary</a>

                       <ul>
                           <li><a href="<?php echo base_url() ?>viewPackageitinerary">View Package Itinerary</a></li>
                           <li><a href="<?php echo base_url() ?>addPackageitinerary">Add Package Itinerary</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                      <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Vendor Packages</a>

                      <ul>
                          <li><a href="<?php echo base_url() ?>viewVendorpackage">View Vendor Packages</a></li>
                          <li><a href="<?php echo base_url() ?>addVendorpackage">Add Vendor Packages</a></li>
                      </ul>
                  </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Packages</a>

                       <ul>
                           <li><a href="<?php echo base_url() ?>viewPackage">View Packages</a></li>
                           <li><a href="<?php echo base_url() ?>addPackage">Add Packages</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Catering Category</a>

                       <ul>
                           <li><a href="<?php echo base_url() ?>viewCateringcategory">View Catering Category</a></li>
                           <li><a href="<?php echo base_url() ?>addCateringcategory">Add Catering Category</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Catering Sub Category</a>

                       <ul>
                           <li><a href="<?php echo base_url() ?>viewCateringsubcategory">View Catering Sub Category</a></li>
                           <li><a href="<?php echo base_url() ?>addCateringsubcategory">Add Catering Sub Category</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Cuisine Types</a>

                       <ul>
                           <li><a href="<?php echo base_url() ?>viewCusinetype">View Cuisine Types</a></li>
                           <li><a href="<?php echo base_url() ?>addCusinetype">Add Cuisine Types</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Product Weights</a>

                       <ul>
                           <li><a href="<?php echo base_url() ?>viewProductweights">View Product Weights</a></li>
                           <li><a href="<?php echo base_url() ?>addProductweights">Add Product Weights</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Products</a>

                       <ul>
                           <li><a href="<?php echo base_url() ?>viewProducts">View Products</a></li>
                           <li><a href="<?php echo base_url() ?>addProducts">Add Products</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Product Price</a>

                       <ul>
                           <li><a href="<?php echo base_url() ?>viewProductprice">View Product Price</a></li>
                           <li><a href="<?php echo base_url() ?>addProductprice">Add Product Price</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Venue Types</a>

                       <ul>
                           <li><a href="<?php echo base_url() ?>viewVenueTypes">View Venue Types</a></li>
                           <li><a href="<?php echo base_url() ?>addVenueTypes">Add Venue Types</a></li>
                       </ul>
                   </li>
                   <li class="sub-menu">
                       <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-chart"></i>Facilities</a>

                       <ul>
                           <li><a href="<?php echo base_url() ?>viewFacilities">View Facilities</a></li>
                           <li><a href="<?php echo base_url() ?>addFacility">Add Facilities</a></li>
                       </ul>
                   </li>

                </ul>