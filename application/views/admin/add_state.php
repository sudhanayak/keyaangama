       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Add State
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>state/add_state" method="POST" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?> 
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm"  name="country_id" id="countryId">
                                                     <option value="">select Country </option>
                                                     <?php 
                                                        foreach($country as $val)
                                                        {
                                                            echo '<option value="'.$val['id'].'">'.$val['country_name'].'</option>';
                                                        }
                                                     ?> 
                                                    </select>                                      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div> 
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="state_name" class="form-control input-sm" placeholder="State Name" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <!-- <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="state_code" class="form-control input-sm" placeholder="State Code" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>  --> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>