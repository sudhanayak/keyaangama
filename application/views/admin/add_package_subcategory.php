       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Add Package Subcategory
                            </h2>
                        </div>
                        <form method="POST"  action="<?php echo base_url() ?>addPackagesubcategory" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?> 
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm"  name="category_id" id="catmultiple">
                                                     <option value="">Select Category </option>
                                                     <?php 
                                                        foreach($category as $val)
                                                        { 
                                                            echo '<option value="'.$val['id'].'">'.$val['category_name'].'</option>';
                                                        }
                                                     ?> 
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>   
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="subcategory_name" class="form-control input-sm" placeholder="subcategory Name" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <!-- <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select class="form-control input-sm" name="category_type" id="">
                                                        <option value="">Select Category Type</option>
                                                        <option value="0">Single</option>
                                                        <option value="1">Multiple days</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div> -->
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="base_price" class="form-control input-sm" placeholder="Enter Price" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="details" class="form-control input-sm" placeholder="Enter Details"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="image" required>
                                            </span>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <button  type='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>