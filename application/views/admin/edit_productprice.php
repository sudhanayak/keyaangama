       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Edit Product Price
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>Productprice/updateProductprice" method="POST" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?> 
                                
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm"  name="product_id" id="ProductId">
                                                    <option value="<?php echo $result['product_id']; ?>"><?php echo $result['product']; ?> </option>
                                                    <option value="">Select Product</option>
                                                    <?php                    
                                                        $count = count(array_filter($resultProduct));
                                                        if($count > 0) {
                                                        $i=0;
                                                        foreach($resultProduct as $key => $row){
                                                        ?>
                                                            <option value="<?php echo  $row['id'] ?>"><?php echo  $row['product_name'] ?></option>
                                                        <?php
                                                        }
                                                        }
                                                    ?>
                                                  </select>                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div> 
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm"  name="product_weight_id" id="productWeightId">
                                                    <option value="<?php echo $result['product_weight_id']; ?>"><?php echo $result['proweight']; ?> </option>
                                                     <option value="">Select Product Weight</option>
                                                    </select>                      
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="product_price" class="form-control input-sm" placeholder="Enter product price" value="<?php echo $result['product_price']; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="hidden" class="form-control" name="id" value="<?php echo $result['id']; ?>">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>