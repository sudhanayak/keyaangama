       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
               <div class="container">                  
                   <div class="card">
                       <div class="card-header">
                            <div class="row">   
                                <div class="col-sm-6">   
                                    <h2>
                                    States List
                                    </h2>
                                </div>
                                <div class="col-sm-6">
                                   <div class='row'>
                                       <form action='<?php echo base_url() ?>State/viewstate' method='GET'>
                                           <div class="col-sm-6 ">
                                               <div class="form-group pull-right">
                                                   <input type="text" name='search' value="<?php echo $searchVal; ?>" ng-model="search" class="form-control" placeholder="Search">
                                               </div>
                                           </div>
                                           <div class="col-sm-6 ">
                                               <div class="form-group pull-right">
                                                   <button type="submit" class="btn  btn-fill btn-primary btn-wd waves-effect">Search</button>
                                               </div>
                                           </div>
                                       </form>
                                   </div>
                               </div>
                                <div class="col-sm-3 ">  </div>
                                <!-- <div class="col-sm-3 ">  
                                    <div class="form-group">
                                        <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                    </div>
                                </div> -->
                            </div>
                       </div>
                       
                       <div class="card-body table-responsive">
                           <!-- <div class="alert alert-success" role="alert">
                               Successfully status Updated
                            </div>
                            <div class="alert alert-danger" role="alert">
                               Error
                            </div> -->
                           <table class="table table-striped">
                               <thead class="black white-text">
                               <tr>
                                <th>Sno</th>
                                <th>Country</th>
                                <th>State Name</th>
                                <!-- <th>State Code</th> -->
                                <th>Action</th>
                               </tr>
                               </thead>
                               <tbody>
                                   <?php
                                   $i =$this->uri->segment(3)+1;
                                    $count = count(array_filter($result));
                                    if($count > 0) {
                                        foreach($result as $key => $row){
                                    ?>
                                   <tr class="zebra-striping">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row['country'];?></td>
                                        <td><?php echo $row['state_name'];?></td>
                                        <!-- <td><?php echo $row['state_code'];?></td> -->
                                        <td>
                                            <a href="<?php echo base_url(); ?>state/editstate/<?php echo $row['id'] ?>"><i class="zmdi zmdi-edit"></i></a>
                                            <?php
                                            if($row['keyaan_status']==1){
                                            ?>
                                            <a onclick="return confirm('Confirm to Enable?');" href="<?php echo base_url()?>state/stateEnable/<?php echo $row['id'];?>"> <i class="zmdi zmdi-close-circle-o"></i></a> 
                                           <?php
                                            }else{
                                           ?>
                                            <a onclick="return confirm('Confirm to Disable?');" href="<?php echo base_url()?>state/stateDisable/<?php echo $row['id'];?>" style="cursor:pointer"><i class="zmdi zmdi-check"></i></a>
                                            <?php
                                            }
                                            ?>
                                            <a onclick="return confirm('Are You Sure to Delete ?');" href="<?php echo base_url()?>state/deletestate/<?php echo $row['id'];?>" style="cursor:pointer"><i class="zmdi zmdi-delete"></i></a> 
                                        </td>
                                    </tr>
                                    <?php
                                       $i++; }
                                    }else{
                                        ?>
                                    <tr><td colspan="7">No State Found</td></tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                           </table>
                           <div class='col-md-12'>
                                <div class='pull-right'><?php echo $links; ?></div>
                            </div>
                       </div>
                   </div>
               </div>                
           </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>