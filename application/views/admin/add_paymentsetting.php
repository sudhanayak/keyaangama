            <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Add Paymentsetting
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>addPaymentsetting" 
                            method="POST" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select id="payment_mode" required class="form-control input-sm" name="payment_mode">
                                                    <option value="">Select Payment mode</option>
                                                    <option value="1">COD</option>
                                                    <option value="2">Online</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="payment_cod" style="display: none"> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select id="payment_gateway" class="form-control input-sm" name="payment_gateway">
                                                    <option value="">Select Payment gateway</option>
                                                    <option value="1">Paytm</option>
                                                    <option value="2">Payumoney</option>
                                                    <option value="3">ccavenue</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select id="payment_status" class="form-control input-sm" name="payment_status">
                                                    <option value="">Select Payment status</option>
                                                    <option value="1">Sand box</option>
                                                    <option value="2">Live</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="merchant_keys"class="form-control input-sm" placeholder="Merchant keys">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="salt_keys"class="form-control input-sm" placeholder="Salt keys">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>