       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                               <?php echo $package; ?>
                            </h2>
                        </div>
                        <form method="POST" action="<?php echo base_url() ?>Itinerary/addItinerary/<?php echo $packageCode; ?>" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?>
                            <div class="row">
                                   <div class="col-sm-6">
                                       <div class="form-group">
                                           <div class="fg-line">
                                               <?php if($number_of_days != 1){?>
                                               <div class="select">
                                                   <select class="form-control input-sm"  name="event_day">
                                                    <option value="">Select days</option>
                                                    <?php
                                                       for($i = 1; $i <= $number_of_days; $i++)
                                                           {
                                                           echo '<option value="'.$i.'">Day'.$i.'</option>';
                                                       }
                                                    ?>
                                                   </select>
                                               </div>
                                           <?php } ?>
                                           </div>
                                       </div>
                                   </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select class="form-control input-sm"  name="itenerary_id">
                                                     <option value="">Select Itenerary</option>
                                                     <?php 
                                                        foreach($itenerary as $val)
                                                        { 
                                                            echo '<option value="'.$val['id'].'">'.$val['name'].'</option>';
                                                        }
                                                     ?> 
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="itenerary_name" class="form-control input-sm" placeholder="Itinerary Name" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                             <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="number" name="itinerary_price" class="form-control input-sm" placeholder="Enter Price" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select class="form-control input-sm" name="itinerary_type" id="Itinerarytype" >
                                                        <option value="">Select Itinerary Type</option>
                                                        <option value="1">Image</option>
                                                        <option value="2">Description</option>
                                                        <option value="3">Images and Description</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div id="Itineraryimage" style="display: none">
                                <div class="tab-pane" id="itineraryimages"> 
                                    <div class="row">
                                        <div class="col-sm-5 col-sm-offset-1">
                                            <div class="text-box1">                                     
                                                <div class="row newImg">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                                            <img id="falseinput0" height="150" width="150">
                                                        </div>
                                                        <div>
                                                            <span class="btn btn-info btn-file">
                                                                <span class="fileinput-new">Select Itinerary image</span>
                                                                <span class="fileinput-exists">Change</span>
                                                                <input onchange="readURL(this,'falseinput0','baseInput0')" type="file" name="images[]">
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <a class="add-box btn btn-danger fileinput-exists" id="addmoreitinerarayimg" style="cursor:pointer"> Add More + </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div id="Itinerarydescription" style="display: none">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            Details
                                            <textarea rows="5" name="details" class="form-control input-sm html-editor" placeholder="Enter Details"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                               <div class="col-sm-6">
                                   <div class="form-group">
                                       <div class="fg-line">
                                           <input type="checkbox" name='make_it_default'>Make it default
                                       </div>
                                   </div>
                               </div>
                       </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <button  type='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>