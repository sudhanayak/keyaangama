       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Add Sub Subcategory
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>subsubcategory/addSubsubcategory" 
                            method="POST" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?> 
                                
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm"  name="cat_id" id="catId">
                                                     <option value="">Select Category </option>
                                                     <?php 
                                                        foreach($category as $val)
                                                        { 
                                                            echo '<option value="'.$val['id'].'">'.$val['category_name'].'</option>';
                                                        }
                                                     ?> 
                                                    </select>                                      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div> 
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm"  name="Subcat_id" id="subCatId">
                                                     <option value="">select Subcategory </option>
                                                    </select>                      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div> 
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="subsubcat_name" id="subsubCatId" class="form-control input-sm" placeholder="Sub Subcategory " required>
                                            </div>
                                        </div>
                                    </div>
                            </div> 
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="hidden" name="icon" class="form-control input-sm" value="1" placeholder="Icon">
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select class="form-control input-sm" name="price_selection" id="pricerange">
                                                        <option value="">Type of price</option>
                                                        <option value="0">Fixed</option>
                                                        <option value="1">Variable</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div> 
                        <div id="pricevariable" style="display: none"> 
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="price_from" class="form-control input-sm" placeholder="Enter price">
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="price_to" class="form-control input-sm" placeholder="Enter price">
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                            <div class="row" id="price" style="display: none">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="price" class="form-control input-sm" placeholder="Enter price">
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="meta_tag" class="form-control input-sm" placeholder="Enter tag"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="meta_keyword" class="form-control input-sm" placeholder="Enter keyword"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="meta_detail" class="form-control input-sm" placeholder="Enter description"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select image</span>                                           
                                                <input type="file" name="image" required>
                                            </span>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                               <div class="col-sm-12">
                                   <div class="fileinput fileinput-new" data-provides="fileinput">
                                       <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                       <div>
                                           <span class="btn btn-info btn-file">
                                               <span class="fileinput-new">Select Banner image</span>
                                               <span class="fileinput-exists">Change</span>
                                               <input type="file" name="subsubcat_banner" required>
                                           </span>
                                       </div>
                                   </div>
                               </div>
                           </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>