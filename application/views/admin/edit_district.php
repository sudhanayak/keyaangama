       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Edit District
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>District/updateDistrict" method="POST" enctype="multipart/form-data" >
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select  required class="form-control input-sm" name="country_id" id="countryId">
                                                <option value="<?php echo $result[0]['country_id']; ?>"><?php echo $result[0]['country']; ?> </option> 
                                                <option value="">select Country </option>
                                                 <?php 
                                                    foreach($country as $val)
                                                    { 
                                                        echo '<option value="'.$val['id'].'">'.$val['country_name'].'</option>';
                                                    }
                                                 ?> 
                                                </select>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select  required class="form-control input-sm"  name="state_id" id="stateId">
                                                    <option value="<?php echo $result[0]['state_id']; ?>"><?php echo $result[0]['state']; ?> </option>
                                                     <option value="">select State </option>
                                                </select>                                     
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="district_name" class="form-control input-sm" placeholder="City Name" value="<?php echo $result[0]['district_name']; ?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="hidden"  name="id" value="<?php echo $result[0]['id']; ?>">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>