<?php
include_once'header.php';
?>
<section id="main">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php include_once 'sidebar.php';?>
    </aside>
    <section id="content">
        <div class="container">                  
            <div class="card">
                <div class="card-header">
                    <div class="row">   
                        <div class="col-sm-6">   
                            <h2>UpgradeVendor Details</h2>
                        </div>
                        <div class="col-sm-3 "></div>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <dl class="dl-horizontal">
                                <dt>Pacakage Name</dt>
                                <dd><?php echo $result[0]['package']; ?></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Amount</dt>
                                <dd><?php echo $result[0]['amount']; ?></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Order Date</dt>
                                <dd><?php echo $result[0]['order_date']; ?></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Duration</dt>
                                <dd><?php echo $result[0]['duration']; ?></dd>
                            </dl>
                            
                            <dl class="dl-horizontal">
                                <dt>Start Date</dt>
                                <dd><?php echo $result[0]['start_date']; ?></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>End Date</dt>
                                <dd><?php echo $result[0]['end_date']; ?></dd>
                            </dl>
                             <dl class="dl-horizontal">
                                <dt>Payment Status</dt>
                                <dd><?php echo $result[0]['payment_status']; ?></dd>
                            </dl>
                             <dl class="dl-horizontal">
                                <dt>Payment Type</dt>
                                <dd><?php if($result[0]['payment_type'] == 1) { echo "By Cash"; } elseif($result[0]['payment_type'] == 2) { echo "By cheque"; } ?></dd>
                            </dl>
                            <?php if($result[0]['payment_type'] == 1) { ?>
                                <dl class="dl-horizontal">
                                    <dt>Cash Detail</dt>
                                    <dd><?php echo $result[0]['cash_detail']; ?></dd>
                                </dl>
                            <?php } ?>
                            <?php if($result[0]['payment_type'] == 2) { ?>
                             <dl class="dl-horizontal">
                                <dt>Cheque Number</dt>
                                <dd><?php echo $result[0]['cheque_number']; ?></dd>
                            </dl>
                             <dl class="dl-horizontal">
                                <dt>Cheque Detail</dt>
                                <dd><?php echo $result[0]['cheque_detail']; ?></dd>
                            </dl>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>                
    </section>
</section>        
<?php
    include_once'footer.php';
?>