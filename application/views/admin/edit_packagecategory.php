       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Edit Packagecategory
                            </h2>
                        </div>
                        <form method="POST"  action="<?php echo base_url() ?>Packagecategory/updatePackagecategory" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?>    
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="category_name" class="form-control input-sm" placeholder="Category Name" value="<?php echo $result['category_name']; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select class="form-control input-sm" name="category_type" id="">
                                                        <option value="<?php echo $result['category_type']; ?>">
                                                    <?php if($result['category_type'] == 0){ echo 'Single'; } ?>
                                                    <?php if($result['category_type'] == 1){ echo 'Multiple Days'; } ?>
                                                        </option>
                                                        <option value="">Select Category Type</option>
                                                        <option value="0">Single</option>
                                                        <option value="1">Multiple days</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="base_price" class="form-control input-sm" placeholder="Enter Price" value="<?php echo $result['base_price']; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="details" class="form-control input-sm" placeholder="Enter Details"><?php echo $result['details']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                            <img src='<?php echo base_url() ?>uploads/packagecategory/<?php echo $result['image']; ?>' width="250" height="250" >
                                        </div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="image">
                                            </span>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="hidden" value="<?php echo $result['id']; ?>" name="id">
                                    <button  type='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>