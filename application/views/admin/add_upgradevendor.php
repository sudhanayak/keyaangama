<?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Add Upgrade Vendor
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>addUpgradevendor/<?php echo $vendorCode; ?>" method="POST" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?>    
                            <div class="row" id="vendorPackage">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <select class="form-control" name="package_id" id="Package_id">
                                                <option>Select Vendor Package</option>
                                                <?php
                                                $count = count(array_filter($resultPackage));
                                                if($count > 0) {
                                                $i=0;
                                                foreach($resultPackage as $key => $row){
                                                ?>
                                                    <option value="<?php echo  $row['id'] ?>"><?php echo  $row['package_name'] ?></option>
                                                <?php } } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="number" id="packageprice" name="amount" class="form-control input-sm" placeholder="Enter amount" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="number" id="packageduration" name="duration" class="form-control input-sm" placeholder="Enter Duration" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select class="form-control" name="payment_type" id="payment_type">
                                                       <option value="">Payment Type</option>
                                                       <option value="1">By Cash</option>
                                                       <option value="2">By cheque</option>
                                                   </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div id="cash" style="display: none"> 
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <textarea rows="5" name="cash_detail" class="form-control input-sm" placeholder="Enter Description"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="cheque" style="display: none"> 
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="cheque_number" class="form-control input-sm" placeholder="Enter cheque number" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <textarea rows="5" name="cheque_detail" class="form-control input-sm" placeholder="Enter Description"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <button  type='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>