<?php
include_once'header.php';
?>
<section id="main">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php include_once 'sidebar.php';?>
    </aside>
    <section id="content">
        <div class="container">                  
            <div class="card">
                <div class="card-header">
                    <div class="row">   
                        <div class="col-sm-6">   
                            <h2>Event Booking Details</h2>
                        </div>
                        <div class="col-sm-3 "></div>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <dl class="dl-horizontal">
                                <dt>Name</dt>
                                <dd><?php echo $result[0]['user_name']; ?></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Email</dt>
                                <dd><?php echo $result[0]['user_email']; ?></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Mobile Number</dt>
                                <dd><?php echo $result[0]['user_mobile']; ?></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Category</dt>
                                <dd><?php echo $result[0]['category']; ?></dd>
                            </dl>
                            
                            <dl class="dl-horizontal">
                                <dt>Address</dt>
                                <dd><?php echo $result[0]['user_address']; ?></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Date of Event</dt>
                                <dd><?php echo $result[0]['date_of_event']; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>                
    </section>
</section>        
<?php
    include_once'footer.php';
?>