       <?php include_once'header.php'; ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container container-alt">

                    <div class="block-header">
                        <h2>Employee Details
                            <small><?php echo $result[0]['emp_fname']; ?></small>
                        </h2>
                    </div>

                    <div class="card" id="profile-main" style="min-height:800px">
                        <div class="pm-overview c-overflow">
                            <div class="pmo-pic">
                                <div class="p-relative">
                                     <img class="img-responsive" src="<?php echo $result[0]['emp_profile_img']; ?>" alt="">
                                </div>
                            </div>

                            <div class="pmo-block pmo-contact hidden-xs">
                                <h2>Contact</h2>
                                <ul>
                                    <li><i class="zmdi zmdi-phone"></i><?php echo $result[0]['emp_mobile']; ?></li>
                                    <li><i class="zmdi zmdi-email"></i><?php echo $result[0]['emp_email']; ?></li>
                                    <?php if($result3[0]['per_address_name'] != '' && $result3[0]['per_city_name'] != '' && $result3[0]['per_district_name'] != '' && $result3[0]['per_state_name'] != '' && $result3[0]['per_country_name'] != '' && $result3[0]['per_pincode_name'] != '') { ?>
                                    <li>
                                        <i class="zmdi zmdi-pin"></i>
                                        <address class="m-b-0 ng-binding">
                                            <?php echo $result3[0]['per_address_name']; ?>,<?php echo $result3[0]['per_city_name']; ?>,<?php echo $result3[0]['per_district_name'];?>,<?php echo $result3[0]['per_state_name']; ?>,<?php echo $result3[0]['per_country_name']; ?> -<?php echo  $result3[0]['per_pincode_name']; ?>
                                        </address>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="pm-body clearfix">
                            <ul class="tab-nav tn-justified">
                                <li class="active"><a style="cursor: pointer;" class="vendorDetails">About</a></li>
                                <li><a style="cursor: pointer;" class="vendorPhotos">Photos</a></li>
                            </ul>
                            <div  class="basic_profile">
                                <div class="pmb-block">
                                    <div class="pmbb-header">
                                        <h2><i class="zmdi zmdi-equalizer m-r-10"></i> Summary</h2>
                                    </div>
                                    <div class="pmbb-body p-l-30">
                                        <div class="pmbb-view">
                                           Employee Details
                                        </div>
                                    </div>
                                </div>                    
                                <div class="pmb-block">
                                    <div class="pmbb-header">
                                        <h2><i class="zmdi zmdi-account m-r-10"></i> Basic Information</h2>
                                    </div>
                                    <div class="pmbb-body p-l-30">
                                        <div class="pmbb-view">
                                            <dl class="dl-horizontal">
                                                <dt>Full Name</dt>
                                                <dd><?php echo $result[0]['emp_fname']; ?> <?php echo $result[0]['emp_lname']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Email</dt>
                                                <dd><?php echo $result[0]['emp_email']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Mobile</dt>
                                                <dd><?php echo $result[0]['emp_mobile']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Date of Birth</dt>
                                                <dd><?php echo $result[0]['emp_dob']; ?></dd>
                                            </dl>
                                             <dl class="dl-horizontal">
                                                <dt>Age</dt>
                                                <dd><?php echo $result[0]['emp_age']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Emp Marital Status</dt>
                                                <dd><?php if($result[0]['emp_marital_status'] == "1") { echo "Yes"; } elseif($result[0]['emp_marital_status'] == "2") { echo "No"; } ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Address</dt>
                                                <dd><?php echo $result3[0]['per_address_name']; ?>,<?php echo $result3[0]['per_city_name']; ?>,<?php echo $result3[0]['per_district_name'];?>,<?php echo $result3[0]['per_state_name']; ?>,<?php echo $result3[0]['per_country_name']; ?> -<?php echo  $result3[0]['per_pincode_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Designation</dt>
                                                <dd><?php echo $result[0]['emp_designation']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Disability</dt>
                                                <dd><?php if($result[0]['any_disability'] == "1") { echo "Yes"; } elseif($result[0]['any_disability'] == "2") { echo "No"; } ?></dd>
                                            </dl>
                                            <?php if($result[0]['any_disability'] == "1") { ?>
                                            <dl class="dl-horizontal">
                                                <dt>Disability</dt>
                                                <dd><?php if($result[0]['any_disability'] == "1") { echo "Yes"; } elseif($result[0]['any_disability'] == "2") { echo "No"; } ?></dd>
                                            </dl>
                                            <?php } ?>
                                            <dl class="dl-horizontal">
                                                <dt>Resume</dt>
                                                <dd><?php echo $result[0]['resume']; ?></dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
                                    <div class="pmb-block">
                                        <div class="pmbb-header">
                                            <h2><i class="zmdi zmdi-book-image"></i> EDUCATION DETAILS</h2>
                                        </div>
                                        <div class="pmbb-body p-l-30">
                                            <div class="pmbb-view">
                                                <dl class="dl-horizontal">
                                                    <dt>Master Degree</dt>
                                                    <dd><?php echo $result2[0]['master_degree']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Master Passout Year</dt>
                                                    <dd><?php echo $result2[0]['master_passout']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Master Specialization</dt>
                                                    <dd><?php echo $result2[0]['master_spl']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Master Institutes</dt>
                                                    <dd><?php echo $result2[0]['master_intitutes']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Graduation</dt>
                                                    <dd><?php echo $result2[0]['graduation']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Graduation Passout Year</dt>
                                                    <dd><?php echo $result2[0]['graduation_passout']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Graduation Specialization</dt>
                                                    <dd><?php echo $result2[0]['graduation_spl']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Graduation Institutes</dt>
                                                    <dd><?php echo $result2[0]['graduation_intitutes']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Inter</dt>
                                                    <dd><?php echo $result2[0]['inter']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Inter Passout Year</dt>
                                                    <dd><?php echo $result2[0]['inter_passout']; ?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt>Inter Specialization</dt>
                                                    <dd><?php echo $result2[0]['inter_spl']; ?></dd>
                                                </dl>
                                                 <dl class="dl-horizontal">
                                                    <dt>Inter Institutes</dt>
                                                    <dd><?php echo $result2[0]['inter_intitutes']; ?></dd>
                                                </dl>
                                            </div>
                                        </div>
                                    </div><br/>
                                    <div class="pmb-block" style='display:block'>
                                    <div class="pmbb-header">
                                        <h2><i class="zmdi zmdi-pin"></i> Family Details</h2>
                                    </div>
                                    <div class="pmbb-body p-l-30">
                                        <div class="pmbb-view">
                                            <dl class="dl-horizontal">
                                                <dt>Father Name</dt>
                                                <dd><?php echo $result3[0]['father_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Mother Name</dt>
                                                <dd><?php echo $result3[0]['mother_name']; ?></dd>
                                            </dl>
                                            Permenant Adress Details
                                            <dl class="dl-horizontal">
                                                <dt>Country</dt>
                                                <dd><?php echo $result3[0]['per_country_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>State</dt>
                                                <dd><?php echo $result3[0]['per_state_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>District</dt>
                                                <dd><?php echo $result3[0]['per_district_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>City</dt>
                                                <dd><?php echo $result3[0]['per_city_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Pincode</dt>
                                                <dd><?php echo $result3[0]['per_pincode_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Location</dt>
                                                <dd><?php echo $result3[0]['per_address_name']; ?></dd>
                                            </dl>
                                            Resident Adress Details
                                            <dl class="dl-horizontal">
                                                <dt>Country</dt>
                                                <dd><?php echo $result3[0]['res_country_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>State</dt>
                                                <dd><?php echo $result3[0]['res_state_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>District</dt>
                                                <dd><?php echo $result3[0]['res_district_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>City</dt>
                                                <dd><?php echo $result3[0]['res_city_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Pincode</dt>
                                                <dd><?php echo $result3[0]['res_pincode_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Location</dt>
                                                <dd><?php echo $result3[0]['res_address_name']; ?></dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
                                <div class="pmb-block">
                                    <div class="pmbb-header">
                                        <h2> Salary Details </h2>
                                    </div>
                                    <div class="pmbb-body p-l-30">
                                        <div class="pmbb-view">
                                            <dl class="dl-horizontal">
                                                <dt>Grade Type</dt>
                                                <dd><?php if($result4[0]['grade_type'] == "1") { echo "Label 1"; } elseif($result4[0]['grade_type'] == "2") { echo "Label 2"; } elseif($result4[0]['grade_type'] == "3") { echo "Label 3"; } ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Total Salary</dt>
                                                <dd><?php echo $result4[0]['total_salary']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Take Home</dt>
                                                <dd><?php echo $result4[0]['take_home']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal"">
                                                <dt>PF</dt>
                                                <dd><?php echo $result4[0]['pf']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal"">
                                                <dt>PF Amount</dt>
                                                <dd><?php echo $result4[0]['pf_amount']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal"">
                                                <dt>Bank Name</dt>
                                                <dd><?php echo $result4[0]['bank_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal"">
                                                <dt>Account Number</dt>
                                                <dd><?php echo $result4[0]['account_number']; ?></dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
                                <div class="pmb-block">
                                    <div class="pmbb-header">
                                        <h2> Work History Details </h2>
                                    </div>
                                    <div class="pmbb-body p-l-30">
                                        <div class="pmbb-view">
                                            <dl class="dl-horizontal">
                                                <dt>Designation</dt>
                                                <dd><?php echo $result5[0]['designation']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Company Name</dt>
                                                <dd><?php echo $result5[0]['company_name']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>Employee Mode</dt>
                                                <dd><?php echo $result5[0]['emp_mode']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>From Date</dt>
                                                <dd><?php echo $result5[0]['from_date']; ?></dd>
                                            </dl>
                                            <dl class="dl-horizontal">
                                                <dt>To Date</dt>
                                                <dd><?php echo $result5[0]['to_date']; ?></dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>                           
                            </div>                           
                          <!--
                          first tab end here
                          -->
                        <div   class="m-t-25 photo_profile" >
                            <div class="row">
                                <h3><center>Employee Documents</center></h3>
                                <div class="lightbox">
                                    <?php 
                                    if($result1['imglist'] != "") { 
                                        foreach ($result1['imglist'] as $key => $img) { 
                                    ?>
                                        <div data-src="<?php echo base_url(); ?><?php echo ($img['doc_img']); ?>" class="col-md-3 col-sm-4 col-xs-6">
                                            <div class="lightbox-item p-item">
                                                <img src="<?php echo base_url(); ?><?php echo ($img['doc_img']); ?>" alt="" width="100px" height="100px"/>
                                            </div>
                                        </div>
                                    <?php } } else {
                                        echo "<h4><center>No Employee Documents.</center></h4>";
                                    }
                                    ?>    
                                </div>
                            </div>
                            </div>                                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>