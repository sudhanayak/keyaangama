       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
               <div class="container">                  
                   <div class="card">
                       <div class="card-header">
                            <div class="row">   
                                <div class="col-sm-6">   
                                    <h2>
                                    View Itinerary List
                                    </h2>
                                </div>
                                <div class="col-sm-3 "> </div>
                            </div>
                       </div>
                       
                       <div class="card-body table-responsive">
                           <table class="table table-striped">
                               <thead class="black white-text">
                               <tr>
                                <th>Sno</th>
                                <th>Itinerary Name</th>
                                <th>Action</th>
                               </tr>
                               </thead>
                               <tbody>
                                   <?php
                                    $i =$this->uri->segment(3)+1;
                                    $count = count(array_filter($result));
                                    if($count > 0) {
                                        foreach($result as $key => $row){
                                    ?>
                                   <tr class="zebra-striping">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row['name']; ?></td>
                                            <td>
                                            <a data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Edit" href="<?php echo base_url(); ?>Packageitinerary/editPackageitinerary/<?php echo $row['id'] ?>"><i class="zmdi zmdi-edit"></i></a>
                                            <?php
                                            if($row['keyaan_status']==1){
                                            ?>
                                            <a data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Disable" onclick="return confirm('Confirm to Enable?');" href="<?php echo base_url()?>Packageitinerary/packageitineraryEnable/<?php echo $row['id'];?>"> <i class="zmdi zmdi-close-circle-o"></i></a> 
                                           <?php
                                            }else{
                                           ?>
                                            <a data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Enable" onclick="return confirm('Confirm to Disable?');" href="<?php echo base_url()?>Packageitinerary/packageitineraryDisable/<?php echo $row['id'];?>" style="cursor:pointer"><i class="zmdi zmdi-check"></i></a>
                                            <?php
                                            }
                                            ?>
                                            <a data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete" onclick="return confirm('Are You Sure to Delete ?');" href="<?php echo base_url()?>Packageitinerary/deletePackageitinerary/<?php echo $row['id'];?>" style="cursor:pointer"><i class="zmdi zmdi-delete"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                       $i++; }
                                    }else{
                                        ?>
                                    <tr><td colspan="7">No Itinerary Found</td></tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                           </table>
                       </div>
                   </div>
               </div>                
           </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>