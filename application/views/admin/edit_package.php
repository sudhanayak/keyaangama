       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Edit Package
                            </h2>
                        </div>
                        <form method="POST"  action="<?php echo base_url() ?>Package/updatePackage" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?>    
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="package_name" class="form-control input-sm" placeholder="Package Name" value="<?php echo $result['package_name']; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm packagecategory"  name="category_id" id="catmultiple">
                                                        <option value="<?php echo $result['category_id']; ?>"><?php echo $result['category']; ?> </option> 
                                                     <option value="">Select Category </option>
                                                     <?php 
                                                        foreach($category as $val)
                                                        { 
                                                            echo '<option value="'.$val['id'].'">'.$val['category_name'].'</option>';
                                                        }
                                                     ?> 
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm packagesubcategory"  name="sub_category_id">
                                                    <option value="<?php echo $result['sub_category_id']; ?>"><?php echo $result['subcategory']; ?> </option>
                                                     <option value="">select Subcategory </option>
                                                    </select>                      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div id="cat_type">
                             <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="number_of_days" class="form-control input-sm" placeholder="Enter number of days" value="<?php echo $result['number_of_days']; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                           <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="number" name="price" class="form-control input-sm" placeholder="Enter Price" value="<?php echo $result['price']; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            Details
                                            <textarea rows="5" name="package_detail" class="form-control input-sm html-editor" placeholder="Enter Details"><?php echo $result['package_detail']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5 col-sm-offset-1">
                                <?php 
                                    if($result['packageimage'] != "") { 
                                        foreach ($result['packageimage'] as $key => $img) {
                                            if($img['media_type'] == 1) { 
                                ?>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                            <img  src="<?php echo base_url()."uploads/packageimg/".$img['image']; ?>" height="150" width="150">
                                        </div>
                                        <div>                              
                                            <a imgId='<?php echo $img['id'] ?>' style="cursor:pointer" class="btn btn-danger removePackageImg packageImgs"
                                                    data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                <?php } } } ?> 
                                    <div class="package-image">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                        <img id="falseinput0" height="150" width="150">
                                        </div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select Package image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input onchange="readURL(this,'falseinput0','baseInput0')" type="file" name="image[]">
                                            </span>
                                            <a class="add-box btn btn-danger fileinput-exists" id="addmorepacakges" style="cursor:pointer"> Add More + </a>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                               <?php
                                   if($result['packagehighlights'] != "") {
                                       foreach ($result['packagehighlights'] as $key => $highlights) {
                               ?>
                               <div class="col-sm-6">
                                       <div class="form-group">
                                           <div class="fg-line">
                                               <input type="text" name="highlights[]" class="form-control input-sm" placeholder="Enter package highlights" value="<?php echo $highlights['highlights']; ?>">
                                           </div>
                                           <a highlightsId='<?php echo $highlights['id'] ?>' style="cursor:pointer" class="btn btn-danger removePackageHighlights packageHighlights" data-dismiss="fileinput">Remove</a>
                                       </div>
                                   </div>
                               <?php } } ?>
                               <div class="package-highlights">
                                   <div class="col-sm-6">
                                       <div class="form-group">
                                           <div class="fg-line">
                                               <input type="text" name="highlights[]" class="form-control input-sm" placeholder="Enter package highlights" id="Packagehighlights">
                                           </div>
                                           <a class="add-box btn btn-danger fileinput-exists" id="addmorepackagehighlights" style="cursor:pointer"> Add More + </a>
                                       </div>
                                   </div>
                               </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="hidden" value="<?php echo $result['id']; ?>" name="id">
                                    <input type="hidden" value="<?php echo $result['package_code']; ?>" name="package_code">
                                    <button  type='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>