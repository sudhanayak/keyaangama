       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
               <div class="container">                  
                   <div class="card">
                       <div class="card-header">
                            <div class="row">   
                                <div class="col-sm-6">   
                                    <h2>
                                    Catering category List
                                    </h2>
                                </div>
                                <div class="col-sm-6">
                                   <div class='row'>
                                       <form action='<?php echo base_url() ?>Cateringcategory/viewCateringcategory' method='GET'>
                                           <div class="col-sm-6 ">
                                               <div class="form-group pull-right">
                                                   <input type="text" name='search' value="<?php echo $searchVal; ?>" ng-model="search" class="form-control" placeholder="Search">
                                               </div>
                                           </div>
                                           <div class="col-sm-6 ">
                                               <div class="form-group pull-right">
                                                   <button type="submit" class="btn  btn-fill btn-primary btn-wd waves-effect">Search</button>
                                               </div>
                                           </div>
                                       </form>
                                   </div>
                               </div>
                                <div class="col-sm-3 ">  </div>
                                
                            </div>
                       </div>
                       
                       <div class="card-body table-responsive">
                         <!--  <?php
                                //echo $this->session->flashdata('msg');
                            ?> -->
                           <table class="table table-striped">
                               <thead class="black white-text">
                               <tr>
                                <th>Sno</th>
                                <th>category</th>
                                <th>Action</th>
                               </tr>
                               </thead>
                               <tbody>
                                   <?php
                                   $i =$this->uri->segment(3)+1;
                                    $count = count(array_filter($result));
                                    if($count > 0) {
                                        foreach($result as $key => $row){
                                    ?>
                                   <tr class="zebra-striping">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row['category_name']; ?> </td>
                                        <td>
                                            <a data-toggle="tooltip"data-placement="bottom" title="Edit catering category" href="<?php echo base_url(); ?>Cateringcategory/editCateringcategory/<?php echo $row['id'] ?>"><i class="zmdi zmdi-edit"></i></a>
                                            <?php
                                            if($row['keyaan_status']==1){
                                            ?>
                                            <a data-toggle="tooltip"data-placement="bottom" title="Disable" onclick="return confirm('Confirm to Enable?');" href="<?php echo base_url()?>Cateringcategory/cateringcatEnable/<?php echo $row['id'];?>"> <i class="zmdi zmdi-close-circle-o"></i></a> 
                                           <?php
                                            }else{
                                           ?>
                                            <a data-toggle="tooltip"data-placement="bottom" title="Enable" onclick="return confirm('Confirm to Disable?');" href="<?php echo base_url()?>Cateringcategory/cateringcatDisable/<?php echo $row['id'];?>" style="cursor:pointer"><i class="zmdi zmdi-check"></i></a>
                                            <?php
                                            }
                                            ?>
                                            <a data-toggle="modal" title="Image" 
                                               data-target="#myModal<?php echo $i; ?>">
                                                <i class="zmdi zmdi-collection-folder-image">                                              
                                                </i>
                                            </a>
                                            <div class="modal fade"
                                            id="myModal<?php echo $i; ?>" 
                                            tabindex="-1" 
                                            role="dialog" 
                                            aria-labelledby="myModalLabel<?php echo $i; ?>">
                                            <div class="modal-dialog" role="document">
                                               <div class="modal-content">
                                                 <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                   <h4 class="modal-title">catering category Detail</h4>
                                                 </div>
                                                 <div class="modal-body">
                                                    <table>
                                                    <tr>
                                                    <th>Master Category:</th>
                                                    <td><?php echo $row['mastercategory']; ?></td>
                                                    </tr>
                                                    <tr>
                                                    <th>Category:</th>
                                                    <td><?php echo $row['category_name']; ?></td>
                                                    </tr>
                                                    <tr>
                                                    <th>Category Web Image:</th>
                                                    <td><img src='<?php echo base_url() ?>uploads/category_webimage/<?php echo $row['category_webimage']; ?>' width="150" height="150"></td>
                                                    </tr>
                                                    <tr>
                                                    <th>Category App Image:</th>
                                                    <td><img src='<?php echo base_url() ?>uploads/category_appimage/<?php echo $row['category_appimage']; ?>' width="150" height="150"></td>
                                                    </tr>
                                                    </table>
                                                  </div>
                                               </div><!-- /.modal-content -->
                                               </div><!-- /.modal-content -->
                                            </div><!-- /.modal-content -->
                                             <a data-toggle="tooltip"data-placement="bottom" title="Delete Catering category" onclick="return confirm('Are You Sure to Delete ?');" href="<?php echo base_url()?>Cateringcategory/deleteCateringcategory/<?php echo $row['id'];?>" style="cursor:pointer"><i class="zmdi zmdi-delete"></i></a> 
                                        </td>
                                    </tr>
                                    <?php
                                        $i++;}
                                    }else{
                                        ?>
                                    <tr><td colspan="7">No Catering category Found</td></tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                           </table>
                           <div class='col-md-12'>
                                <div class='pull-right'><?php echo $links; ?></div>
                            </div>
                            <!--
                            //sample data start here
                            -->
                            
                       </div>
                   </div>
               </div>                
           </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>