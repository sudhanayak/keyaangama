
<section id="content">
<div class="container">
<div class="block-header">
<h2>Form Elements</h2>
<ul class="actions">
	<li>
		<a class="icon-pop" href="#">
			<i class="zmdi zmdi-trending-up"></i>
		</a>
	</li>
	<li>
		<a class="icon-pop" href="#">
			<i class="zmdi zmdi-check-all"></i>
		</a>
	</li>
	<li class="dropdown">
		<a class="icon-pop" href="#" data-toggle="dropdown">
			<i class="zmdi zmdi-more-vert"></i>
		</a>

		<ul class="dropdown-menu dropdown-menu-right">
			<li>
				<a href="#">Refresh</a>
			</li>
			<li>
				<a href="#">Manage Widgets</a>
			</li>
			<li>
				<a href="#">Widgets Settings</a>
			</li>
		</ul>
	</li>
</ul>

</div>

<div class="card">


<div class="card-body card-padding">
	<div class="row">
		<div class="col-sm-1"></div>
<div class="col-sm-10">
<!--      Wizard container        -->
<div class="wizard-container">
<div class="card wizard-card" data-color="purple" id="wizard">
	<form action="" method="">
	<!--        You can switch " data-color="rose" "  with one of the next bright colors: "blue", "green", "orange", "purple"        -->

		<div class="wizard-header">
			<h3 class="wizard-title">
				Vendor Registration
			</h3>
			<h5>This information will let us know more about your place.</h5>
		</div>
		<div class="wizard-navigation">
			<ul>
				<li><a href="#location" data-toggle="tab">Basic Details</a></li>
				<li><a href="#type" data-toggle="tab">Business Details</a></li>
				<li><a href="#facilities" data-toggle="tab">Social Media Details</a></li>
				<li><a href="#images" data-toggle="tab">Images and Videos</a></li>
							<li><a href="#description" data-toggle="tab">Description</a></li>
			</ul>
		</div>

		<div class="tab-content">
			<div class="tab-pane" id="location">
				<div class="row">
							<div class="col-sm-12">
							<h4 class="info-text"> Let's start with the basic details</h4>
							</div>
							<div class="col-sm-5 col-sm-offset-1">
								 <div class="form-group">
									<div class="fg-line">
										<input type="text" class="form-control" placeholder="Enter Your Name">
									</div>
								</div>

							</div>
							<div class="col-sm-5 col-sm-offset-1">
								 <div class="form-group">
									<div class="fg-line">
										<input type="email" class="form-control" placeholder="Enter Your Email">
									</div>
								</div>

							</div>
							<div class="col-sm-5 col-sm-offset-1">
								 <div class="form-group">
									<div class="fg-line">
										<input type="text" class="form-control" placeholder="Enter Your Mobile Number">
									</div>
								</div>

							</div>
							<div class="col-sm-5 col-sm-offset-1">
								 <div class="form-group">
									<div class="fg-line">
										<input type="text" class="form-control" placeholder="Enter Your Alternate Mobile Number">
									</div>
								</div>

							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="col-sm-6 padd0">
									<div class="form-group">
									   <div class="fg-line">
										   <div class="select">
											   <select class="form-control">
												   <option>Type of Proof</option>
												   <option>Pan Card</option>
												   <option>Aadhaar Number</option>
												   <option>Driving License</option>
												   <option>Voter Id</option>
												   
											   </select>
										   </div>
									   </div>
								   </div>
								</div>
								<div class="col-sm-6 paddr0">
									<div class="form-group">
										<div class="fg-line">
											<input type="text" class="form-control" placeholder="Proof Number">
										</div>
									</div>
								</div>


							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<div class="fg-line">
										<textarea class="form-control auto-size"
												  placeholder="Start pressing Enter to see growing..."></textarea>
									</div>
								</div>
							</div>
							
				</div>
			</div>
			<div class="tab-pane" id="type">
				<h4 class="info-text">What type of location do you have? </h4>
				<div class="row">
					<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<div class="fg-line">
										<input type="text" class="form-control" placeholder="Enter Your Company Name">
									</div>
								</div>
							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<div class="fg-line">
										<input type="text" class="form-control" placeholder="Enter Your Business Name">
									</div>
								</div>
							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<div class="fg-line">
										<input type="text" class="form-control" placeholder="Enter Your Business Email">
									</div>
								</div>
							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<div class="fg-line">
										<input type="text" class="form-control" placeholder="Enter Your Mobile Number">
									</div>
								</div>
							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<div class="fg-line">
										<input type="text" class="form-control" placeholder="Enter Your Landline Number">
									</div>
								</div>
							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<div class="fg-line">
										<input type="text" class="form-control" placeholder="Enter Your Business Pancard">
									</div>
								</div>
							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<div class="fg-line">
										<input type="text" class="form-control" placeholder="Enter Your Company Registration Number">
									</div>
								</div>
							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<div class="fg-line">
										<textarea class="form-control auto-size"
												  placeholder="Enter your business address..."></textarea>
									</div>
								</div>
							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<div class="fg-line">
										<div class="select">
											<select class="form-control">
												<option>Select Category</option>
												<option>Pan Card</option>
												<option>Aadhaar Number</option>
												<option>Driving License</option>
												<option>Voter Id</option>

											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<div class="fg-line">
										<div class="select">
											<select class="form-control">
												<option>Select Sub Category</option>
												<option>Pan Card</option>
												<option>Aadhaar Number</option>
												<option>Driving License</option>
												<option>Voter Id</option>

											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<div class="fg-line">
										<input type="text" class="form-control" placeholder="Enter Your Website Link">
									</div>
								</div>
							</div>
							
							<div class="col-sm-5 col-sm-offset-1">
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
									<div>
										<span class="btn btn-info btn-file">
											<span class="fileinput-new">Select image</span>
											<span class="fileinput-exists">Change</span>
											<input type="file" name="...">
										</span>
										<a href="#" class="btn btn-danger fileinput-exists"
										   data-dismiss="fileinput">Remove</a>
									</div>
								</div>
							</div>
						</div>
			</div>
					
			<div class="tab-pane" id="facilities">
				<h4 class="info-text">Tell us more about facilities. </h4>
				<div class="row">
					<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<div class="fg-line">
										<input type="text" class="form-control" placeholder="Enter Your Facebook Link">
									</div>
								</div>
							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<div class="fg-line">
										<input type="text" class="form-control" placeholder="Enter Your Google Plus Link">
									</div>
								</div>
							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<div class="fg-line">
										<input type="text" class="form-control" placeholder="Enter Your Twitter Link">
									</div>
								</div>
							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<div class="fg-line">
										<input type="text" class="form-control" placeholder="Enter Your linkedln Link">
									</div>
								</div>
							</div>
				</div>
			</div>
					<div class="tab-pane" id="images">
				<h4 class="info-text">Please add you images and videos. </h4>
				<div class="row">
							<div class="col-sm-5 col-sm-offset-1">
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
									<div>
										<span class="btn btn-info btn-file">
											<span class="fileinput-new">Select image</span>
											<span class="fileinput-exists">Change</span>
											<input type="file" name="...">
										</span>
										<a href="#" class="btn btn-danger fileinput-exists"
										   data-dismiss="fileinput">Remove</a>
									</div>
								</div>
							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
									<div>
										<span class="btn btn-info btn-file">
											<span class="fileinput-new">Select Video</span>
											
											<input type="file" name="...">
										</span>
										<a href="#" class="btn btn-danger fileinput-exists"
										   data-dismiss="fileinput">Remove</a>
									</div>
								</div>
							</div>
						</div>
					</div>
			<div class="tab-pane" id="description">
				<div class="row">
					<h4 class="info-text"> Drop us a small description. </h4>
					<div class="col-sm-6 col-sm-offset-1">
						<div class="form-group label-floating">
							<label class="control-label">Place description</label>
							<textarea class="form-control" placeholder="" rows="9"></textarea>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group label-floating">
							<label class="control-label">Example</label>
							<p class="description">"The place is really nice. We use it every sunday when we go fishing. It is so awesome."</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="wizard-footer">
			<div class="pull-right">
				<input type='button' class='btn btn-next btn-fill btn-primary btn-wd' name='next' value='Next' />
				<input type='button' class='btn btn-finish btn-fill btn-primary btn-wd' name='finish' value='Finish' />
			</div>
			<div class="pull-left">
				<input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />
			</div>
			<div class="clearfix"></div>
		</div>
	</form>
</div>
</div> <!-- wizard container -->
</div>
		<div class="col-sm-1"></div>
</div> <!-- row -->
</div>
</div>


</div>
</section>
</section>
        
       