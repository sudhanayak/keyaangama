       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Edit Vendor Package
                            </h2>
                        </div>
                        <form method="POST"  action="<?php echo base_url() ?>Vendorpackage/updateVendorpackage" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?>    
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="package_name" class="form-control input-sm" placeholder="Package Name" value="<?php echo $result['package_name']; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="number" name="price" class="form-control input-sm" placeholder="Enter Price" value="<?php echo $result['price']; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                             <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="number" name="duration" class="form-control input-sm" placeholder="Enter Duration" value="<?php echo $result['duration']; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="package_detail" class="form-control input-sm" placeholder="Enter Details"><?php echo $result['package_detail']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="hidden" value="<?php echo $result['id']; ?>" name="id">
                                    <button  type='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>