    <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Edit Expenditure
                            </h2>
                        </div>
                        <form enctype="multipart/form-data" method="POST" action="<?php echo base_url() ?>Expenditure/updateExpenditure">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="expenditure_name"class="form-control input-sm" placeholder="Enter expenditure name" value="<?php echo $result[0]['expenditure_name']; ?>"  required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">                            
                                        <div class="fg-line">   
                                            <input type='text' class="form-control date-picker"
                                                placeholder="Date" name="date" value="<?php echo $result[0]['date']; ?>" >
                                        </div>    
                                    </div>
                                </div>
                            </div>  
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="amount"class="form-control input-sm" placeholder="Enter amount" value="<?php echo $result[0]['amount']; ?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                                <div class="row">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                            <img src='<?php echo base_url() ?>uploads/receiptimage/<?php echo $result[0]['receipt']; ?>' width="250" height="250" > 
                                        </div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select Receipt Image</span>
                                                <input type="file" name="receipt">
                                            </span>   
                                        </div>
                                    </div>
                                </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="hidden" value="<?php echo $result[0]['id']; ?>" name="id">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>