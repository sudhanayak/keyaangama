       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Edit Sub Category
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>subcategory/updateSubcategory" method="POST" enctype="multipart/form-data" >
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?> 
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm"  name="cat_id">
                                                    <option value="<?php echo $result[0]['cat_id']; ?>"><?php echo $result[0]['category']; ?> </option> 
                                                    <option value="">select category </option>
                                                     <?php 
                                                        foreach($category as $val)
                                                        { 
                                                            echo '<option value="'.$val['id'].'">'.$val['category_name'].'</option>';
                                                        }
                                                     ?> 
                                                    </select>                                      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div> 
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="subcategory_name" class="form-control input-sm" placeholder="Sub Category" value="<?php echo $result[0]['subcategory_name']; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                            </div> 
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="hidden" name="icon" class="form-control input-sm" placeholder="Icon" value="1" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select class="form-control input-sm" name="price_selection" id="pricerange">
                                                        <option value="<?php echo $result[0]['price_selection']; ?>">
                                                    <?php if($result[0]['price_selection'] == 0){ echo 'Fixed'; } ?>
                                                    <?php if($result[0]['price_selection'] == 1){ echo 'Variable'; } ?>
                                                </option>
                                                        <option value="">Type of price</option>
                                                        <option value="0">Fixed</option>
                                                        <option value="1">Variable</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        <?php if($result[0]['price_selection'] == 1){ ?><div id="pricevariable"> <?php } else { ?><div id="pricevariable" style="display: none"> <?php } ?>  
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="price_from" class="form-control input-sm" placeholder="Enter price"value="<?php echo $result[0]['price_from']; ?>">
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="price_to" class="form-control input-sm" placeholder="Enter price" value="<?php echo $result[0]['price_to']; ?>">
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                             <?php if($result[0]['price_selection'] == 0){ ?><div class="row" id="price"> <?php } else { ?><div class="row" id="price" style="display: none"> <?php } ?>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="price" class="form-control input-sm" placeholder=" Enter price" value="<?php echo $result[0]['price']; ?>">
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="meta_tag" class="form-control input-sm" placeholder="Enter tag"><?php echo $result[0]['meta_tag']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="meta_keyword" class="form-control input-sm" placeholder="Enter Keyword"><?php echo $result[0]['meta_keyword']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="meta_detail" class="form-control input-sm" placeholder="Enter Description"><?php echo $result[0]['meta_detail']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                            <img src='<?php echo $result[0]['image']; ?>' width="250" height="250" >  
                                            
                                        </div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Change image</span>                                               
                                                <input type="file" name="image">
                                            </span>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                               <div class="col-sm-12">
                                   <div class="fileinput fileinput-new" data-provides="fileinput">
                                       <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                           <img src='<?php echo base_url() ?>uploads/subcatbanner/<?php echo $result[0]['subcat_banner']; ?>' width="250" height="250" >
                                       </div>
                                       <div>
                                           <span class="btn btn-info btn-file">
                                               <span class="fileinput-new">Select Banner image</span>
                                               <span class="fileinput-exists">Change</span>
                                               <input type="file" name="subcat_banner">
                                           </span>
                                       </div>
                                   </div>
                               </div>
                           </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="hidden"  name="id" value="<?php echo $result[0]['id']; ?>">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>