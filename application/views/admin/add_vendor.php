       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>

            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h2>Add vendor</h2>

                        <ul class="actions">
                            <li>
                                <a class="icon-pop" href="#">
                                    <i class="zmdi zmdi-trending-up"></i>
                                </a>
                            </li>
                            <li>
                                <a class="icon-pop" href="#">
                                    <i class="zmdi zmdi-check-all"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a class="icon-pop" href="#" data-toggle="dropdown">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="#">Refresh</a>
                                    </li>
                                    <li>
                                        <a href="#">Manage Widgets</a>
                                    </li>
                                    <li>
                                        <a href="#">Widgets Settings</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                    </div>

                    <div class="card">
                        

                <div class="card-body card-padding">
                <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <!--      Wizard container        -->
                    <div class="wizard-container">
                        <div class="card wizard-card" data-color="purple" id="wizard">
                            <form  method="POST" action="<?php echo base_url() ?>Vendor/addVendor" enctype="multipart/form-data">
                            <!--        You can switch " data-color="rose" "  with one of the next bright colors: "blue", "green", "orange", "purple"        -->
                                        <?php
                                           echo $this->session->flashdata('msg');
                                        ?> 
                                <div class="wizard-header">
                                    <h3 class="wizard-title">
                                        Vendor Registration
                                    </h3>
                                    <h5>This information will let us know more about your place.</h5>
                                </div>
                                <div class="wizard-navigation">
                                    <ul>
                                        <li><a href="#location" data-toggle="tab">Basic Details</a></li>
                                        <li><a href="#type" data-toggle="tab">Business Details</a></li>
                                        <li><a href="#facilities" data-toggle="tab">Social Media Details</a></li>
                                        <li><a href="#images" data-toggle="tab">Images and Videos</a></li>
                                        <li><a href="#bank" data-toggle="tab">Bank Details</a></li>
                                        <li><a href="#description" data-toggle="tab">Description</a></li>
                                    </ul>
                                </div>

                                <div class="tab-content">
                                    <div class="tab-pane" id="location">
                                        <div class="row">
                                            <div class="col-sm-12">
                                            <h4 class="info-text"> Let's start with the basic details</h4>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                 <div class="form-group">
                                                    <div class="fg-line">
                                                        <input type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                 <div class="form-group">
                                                    <div class="fg-line">
                                                        <input type="email" class="form-control" name="email" placeholder="Enter Your Email" required>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                 <div class="form-group">
                                                    <div class="fg-line">
                                                        <input type="number" class="form-control" name="mobile" placeholder="Enter Your Mobile Number" maxlength="10" minlength="10" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                 <div class="form-group">
                                                    <div class="fg-line">
                                                        <input type="number" class="form-control" name="alt_mobile" maxlength="10" minlength="10" placeholder="Enter Your Alternate Mobile Number">
                                                    </div>
                                                </div>
                                            </div>


 <div class="col-sm-5 col-sm-offset-1">
                                                 <div class="form-group">
                                                    <div class="fg-line">
                                                        <input type="text" required='required' class="form-control" name="username" placeholder="username">
                                                    </div>
                                                </div>
                                            </div>


 <div class="col-sm-5 col-sm-offset-1">
                                                 <div class="form-group">
                                                    <div class="fg-line">
                                                        <input type="password" required='required' class="form-control" name="password" placeholder="Enter password">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="col-sm-6 padd0">
                                                    <div class="form-group">
                                                       <div class="fg-line">
                                                           <div class="select">
                                                               <select class="form-control" name="proof_type">
                                                                   <option value="">Type of Proof</option>
                                                                   <option value="1">Pan Card</option>
                                                                   <option value="2">Aadhaar Number</option>
                                                                   <option value="3">Driving License</option>
                                                                   <option value="4">Voter Id</option>  
                                                               </select>
                                                           </div>
                                                       </div>
                                                   </div>
                                                </div>
                                                <div class="col-sm-6 paddr0">
                                                    <div class="form-group">
                                                        <div class="fg-line">
                                                            <input type="text" class="form-control" name="proof_number" placeholder="Proof Number">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <textarea class="form-control auto-size" name="address" placeholder="Enter Your Address"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                                    <div>
                                                        <span class="btn btn-info btn-file">
                                                            <span class="fileinput-new">Select Basic Image</span>
                                                            <input type="file" name="basic_image">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>                                                   
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="type">
                                        <h4 class="info-text">What type of location do you have? </h4>
                                        <div class="row">
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <input type="text" class="form-control" name="company_name" placeholder="Enter Your Company Name">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <input type="text" class="form-control" name="business_name" placeholder="Enter Your Business Name">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <input type="text" class="form-control" name="business_email" placeholder="Enter Your Business Email">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <div class="select">
                                                           
                                                            <select id="countryId" class="form-control" name="country_id">
                                                                <option value="">Select Country</option>
                                                                <?php
                                                                    
                                                                    $count = count(array_filter($resultCnt));
                                                                    if($count > 0) {
                                                                    $i=0;
                                                                    foreach($resultCnt as $key => $row){
                                                                    ?>
                                                                        <option value="<?php echo  $row['id'] ?>"><?php echo  $row['country_name'] ?></option>
                                                                    <?php
                                                                    }
                                                                    }
                                                                ?>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <div class="select">
                                                            <select id="stateId" class="form-control" name="state_id">
                                                                <option>Select State</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <div class="select">
                                                            <select id="districtId" class="form-control" name="district_id">
                                                                <option>Select District</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <div class="select">
                                                            <select id="cityId" class="form-control" name="city_id">
                                                                <option>Select City</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <div class="select">
                                                            <select id="pincodeId" class="form-control" name="pincode_id">
                                                                <option>Select Pincode</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <div class="select">
                                                            <select id="locationId" class="form-control" name="location_id">
                                                                <option>Select Location</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="col-sm-6 paddr0">
                                                    <div class="form-group">
                                                        <div class="fg-line">
                                                            <div class="select">
                                                                <select id="catId" class="form-control" name="cat_id">
                                                                    <option>Select Category</option>
                                                                    <?php
                                                                    
                                                                    $count = count(array_filter($resultCat));
                                                                    if($count > 0) {
                                                                    $i=0;
                                                                    foreach($resultCat as $key => $row){
                                                                    ?>
                                                                        <option value="<?php echo  $row['id'] ?>"><?php echo  $row['category_name'] ?></option>
                                                                    <?php
                                                                    }
                                                                    }
                                                                ?>
                                                                    

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 paddr0">
                                                    <div class="form-group">
                                                        <div class="fg-line">
                                                            <input type="text" class="form-control" name="category_base_price" placeholder="Enter Base Price">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="col-sm-6 paddr0">
                                                    <div class="form-group">
                                                        <div class="fg-line">
                                                            <div class="select">
                                                                <select id="subCatId" class="form-control" name="subcat_id">
                                                                    <option>Select Sub Category</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 paddr0">
                                                    <div class="form-group">
                                                        <div class="fg-line">
                                                            <input type="text" name="subcat_base_price" class="form-control"  placeholder="Enter Base Price">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="col-sm-6 paddr0">
                                                    <div class="form-group">
                                                        <div class="fg-line">
                                                            <div class="select">
                                                                <select id="subsubCatId" class="form-control" name="subsubcat_id">
                                                                    <option>Select Sub Sub Category</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 paddr0">
                                                    <div class="form-group">
                                                        <div class="fg-line">
                                                            <input type="text" class="form-control" name="sub_sub_cat_base_price" placeholder="Enter Base Price">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="col-sm-3 padd0">
                                                    <div class="form-group">
                                                        <div class="fg-line">
                                                            <input readonly="true" type="text" id="countryCode"  class="form-control" name="business_mobile_code" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-9 padd0">
                                                    <div class="form-group">
                                                        <div class="fg-line">
                                                            <input type="number" class="form-control" name="business_mobnumber" maxlength="10" minlength="10" placeholder="Enter Your Mobile Number">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="col-sm-3 padd0">
                                                    <div class="form-group">
                                                        <div class="fg-line">
                                                            <input readonly="true" id="cityCode" type="text" class="form-control" name="landline_code" placeholder="Code">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-9 padd0">
                                                    <div class="form-group">
                                                        <div class="fg-line">
                                                            <input type="number" class="form-control" name="landline_number" placeholder="Enter Your Landline Number">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <input type="text" class="form-control" name="business_pancard" placeholder="Enter Your Business Pancard">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <input type="text" class="form-control" name="company_reg_number" placeholder="Enter Your Company Registration Number">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <textarea class="form-control auto-size" name="business_address" placeholder="Enter your business address..."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <input type="text" class="form-control" name="website_link" placeholder="Enter Your Website Link">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="clearfix"></div>
                                            
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                                    <div>
                                                        <span class="btn btn-info btn-file">
                                                            <span class="fileinput-new">Select Logo</span>
                                                            
                                                            <input type="file" name="logo" required='true' title='provide logo'>
                                                        </span>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                                    <div>
                                                        <span class="btn btn-info btn-file">
                                                            <span class="fileinput-new">Upload proof</span>
                                                            <input type="file" name="proofImg">
                                                        </span>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                                </div>
                                    </div>
                                            
                                    <div class="tab-pane" id="facilities">
                                        <h4 class="info-text">Tell us more about facilities. </h4>
                                        <div class="row">
                                            <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control" name="fb_link" placeholder="Enter Your Facebook Link">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control" name="google_plus_link" placeholder="Enter Your Google Plus Link">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control" name="twitter_link" placeholder="Enter Your Twitter Link">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control" name="linkedln_link" placeholder="Enter Your linkedln Link">
                                                            </div>
                                                        </div>
                                                    </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="images">
                                        <h4 class="info-text">Please add you images and videos. </h4>
                                         <div class="row">
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="text-box">                                                            
                                                            <div class="row newImg">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                                                        <img id="falseinput0" height="150" width="150">
                                                                    </div>
                                                                    <div>
                                                                        <span class="btn btn-info btn-file">
                                                                            <span class="fileinput-new">Select image</span>
                                                                            <span class="fileinput-exists">Change</span>
                                                                            <input onchange="readURL(this,'falseinput0','baseInput0')" type="file" name="image[]">
                                                                        </span>                                                                        
                                                                        <a style="cursor:pointer" class="btn btn-danger fileinput-exists removeImg"
                                                                           data-dismiss="fileinput">Remove</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                         <a class="add-box btn btn-danger fileinput-exists" id="addmoresudha" style="cursor:pointer"> Add More + </a>
                                                    </div>
                                                    
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                                            <div>
                                                                <span class="btn btn-info btn-file">
                                                                    <span class="fileinput-new">Select Video</span>
                                                                    
                                                                    <input class='allVideo' type="file" name="video">
                                                                </span>
                                                                <a href="#" class="btn btn-danger fileinput-exists"
                                                                   data-dismiss="fileinput">Remove</a>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                    
                                                </div> 
                                            </div>
                                            <div class="tab-pane" id="bank">
                                               <div class="row">
                                            <h4 class="info-text">Bank Details</h4>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control" name="bank_name" placeholder="Enter Your Bank Name">
                                                            </div>
                                                        </div>
                                            </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control" name="bank_account_no" placeholder="Enter Your Bank Account Number">
                                                            </div>
                                                        </div>
                                            </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control" name="bank_branch" placeholder="Enter Your Branch">
                                                            </div>
                                                        </div>
                                            </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control" name="bank_ifsc" placeholder="Enter Your Bank IFSC Code">
                                                            </div>
                                                        </div>
                                            </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control" name="bank_account_name" placeholder="Enter Your Bank Account Name">
                                                            </div>
                                                        </div>
                                            </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                                            <div>
                                                                <span class="btn btn-info btn-file">
                                                                    <span class="fileinput-new">Upload Cancel Cheque</span>
                                                                    
                                                                    <input type="file" name="bank_upload_cancel_cheque">
                                                                </span>
                                                                <a href="#" class="btn btn-danger fileinput-exists"
                                                                   data-dismiss="fileinput">Remove</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                               </div>
                                            </div>
                                    <div class="tab-pane" id="description">
                                        <div class="row">
                                            <h4 class="info-text"> Drop us a small description. </h4>
                                            <div class="col-sm-6 col-sm-offset-1">
                                                <div class="form-group label-floating">
                                                    <label class="control-label"> Description</label>
                                                    <textarea class="form-control" name="vendor_detail" placeholder="vendor_detail" rows="9"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-sm-offset-1">
                                               <div class="form-group label-floating">
                                                   <label class="control-label">About Vendor</label>
                                                   <textarea class="form-control" name="about_vendor" rows="9"></textarea>
                                               </div>
                                           </div>
                                           <div class="col-sm-6 col-sm-offset-1">
                                               <div class="form-group label-floating">
                                                   <label class="control-label">Search Text</label>
                                                   <input type="text" class="form-control" name="search_text">
                                               </div>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wizard-footer">
                                    <div class="pull-right">
                                        <input type='button' class='btn btn-next btn-fill btn-primary btn-wd' name='next' value='Next' />
                                        <input type='submit' class='btn btn-finish btn-fill btn-primary btn-wd' name='finish' value='Finish' />
                                    </div>
                                    <div class="pull-left">
                                        <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div> <!-- wizard container -->
                </div>
                <div class="col-sm-1"></div>
            </div> <!-- row -->
                        </div>
                    </div>
                </div>
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>
<link href="<?php echo base_url(); ?>assets1/css/material-bootstrap-wizard.css" rel="stylesheet" />