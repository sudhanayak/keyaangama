<footer id="footer">
            Copyright &copy; 2018 Keyaan
            
            <ul class="f-menu">
                <li><a href="#">Home</a></li>
                <li><a href="#">Dashboard</a></li>
                <li><a href="#">Reports</a></li>
                <li><a href="#">Support</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </footer>
        <!-- Page Loader -->
        <div class="page-loader">
            <div class="preloader pls-blue">
                <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20" />
                </svg>

                <p>Please wait...</p>
            </div>
        </div>
        <!-- Javascript Libraries -->
        <script src="<?php echo base_url(); ?>assets1/vendors/bower_components/jquery/dist/jquery.min.js"></script>        
        <script src="<?php echo base_url(); ?>assets1/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="<?php echo base_url(); ?>assets1/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="<?php echo base_url(); ?>assets1/js/jquery-2.2.4.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets1/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets1/js/jquery.bootstrap.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets1/js/allscript.js" type="text/javascript"></script>

      <script src="<?php echo base_url(); ?>assets1/vendors/bower_components/moment/min/moment.min.js"></script>
     <script src="<?php echo base_url(); ?>assets1/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>           
    <script src="<?php echo base_url(); ?>assets1/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>          
		<script src="<?php echo base_url(); ?>assets1/js/material-bootstrap-wizard.js"></script>
		 <script src="<?php echo base_url(); ?>assets1/vendors/fileinput/fileinput.min.js"></script>
		<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
		<script src="<?php echo base_url(); ?>assets1/js/jquery.validate.min.js"></script>
        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->
           <script src="<?php echo base_url(); ?>assets1/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
        <script src="<?php echo base_url(); ?>assets1/vendors/bower_components/lightgallery/lib/lightgallery-all.min.js"></script>

        <script src="<?php echo base_url(); ?>assets1/js/app.min.js"></script>
        <script src="<?php echo base_url(); ?>assets1/vendors/summernote/dist/summernote-updated.min.js"></script>
        
    </body>
</html>