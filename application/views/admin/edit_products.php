       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Edit Products
                            </h2>
                        </div>
                        <form action="<?php echo base_url() ?>Products/updateProducts" method="POST" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?> 
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <input type="text" name="product_name" class="form-control input-sm" placeholder="Enter product name" value="<?php echo $result['product_name']; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="product_description" class="form-control input-sm" placeholder="Enter Product Details"><?php echo $result['product_description']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm"  name="master_category_id" id="masterCategory">
                                                    <option value="<?php echo $result['master_category_id']; ?>"><?php echo $result['mastercategory']; ?> </option> 
                                                    <option value="">Select Master Category</option>
                                                    <?php                    
                                                        $count = count(array_filter($resultCat));
                                                        if($count > 0) {
                                                        $i=0;
                                                        foreach($resultCat as $key => $row){
                                                        ?>
                                                            <option value="<?php echo  $row['id'] ?>"><?php echo  $row['master_category_name'] ?></option>
                                                        <?php
                                                        }
                                                        }
                                                    ?>
                                                  </select>                               
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div> 
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm"  name="category_id" id="cateringcatId">
                                                    <option value="<?php echo $result['category_id']; ?>"><?php echo $result['category']; ?> </option>
                                                     <option value="">Select  Category</option>
                                                    </select>                      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div> 
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select  required class="form-control input-sm"  name="sub_category_id" id="cateringsubCatId">
                                                    <option value="<?php echo $result['sub_category_id']; ?>"><?php echo $result['subcategory']; ?> </option>
                                                     <option value="">Select Subcategory</option>
                                                    </select>                      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select id="Producttype" class="form-control input-sm" name="product_type">
                                                   <option value="<?php echo $result['product_type']; ?>">
                                                     <?php if($result['product_type'] == 1){ echo 'Veg'; } ?>
                                                     <?php if($result['product_type'] == 2){ echo 'Non Veg'; } ?>
                                                    </option>
                                                    <option value="">select Product Type</option>
                                                    <option value="1">Veg</option>
                                                    <option value="2">Non Veg</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"><img src="<?php echo  base_url()."uploads/product_image/".$result['product_image'] ?>" width="150 px" height="150 px"></div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select image</span>                                           
                                                <input type="file" name="product_image">
                                            </span>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="hidden"  name="id" value="<?php echo $result['id']; ?>">
                                    <button  type='submit' name='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>