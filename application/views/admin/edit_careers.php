       <?php
       include_once'header.php';
       ?>
        <section id="main">
            <aside id="sidebar" class="sidebar c-overflow">
                <?php include_once 'sidebar.php';?>
            </aside>
            <section id="content">
                <div class="container">                   
                    <div class="card card-min">
                        <div class="card-header">
                            <h2>
                                Edit Careers
                            </h2>
                        </div>
                        <form method="POST"  action="<?php echo base_url() ?>Careers/updateCareers" enctype="multipart/form-data">
                        <div class="card-body card-padding">
                            <?php
                                echo $this->session->flashdata('msg');
                            ?>    
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" name="job_title" class="form-control input-sm" placeholder="Job Title" value="<?php echo $result[0]['job_title']; ?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="job_detail" class="form-control input-sm" placeholder="Enter Job Detail"><?php echo $result[0]['job_detail']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                
                                                <select id="countryId" class="form-control" name="country_id">
                                                    <option value="<?php echo $result[0]['country_id']; ?>"><?php echo $result[0]['country']; ?> </option>
                                                    <option value="">Select Country</option>
                                                    <?php
                                                        $count = count(array_filter($resultCnt));
                                                        if($count > 0) {
                                                        $i=0;
                                                        foreach($resultCnt as $key => $row){
                                                        ?>
                                                            <option value="<?php echo  $row['id'] ?>"><?php echo  $row['country_name'] ?></option>
                                                        <?php
                                                        }
                                                        }
                                                    ?>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select id="stateId" class="form-control" name="state_id">
                                                    <option value="<?php echo $result[0]['state_id']; ?>"><?php echo $result[0]['state']; ?> </option>
                                                    <option>Select State</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select id="districtId" class="form-control" name="district_id">
                                                    <option value="<?php echo $result[0]['district_id']; ?>"><?php echo $result[0]['district']; ?> </option>
                                                    <option>Select District</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select id="cityId" class="form-control" name="city_id">
                                                    <option value="<?php echo $result[0]['city_id']; ?>"><?php echo $result[0]['city']; ?> </option>
                                                    <option>Select City</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select id="pincodeId" class="form-control" name="pincode_id">
                                                    <option value="<?php echo $result[0]['pincode_id']; ?>"><?php echo $result[0]['pincode']; ?> </option>
                                                    <option>Select Pincode</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select id="locationId" class="form-control" name="location_id">
                                                    <option value="<?php echo $result[0]['location_id']; ?>"><?php echo $result[0]['location']; ?> </option>
                                                    <option>Select Location</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        <input type="text" name="package" class="form-control input-sm" placeholder="Package" required value="<?php echo $result[0]['package']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea rows="5" name="skills_required" class="form-control input-sm" placeholder="Enter Required Skills"><?php echo $result[0]['skills_required']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <h6>Job Type</h6>
                                            <label class="radio radio-inline m-r-20"><input <?php if($result[0]['job_type'] == 1) { echo "checked='checked'"; } ?> type="radio" name="job_type" value="1"><i class="input-helper"></i>Contract Based</label>
                                            <label class="radio radio-inline m-r-20"><input <?php if($result[0]['job_type'] == 2) {echo "checked='checked'";} ?>type="radio" name="job_type" value="2"><i class="input-helper"></i>Permanent</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        <input type="text" name="no_of_positions" class="form-control input-sm" placeholder="No of Positions" value="<?php echo $result[0]['no_of_positions']; ?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <Select class="form-control" name="exprience_years">
                                                    <option value="<?php echo $result[0]['exprience_years']; ?>"><?php if($result[0]['exprience_years'] == 1) { echo "1 Year" ;} elseif($result[0]['exprience_years'] == 2) { echo "2 Years" ;} elseif($result[0]['exprience_years'] == 3) { echo "3 Years" ;} elseif($result[0]['exprience_years'] == 4) { echo "4 Years" ;} elseif($result[0]['exprience_years'] == 5) { echo "5 Years" ;} elseif($result[0]['exprience_years'] == 6) { echo "6 Years" ;} elseif($result[0]['exprience_years'] == 7) { echo "7 Years" ;} elseif($result[0]['exprience_years'] == 8) { echo "8 Years" ;} elseif($result[0]['exprience_years'] == 9) { echo "9 Years" ;} elseif($result[0]['exprience_years'] == 10) { echo "10 Years" ;}  ?> </option>
                                                    <option>Select Experience in Years</option>
                                                    <option value="1">1 Year</option>
                                                    <option value="2">2 Years</option>
                                                    <option value="3">3 Years</option>
                                                    <option value="4">4 Years</option>
                                                    <option value="5">5 Years</option>
                                                    <option value="6">6 Years</option>
                                                    <option value="7">7 Years</option>
                                                    <option value="8">8 Years</option>
                                                    <option value="9">9 Years</option>
                                                    <option value="10">10 Years</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <Select class="form-control" name="exprience_months">
                                                    <option value="<?php echo $result[0]['exprience_months']; ?>"><?php if($result[0]['exprience_months'] == 1) { echo "1 Month" ;} elseif($result[0]['exprience_months'] == 2) { echo "2 Months" ;} elseif($result[0]['exprience_months'] == 3) { echo "3 Months" ;} elseif($result[0]['exprience_months'] == 4) { echo "4 Months" ;} elseif($result[0]['exprience_months'] == 5) { echo "5 Months" ;} elseif($result[0]['exprience_months'] == 6) { echo "6 Months" ;} elseif($result[0]['exprience_months'] == 7) { echo "7 Months" ;} elseif($result[0]['exprience_months'] == 8) { echo "8 Months" ;} elseif($result[0]['exprience_months'] == 9) { echo "9 Months" ;} elseif($result[0]['exprience_months'] == 10) { echo "10 Months" ;}  ?> </option>
                                                    <option>Select Experience in Months</option>
                                                    <option value="1">1 Month</option>
                                                    <option value="2">2 Months</option>
                                                    <option value="3">3 Months</option>
                                                    <option value="4">4 Months</option>
                                                    <option value="5">5 Months</option>
                                                    <option value="6">6 Months</option>
                                                    <option value="7">7 Months</option>
                                                    <option value="8">8 Months</option>
                                                    <option value="9">9 Months</option>
                                                    <option value="10">10 Months</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                        <input type="text" name="notice_period" class="form-control input-sm" placeholder="Notice Period" required value="<?php echo $result[0]['notice_period']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="hidden" name="id" class="form-control input-sm" placeholder="Notice Period" required value="<?php echo $result[0]['id']; ?>">
                                    <button  type='submit' class='btn btn-next btn-fill btn-primary btn-wd'>Submit</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>                
            </section>
        </section>        
        <?php
            include_once'footer.php';
        ?>