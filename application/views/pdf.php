<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>   
</head>
<body>
    <div class="id-card-tag"></div>
	<div class="id-card-tag-strip"></div>
	<div class="id-card-hook"></div>
	<div class="id-card-holder" 
             style="width: 225px;padding: 4px;margin: 0 auto;background-color: #51a4da;border-radius: 5px;position: relative;">
		<div class="id-card" style="background-color: #fff;padding: 10px;border-radius: 10px;text-align: center;
box-shadow: 0 0 2.5px 0px #b9b9b9;">
			<div class="header">
                                
				<img style="width: 100px;margin-top: 15px;" src="<?php echo 'file:///'.FCPATH ?>/assets/images/logoidcard.png">
			</div>
			<div class="photo">
				<img style="width: 80px;margin-top: 15px;" src="<?php echo 'file:///'.FCPATH ?>/uploads/vendorlogo/<?php echo $image ?>">
			</div>
			<h2 style="font-size: 15px;margin: 5px 0;"><?php echo $name ."(".$vendorCode.")" ?></h2>			
			<h3 style="font-size: 12px;margin: 2.5px 0;font-weight: 300;border-bottom:1px solid #d3d3d3">www.keyaan.co</h3>
			
			<p style="font-size:8px;margin: 2px;"> Flat No:403, 4th Floor, V.R. Sunshine Building,<p>
			<p style="font-size:8px;margin: 2px;"> Patrika Nagar Street NO:3, Madhapur,<strong>500081</strong></p>
			<p style="font-size:8px;margin: 2px;"> Ph: 040-48504774 | E-ail: info@keyaan.co</p>
		</div>
	</div>
</body>
</html>