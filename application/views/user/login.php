<div class="main-register-wrap modal">
                <div class="reg-overlay"></div>
                <div class="main-register-holder">
                    <div class="main-register fl-wrap">
                        <div class="close-reg color-bg"><i class="fal fa-times"></i></div>
                        <ul class="tabs-menu">
                            <li class="current"><a href="#tab-1"><i class="fal fa-sign-in-alt"></i> Login</a></li>
                            <li><a href="#tab-2"><i class="fal fa-user-plus"></i> Register</a></li>
                        </ul>
                        <!--tabs -->                       
                        <div id="tabs-container">
                            <div class="tab">
                                <!--tab -->
                                <div id="tab-1" class="tab-content">
                                    <h3>Sign In <span>Key<strong>aan</strong></span></h3>
                                    <div class="custom-form">
                                        <form method="post"  name="registerform">
                                            <span class="errorList"></span>
                                            <label> Email Address <span>*</span> </label>
                                            <input name="username" class="username" type="email"  required='true' value="">
                                            <label >Password <span>*</span> </label>
                                            <input name="password" class="password" type="password" required='true'  value="" >
                                            <button type="button"  class="log-submit-btn color-bg userLogin"><span>Log In </span></button>
                                            <div class="clearfix"></div>
                                            
                                        </form>
                                        <div class="lost_password">
                                            <a href="#" class='modal-open3'>Lost Your Password?</a>
                                        </div>
                                    </div>
                                </div>
                                <!--tab end -->
                                <!--tab -->
                                <div class="tab">
                                    <div id="tab-2" class="tab-content">
                                        <h3>Sign Up <span>Key<strong>aan</strong></span></h3>
                                        <form method="post"   name="registerform" class="main-register-form" id="main-register-form2">
                                            <div class="custom-form" id="regControl">
                                                    <span class="errorList2"></span>             
                                                    <label >Full Name <span>*</span> </label>
                                                    <input name="rname" class='rname'  type="text" value="">
                                                    <label >Mobile Number<span>*</span> </label>
                                                    <input name="mobile" maxlength="10" class='rmobile mobNumber' type="text"   value="">
                                                    <label>Email Address <span>*</span></label>
                                                    <input name="email" class='remail' type="text"   value="">
                                                    <label >Password <span>*</span></label>
                                                    <input name="password"  class='rpassword' type="password"    value="" >
                                                    <button type="button" class="log-submit-btn color-bg regBtn"  ><span>Continue</span></button>
                                               
                                            </div>
                                            <div class="custom-form" id="otpControl" style='display:none'>
                                                    <span class="errorList3"></span>
                                                    <label >Code<span>*</span> </label>
                                                    <input name="code" class='code' type="text"   value="">                                       
                                                    <button type="button"     class="log-submit-btn color-bg otpBtn"  ><span>Verify</span></button>
                                                
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!--tab end -->
                            </div>
                            <!--tabs end -->
                            <div class="log-separator fl-wrap"><span>or</span></div>
                            <div class="soc-log fl-wrap">
                                <p>For faster login or register use your social account.</p>
                                <a href="#" class="facebook-log"><i class="fab fa-facebook-f"></i>Connect with Facebook</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>









            <div class="main-register-wrap modal3">
                <div class="reg-overlay3"></div>
                <div class="main-register-holder">
                    <div class="main-register fl-wrap">
                        <div class="close-reg close-reg3 color-bg"><i class="fal fa-times"></i></div>
                        <ul class="tabs-menu tabs-menu2">
                            <li class="current"><a href="#tab-10"><i class="fal fa-sign-in-alt"></i> Forget Password</a></li>
                            
                        </ul>
                        <!--tabs -->                       
                        <div class="col-md-12">
                            <div class='mg-top301'>
                                        <h3>Forget Password <span>Key<strong>aan</strong></span></h3>
                                        <form method="post"   name="registerform" class="main-register-form" id="main-register-form2">

                                            <div class="custom-form" id="forget_regControl">
                                                   <span class="errorList4"></span>
                                                    <label >Mobile Number<span>*</span> </label>
                                                    <input name="fmobile" maxlength="10" class='fmobile mobNumber' type="text"   value="">
                                                    
                                                    <button type="button" class="log-submit-btn color-bg forget_regBtn"  ><span>Continue</span></button>
                                               
                                            </div>
                                            <div class="custom-form" id="forget_otpControl" style='display:none'>
                                                    <span class="errorList4"></span>
                                                    <label >Code<span>*</span> </label>
                                                    <input name="code" class='fcode' type="text"   value="">                                       
                                                    <button type="button"     class="log-submit-btn color-bg forget_otpBtn"  ><span>Verify</span></button>
                                                
                                            </div>
                                        </form>
                                   </div></div>
                            
                            
                        </div>
                    </div>
                </div>

                <!-- For Vendor Registration -->
                <div class="main-register-wrap modal5">
                    <div class="reg-overlay5"></div>
                    <div class="main-register-holder">
                        <div class="main-register fl-wrap">
                            <div class="close-reg close-reg3 close-reg4 close-reg5 color-bg"><i class="fal fa-times"></i></div>
                            <ul class="tabs-menu tabs-menu2">
                                <li class="current"><a href="#tab-10"><i class="fal fa-sign-in-alt"></i> Vendor Registartaion</a></li>
                            </ul>
                            <!--tabs -->                       
                            <div class="col-md-12">
                                <div class='mg-top301'>
                                    <h3>Vendor Registartaion <span>Key<strong>aan</strong></span></h3>
                                    <form method="post"   name="registerform" class="main-register-form" id="main-register-form2">
                                        <div class="custom-form" id="vendor_regControl">
                                            <span class="errorList5"></span>             
                                            <label >Full Name <span>*</span> </label>
                                            <input name="name" class='vname'  type="text" value="">
                                            <label >Mobile Number<span>*</span> </label>
                                            <input name="mobile" maxlength="10" class='vmobile mobNumber' type="text"   value="">
                                            <label>Email Address <span>*</span></label>
                                            <input name="email" class='vemail' type="text"   value="">
                                            <button type="button" class="log-submit-btn color-bg vendor_regBtn"><span>Continue</span></button>
                                        </div>
                                        <div class="custom-form" id="vendor_otpControl" style='display:none'>
                                            <span class="errorList6"></span>
                                            <label >Code<span>*</span> </label>
                                            <input name="code" class='vcode' type="text" value=""> 
                                            <button type="button" class="log-submit-btn color-bg vendor_otpBtn"><span>Verify</span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
     