<?php
include_once'header1.php';
?>
            <!--  header end -->
            <!--  wrapper  -->
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    <!--section -->
                    <section class="hero-section" data-scrollax-parent="true" id="sec1">
                        <div class="hero-parallax">
                        <div class="slideshow-container" data-scrollax="properties: { translateY: '200px' }" >
                            <!-- slideshow-item --> 
                            <?php               
                            $count2 = count(array_filter($bannerList));         
                            if($count2 > 0) {
                                $i=0;
                                $c=0;
                                foreach($bannerList as $key => $row2){  
                                    ?>
                                        <div class="slideshow-item">
                                            <div class="bg"  data-bg="<?php echo base_url(); ?>uploads/banners/<?php echo $row2['image'] ?>"></div>
                                        </div>
                                        <!--  slideshow-item end  -->   
                                    <?php
                                }
                            }

                            ?>
                                                    
                        </div>
                            <div class="overlay op7"></div>
                        </div>
                        <?php include_once 'banner_search_icons.php';?>
                        <div class="header-sec-link">
                            <div class="container"><a href="#sec2" class="custom-scroll-link color-bg"><i class="fal fa-angle-double-down"></i></a></div>
                        </div>
                    </section>
                    <!-- section end -->
                     <!--section -->
                   
                
                </div>
                <!-- content end-->
            </div>
            <!--wrapper end -->
            <!--footer -->
            <?php
            include_once'footer.php';
            ?>