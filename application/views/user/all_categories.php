<?php include_once 'header1.php';?>
            <!--  wrapper  -->
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    <div class="breadcrumbs-fs fl-wrap">
                        <div class="container">
                            <div class="breadcrumbs fl-wrap"><a href="<?php echo base_url(); ?>">Home</a></div>
                        </div>
                    </div>
                    <!--  section-->
                    <section class="grey-blue-bg small-padding" id="sec1">
                        <div class="big-container">
                            <div class="row">
                                <div class="col-md-12">
                                <?php                                                                    
                                    $count_cat = count(array_filter($menuCategoryAll));
                                    if($count_cat > 0) {
                                    foreach($menuCategoryAll as $key => $row_catList){   
                                        $urlCode = base_url().'listing/'.$row_catList['id']                                    
                                ?>
                                    <div class="col-md-3">
                                    <a href="<?php echo $urlCode; ?>"><h1 class="pull-left" style="font-size: 15px; font-weight:600;"><?php echo $row_catList['category_name']; ?></h1></a>
                                        <div class="listing-features fl-wrap">
                                                <ul>
                                                <?php         
                                                $count_subcat = count(array_filter($row_catList['subcatList']));
                                                if($count_subcat > 0) {
                                                foreach($row_catList['subcatList'] as $key => $row_subcatList){ 
                                                    $urlCode1 = base_url().'Subcatlisting/'.$row_subcatList['id']   
                                                ?>
                                                    <a href="<?php echo $urlCode; ?>"><li class="wdth100" style="padding-left: 15px; margin-bottom:0px;"><a href="<?php echo $urlCode1; ?>" style="font-weight: normal;"><?php echo $row_subcatList['subcategory_name'] ?></a></li>
                                                <?php } } ?>
                                                </ul>
                                            </div>
                                    </div>
                                    <?php } } ?>
                                </div>
                            </div>
                            <!--row end-->
                        </div>
                        <div class="limit-box fl-wrap"></div>
                    </section>
                </div>
                <!-- content end-->
            </div>
            <!--wrapper end -->
            <?php include_once 'footer.php';?>