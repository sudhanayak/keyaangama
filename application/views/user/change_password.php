<?php
include_once'header1.php';
?>
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    
                     <section class="flat-header color-bg adm-header">
                        <div class="wave-bg wave-bg2"></div>
                        <div class="container">
                            <?php include_once 'user_dashboard.php';?>
                        </div>
                    </section>
                    <!-- section end-->
                    <!-- section-->
                    <section class="middle-padding">
                        <div class="container">
                            <!--dasboard-wrap-->
                            <div class="dasboard-wrap fl-wrap">
                                <!-- dashboard-content--> 
                                <div class="dashboard-content fl-wrap">
                                    <div class="box-widget-item-header">
                                        <h3> Change Password</h3>
                                    </div>
                                    <span class="errorList" style="color:green;">
                                    <span class="errorList2"  style="color:red;"></span>
                                    <div class="custom-form no-icons">
                                        <div class="pass-input-wrap fl-wrap">
                                            <label>Current Password</label>
                                            <input type="password" class="pass-input old_password" placeholder="" required>
                                            <span class="eye"><i class="far fa-eye" aria-hidden="true"></i> </span>
                                        </div>
                                        <div class="pass-input-wrap fl-wrap">
                                            <label>New Password</label>
                                            <input type="password" class="pass-input new_password" id="user_password" required>
                                            <span class="eye"><i class="far fa-eye" aria-hidden="true"></i> </span>
                                        </div>
                                        <div class="pass-input-wrap fl-wrap">
                                            <label>Confirm New Password</label>
                                            <input type="password" class="pass-input" id="confirm_password" onChange="checkPasswordMatch();" required>
                                            <div id="divCheckPasswordMatch" style="color:red"></div>
                                            <div id="pass-info" class="clearfix"></div>
                                            <span class="eye"><i class="far fa-eye" aria-hidden="true"></i> </span>
                                        </div>
                                        <input type="hidden" class="form-control id" value="<?php echo $result[0]['id']; ?>">
                                        <button class="btn  big-btn  color2-bg flat-btn float-btn changePasswordBtn">Change Password<i class="fal fa-save"></i></button>
                                    </div>
                                </div>
                                <!-- dashboard-list-box end--> 
                            </div>
                            <!-- dasboard-wrap end-->
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                    <!-- section end -->
                </div>
                <!-- content end-->
            </div>
<?php
include_once'footer.php';
?>

<script type="text/javascript">
        
    function checkPasswordMatch() {
        var password = $("#user_password").val();
        var confirmPassword = $("#confirm_password").val();
        if (confirmPassword != password) {
            $("#divCheckPasswordMatch").html("Passwords do not match!");
            $("#confirm_password").val("");
        } else {
            $("#divCheckPasswordMatch").html("");
        }
    }
</script>