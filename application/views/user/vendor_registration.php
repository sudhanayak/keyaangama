<?php
include_once'header1.php';
?>
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    
                    <section class="middle-padding gre y-blue-bg">
                        <div class="container">
                            <div class="list-main-wrap-title single-main-wrap-title fl-wrap">
                                <h2>Vendor Registration</h2>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="bookiing-form-wrap">
                                        <ul id="progressbar">
                                            <li class="active" style="width: 14%"><span>01.</span>Basic Details</li>
                                            <li style="width: 14%"><span>02.</span>Business Details</li>
                                            <li style="width: 14%"><span>03.</span>Social Media Details</li>
                                            <li style="width: 14%"><span>04.</span>Image and Videos</li>
                                            <li style="width: 14%"><span>05.</span>Bank Details</li>
                                            <li style="width: 14%"><span>06.</span>Description</li>
                                            <li style="width: 14%"><span>07.</span>Confirmation</li>
                                        </ul>
                                        <!--   list-single-main-item -->
                                        <div class="list-single-main-item fl-wrap">
                                            <div class="profile-edit-container">
                                                <div class="custom-form">
                                                    <form  method="POST" action="<?php echo base_url() ?>addVendors" enctype="multipart/form-data">
                                                        <?php
                                                            echo $this->session->flashdata('msg');
                                                        ?> 
                                                        <fieldset class="fl-wrap">

                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Your personal Information</h3>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label>Name<i class="far fa-user"></i></label>
                                                                    <input readonly type="text" name="name" placeholder="Your Name" value="<?php echo $vendorName; ?>"/>                     
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Email<i class="far fa-envelope"></i></label>
                                                                    <input readonly type="email" name="email" placeholder="Your Email" value="<?php echo $vendorEmail; ?>"/> 
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label>Mobile Number<i class="far fa-mobile"></i>  </label>
                                                                    <input readonly type="text" name="mobile" placeholder="Enter your mobile number" value="<?php echo $vendorMobile; ?>"/>                                                  
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Alternate Mobile Number<i class="far fa-phone"></i>  </label>
                                                                    <input type="text" name="alt_mobile" placeholder="Enter Alternate Mobile Number" value=""/>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label>Username<i class="far fa-users"></i>  </label>
                                                                    <input type="text" name="username" placeholder="Enter your Username" value=""/>                                                  
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Password<i class="far fa-key"></i>  </label>
                                                                    <input type="password" name="password" placeholder="Enter Password" value=""/>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="col-md-7">
                                                                        <label>Proof<i class="far fa-users"></i>  </label>
                                                                        <div class="listsearch-input-item ">
                                                                            <select data-placeholder="Your Country" class="chosen-select" name="proof_type" >
                                                                            <option value="1">Pan Card</option>
                                                                            <option value="2">Aadhaar Number</option>

                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                        <label>Proof Number<i class="fal fa-key"></i></label>
                                                                        <input type="text" name="proof_number" placeholder="Proof Number" value=""/>   
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="col-md-6">
                                                                        <label>Basic Image</label>
                                                                    <div class="fu-text">
                                                                        <img src=''  width='150' class='basicimg'>
                                                                        <span class='img_uploadtext' id="basicimg" style='display:block'><i class="fal fa-image"></i> Click here or drop files to upload</span>
                                                                         <input type="file" name="basic_image" class="upload input2 imgInp" imgClass='basicimg'>
                                                                    </div>
                                                                </div>
                                                                    <div class="col-md-6">
                                                                        <label>Logo</label>
                                                                    <div class="fu-text">
                                                                        <img src=''  width='150' class='vendorlogo'>
                                                                        <span class='img_uploadtext' id="vendorlogo" style='display:block'><i class="fal fa-image"></i> Click here or drop files to upload</span>
                                                                         <input type="file" name="logo" class="upload input2 imgInp" imgClass='vendorlogo'>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </div>
                                                            <div class="row">
                                                                
                                                                <div class="col-sm-12">
                                                                    <label>Address</label>
                                                                    <textarea cols="20" rows="3" name="address" placeholder="Enter your address"></textarea>
                                                                </div>
                                                            </div>
                                                            
                                                            <span class="fw-separator"></span>
                                                            <a  href="#"  class="next-form  next-form1   action-button btn no-shdow-btn color-bg">Next<i class="fal fa-angle-right"></i></a>
                                                        </fieldset>
                                                        <fieldset class="fl-wrap">
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Business Details</h3>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label>Company Name <i class="fal fa-globe-asia"></i></label>
                                                                    <input type="text" name="company_name" placeholder="Enter Company Name" value=""/>                                                  
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Business Name <i class="fal fa-globe-asia"></i></label>
                                                                    <input type="text" name="business_name" placeholder="Enter Business Name" value=""/>                                                  
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label>Business Email <i class="fal fa-envelope"></i> </label>
                                                                    <input type="text" name="business_email" placeholder="Enter Business Email" value=""/>                                                  
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Country<i class="fal fa-place"></i> </label>
                                                                    <div class="listsearch-input-item ">
                                                                        <select id="countryId" data-placeholder="Your Country" class="chosen-select" name="country_id" >
                                                                            <option value="">Select Country</option>
                                                                            <?php
                                                                                
                                                                                $count = count(array_filter($resultCnt));
                                                                                if($count > 0) {
                                                                                $i=0;
                                                                                foreach($resultCnt as $key => $row){
                                                                                ?>
                                                                                    <option value="<?php echo  $row['id'] ?>"><?php echo  $row['country_name'] ?></option>
                                                                                <?php
                                                                                }
                                                                                }
                                                                            ?>


                                                                        </select>
                                                                    </div>                                                  
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label>State<i class="fal fa-place"></i> </label>
                                                                    <div class="listsearch-input-item ">
                                                                        <select id="stateId" data-placeholder="Your Country" class="chosen-select" name="state_id" >
                                                                            <option>Select State</option>
                                                                        </select>
                                                                    </div>                                                  
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>District<i class="fal fa-place"></i> </label>
                                                                    <div class="listsearch-input-item ">
                                                                        <select id="districtId" data-placeholder="Your Country" class="chosen-select" name="district_id">
                                                                            <option>Select District</option>
                                                                        </select>
                                                                    </div>                                                  
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label>City<i class="fal fa-place"></i> </label>
                                                                    <div class="listsearch-input-item ">
                                                                        <select id="cityId" data-placeholder="Your Country" class="chosen-select" name="city_id">
                                                                            <option>Select City</option>
                                                                        </select>
                                                                    </div>                                                  
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Pincode<i class="fal fa-place"></i> </label>
                                                                    <div class="listsearch-input-item ">
                                                                        <select id="pincodeId" data-placeholder="Your Country" class="chosen-select   " name="pincode_id">
                                                                            <option>Select Pincode</option>
                                                                        </select>
                                                                    </div>                                                  
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label>Location<i class="fal fa-place"></i> </label>
                                                                    <div class="listsearch-input-item ">
                                                                        <select id="locationId" data-placeholder="Your Country" class="chosen-select   " name="location_id">
                                                                             <option>Select Location</option>
                                                                        </select>
                                                                    </div>                                                  
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="col-md-7">
                                                                        <label>Category<i class="fal fa-place"></i> </label>
                                                                        <div class="listsearch-input-item ">
                                                                            <select id="catId" data-placeholder="Your Country" class="chosen-select" name="cat_id">
                                                                                <option>Select Category</option>
                                                                                <?php
                                                                                
                                                                                $count = count(array_filter($resultCat));
                                                                                if($count > 0) {
                                                                                $i=0;
                                                                                foreach($resultCat as $key => $row){
                                                                                ?>
                                                                                    <option value="<?php echo  $row['id'] ?>"><?php echo  $row['category_name'] ?></option>
                                                                                <?php
                                                                                }
                                                                                }
                                                                            ?>

                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                       <label>Base Price<i class="fal fa-key"></i></label>
                                                                        <input type="text" name="category_base_price" placeholder="Enter amount" value=""/>   
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="col-md-7">
                                                                        <label>Sub Category<i class="fal fa-place"></i> </label>
                                                                        <div class="listsearch-input-item ">
                                                                            <select id="subCatId" data-placeholder="Your Country" class="chosen-select" name="subcat_id" >
                                                                                <option>Select Sub Category</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                       <label>Base Price<i class="fal fa-key"></i></label>
                                                                        <input type="text" name="subcat_base_price" placeholder="Enter amount" value=""/>   
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="col-md-7">
                                                                        <label>Sub Sub Category<i class="fal fa-place"></i> </label>
                                                                        <div class="listsearch-input-item ">
                                                                            <select id="subsubCatId" data-placeholder="Your Country" class="chosen-select   " name="subsubcat_id">
                                                                                <option>Select Sub Sub Category</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                       <label>Base Price<i class="fal fa-key"></i></label>
                                                                        <input type="text" name="sub_sub_cat_base_price" placeholder="Enter amount" value=""/>   
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                        <label>Code<i class="far fa-envelope"></i></label>
                                                                        <input type="text" name="business_mobile_code" placeholder="Enter Mobile code" value=""/> 
                                                                    </div>
                                                                <div class="col-sm-6">
                                                                    <label>Mobile Number<i class="far fa-user"></i></label>
                                                                    <input type="text" name="business_mobnumber" placeholder="Enter Mobile Number" value=""/>             
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="col-sm-3">
                                                                        <label>Code<i class="far fa-envelope"></i></label>
                                                                        <input type="text" name="landline_code" placeholder="Enter Code" value=""/> 
                                                                    </div>
                                                                    <div class="col-sm-9">
                                                                        <label>Landline<i class="far fa-envelope"></i></label>
                                                                        <input type="text" name="landline_number" placeholder="Enter Landline Number" value=""/> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label>Business Pancard<i class="far fa-user"></i></label>
                                                                    <input type="text" name="business_pancard" placeholder="Enter Business Pancard" value=""/>                                                  
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Company Registration Number<i class="far fa-user"></i></label>
                                                                    <input type="text" name="company_reg_number" placeholder="Enter Company Registration Number" value=""/> 
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label>Website Link<i class="far fa-user"></i></label>
                                                                    <input type="text" name="website_link" placeholder="Enter Website Link" value=""/>              
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="fu-text">
                                                                    <label>Proof Image</label>
                                                                        <img src=''  width='150' class='Proofimg'>
                                                                        <span class='img_uploadtext' id="Proofimg" style='display:block'><i class="fal fa-image"></i> Click here or drop files to upload</span>
                                                                         <input type="file" name="proofImg" class="upload input2 imgInp" imgClass='Proofimg'>
                                                                    </div>
                                                                
                                                            </div>
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Business Address</h3>
                                                            </div>
                                                            <textarea cols="40" rows="3" name="business_address" placeholder="Business Address"></textarea>
                                                            <span class="fw-separator"></span>
                                                            <a  href="#"  class="previous-form action-button back-form  back-form1    color-bg"><i class="fal fa-angle-left"></i> Back</a>
                                                            <a  href="#"  class="next-form  next-form1   action-button btn no-shdow-btn color-bg">Next<i class="fal fa-angle-right"></i></a>
                                                        </fieldset>
                                                        <fieldset class="fl-wrap">
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Social Media Details</h3>
                                                            </div>
                                                            
                                                            
                                                            <div class="soc-log fl-wrap">
                                                               <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label>Facebook<i class="far fa-check"></i></label>
                                                                    <input type="text" name="fb_link" placeholder="Enter Facebook Link" value=""/>                                                  
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Google Plus<i class="far fa-check"></i></label>
                                                                    <input type="text" name="google_plus_link" placeholder="Google Plus link" value=""/> 
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label>Twitter Link<i class="far fa-user"></i></label>
                                                                    <input type="text" name="twitter_link" placeholder="Enter Twitter Link" value=""/>                                                  
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Linkedln<i class="far fa-user"></i></label>
                                                                    <input type="text" name="linkedln_link" placeholder="Enter Linkedln Link" value=""/> 
                                                                </div>
                                                            </div>
                                                            </div>
                                                            <span class="fw-separator"></span>
                                                            <a  href="#"  class="previous-form  back-form  back-form1  action-button    color-bg"><i class="fal fa-angle-left"></i> Back</a>
                                                            <a  href="#"  class="next-form  next-form1  back-form  back-form1  action-button btn no-shdow-btn color-bg">Next<i class="fal fa-angle-right"></i></a>
                                                                                                           
                                                        </fieldset>
                                                        <fieldset class="fl-wrap">
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Images and Videos</h3>
                                                            </div>
                                                            
                                                            
                                                            <div class="soc-log fl-wrap">
                                                              <div class="row">
                                                                   <div class="col-sm-6">
                                                                       <div class="col-md-8">
                                                                            <div class='p-text'>
                                                                                <div class='fu-text'>
                                                                                    <img src=''  width='250' class='blah4'>
                                                                                    <span class='img_uploadtext' style='display:block' id='blah4'><i class='fal fa-image'></i> Click here or drop files to upload</span>
                                                                                     <input type='file' name="image[]"  class='upload input2 imgInp'  imgClass='blah4'>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            
                                                                        </div>
                                                                       <div class="col-md-4">
                                                                           <a  href="#"  class="previous-form color-bg  addMoreImg"> Add More <i class="fal fa-plus"></i></a>
                                                                       </div>
                                                                   </div>
                                                                   <!-- <div class="col-sm-6">

                                                                           <label>Video</label>
                                                                           <input type="file" class="upload"/>

                                                                   </div> -->
                                                                   <div class="col-sm-6">

                                                                           <label>Video</label>
                                                                           <input type="text" name="video" value=""/> 

                                                                   </div>
                                                               </div>

                                                           </div>
                                                            <span class="fw-separator"></span>
                                                            <a  href="#"  class="previous-form  back-form  back-form1  action-button    color-bg"><i class="fal fa-angle-left"></i> Back</a>
                                                            <a  href="#"  class="next-form  next-form1  back-form  back-form1  action-button btn no-shdow-btn color-bg">Next<i class="fal fa-angle-right"></i></a>                                             
                                                        </fieldset>
                                                        <fieldset class="fl-wrap">
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Bank Details</h3>
                                                            </div>
                                                            
                                                            
                                                            <div class="soc-log fl-wrap">
                                                               <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <label>Bank Name<i class="far fa-check"></i></label>
                                                                        <input type="text" name="bank_name" placeholder="Enter Your Bank Name" value=""/>                                                  
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <label>Bank Account Number<i class="far fa-check"></i></label>
                                                                        <input type="text" name="bank_account_no"placeholder="Enter Your Bank Account Number" value=""/> 
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <label>Enter Branch<i class="far fa-user"></i></label>
                                                                        <input type="text" name="bank_branch" placeholder="Enter Your Branch" value=""/>                                                  
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <label>IFSC Code<i class="far fa-user"></i></label>
                                                                        <input type="text" name="bank_ifsc" placeholder="Enter Bank IFSC Code" value=""/> 
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <label>Account Name<i class="far fa-user"></i></label>
                                                                        <input type="text" name="bank_account_name" placeholder="Enter Account Name" value=""/>                                                  
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div class="fu-text">
                                                                            <label>Cancelled Cheque</label>
                                                                        <img src=''  width='150' class='Chequeimg'>
                                                                        <span class='img_uploadtext' id="Chequeimg" style='display:block'><i class="fal fa-image"></i> Click here or drop files to upload</span>
                                                                         <input type="file" name="bank_upload_cancel_cheque" class="upload input2 imgInp" imgClass='Chequeimg'>
                                                                    </div> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <span class="fw-separator"></span>
                                                            <a  href="#"  class="previous-form  back-form  back-form1  action-button    color-bg"><i class="fal fa-angle-left"></i> Back</a>
                                                            <a  href="#"  class="next-form  next-form1  back-form  back-form1  action-button btn no-shdow-btn color-bg">Next<i class="fal fa-angle-right"></i></a>
                                                                                                           
                                                        </fieldset>
                                                        <fieldset class="fl-wrap">
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Description</h3>
                                                            </div>
                                                            
                                                            
                                                            <div class="soc-log fl-wrap">
                                                               <div class="row">
                                                                    
                                                                    <textarea cols="40" rows="3"  name="vendor_detail" placeholder="Description"></textarea>
                                                                </div>
                                                                <div class="row">
                                                                    
                                                                    <textarea cols="40" rows="3"  name="about_vendor" placeholder="About Vendor"></textarea>
                                                                </div>
                                                                <div class="row">
                                                                    <input type="text" name="search_text" placeholder="Search Text" value=""/>
                                                                </div>
                                                            </div>
                                                            <span class="fw-separator"></span>
                                                            <a  href="#"  class="previous-form  back-form  back-form1  action-button    color-bg"><i class="fal fa-angle-left"></i> Back</a>
                                                            <!-- <a  href="#"  class="next-form  next-form1   action-button btn color2-bg no-shdow-btn">Submit<i class="fal fa-angle-right"></i></a> -->
                                                           <button type='submit' name='finish' class="btn    color2-bg ">Finish<i class="fal fa-paper-plane"></i>
                                                           </button>
                                                            
                                                                                                           
                                                        </fieldset>
                                                        <!-- <fieldset class="fl-wrap">
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Confirmation</h3>
                                                            </div>
                                                            <div class="success-table-container">
                                                                <div class="success-table-header fl-wrap">
                                                                    <i class="fal fa-check-circle decsth"></i>
                                                                    <h4>Thank you. Your Vendor Registration Submitted Sucessfully.</h4>
                                                                    <div class="clearfix"></div>
                                                                    <p>Once after approval we will send you the login credentials to the registared Email-id.</p>
                                                                    <a href="#" target="_blank" class="color-bg">View Invoice</a>
                                                                </div>
                                                            </div>
                                                            
                                                        </fieldset> -->
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!--   list-single-main-item end -->
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <!-- section end -->
                </div>
                <!-- content end-->
            </div>
            <!--wrapper end -->
            <!--wrapper end -->
            <?php
                include_once'footer.php';
            ?>