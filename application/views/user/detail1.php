<?php include_once 'header1.php';?>
<div id="wrapper">
    <!-- content-->
    <div class="content">
        
        <!--  carousel--> 
        <div class="list-single-carousel-wrap fl-wrap" id="sec1">
            <div class="fw-carousel fl-wrap full-height lightgallery">
                <!-- slick-slide-item -->
                <div class="slick-slide-item">
                    <div class="box-item">
                        <img  src="<?php echo base_url(); ?>assets/images/bg/10.jpg"   alt="">
                        <a href="<?php echo base_url(); ?>assets/images/bg/10.jpg" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                    </div>
                </div>
                <!-- slick-slide-item end -->
                <!-- slick-slide-item -->
                <div class="slick-slide-item">
                    <div class="box-item">
                        <img  src="<?php echo base_url(); ?>assets/images/bg/8.jpg"   alt="">
                        <a href="<?php echo base_url(); ?>assets/images/bg/8.jpg" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                    </div>
                </div>
                <!-- slick-slide-item end -->
                <!-- slick-slide-item -->
                <div class="slick-slide-item">
                    <div class="box-item">
                        <img  src="<?php echo base_url(); ?>assets/images/bg/12.jpg"   alt="">
                        <a href="<?php echo base_url(); ?>assets/images/bg/12.jpg" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                    </div>
                </div>
                <!-- slick-slide-item end -->
                <!-- slick-slide-item -->
                <div class="slick-slide-item">
                    <div class="box-item">
                        <img  src="<?php echo base_url(); ?>assets/images/bg/11.jpg"   alt="">
                        <a href="<?php echo base_url(); ?>assets/images/bg/11.jpg" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                    </div>
                </div>
                <!-- slick-slide-item end -->
                <!-- slick-slide-item -->
                <div class="slick-slide-item">
                    <div class="box-item">
                        <img  src="<?php echo base_url(); ?>assets/images/bg/7.jpg"   alt="">
                        <a href="<?php echo base_url(); ?>assets/images/bg/7.jpg" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                    </div>
                </div>
                    <div class="slick-slide-item">
                    <div class="box-item">
                        <img  src="<?php echo base_url(); ?>assets/images/bg/13.jpg"   alt="">
                        <a href="<?php echo base_url(); ?>assets/images/bg/13.jpg" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                    </div>
                </div>
                    <div class="slick-slide-item">
                    <div class="box-item">
                        <img  src="<?php echo base_url(); ?>assets/images/bg/9.jpg"   alt="">
                        <a href="<?php echo base_url(); ?>assets/images/bg/9.jpg" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                    </div>
                </div>
                <!-- slick-slide-item end -->
            </div>
            <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
            <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>
        </div>
        <!--  carousel  end--> 
        
        <!--  section   --> 
        
        <section class="grey-blue-bg small-padding scroll-nav-container" id="sec2">
            <!--  scroll-nav-wrapper  -->
            <div class="scroll-nav-wrapper fl-wrap">
                
                <div class="clearfix"></div>
                <div class="container">
                    <nav class="scroll-nav scroll-init">
                        <ul>
                            
                            <li><a class="act-scrlink" href="#sec2">About Vendor</a></li>
                            
                            
                            <li><a href="#sec5">Reviews</a></li>
                        </ul>
                    </nav>
                    
                </div>
            </div>
            <!--  scroll-nav-wrapper end  -->                    
            <!--   container  -->
            <div class="container">
                <!--   row  -->
                <div class="row">
                    <!--   datails -->
                    <div class="col-md-8">
                        <div class="list-single-main-container ">
                            <!-- fixed-scroll-column  -->
                            <div class="fixed-scroll-column">
                                <div class="fixed-scroll-column-item fl-wrap">
                                    <div class="showshare sfcs fc-button"><i class="far fa-share-alt"></i><span>Share </span></div>
                                    <div class="share-holder fixed-scroll-column-share-container">
                                        <div class="share-container  isShare"></div>
                                    </div>
                                    <a class="fc-button custom-scroll-link" href="#sec6"><i class="far fa-comment-alt-check"></i> <span>  Add review </span></a>
                                    <a class="fc-button" href="#"><i class="far fa-heart"></i> <span>Save</span></a>
                                    <a class="fc-button" href="cart.php"><i class="far fa-bookmark"></i> <span> Book Now </span></a>
                                </div>
                            </div>
                            
                            
                            <!--   list-single-main-item -->
                            <div class="list-single-main-item fl-wrap">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>About Vendor </h3>
                                </div>
                                <p>Praesent eros turpis, commodo vel justo at, pulvinar mollis eros. Mauris aliquet eu quam id ornare. Morbi ac quam enim. Cras vitae nulla condimentum, semper dolor non, faucibus dolor. Vivamus adipiscing eros quis orci fringilla, sed pretium lectus viverra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec nec velit non odio aliquam suscipit. Sed non neque faucibus, condimentum lectus at, accumsan enim. Fusce pretium egestas cursus. Etiam consectetur, orci vel rutrum volutpat, odio odio pretium nisiodo tellus libero et urna. Sed commodo ipsum ligula, id volutpat risus vehicula in. Pellentesque non massa eu nibh posuere bibendum non sed enim. Maecenas lobortis nulla sem, vel egestas  . </p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat .</p>
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/aQf0ETjC4eE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
                            <!--   list-single-main-item end -->
                                
                            
                            <!-- list-single-main-item -->   
                            <div class="list-single-main-item fl-wrap" id="sec5">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>Sucessfull Reviews -  <span> 2 </span></h3>
                                </div>
                                <!--reviews-score-wrap-->   
                                <div class="reviews-score-wrap fl-wrap">
                                    <div class="review-score-total">
                                        <span>
                                        4.5
                                        <strong>Very Good</strong>
                                        </span>
                                        
                                    </div>
                                    <div class="review-score-detail">
                                        <!-- review-score-detail-list-->
                                        <div class="review-score-detail-list">
                                            <!-- rate item-->
                                            <div class="rate-item fl-wrap">
                                                <div class="rate-item-title fl-wrap"><span>Dedication</span></div>
                                                <div class="rate-item-bg" data-percent="100%">
                                                    <div class="rate-item-line color-bg"></div>
                                                </div>
                                                <div class="rate-item-percent">5.0</div>
                                            </div>
                                            <!-- rate item end-->
                                            <!-- rate item-->
                                            <div class="rate-item fl-wrap">
                                                <div class="rate-item-title fl-wrap"><span>Execution</span></div>
                                                <div class="rate-item-bg" data-percent="90%">
                                                    <div class="rate-item-line color-bg"></div>
                                                </div>
                                                <div class="rate-item-percent">5.0</div>
                                            </div>
                                            <!-- rate item end-->                                                        
                                            <!-- rate item-->
                                            <div class="rate-item fl-wrap">
                                                <div class="rate-item-title fl-wrap"><span>New Innovations</span></div>
                                                <div class="rate-item-bg" data-percent="80%">
                                                    <div class="rate-item-line color-bg"></div>
                                                </div>
                                                <div class="rate-item-percent">4.0</div>
                                            </div>
                                            <!-- rate item end-->  
                                            <!-- rate item-->
                                            <div class="rate-item fl-wrap">
                                                <div class="rate-item-title fl-wrap"><span>Customer Satisfaction</span></div>
                                                <div class="rate-item-bg" data-percent="90%">
                                                    <div class="rate-item-line color-bg"></div>
                                                </div>
                                                <div class="rate-item-percent">4.5</div>
                                            </div>
                                            <!-- rate item end--> 
                                        </div>
                                        <!-- review-score-detail-list end-->
                                    </div>
                                </div>
                                <!-- reviews-score-wrap end -->   
                                <div class="reviews-comments-wrap">
                                    <!-- reviews-comments-item -->  
                                    <div class="reviews-comments-item">
                                        <div class="review-comments-avatar">
                                            <img src="<?php echo base_url(); ?>assets/images/avatar/2.jpg" alt=""> 
                                        </div>
                                        <div class="reviews-comments-item-text">
                                            <h4><a href="#">Customer Name</a></h4>
                                            <div class="review-score-user">
                                                <span>4.4</span>
                                                <strong>Good</strong>
                                            </div>
                                            <div class="clearfix"></div>
                                            <p>" Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. "</p>
                                            <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>12 April 2018</span><a href="#"><i class="fal fa-reply"></i> Reply</a></div>
                                        </div>
                                    </div>
                                    <!--reviews-comments-item end--> 
                                    <!-- reviews-comments-item -->  
                                    <div class="reviews-comments-item">
                                        <div class="review-comments-avatar">
                                            <img src="<?php echo base_url(); ?>assets/images/avatar/5.jpg" alt=""> 
                                        </div>
                                        <div class="reviews-comments-item-text">
                                            <h4><a href="#">Customer Name</a></h4>
                                            <div class="review-score-user">
                                                <span>4.7</span>
                                                <strong>Very Good</strong>
                                            </div>
                                            <div class="clearfix"></div>
                                            <p>" Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc posuere convallis purus non cursus. Cras metus neque, gravida sodales massa ut. "</p>
                                            <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>03 December 2017</span><a href="#"><i class="fal fa-reply"></i> Reply</a></div>
                                        </div>
                                    </div>
                                    <!--reviews-comments-item end-->                                                                  
                                </div>
                            </div>
                            <!-- list-single-main-item end -->   
                                                            
                        </div>
                    </div>
                    <!--   datails end  -->
                    <!--   sidebar  -->
                    <div class="col-md-4">
                        <!--box-widget-wrap -->  
                        <div class="box-widget-wrap">
                            <!--box-widget-item -->
                            <div class="box-widget-item fl-wrap">
                                <div class="box-widget">
                                    <div class="box-widget-content">
                                        <div class="box-widget-item-header">
                                            <h3> Book Now</h3>
                                        </div>
                                        <form name="bookFormCalc"   class="book-form custom-form">
                                            <fieldset>
                                                <div class="cal-item">
                                                    <div class="listsearch-input-item">
                                                        <label>Name</label>
                                                        <input type="text" name="name">
                                                        
                                                    </div>
                                                </div>
                                                <div class="cal-item">
                                                    <div class="bookdate-container  fl-wrap">
                                                        <label><i class="fal fa-calendar-check"></i> When </label>
                                                        <input type="text"    placeholder="Date Of Event" name="bookdates"   value=""/>
                                                        <div class="bookdate-container-dayscounter"><i class="far fa-question-circle"></i><span>Days : <strong>0</strong></span></div>
                                                    </div>
                                                </div>
                                                <div class="cal-item">
                                                    <div class="listsearch-input-item">
                                                        <label>Mobile Number</label>
                                                        <input type="text" name="name">
                                                        
                                                    </div>
                                                </div>
                                                <div class="cal-item">
                                                    <div class="listsearch-input-item">
                                                        <label>Select City</label>
                                                            <select data-placeholder="Room Type" name="repopt"   class="chosen-select no-search-select" >
                                                            <option value="0" selected>Select City</option>
                                                            <option value="81">Hyderabad</option>
                                                            <option value="122">Vijayawada</option>
                                                            <option value="310">Guntur</option>
                                                        </select>

                                                        
                                                    </div>
                                                </div>
                                            </fieldset>
                                            
                                            <button class="btnaplly color2-bg" oncloncick="window.location.href='cart.php'">Book Now<i class="fal fa-paper-plane"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--box-widget-item end -->                                      
                            <!--box-widget-item -->
                            <div class="box-widget-item fl-wrap">
                                <div class="box-widget counter-widget" data-countDate="09/12/2019">
                                    <div class="banner-wdget fl-wrap">
                                        <div class="overlay"></div>
                                        <div class="bg"  data-bg="<?php echo base_url(); ?>assets/images/bg/10.jpg"></div>
                                        <div class="banner-wdget-content fl-wrap">
                                            <h4>Get a discount <i>20%</i> discount if order <i>60 days</i> before from the date of event.</h4>
                                            
                                            <a href="cart.php">Book Now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--box-widget-item end -->                                       
                                                        
                            
                            
                            <div class="box-widget-item fl-wrap">
                                <div class="box-widget">
                                    <div class="box-widget-content">
                                        <div class="box-widget-item-header">
                                            <h3>Similar Listings</h3>
                                        </div>
                                        <div class="widget-posts fl-wrap">
                                            <ul>
                                                <?php for($k=0; $k<5; $k++) {?>
                                                <li class="clearfix">
                                                    
                                                    <div class="widget-posts-descr" style="width: 100%;">
                                                        <a href="detail.php" title="">Vidya Weddings</a>
                                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> From Hyderabad</a></div>
                                                        <span class="rooms-price pull-right">Rs. 5,00,000 </span>
                                                    </div>
                                                </li>
                                                <?php } ?>
                                                
                                            </ul>
                                            <a class="widget-posts-link" href="listing.php">See All Listing <i class="fal fa-long-arrow-right"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--box-widget-item end -->                            
                        </div>
                        <!--box-widget-wrap end -->  
                    </div>
                    <!--   sidebar end  -->
                </div>
                <!--   row end  -->
            </div>
            <!--   container  end  -->
        </section>
        <!--  section  end-->
    <!-- </div> -->
    <!-- content end-->
    <div class="limit-box fl-wrap"></div>
<!--  </div> -->
            <!--
           gallery code here
       -->
       <div class="main-register-wrap modal4">
            <div class="reg-overlay4"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pop_gal-holder">
                            <div class="pop_gal fl-wrap">
                                <div class="close-reg close-reg4 color-bg"><i class="fal fa-times"></i></div>
                                <!--tabs -->
                                <div id="modal_view"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
            <?php include_once 'footer.php';?>