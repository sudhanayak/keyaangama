<div class="col-md-4">
    <h3><?php echo $result['theme_name'];?></h3>
    <h5>Description</h5>
   
    <p><?php echo $result['theme_detail'];?></p>
    <div class="col-md-5">
        <p>Price: <?php echo $result['price'];?></p>
    </div>
    <input type='hidden' value="<?php echo $result['vendor_code']; ?>" class='vendorId'>
    <input type='hidden' value="<?php echo $result['id']; ?>" class='themeId'>
    <input type="hidden" class="theme_price" value="<?php echo $result['price']; ?>"/>
    <input type='hidden' value="<?php echo @$this->session->userdata('userCCode'); ?>" class='userId'>
    <div class="col-md-7">
        <?php if($this->session->userdata('userCId') !=""){ ?>
            <a class="btn color-bg ajax-link addToCart">Add To Cart<i class="fas fa-caret-right"></i></a>
        <?php } else { ?>
            <a class="btn color-bg ajax-link modal-open">Add To Cart<i class="fas fa-caret-right"></i></a>
        <?php } ?>
    </div>
</div>
<div class="col-md-8">
    <?php
        if($themeimglist != "") { 
            $arr = array();
            foreach ($themeimglist as $key => $img) {
                if($img['media_type'] == 1) { 
                    
                    $galImg = $img['theme_image1'];
                
    ?>
    <div class="col-md-3">
        <div class="rooms-media pop_viewgal">
            <img src="<?php echo $img['theme_image1'];?>" alt="">
            <div class="dynamic-gal more-photos-button back_im" data-dynamicPath="[{'src': '<?php echo $galImg ;?>'}]"></div>
        </div>
    </div>
    <?php } } } ?>
</div>