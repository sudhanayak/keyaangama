<?php include_once 'header1.php';?>
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    <!--  section  -->
                    <section class="grey-blue-bg" data-scrollax-parent="true" id="sec1" style="padding: 40px 0 0 !important;">
                        <div class="container">
                            <div class="col-md-8">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3><?php
                                        echo $package[0]['package_name'];
                                        ?>
                                    </h3>
                                </div>
                                <div class="list-single-main-media fl-wrap">
                                            <!-- gallery-items   -->
                                    <div class="gallery-items grid-small-pad  list-single-gallery three-coulms lightgallery">
                                        <!-- 1 -->
                                        
                                        <?php      
                                       $count_img =count(array_filter($packageImg));
                                        if($count_img > 0) {
                                        foreach($packageImg as $key_img => $row_img){ 
                                        ?>
                                        <div class="gallery-item ">
                                            <div class="grid-item-holder">
                                                <div class="box-item">
                                                    <img  src="<?php echo base_url(); ?>uploads/thumbnail/packageimg/<?php echo $row_img['image'] ?>"   alt="">
                                                    <a href="<?php echo base_url(); ?>uploads/packageimg/<?php echo $row_img['image'] ?>" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 1 end -->
                                    <?php
                                        }
                                    }
                                    ?>    
                                        
                                        
                                       
                                        
                                    </div>
                                    <!-- end gallery items -->                                          
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <div class="box-widget-item-header">
                                                <h3> Short Description</h3>
                                            </div>
                                            <p> 
                                           <?php
                                            echo $package[0]['package_detail'];
                                            ?>
                                            </p>
                                            
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <div class="box-widget-item-header">
                                                <h3> Price</h3>
                                            </div>
                                            <div class="claim-price-wdget fl-wrap">
                                                <div class="claim-price-wdget-content fl-wrap">
                                                    <div class="pricerange fl-wrap"><span>Package Starts From : </span>Rs.
                                                                <?php
                                                                     echo $package[0]['price'];
                                                                ?>
                                            /-</div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--  section  end-->
                    <!--  section  -->
                    <section class="grey-blue-bg small-padding" id="sec2">
                        <!--  scroll-nav-wrapper  -->
                        
                        <!--  scroll-nav-wrapper end  -->                    
                        <!--   container  -->
                        <div class="container">
                            <!--   row  -->
                            <div class="row">
                                <!--   datails -->
                                <div class="col-md-12">
                                    <div class="list-single-main-container ">
                                        <!-- fixed-scroll-column  -->
                                        <div class="fixed-scroll-column">
                                            <div class="fixed-scroll-column-item fl-wrap">
                                                <div class="showshare sfcs fc-button"><i class="far fa-share-alt"></i><span>Share </span></div>
                                                <div class="share-holder fixed-scroll-column-share-container">
                                                    <div class="share-container  isShare"></div>
                                                </div>
                                                <a class="fc-button custom-scroll-link" href="#sec6"><i class="far fa-comment-alt-check"></i> <span>  Add review </span></a>
                                                <a class="fc-button" href="#"><i class="far fa-heart"></i> <span>Save</span></a>
                                                <a class="fc-button" href="cart.php"><i class="far fa-bookmark"></i> <span> Book Now </span></a>
                                            </div>
                                        </div>
                                       
                                       
                                        <!-- loopsing   list-single-main-item -->

                                         <?php      
                                            $count_itry =count(array_filter($allpackResult));
                                            if($count_itry > 0) {
                                            foreach($allpackResult as $key => $row_all){
                                        ?>
                                        <div class="list-single-main-item fl-wrap">
											<div class="list-single-main-item-title fl-wrap">
                                                <h3>Day <?php echo $row_all['event_day'] ?> Itinerary</h3>
                                            </div> 
                                            <div class="clearfix"></div>											
											<div class="accordion">
                                            <?php  
                                            $count_index =count(array_filter($row_all['itineraryList']));
                                            if($count_index > 0) {
                                            foreach($row_all['itineraryList'] as $key => $row_itineraryList){
                                            ?>
											<a class="toggle act-accordion" href="#"> <?php echo $row_itineraryList['itenerary'] ?>   <i class="fa fa-angle-down"></i></a>
                                               
                                            <div class="accordion-inner visible">
                                            <?php                                                
                                            $count_indexit =count($row_itineraryList['iteneraryDetail']);
                                            if($count_indexit > 0) {
                                            ?>
                                            <table class="table table-bordered table-sm m-0 tables" width='100%'>
                                                <thead class="">
                                                    <tr>
                                                    <th>Select Item</th>
                                                    <th>Name</th>
                                                    <th>Price</th>
                                                    <th>Details</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php    
                                                    $i=0;
                                                    foreach($row_itineraryList['iteneraryDetail'] as $key => $row_itineraryDetail){
                                                    ?>
                                                    <input id="itinerary_code" type="hidden" name="itinerary_code[]">
                                                    <input id="package_code" type="hidden" name="package_code" value="<?php echo $row_itineraryDetail['package_code']; ?>">
                                                    <input type='hidden' value="<?php echo @$this->session->userdata('userCCode'); ?>" class='userId'>
                                                    <tr>
                                                        <td>
                                                        <div class=" fl-wrap filter-tags">
                                                            <input id="check-a" type="checkbox" name="check" code="<?php echo $row_itineraryDetail['id'];?>" class="itinerary" value="<?php echo $row_itineraryDetail['itinerary_code']; ?>">
                                                        </div>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            echo $row_itineraryDetail['itenerary_name'] ;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <div class="pricerange fl-wrap">Rs.
                                                            <?php
                                                                echo $row_itineraryDetail['itinerary_price'];
                                                            ?>/-
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <p>
                                                                <?php
                                                                echo $row_itineraryDetail['details'] ;
                                                                ?>
                                                            </p> 
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                            <?php } ?>  
                                            </div>    
										<?php } } ?>   
										</div>
										<!--
										looping end here
										-->
                                       <?php
                                            }
                                        }
                                       ?>
									
									
									<?php
                                    $i = 0;
                                    if($this->session->userdata('userCId') !=""){
                                    ?> 
                                    <a id='addToPackageCart' style="top: 0px; background: #828387; color: #fff; " class="show-reg-form add-hotel color2-bg">Add To Cart</a>
                                        <?php
                                    }else{
                                        ?>
                                        <a  style="top: 0px; background: #828387; color: #fff; " class="show-reg-form add-hotel color2-bg modal-open">Add To Cart</a>
                                    <?php } ?>
                                </div>
                                <!--   datails end  -->                              
                            </div>
                            <!--   row end  -->
                        </div>
                        <!--   container  end  -->
                    </section>
                </div>
                <!-- content end-->
                <div class="limit-box fl-wrap"></div>
            </div>
            <?php
            include_once 'footer.php';
            ?>