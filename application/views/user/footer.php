            <footer class="main-footer">
                <!--footer-inner-->
                <div class="footer-inner">
                    <div class="container">
                       
                        <div class="row">
                            <div class="col-md-4">
                                <div class="footer-widget fl-wrap">
                                    <h3>About Us</h3>
                                    <div class="footer-contacts-widget fl-wrap">
                                        <p>Sample text will be replaced with original keyaan about us text.</p>
                                        <div class="footer-social">
                                            <span>Find us : </span>
                                            <ul>
                                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="footer-widget fl-wrap">
                                    <div class="subscribe-widget fl-wrap">
                                        <h3>Subscribe</h3>
                                        <p style="color: #fff;">Want to be notified when we launch a new template or an udpate. Just sign up and we'll send you a notification by email.</p>
                                        <div class="subcribe-form">
                                            <form id="subscribe">
                                                <input class="enteremail fl-wrap" name="email" id="subscribe-email" placeholder="Enter Your Email" spellcheck="false" type="text">
                                                <button type="submit" id="subscribe-button" class="subscribe-button"><i class="fas fa-rss-square"></i> Subscribe</button>
                                                <label for="subscribe-email" class="subscribe-message"></label>
                                            </form>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="footer-widget fl-wrap">
                                    <h3>Corporate Office</h3>
                                    <div class="footer-contacts-widget fl-wrap">
                                        <ul  class="footer-contacts fl-wrap">
                                            <li><span><i class="fal fa-envelope"></i> Mail :</span><a href="#" target="_blank">support@keyaan.co</a></li>
                                            <li> <span><i class="fal fa-map-marker-alt"></i> Adress :</span><a href="#" target="_blank">Flat no: 403, VR Sunshine Building, Patrika Nagar, Street No:3, Madhapur - 500081</a></li>
                                            <li><span><i class="fa fa-mobile"></i> Phone :</span><a href="#">+91 - 999 999 9999</a></li>
                                        </ul>
                                        
                                    </div>
                                </div>
                            </div>
                           
                            
                            
                        </div>
                        <div class="clearfix"></div>
                       
                    </div>
                </div>
                <!--footer-inner end -->
                <div class="footer-bg">
                </div>
                <!--sub-footer-->
                <div class="sub-footer">
                    <div class="container">
                        <div class="copyright"> &#169; Keyaan 2018 .  All rights reserved.</div>
                        
                        <div class="subfooter-nav">
                            <div class="copyright"> Designed and Developed by: &nbsp; <a href="#" style="color: #fff;"> Lancius IT Solutions</a></div>
                        </div>
                    </div>
                </div>
                <!--sub-footer end -->
            </footer>
            <!--footer end -->
            <!--map-modal -->
           
            <!--map-modal end -->            
            <!--register form -->
            <?php include_once 'login.php';?>
            <!--register form end -->
            <a class="to-top"><i class="fas fa-caret-up"></i></a>
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom_ajax.js"> 
        </script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sweetalert.min.js"> 
        </script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom.js"> 
        </script>
        <script src="<?php echo base_url(); ?>assets/js/easyResponsiveTabs.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $('#parentVerticalTab1').easyResponsiveTabs({
                    type: 'vertical', 
                    width: 'auto',
                    fit: true, 
                    closed: 'accordion', 
                    tabidentify: 'hor_1', 
                    activate: function(event) {
                        initCitybook(); 
                        var $tab = $(this);
                        var $info = $('#nested-tabInfo2');
                        var $name = $('span', $info);
                        $name.text($tab.text());
                        $info.show();
                    }
                });
                $('#parentVerticalTab2').easyResponsiveTabs({
                    type: 'vertical', 
                    width: 'auto',
                    fit: true, 
                    closed: 'accordion', 
                    tabidentify: 'hor_1', 
                    activate: function(event) { 
                        initCitybook();
                        var $tab = $(this);
                        var $info = $('#nested-tabInfo2');
                        var $name = $('span', $info);
                        $name.text($tab.text());
                        $info.show();
                    }
                });
                $('#parentVerticalTab3').easyResponsiveTabs({
                    type: 'vertical', 
                    width: 'auto', 
                    fit: true, 
                    closed: 'accordion', 
                    tabidentify: 'hor_1', 
                    activate: function(event) { 
                        initCitybook();
                        var $tab = $(this);
                        var $info = $('#nested-tabInfo2');
                        var $name = $('span', $info);
                        $name.text($tab.text());
                        $info.show();
                    }
                });
            });
        </script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.menu-aim.js"></script> 
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script> 
            
    </body>
</html>