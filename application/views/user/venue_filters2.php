 <div class="mobile-list-controls fl-wrap">
    <div class="mlc show-list-wrap-search fl-wrap"><i class="fal fa-filter"></i> Filter</div>
    </div>
    <div class="fl-wrap filter-sidebar_item fixed-bar">
                                        <div class="filter-sidebar fl-wrap lws_mobile">
                                           <div class="col-list-search-input-item fl-wrap">
                                                <label>Sub Category</label>
                                                <div class="search-opt-container fl-wrap">
                                                    <!-- Checkboxes -->
                                                    <ul class="fl-wrap filter-tags">
                                                    <?php
                                                    //resultSubcat                                                                  
                                                        $count2 =count(array_filter($resultSubcat));
                                                        if($count2 > 0) {
                                                        $i=0;
                                                        foreach($resultSubcat as $key => $row2){
                                                    ?>
                                                        <li>
                                                            <input  class="subcategory_filters" 
                                                            id="pathway" value="<?php echo $row2['id'] ?>" type="checkbox" 
                                                            name="subcatId[]">
                                                            <label for="pathway"><?php echo $row2['subcategory_name'] ?></label>
                                                        </li>
                                                        <?php } } ?>
                                                    </ul>
                                                    <!-- Checkboxes end -->
                                                  <!--   <button class="header-search-button wdthauto pull-right" onclick="window.location.href='#'">View More</button> -->
                                                </div>
                                            </div> 
                                            
                                            <div class="col-list-search-input-item fl-wrap">
                                                <label>Price</label>
                                                <div class="search-opt-container fl-wrap">
                                                    <!-- Checkboxes -->
                                                    <ul class="fl-wrap filter-tags">
                                                        <li>
                                                            <input class='pricelist_filter' id="price1" value='1' type="checkbox" name="check">
                                                            <label for="price1">0 - 5000</label>
                                                        </li>
                                                        <li>
                                                            <input class='pricelist_filter' 
                                                            id="price2" value='2' type="checkbox"
                                                             name="price_filter">
                                                            <label for="price2">5001 - 10000</label>
                                                        </li>
                                                        <li>
                                                            <input class='pricelist_filter' id="price3" value='3' type="checkbox" 
                                                            name="price_filter">
                                                            <label for="price3">10001 - 50000</label>
                                                        </li>
                                                        <li>
                                                            <input class='pricelist_filter'
                                                             id="price4" value='4' type="checkbox" 
                                                            name="price_filter">
                                                            <label for="price4">50001 - 100000</label>
                                                        </li>
                                                        <li>
                                                            <input class='pricelist_filter' 
                                                            id="price5" value='5' type="checkbox" 
                                                            name="price_filter">
                                                            <label for="price5">100001 - 5000000</label>
                                                        </li>
                                                    </ul>
                                                    <!-- Checkboxes end -->
                                                   <!--  <button class="header-search-button wdthauto pull-right" onclick="window.location.href='#'">View More</button> -->
                                                </div>
                                            </div>                                
                                            <!--col-list-search-input-item -->
                                            <div class="col-list-search-input-item fl-wrap">
                                                <label>Star Rating</label>
                                                <div class="search-opt-container fl-wrap">
                                                    <!-- Checkboxes -->
                                                    <ul class="fl-wrap filter-tags">
                                                        <li class="five-star-rating">
                                                            <input value='5' class='rating_filter' id="check-aa2" type="checkbox" 
                                                            name="rating_filter[]">
                                                            <label for="check-aa2"><span class="listing-rating card-popup-rainingvis" data-starrating2="5"><span>5 Stars</span></span></label>
                                                        </li>
                                                        <li class="four-star-rating">
                                                            <input value='4' class='rating_filter' id="check-aa3" type="checkbox" 
                                                            name="rating_filter[]">
                                                            <label for="check-aa3"><span class="listing-rating card-popup-rainingvis" data-starrating2="5"><span>4 Star</span></span></label>
                                                        </li>
                                                        <li class="three-star-rating">                                          
                                                            <input value='3' class='rating_filter' id="check-aa4" type="checkbox" 
                                                            name="rating_filter[]">
                                                            <label for="check-aa4"><span class="listing-rating card-popup-rainingvis" data-starrating2="5"><span>3 Star</span></span></label>
                                                        </li>
                                                    </ul>
                                                    <!-- Checkboxes end -->
                                                </div>
                                            </div>
                                            <!--col-list-search-input-item end--> 
                                            
                                            <!--col-list-search-input-item  -->                                         
                                            <!-- <div class="col-list-search-input-item fl-wrap">
                                                <button class="header-search-button" onclick="window.location.href='listing.html'">Search <i class="far fa-search"></i></button>
                                            </div> -->
                                            <!--col-list-search-input-item end--> 
                                        </div>
</div>