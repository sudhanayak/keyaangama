<?php include_once 'header1.php';?>
            <!--  header end -->
            <!--  wrapper  -->
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    <!--  section  -->
                    
                    <!--  section-->
                    <section class="grey-blue-bg small-padding" id="sec1">
                        <div class="big-container">
                            <div class="row">
                                <!--filter sidebar -->
                                <div class="col-md-2">

                                   <?php 
                                   $count = count(array_filter($result));
                                    if($count > 0) {
                                        include_once 'search_filters.php';
                                    }
                                   ?>
                                </div>
                                <!--filter sidebar end-->
                                <!--listing -->
                                <div class="col-md-10">
                                    <!--col-list-wrap -->
                                    <div class="col-list-wrap fw-col-list-wrap post-container">
                                        <!-- list-main-wrap-->
                                        <div class="list-main-wrap fl-wrap card-listing">
                                            <!-- list-main-wrap-opt-->
                                            <div class="list-main-wrap-opt fl-wrap">
                                                <div class="list-main-wrap-title fl-wrap col-title">
                                                    <?php
                                                    $count = count(array_filter($result));
                                                    if($count > 0) {
                                                    ?>
                                                    <div class="price-opt pull-right">
                                                        <span class="price-opt-title">Sort results by:</span>
                                                        <div class="listsearch-input-item">
                                                            <select data-placeholder="Popularity" class="chosen-select no-search-select" >
                                                                <option>Popularity</option>
                                                                <option>Average rating</option>
                                                                <option>Price: low to high</option>
                                                                <option>Price: high to low</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                                <!-- price-opt-->
                                                
                                                <!-- price-opt end-->
                                                <!-- price-opt-->
                                                
                                                <!-- price-opt end-->                               
                                            </div>
                                            <!-- list-main-wrap-opt end-->
                                            <!-- listing-item-container -->
                                            <div class="listing-item-container init-grid-items fl-wrap">
                                            <?php
                                            $i = 0;
                                            $count = count(array_filter($result));
                                            if($count > 0) {
                                                foreach($result as $key => $row){
                                            ?>
                                                
                                                <div class="col-md-4 paddlr5">
                                                     <!-- listing-item  -->
                                                    <div class="listing-item wdth100">
                                                        <article class="geodir-category-listing fl-wrap">
                                                            <div class="geodir-category-img">
                                                                <a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>"><img src="<?php echo $row['image']?>" alt=""></a>
                                                                <div class="listing-avatar"><a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>"><img src="<?php echo base_url(); ?>assets/images//keyaan_latest_logo2.jpg" alt=""></a>
                                                                    <span class="avatar-tooltip">Added By  <strong><?php echo $row['name']?></strong></span>
                                                                </div>
                                                                
                                                                <div class="geodir-category-opt">
                                                                    <div class="listing-rating card-popup-rainingvis" style="color: #fff;">27 Reviews </div>
                                                                    <div class="rate-class-name">

                                                                        <span>5.0</span>                                             
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="geodir-category-content fl-wrap title-sin_item">
                                                                <div class="geodir-category-content-title fl-wrap">
                                                                    <div class="geodir-category-content-title-item">
                                                                        <h3 class="title-sin_map">
                                                                            <a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>"><?php echo $row['name']?></a></h3>
                                                                        <div class="geodir-category-location fl-wrap"><a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>" class="map-item">Specialized: <?php echo $row['subcategory']?>, <?php echo $row['subsubcategory']?></a></div>
                                                                    </div>
                                                                </div>
                                                                <p><?php echo substr($row['vendor_detail'],0,150);?></p>
                                                                
                                                                <div class="geodir-category-footer fl-wrap">
                                                                    <div class="geodir-category-price">Starts @<span> Rs. <?php echo $row['category_base_price']?></span></div>
                                                                    <div class="geodir-opt-list">
                                                                        <a href="#" class="geodir-js-favorite"><i class="fal fa-heart"></i><span class="geodir-opt-tooltip">Add To Favourites</span></a>
                                                                        <a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>" class="geodir-js-booking"><i class="fal fa-eye"></i><span class="geodir-opt-tooltip">View Details</span></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </article>
                                                    </div>
                                                    <!-- listing-item end -->
                                                </div>
                                                <?php
                                                $i++; }
                                                }else{
                                                    echo "No listing found";
                                                }
                                                ?>
                                                                                             
                                            </div>
                                            <!-- listing-item-container end-->
                                            <!-- <a class="load-more-button" href="#">Load more <i class="fal fa-spinner"></i> </a> -->
                                        </div>
                                        <!-- list-main-wrap end-->
                                    </div>
                                    <!--col-list-wrap end -->
                                </div>
                                <!--listing  end-->
                            </div>
                            <!--row end-->
                        </div>
                        <div class="limit-box fl-wrap"></div>
                    </section>
                </div>
                <!-- content end-->
            </div>
            <!--wrapper end -->
            <!--footer -->
           <?php include_once 'footer.php';?>