<header class="main-header2"  style="height:50px !important;">
                <!-- header-top-->
                <div class="header-top" id="header-top" fl-wrap" style="height:50px !important;">
                    <div class="container-fluid" id="stickkheader">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="logo-holder" id="stickylogo">
                                     <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/keyaan_logo1.png" class="img_keyaan_responsive" alt=""></a>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="nav-button-wrap color-bg">
                                    <div class="nav-button">
                                        <span></span><span></span><span></span>
                                    </div>
                                </div>
                                <!-- nav-button-wrap end-->
                                <!--  navigation -->
                                <div class="nav-holder main-menu" id="navx-holder" style="margin-top: 1.5%;">
                                    <nav>
                                        <ul>

                                            <li>
                                                <a href="#">Categories <i class="fas fa-caret-down"></i></a>
                                                <!--second level -->
                                                <ul>
                                                        <?php                                                                    
                                                                $count2 = count(array_filter($categoryList));
                                                                if($count2 > 0) {
                                                                
                                                                foreach($categoryList as $key => $row22){                                       
                                                                ?>                              
                                                                <li><a  href="<?php echo base_url() ?>listing/<?php echo $row22['id'] ?>"> <?php echo $row22['category_name'] ?></a></li>

                                                                <?php
                                                                }
                                                                }
                                                        ?>
                                                </ul>
                                                <!--second level end-->
                                            </li>
                                            <li>
                                                <a href="#">Packages <i class="fas fa-caret-down"></i></a>
                                                <!--second level -->
                                                <ul>
                                                    <li><a href="#">Wedding Packages</a></li>
                                                    <li><a href="#">Birthday Packages</a></li>
                                                    <li><a href="#">Traditional Packages</a></li>
                                                    <li><a href="#">Celebration Packages</a></li>
                                                    <li><a href="#">Entertainment Packages</a></li>
                                                    <li><a href="#">Destination Packages</a></li>
                                                    <li><a href="#">Corporate Packages</a></li>
                                                    <li><a href="#">Commercial Packages</a></li>

                                                </ul>
                                                <!--second level end-->
                                            </li>

                                            <li>
                                                <a href="#">Events <i class="fas fa-caret-down"></i></a>
                                                <!--second level -->
                                                <ul>
                                                    <li><a href="#">Wedding</a></li>
                                                </ul>
                                                <!--second level end-->
                                            </li>

                                            <li>
                                                <a href="#">Blog <i class="fas fa-caret-down"></i></a>
                                                <!--second level -->
                                                <ul>
                                                    <li><a href="#">Wedding</a></li>
                                                </ul>
                                                <!--second level end-->
                                            </li>
                                            <li>
                                                <a href="#">K-Pay<i class="fas fa-caret-down"></i></a>
                                                <!--second level -->
                                                <ul>
                                                    <li><a href="#">Wedding</a></li>
                                                </ul>
                                                <!--second level end-->
                                            </li>

                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="wishlist-link brdrrt hl70" id="wishhlist"><i class="fal fa-heart"></i><span class="wl_counter"><?php echo $cartCount; ?></span></div>
                               <a href="<?php echo base_url() ?>Cart">  <div class="wishlist-link hl70" id="wishhlist"><i class="fal fa-shopping-cart"></i><span class="wl_counter"><?php echo $cartCount; ?></span></div></a>
                                 <?php
                                        if($this->session->userdata('userCId') ==""){
                                        ?> 
                                        <a id="logh" href="#" class="show-reg-form modal-open add-hotel">Login / Signup</a>  
                                        <?php
                                           }else{
                                        ?>
                                            <div class="header-user-menu" id="logh2">
                                                <div class="header-user-name">
                                                    <span><img src="images/profile.jpg" alt=""></span>
                                                    <?php
                                                        echo "Hi ".$this->session->userdata('userCId');
                                                       ?>
                                                </div>
                                                <ul>
                                                    <!-- <li><a href="#"> Edit profile</a></li>
                                                    <li><a href="#"> Add Listing</a></li>
                                                    <li><a href="#">  Bookings  </a></li>
                                                    <li><a href="#"> Reviews </a></li> -->
                                                    <li><a href="<?php echo base_url() ?>MyDasboard">My Profile</a></li>
                                                    <li><a href="<?php echo base_url() ?>User/logout">Log Out</a></li>
                                                </ul>
                                            </div>
                                        <?php
                                        }
                                        ?> 
                                 <div class="show-search-button" id="show-search"><span>Search</span> <i class="fas fa-search"></i> </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!-- header-top end-->
                <!-- header-inner-->
                
                <!-- header-inner end-->
                <!-- header-search -->
                <div class="header-search vis-search">
                    <div class="container">
                        <div class="row">
                            <form action id='search_form1' method="POST">
                            <!-- header-search-input-item -->
                            <div class="col-sm-4">
                                <div class="header-search-input-item fl-wrap location autocomplete-container">
                                    <label>Select Category</label>
                                     <select id='search_cat1' required='true' data-placeholder="Apartments" class="chosen-select no-search-select slct" name="cat_id">
                                                            <option value=""> Select Categories</option>
                                                            <?php                                                                    
                                                            $count = count(array_filter($categoryList));
                                                            if($count > 0) {
                                                            $i=0;
                                                            foreach($categoryList as $key => $row){
                                                            ?>
                                                            <option class="icon-entertainment" value="<?php echo  $row['id'] ?>"><?php echo  $row['category_name'] ?></option>
                                                            <?php
                                                            }
                                                            }
                                                            ?>
                                                    </select>
                                </div>
                            </div>
                            <!-- header-search-input-item end -->
                            <!-- header-search-input-item -->
                            <div class="col-sm-3">
                                <div class="header-search-input-item fl-wrap date-parent">
                                    <label>Select City</label>
                                    <select id='search_city1' required='true' data-placeholder="Apartments" class="chosen-select no-search-select slct" name="city_id">
                                                        <option value="">Select Location</option>
                                                        <?php                                                                    
                                                            $count = count(array_filter($cityList));
                                                            if($count > 0) {
                                                            $i=0;
                                                            foreach($cityList as $key => $row){
                                                            ?>
                                                            <option  value="<?php echo  $row['id'] ?>"><?php echo  $row['city_name'] ?></option>
                                                            <?php
                                                            }
                                                            }
                                                        ?>
                                                    </select>
                                </div>
                            </div>
                            <!-- header-search-input-item end -->                             
                            <!-- header-search-input-item -->
                            <div class="col-sm-3">
                                <div class="header-search-input-item fl-wrap date-parent">
                                    <label>Select Price</label>
                                   <select id='search_price1' data-placeholder="Apartments" class="chosen-select no-search-select slct" name="priceList">
                                                        <option value="">Select Price</option>
                                                        <option value="1">0 - 5000</option>
                                                        <option value="2">5001 - 10000</option>
                                                        <option value="3">10001 - 50000</option>
                                                        <option value="4">50001 - 100000</option>
                                                        <option value="5">100001 - 500000</option>
                                    </select>
                                </div>
                            </div>
                            <!-- header-search-input-item end -->                             
                            <!-- header-search-input-item -->
                            <div class="col-sm-2">
                                <div class="header-search-input-item fl-wrap">
                                    <button id='search_btn1' class="header-search-button">Search <i class="far fa-search"></i></button>
                                </div>
                            </div>
                            <!-- header-search-input-item end -->
                            </form>                                                          
                        </div>
                    </div>
                    <div class="close-header-search"><i class="fal fa-angle-double-up"></i></div>
                </div>
                <!-- header-search  end -->
            </header>
