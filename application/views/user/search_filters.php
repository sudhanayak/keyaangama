 <div class="mobile-list-controls fl-wrap">
                                        <div class="mlc show-list-wrap-search fl-wrap"><i class="fal fa-filter"></i> Filter</div>
                                    </div>
                                    <div class="fl-wrap filter-sidebar_item fixed-bar">
                                        <div class="filter-sidebar fl-wrap lws_mobile">
                                           <div class="col-list-search-input-item fl-wrap">
                                                <label>Category</label>
                                                <div class="search-opt-container fl-wrap">
                                                    <!-- Checkboxes -->
                                                    <ul class="fl-wrap filter-tags">
                                                    <?php
                                                    //resultSubcat                                                                  
                                                        $count2 =count(array_filter($categoryList));
                                                        if($count2 > 0) {
                                                        $i=0;
                                                        foreach($categoryList as $key => $row2){
                                                    ?>
                                                        <li>
                                                            <input id="pathway" type="checkbox" name="check">
                                                            <label for="pathway"><?php echo $row2['category_name'] ?></label>
                                                        </li>
                                                        <?php } } ?>
                                                    </ul>
                                                    <!-- Checkboxes end -->
                                                  <!--   <button class="header-search-button wdthauto pull-right" onclick="window.location.href='#'">View More</button> -->
                                                </div>
                                            </div> 
                                            
                                            <div class="col-list-search-input-item fl-wrap">
                                                <label>Price</label>
                                                <div class="search-opt-container fl-wrap">
                                                    <!-- Checkboxes -->
                                                    <ul class="fl-wrap filter-tags">
                                                        <li>
                                                            <input id="0-50000" type="checkbox" name="check">
                                                            <label for="0-50000">0 - 50000</label>
                                                        </li>
                                                        <li>
                                                            <input id="50000-500000" type="checkbox" name="check">
                                                            <label for="50000-500000">50000 - 500000</label>
                                                        </li>
                                                        <li>
                                                            <input id="500000-5000000" type="checkbox" name="check">
                                                            <label for="500000-5000000">5000000 - 5000000</label>
                                                        </li>
                                                        <li>
                                                            <input id="5000000-10000000" type="checkbox" name="check">
                                                            <label for="5000000-10000000">5000000 - 10000000</label>
                                                        </li>
                                                    </ul>
                                                    <!-- Checkboxes end -->
                                                   <!--  <button class="header-search-button wdthauto pull-right" onclick="window.location.href='#'">View More</button> -->
                                                </div>
                                            </div>                                
                                            <!--col-list-search-input-item -->
                                            <div class="col-list-search-input-item fl-wrap">
                                                <label>Star Rating</label>
                                                <div class="search-opt-container fl-wrap">
                                                    <!-- Checkboxes -->
                                                    <ul class="fl-wrap filter-tags">
                                                        <li class="five-star-rating">
                                                            <input id="check-aa2" type="checkbox" name="check">
                                                            <label for="check-aa2"><span class="listing-rating card-popup-rainingvis" data-starrating2="5"><span>5 Stars</span></span></label>
                                                        </li>
                                                        <li class="four-star-rating">
                                                            <input id="check-aa3" type="checkbox" name="check">
                                                            <label for="check-aa3"><span class="listing-rating card-popup-rainingvis" data-starrating2="5"><span>4 Star</span></span></label>
                                                        </li>
                                                        <li class="three-star-rating">                                          
                                                            <input id="check-aa4" type="checkbox" name="check">
                                                            <label for="check-aa4"><span class="listing-rating card-popup-rainingvis" data-starrating2="5"><span>3 Star</span></span></label>
                                                        </li>
                                                    </ul>
                                                    <!-- Checkboxes end -->
                                                </div>
                                            </div>
                                            <!--col-list-search-input-item end--> 
                                            
                                            <!--col-list-search-input-item  -->                                         
                                            <!-- <div class="col-list-search-input-item fl-wrap">
                                                <button class="header-search-button" onclick="window.location.href='listing.html'">Search <i class="far fa-search"></i></button>
                                            </div> -->
                                            <!--col-list-search-input-item end--> 
                                        </div>
                                    </div>