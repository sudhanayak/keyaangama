<?php
include_once'header1.php';
?>
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    
                     <section class="flat-header color-bg adm-header">
                        <div class="wave-bg wave-bg2"></div>
                        <div class="container">
                            <?php include_once 'user_dashboard.php';?>
                        </div>
                    </section>
                    <!-- section end-->
                    <!-- section-->
                    <section class="middle-padding">
                        <div class="container">
                            <!--dasboard-wrap-->
                            <div class="dasboard-wrap fl-wrap">
                                <!-- dashboard-content--> 
                                <div class="dashboard-content fl-wrap">
                                    <div class="box-widget-item-header">
                                        <h3> My Profile</h3>
                                    </div>
                                    <div class="custom-form no-icons">
                                        <div class="pass-input-wrap fl-wrap">
                                            <label>Name</label>
                                            <input type="text" class="pass-input"  value="<?php echo $result[0]['name']; ?>"/ readonly>
                                        </div>
                                        <div class="pass-input-wrap fl-wrap">
                                            <label>Mobile</label>
                                            <input type="text" class="pass-input" value="<?php echo $result[0]['mobile']; ?>"/ readonly>
                                        </div>
                                        <div class="pass-input-wrap fl-wrap">
                                            <label>Email</label>
                                            <input type="text" class="pass-input" value="<?php echo $result[0]['email']; ?>"/ readonly>
                                        </div>
                                        <!-- <button class="btn  big-btn  color2-bg flat-btn float-btn">Save Changes<i class="fal fa-save"></i></button> -->
                                    </div>
                                </div>
                                <!-- dashboard-list-box end--> 
                            </div>
                            <!-- dasboard-wrap end-->
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                    <!-- section end -->
                </div>
                <!-- content end-->
            </div>
<?php
include_once'footer.php';
?>