
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Keyaan :: We Create You Celebrate</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->
        <link type="text/css" rel="stylesheet" href="css/reset.css">
        <link type="text/css" rel="stylesheet" href="css/plugins.css">
        <link type="text/css" rel="stylesheet" href="css/style.css">
        <link type="text/css" rel="stylesheet" href="css/color.css">
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="images/favicon.ico">
    </head>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="pin">
                <div class="pulse"></div>
            </div>
        </div>
        <!--loader end-->
        <!-- Main  -->
        <div id="main">
            <a href="#" class="frilst_rgt"></a>
        <a href="#" class="frilst_rgt_support"></a>
            <!-- header-->
            <header class="main-header">
                <!-- header-top-->
                <div class="header-top fl-wrap">
                    <div class="container-fluid">
                        
                        <?php include_once 'header1.php';?>
                    </div>
                </div>
                <!-- header-top end-->
                <!-- header-inner-->
                
                <!-- header-inner end-->
                <!-- header-search -->
                
                <!-- header-search  end -->
            </header>
            <!--  wrapper  -->
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    
                    <section class="middle-padding gre y-blue-bg">
                        <div class="container">
                            <div class="list-main-wrap-title single-main-wrap-title fl-wrap">
                                <h2>Customize Package</h2>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="bookiing-form-wrap">
                                        <ul id="progressbar">
                                            <li class="active"><span>01.</span>Event Details</li>
                                            <li><span>02.</span>Requirement For Event</li>
                                            <li><span>03.</span>Preview of Package</li>
                                            <li><span>04.</span>Confirm</li>
                                        </ul>
                                        <!--   list-single-main-item -->
                                        <div class="list-single-main-item fl-wrap hidden-section tr-sec">
                                            <div class="profile-edit-container">
                                                <div class="custom-form">
                                                    <form>
                                                        <fieldset class="fl-wrap">
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Your Event Information</h3>
                                                            </div>
                                                            <div class="row">
                                                                
                                                                <div class="col-md-6">
                                                                    <div class="col-sm-12">
                                                                        <div class="cal-item">
                                                                            <div class="listsearch-input-item">
                                                                                <label>Select Type Of Event</label>
                                                                                 <select data-placeholder="Room Type" name="repopt"   class="chosen-select no-search-select" >
                                                                                    <option value="0" selected>Select Event</option>
                                                                                    <option value="81">Birthday</option>
                                                                                    <option value="122">Wedding</option>
                                                                                    <option value="310">Reception</option>
                                                                                </select>


                                                                            </div>
                                                                        </div>                                                
                                                                    </div>
                                                                   
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="col-sm-12">
                                                                        <div class="cal-item">
                                                                            <div class="listsearch-input-item">
                                                                                <label>No.Of Days</label>
                                                                                 <select data-placeholder="Room Type" name="repopt"   class="chosen-select no-search-select" >
                                                                                    <option value="0" selected>Select Days</option>
                                                                                    <option value="81">1 Day Event</option>
                                                                                    <option value="122">2 Days Event</option>
                                                                                    <option value="310">3 Days Event</option>
                                                                                </select>


                                                                            </div>
                                                                        </div>                                                
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            <span class="fw-separator"></span>
                                                            <a  href="#"  class="next-form action-button btn no-shdow-btn color-bg">Next<i class="fal fa-angle-right"></i></a>
                                                        </fieldset>
                                                        <fieldset class="fl-wrap">
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Select the services for Day 1</h3>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <ul class="fl-wrap filter-tags">
                                                                        <li>
                                                                            <input id="check-aaa5" type="checkbox" name="check" checked>
                                                                            <label for="check-aaa5">Decoration</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="check-bb5" type="checkbox" name="check" checked>
                                                                            <label for="check-bb5">Catering</label>
                                                                        </li>
                                                                        <li>                                       
                                                                            <input id="check-dd5" type="checkbox" name="check">
                                                                            <label for="check-dd5">Photography</label>
                                                                        </li>
                                                                        <li>                                          
                                                                            <input id="check-cc5" type="checkbox" name="check">
                                                                            <label for="check-cc5">Videography</label>
                                                                        </li>
                                                                        <li>                                       
                                                                            <input id="check-ff5" type="checkbox" name="check" checked>
                                                                            <label for="check-ff5">Pandit</label>
                                                                        </li>
                                                                        <li>                                          
                                                                            <input id="check-c4" type="checkbox" name="check">
                                                                            <label for="check-c4">Man Power</label>
                                                                        </li>
                                                                    </ul>

                                                                </div>
                                                            </div>
                                                           
                                                            <textarea cols="40" rows="3" placeholder="Notes"></textarea>
                                                            
                                                            <div class="list-single-main-item-title fl-wrap mt3p">
                                                                <h3>Select the services for Day 2</h3>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <ul class="fl-wrap filter-tags">
                                                                        <li>
                                                                            <input id="check-aaa5" type="checkbox" name="check" checked>
                                                                            <label for="check-aaa5">Decoration</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="check-bb5" type="checkbox" name="check" checked>
                                                                            <label for="check-bb5">Catering</label>
                                                                        </li>
                                                                        <li>                                       
                                                                            <input id="check-dd5" type="checkbox" name="check">
                                                                            <label for="check-dd5">Photography</label>
                                                                        </li>
                                                                        <li>                                          
                                                                            <input id="check-cc5" type="checkbox" name="check">
                                                                            <label for="check-cc5">Videography</label>
                                                                        </li>
                                                                        <li>                                       
                                                                            <input id="check-ff5" type="checkbox" name="check" checked>
                                                                            <label for="check-ff5">Pandit</label>
                                                                        </li>
                                                                        <li>                                          
                                                                            <input id="check-c4" type="checkbox" name="check">
                                                                            <label for="check-c4">Man Power</label>
                                                                        </li>
                                                                    </ul>

                                                                </div>
                                                            </div>
                                                           
                                                            <textarea cols="40" rows="3" placeholder="Notes"></textarea>
                                                            <span class="fw-separator"></span>
                                                            <a  href="#"  class="previous-form action-button back-form   color-bg"><i class="fal fa-angle-left"></i> Back</a>
                                                            <a  href="#"  class="next-form back-form action-button btn no-shdow-btn color-bg">Next <i class="fal fa-angle-right"></i></a>
                                                        </fieldset>
                                                        <fieldset class="fl-wrap">
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Package Preview</h3>
                                                            </div>
                                                            
                                                            
                                                            <div class="soc-log fl-wrap">
                                                                <div class="row">
                                                                    <h4 class="pull-left">Basic Details</h4>
                                                                    <div class="col-md-12">
                                                                        <p class="col-md-6">Type of Event: Wedding</p>
                                                                        <p class="col-md-6">No.of Days: 4 Days</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <h4 class="pull-left">Day 1 Itinerary</h4>
                                                                    <div class="col-md-12">
                                                                        <ul class="fl-wrap filter-tags">
                                                                            <li>
                                                                                
                                                                                <label for="check-aaa5">Decoration</label>
                                                                            </li>
                                                                            <li>
                                                                                
                                                                                <label for="check-bb5">Catering</label>
                                                                            </li>
                                                                            <li>                                       
                                                                                
                                                                                <label for="check-dd5">Photography</label>
                                                                            </li>
                                                                            <li>                                          
                                                                                
                                                                                <label for="check-cc5">Videography</label>
                                                                            </li>
                                                                            <li>                                       
                                                                                
                                                                                <label for="check-ff5">Pandit</label>
                                                                            </li>
                                                                            <li>                                          
                                                                                
                                                                                <label for="check-c4">Man Power</label>
                                                                            </li>
                                                                        </ul>
                                                                        <h5 class="pull-left">Notes::</h5>
                                                                        <div class="clearfix"></div>
                                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <h4 class="pull-left">Day 2 Itinerary</h4>
                                                                    <div class="col-md-12">
                                                                        <ul class="fl-wrap filter-tags">
                                                                            <li>
                                                                                
                                                                                <label for="check-aaa5">Decoration</label>
                                                                            </li>
                                                                            <li>
                                                                                
                                                                                <label for="check-bb5">Catering</label>
                                                                            </li>
                                                                            <li>                                       
                                                                                
                                                                                <label for="check-dd5">Photography</label>
                                                                            </li>
                                                                            <li>                                          
                                                                                
                                                                                <label for="check-cc5">Videography</label>
                                                                            </li>
                                                                            <li>                                       
                                                                                
                                                                                <label for="check-ff5">Pandit</label>
                                                                            </li>
                                                                            <li>                                          
                                                                                
                                                                                <label for="check-c4">Man Power</label>
                                                                            </li>
                                                                        </ul>
                                                                        <h5 class="pull-left">Notes::</h5>
                                                                        <div class="clearfix"></div>
                                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <span class="fw-separator"></span>
                                                            <a  href="#"  class="previous-form  back-form action-button    color-bg"><i class="fal fa-angle-left"></i> Back</a>
                                                            <a  href="#"  class="next-form  action-button btn color2-bg no-shdow-btn">Confirm and Pay<i class="fal fa-angle-right"></i></a>                                               
                                                        </fieldset>
                                                        <fieldset class="fl-wrap">
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Confirmation</h3>
                                                            </div>
                                                            <div class="success-table-container">
                                                                <div class="success-table-header fl-wrap">
                                                                    <i class="fal fa-check-circle decsth"></i>
                                                                    <h4>Thank you. Your Event Enquiry Has Been Received.</h4>
                                                                    <div class="clearfix"></div>
                                                                    <p>Your payment has been processed successfully.</p>
                                                                    <a href="#" target="_blank" class="color-bg">View Invoice</a>
                                                                </div>
                                                            </div>
                                                            <span class="fw-separator"></span>
                                                            <a  href="#"  class="previous-form action-button  back-form   color-bg"><i class="fal fa-angle-left"></i> Back</a>
                                                        </fieldset>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!--   list-single-main-item end -->
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="box-widget-item-header">
                                        <h3> Your  Details</h3>
                                    </div>
                                    <!--cart-details  --> 
                                    <div class="cart-details fl-wrap">
                                        <!--cart-details_header--> 
                                        <div class="cart-details_header">
                                            <a href="#"  class="widget-posts-img"><img src="images/wedding.jpg" class="respimg" alt=""></a>
                                            <div class="widget-posts-descr">
                                                <a href="#" title="">P.Phanendra Kumar</a>
                                                <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i>Flat No:403, Patrika Nagar, Street No:3, Madhapur - 500081</a></div>
                                            </div>
                                        </div>
                                        <!--cart-details_header end-->       
                                        <!--ccart-details_text-->          
                                        <div class="cart-details_text">
                                            <ul class="cart_list">
                                                <li>Email: <span>phanendrakumar@lanciussolutions.com</span></li>
                                                <li>Mobile Number <span>+91 - 9959742195</span></li>
                                                <li>Billing Address: <span>Flat no: 403, VR Sunshine Building, Patrika Nagar, Street no:3, Near Maxcure Hospital, Madhapur, 500081</span></li>
                                                
                                            </ul>
                                        </div>
                                        <!--cart-details_text end --> 
                                    </div>
                                    <!--cart-details end --> 
                                    <!--cart-total --> 
                                    <div class="cart-total">
                                        <span class="cart-total_item_title">Total Amount</span>
                                        <strong>Rs. 5000/-</strong>                                
                                    </div>
                                    <!--cart-total end -->                             
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- section end -->
                </div>
                <!-- content end-->
            </div>
            <!--wrapper end -->
            <!--wrapper end -->
             <footer class="main-footer">
                <?php include_once './footer.php';?>
            </footer>
            <!--footer end -->
            <!--map-modal -->
            
            <!--map-modal end -->           
            <!--register form -->
            <?php include_once 'login.php';?>
            <a class="to-top"><i class="fas fa-caret-up"></i></a>
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwJSRi0zFjDemECmFl9JtRj1FY7TiTRRo&libraries=places&callback=initAutocomplete"></script>        
    </body>
</html>