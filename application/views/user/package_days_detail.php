<?php include_once 'header1.php';?>
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    <!--  section  -->
                    <section class="grey-blue-bg" data-scrollax-parent="true" id="sec1" style="padding: 40px 0 0 !important;">
                        <div class="container">
                            <div class="col-md-8">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>Package Name :: Galley</h3>
                                </div>
                                <div class="list-single-main-media fl-wrap">
                                            <!-- gallery-items   -->
                                    <div class="gallery-items grid-small-pad  list-single-gallery three-coulms lightgallery">
                                        <!-- 1 -->
                                        <div class="gallery-item ">
                                            <div class="grid-item-holder">
                                                <div class="box-item">
                                                    <img  src="<?php echo base_url(); ?>assets/images/wedding/1.jpg"   alt="">
                                                    <a href="<?php echo base_url(); ?>assets/images/wedding/1.jpg" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 1 end -->
                                        <!-- 2 -->
                                        <div class="gallery-item">
                                            <div class="grid-item-holder">
                                                <div class="box-item">
                                                    <img  src="<?php echo base_url(); ?>assets/images/wedding/2.jpg"   alt="">
                                                    <a href="<?php echo base_url(); ?>assets/images/wedding/2.jpg" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 2 end -->
                                        <!-- 3 -->
                                        <div class="gallery-item gallery-item-second">
                                            <div class="grid-item-holder">
                                                <div class="box-item">
                                                    <img  src="<?php echo base_url(); ?>assets/images/wedding/3.jpg"   alt="">
                                                    <a href="<?php echo base_url(); ?>assets/images/wedding/3.jpg" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 3 end -->
                                        <!-- 4 -->
                                        <div class="gallery-item">
                                            <div class="grid-item-holder">
                                                <div class="box-item">
                                                    <img  src="<?php echo base_url(); ?>assets/images/wedding/4.jpg"   alt="">
                                                    <a href="<?php echo base_url(); ?>assets/images/wedding/4.jpg" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 4 end -->
                                       
                                        <!-- 7 -->
                                        <div class="gallery-item">
                                            <div class="grid-item-holder">
                                                <div class="box-item">
                                                    <img  src="<?php echo base_url(); ?>assets/images/wedding/6.jpg"   alt="">
                                                    <div class="more-photos-button dynamic-gal"  data-dynamicPath="[{'src': '<?php echo base_url(); ?>assets/images/wedding/8.jpg'}, {'src': '<?php echo base_url(); ?>assets/images/wedding/9.jpg'},{'src': '<?php echo base_url(); ?>assets/images/wedding/10.jpg'}]">Other <span>4 photos</span><i class="far fa-long-arrow-right"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 7 end -->
                                    </div>
                                    <!-- end gallery items -->                                          
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <div class="box-widget-item-header">
                                                <h3> Short Description</h3>
                                            </div>
                                            <p>Wedding is a lifetime bond between two individuals who are truly in love with one another
Who wish to spend their entire life being partners for each other. It is a lifetime event that has a lot of stories to tell about. Keyaan are a highly dedicated team of professionals who offer a complete package that helps 
to remain stress free for the entire wedding festival.
Keyaan provides the clients assistance in all the fields required, right from planning of the event till the actual execution of the event is taken care of by the team members</p>
                                            
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <div class="box-widget-item-header">
                                                <h3> Price</h3>
                                            </div>
                                            <div class="claim-price-wdget fl-wrap">
                                                <div class="claim-price-wdget-content fl-wrap">
                                                    <div class="pricerange fl-wrap"><span>Package Starts From : </span>Rs. 3,00,000/-</div>
                                                    <div class="claim-widget-link fl-wrap"><span>Sample Text?</span><a href="#">Check Availability</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--  section  end-->
                    <!--  section  -->
                    <section class="grey-blue-bg small-padding" id="sec2">
                        <!--  scroll-nav-wrapper  -->
                        
                        <!--  scroll-nav-wrapper end  -->                    
                        <!--   container  -->
                        <div class="container">
                            <!--   row  -->
                            <div class="row">
                                <!--   datails -->
                                <div class="col-md-12">
                                    <div class="list-single-main-container ">
                                        <!-- fixed-scroll-column  -->
                                        <div class="fixed-scroll-column">
                                            <div class="fixed-scroll-column-item fl-wrap">
                                                <div class="showshare sfcs fc-button"><i class="far fa-share-alt"></i><span>Share </span></div>
                                                <div class="share-holder fixed-scroll-column-share-container">
                                                    <div class="share-container  isShare"></div>
                                                </div>
                                                <a class="fc-button custom-scroll-link" href="#sec6"><i class="far fa-comment-alt-check"></i> <span>  Add review </span></a>
                                                <a class="fc-button" href="#"><i class="far fa-heart"></i> <span>Save</span></a>
                                                <a class="fc-button" href="cart.php"><i class="far fa-bookmark"></i> <span> Book Now </span></a>
                                            </div>
                                        </div>
                                       
                                       <div class="list-single-main-item fl-wrap" id="sec3">
                                           <div class="list-single-main-item-title fl-wrap">
                                                <h3>Complimentary services</h3>
                                            </div>
                                           <div class="clearfix"></div>
                                           <p class="complimentary">Pre-shoot, Online invitations, Galands (2), Pula Jada (1), Wedding</p>
                                        </div>
                                        <!--   list-single-main-item -->
                                        <div class="list-single-main-item fl-wrap">
                                             <div class="list-single-main-item-title fl-wrap">
                                                <h3>Package Itinary</h3>
                                            </div>
                                             <div class="clearfix"></div>
                                            <?php include_once 'tabbing_hor_vertical.php';?>
                                        </div>
                                        <!--   list-single-main-item end -->
                                        <!--   list-single-main-item -->
                                        
                                        <!--   list-single-main-item end -->     
                                       
                                        <!-- list-single-main-item -->   
                                        <div class="list-single-main-item fl-wrap" id="sec5">
                                             <div class="list-single-main-item-title fl-wrap">
                                                <h3>Terms and Conditions</h3>
                                            </div>
                                           <div class="clearfix"></div>
                                           <p>Keyaan provides services (the"Service") via the Website or Phone to assist individuals planning events ("User" or"You") in searching for, identifying, reviewing and securing orders and reservations with participating third party event-related suppliers and vendors ("Vendors"), including, without limitation, planners, decorators, magicians, emcees, game organizers, food counter specialists, Game stalls, entry concepts, stage shows, audio & video , gifts purchases &sales ,cakes &cup cakes,chocolates gifts ,venue providers, caterers and entertainment providers that are listed on the Website ("Suppliers"). The Website acts as a venue to allow the Suppliers to sell their services at any posted price. Keyaan is not a part of the actual transaction between User and Supplier. Hence, Keyaan does not hold any control over the accuracy, quality and the ability of the Vendors to sell and Users to pay for the services respectively. Keyaan shall not be responsible for the completion or failure of a transaction.</p>
                                        </div>
                                        <!-- list-single-main-item end -->   
                                                                      
                                    </div>
                                </div>
                                <!--   datails end  -->
                               
                            </div>
                            <!--   row end  -->
                        </div>
                        <!--   container  end  -->
                    </section>
                    <!--  section  end-->
                </div>
                <!-- content end-->
                <div class="limit-box fl-wrap"></div>
            </div>
            <!--
           gallery code here
       -->
       <div class="main-register-wrap modal4">
            <div class="reg-overlay4"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pop_gal-holder">
                            <div class="pop_gal fl-wrap">
                                <div class="close-reg close-reg4 color-bg"><i class="fal fa-times"></i></div>
                                <!--tabs -->
                                <div id="modal_view"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
            <?php include_once 'footer.php';?>
            