<?php 
$count = count(array_filter($result));
if($count > 0) { 
    foreach($result as $key => $row){
?>
<div class="listing-item-container init-grid-items fl-wrap">
    <!-- listing-item  -->
    <div class="listing-item padd20 has_two_column has_one_column">
        <article class="geodir-category-listing fl-wrap venue_list_art">
            <div class="col-md-4 col-sm-6 col-xs-12 col-lg-4 padd0">
                <div class="geodir-category-img venue_list_img">
                    <a href="listing-single.html"><img src="<?php echo $row['image']; ?>" alt="" class="image_responisve"></a>
                    <div class="listing-avatar"><a href="author-single.html"><img src="<?php echo base_url() ?>assets/images/avatar/1.jpg" alt=""></a>
                        <span class="avatar-tooltip">Added By  <strong>Alisa Noory</strong></span>
                    </div>
                    <div class="sale-window">Sale 20%</div>
                    <div class="geodir-category-opt">
                        <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                        <div class="rate-class-name">
                            <div class="score"><strong>Very Good</strong>27 Reviews </div>
                            <span>5.0</span>        
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 col-lg-6 padd0" >
                <div class="geodir-category-content fl-wrap title-sin_item wdth100" id="content">
                    <div class="geodir-category-content-title fl-wrap">
                        <div class="geodir-category-content-title-item">
                            <h3 class="title-sin_map"><a href="listing-single.html"><?php echo $row['venue_name']; ?></a></h3>
                            <div class="geodir-category-location fl-wrap"><a href="#" class="map-item"><i class="fas fa-map-marker-alt"></i><?php echo $row['address']; ?></a></div>
                        </div>
                    </div>
                    <p><?php echo $row['venue_detail']; ?>...<a href="#">Read More</a></p>

                    <div class="geodir-category-footer fl-wrap venue_highlights">
                        <h4 class="pull-left mb10">Highlights :</h4>
                        <div class="listing-features fl-wrap">
                            <ul>
                                <?php 
                                foreach($facilityList as $key=>$field) { ?>
                                    <li><i class="fal fa-check"></i><?php echo $field['facility_name']; ?></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-12 col-xs-12 col-lg-2">
                <h1 class="ft30"><i class="fal fa-rupee-sign"></i> <?php echo $row['price_start']; ?></h1>
                <small>Price Per Plate</small>
                <p class="pricing-switcher"><i class="fas fa-male"></i> 150 - 300 Pax</p>
                <div class="col-list-search-input-item fl-wrap">
                    <button class="header-search-button" onclick="window.location.href='<?php echo base_url();?>venueDetails/<?php echo $row['id']?>'">View Detail <i class="far fa-eye"></i></button>
                </div>
                <div class="geodir-category-footer fl-wrap venue_highlights btopn">
                    <div class="geodir-opt-list mt10p">
                        <a href="#" class="single-map-item" data-newlatitude="40.72956781" data-newlongitude="-73.99726866"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">View on Map</span></a>
                        <a href="#" class="geodir-js-favorite"><i class="fal fa-heart"></i><span class="geodir-opt-tooltip">Save</span></a>
                        <a href="#" class="geodir-js-booking"><i class="fal fa-exchange"></i><span class="geodir-opt-tooltip">Compare</span></a>
                    </div>
                </div>
                
            </div>
        </article>
    </div>
    <!-- listing-item end -->
</div>
<?php } } ?>