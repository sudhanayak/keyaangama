<?php include_once 'header1.php';?>
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    <!--  section  -->
                    <section class="grey-blue-bg" data-scrollax-parent="true" id="sec1" style="padding: 40px 0 0 !important;">
                        <div class="container">
                            <div class="col-md-8">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3><?php
                                        echo $package[0]['package_name'];
                                        ?>
                                    </h3>
                                </div>
                                <div class="list-single-main-media fl-wrap">
                                            <!-- gallery-items   -->
                                    <div class="gallery-items grid-small-pad  list-single-gallery three-coulms lightgallery">
                                        <!-- 1 -->
                                        
                                        <?php      
                                       $count_img =count(array_filter($packageImg));
                                        if($count_img > 0) {
                                        foreach($packageImg as $key_img => $row_img){ 
                                        ?>
                                        <div class="gallery-item ">
                                            <div class="grid-item-holder">
                                                <div class="box-item">
                                                    <img  src="<?php echo base_url(); ?>uploads/thumbnail/packageimg/<?php echo $row_img['image'] ?>"   alt="">
                                                    <a href="<?php echo base_url(); ?>uploads/packageimg/<?php echo $row_img['image'] ?>" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 1 end -->
                                    <?php
                                        }
                                    }
                                    ?>    
                                        
                                        
                                       
                                        
                                    </div>
                                    <!-- end gallery items -->                                          
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <div class="box-widget-item-header">
                                                <h3> Short Description</h3>
                                            </div>
                                           
                                           <?php
                                            echo $package[0]['package_detail'];
                                            ?>
                                            
                                            
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <div class="box-widget-item-header">
                                                <h3> Price</h3>
                                            </div>
                                            <div class="claim-price-wdget fl-wrap">
                                                <div class="claim-price-wdget-content fl-wrap">
                                                    <div class="pricerange fl-wrap"><span>Package Starts From : </span>Rs.
                                                                <?php
                                                                     echo $package[0]['price'];
                                                                ?>
                                            /-</div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--  section  end-->
                    <!--  section  -->
                   <section class="grey-blue-bg small-padding scroll-nav-container" id="sec2">
                        <!--  scroll-nav-wrapper  -->
                        <div class="scroll-nav-wrapper fl-wrap">
                            
                            <div class="clearfix"></div>
                            <div class="container">
                                <nav class="scroll-nav scroll-init">
                                    <ul>
                                        <?php
                                        $itinary_count =1;
                                        while($itinary_count <= $packageCount){
                                        ?>
                                        <li><a class="act-scrlink" href="#sec<?php echo $itinary_count; ?>">Day <?php echo $itinary_count; ?> Itinerary</a></li>
                                        <?php
                                        $itinary_count++;
                                        }
                                        ?>
                                        
                                        <!-- <li><a href="#sec3">Day 2 Itinerary</a></li>
                                        
                                        <li><a href="#sec5">Day 3 Itinerary</a></li> -->
                                    </ul>
                                </nav>
                                
                                <a href="<?php echo base_url(); ?>Custompackage/<?php echo $package[0]['package_code'];?>" class="btn flat-btn color2-bg float-btn  pull-right" style="margin: 0px; margin-right: 5px;">Customize Package<i class="fal fa-cubes"></i></a>
                                <input type='hidden' value="<?php echo $package[0]['package_code']; ?>" class='packageId'>
                                <input type='hidden' value="<?php echo $package[0]['price']; ?>" class='price'>
                                <input type='hidden' value="<?php echo @$this->session->userdata('userCCode'); ?>" class='userId'>
                                <?php if($this->session->userdata('userCId') !=""){ ?>
                                    <a class="btn flat-btn color-bg float-btn  pull-right packaddtocart" style="margin: 0px; margin-right: 5px;">Add To cart<i class="fal fa-shopping-cart"></i></a>
                                <?php } else { ?>
                                    <a class="btn flat-btn color-bg float-btn  pull-right modal-open" style="margin: 0px; margin-right: 5px;">Add To cart<i class="fal fa-shopping-cart"></i></a>
                                <?php } ?>
                            </div>
                        </div>
                        <!--  scroll-nav-wrapper end  -->                    
                        <!--   container  -->
                        <div class="container">
                            <!--   row  -->
                            <div class="row">
                                <!--   datails -->
                                <div class="col-md-12">
                                    <div class="list-single-main-container ">
                                        <!-- fixed-scroll-column  -->
                                        <div class="fixed-scroll-column">
                                            <div class="fixed-scroll-column-item fl-wrap">
                                                <div class="showshare sfcs fc-button"><i class="far fa-share-alt"></i><span>Share </span></div>
                                                <div class="share-holder fixed-scroll-column-share-container">
                                                    <div class="share-container  isShare"></div>
                                                </div>
                                                <a class="fc-button custom-scroll-link" href="#sec6"><i class="far fa-comment-alt-check"></i> <span>  Add review </span></a>
                                                <a class="fc-button" href="#"><i class="far fa-heart"></i> <span>Save</span></a>
                                                <a class="fc-button" href="cart.php"><i class="far fa-bookmark"></i> <span> Book Now </span></a>
                                            </div>
                                        </div>
                                       

                                            <?php      
                                            $count_itry =count(array_filter($allpackResult));
                                            if($count_itry > 0) {
                                            foreach($allpackResult as $key => $row_all){
                                            ?>

                                                <div class="list-single-main-item fl-wrap" id="sec2">
                                                    <div class="list-single-main-item-title fl-wrap">
                                                        <h3>Day <?php echo $row_all['event_day'] ?> Itinerary</h3>
                                                    </div>
                                                    <?php //include'tabbing_vertical.php';?>

                                    <div id="parentVerticalTab<?php echo $row_all['event_day'] ?>">
                                        <ul class="resp-tabs-list hor_1">                                              
                                        <?php                                                                                
                                            $count200 =count(array_filter($row_all['itineraryList']));
                                            if($count200 > 0) {
                                            $i=0;
                                            foreach($row_all['itineraryList'] as $key => $row_itineraryList){
                                            ?>
                                            <li><?php echo $row_itineraryList['itenerary'] ?></li>
                                            <?php
                                                }
                                            }
                                            ?>   
                                            </ul>
                                            <div class="resp-tabs-container hor_1">
                                            <?php  
                                            $count_index =count(array_filter($row_all['itineraryList']));
                                            if($count_index > 0) {
                                            foreach($row_all['itineraryList'] as $key => $row_itineraryList){
                                            ?>
                                            <div>
                                            
                                            <?php                                                
                                            $count_indexit =count($row_itineraryList['iteneraryDetail']);
                                            if($count_indexit > 0) {
                                            $i=0;
                                            foreach($row_itineraryList['iteneraryDetail'] as $key => $row_itineraryDetail){
                                            ?>
                                            <div class="price-item  best-price" style='margin-top:30px;margin-left:20px;'>
                                            <div class="price-head op2">
                                            <?php
                                              echo $row_itineraryDetail['itenerary_name'] ;
                                             ?>
                                            </div>
                                            <div class='price-content fl-wrap'> 
                                              
                                             <div class="list-single-main-media" style='margin-top:20px;'>
                                            <!-- gallery-items   -->
                                    <div class="gallery-items grid-small-pad  list-single-gallery three-coulms lightgallery">
                                       
                                        
                                        <?php  
                                         
                                        $count_img =count(array_filter($row_itineraryDetail['imageList']));
                                        if($count_img > 0) {
                                        foreach($row_itineraryDetail['imageList'] as $key_img => $row_img){ 
                                        ?>
                                        <div class="gallery-item ">
                                            <div class="grid-item-holder">
                                                <div class="box-item">
                                                    <img  src="<?php echo base_url(); ?>uploads/thumbnail/itineraryimg/<?php echo $row_img['images'] ?>"   alt="">
                                                    <a href="<?php echo base_url(); ?>uploads/itineraryimg/<?php echo $row_img['images'] ?>" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 1 end -->
                                    <?php
                                        }
                                    }
                                    ?>    
                                        
                                        
                                       
                                        
                                    </div>
                                    <!-- end gallery items -->                                          
                                </div>
                                            <div class='price-desc fl-wrap'>
                                             <ul><li> <?php
                                             echo $row_itineraryDetail['details'] ;
                                             ?></li></ul>
                                            </div> 
                                            <div class="price-num fl-wrap">
                                            
                                            <span class="price-num-item">
                                                               Rs <?php
                                                                     echo $row_itineraryDetail['itinerary_price'];
                                                                ?>/
                                            </span>
                                            </div>
                                              
                                        </div>   
                                        </div>   
                                             <?php
                                           
                                           
                                             }
                                            }
                                            ?> 

                                            </div>
                                            <?php
                                                }
                                            }
                                            ?> 
                                                                
                                                                
                                        </div>
                                        </div>
                                        </div>
                                        <?php
                                            }
                                        }
                                        ?>








                                       
                                                                      
                                    </div>
                                </div>
                                <!--   datails end  -->
                               
                            </div>
                            <!--   row end  -->
                        </div>
                        <!--   container  end  -->
                    </section>
                    <!--  section  end-->
                </div>
                <!-- content end-->
                <div class="limit-box fl-wrap"></div>
            </div>
            <?php
            include_once 'footer.php';
            ?>