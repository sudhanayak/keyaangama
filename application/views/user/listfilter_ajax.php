
	 <input type="hidden" 
           class="themeCount"
           value="<?php echo $themeCount ?>">
           <input type="hidden" 
           class="themeCountLimit"
           value="1" name='themeCountLimit'>
<?php
$i = 0;
$count = count(array_filter($result));
if($count > 0) {
foreach($result as $key => $row){
?>
<div class="col-md-3 paddlr5">
     <!-- listing-item  -->
    <div class="listing-item wdth100">
        <article class="geodir-category-listing fl-wrap">
            <div class="geodir-category-img">
                <a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>"><img src="<?php echo $row['image']?>" alt=""></a>
                <div class="listing-avatar"><a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>"><img src="<?php echo base_url(); ?>assets/images//keyaan_latest_logo2.jpg" alt=""></a>
                    <span class="avatar-tooltip">Added By  <strong><?php echo $row['name']?></strong></span>
                </div>
                
                <div class="geodir-category-opt">
                    <div class="listing-rating card-popup-rainingvis" style="color: #fff;">27 Reviews </div>
                    <div class="rate-class-name">

                        <span>5.0</span>                                             
                    </div>
                </div>
            </div>
            <div class="geodir-category-content fl-wrap title-sin_item">
                <div class="geodir-category-content-title fl-wrap">
                    <div class="geodir-category-content-title-item">
                        <h3 class="title-sin_map">
                            <a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>"><?php echo $row['name']?></a></h3>
                        <div class="geodir-category-location fl-wrap"><a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>" class="map-item">Specialized: <?php echo $row['subcategory']?>, <?php echo $row['subsubcategory']?></a></div>
                    </div>
                </div>
                
                
                <div class="geodir-category-footer fl-wrap">
                <div class="geodir-category-price">Starts @<span> Rs. <?php echo $row['category_base_price']?></span></div>
                    <div class="geodir-opt-list">
                        <a href="#" class="geodir-js-favorite"><i class="fal fa-heart"></i><span class="geodir-opt-tooltip">Add To Favourites</span></a>
                        <a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>" class="geodir-js-booking"><i class="fal fa-eye"></i><span class="geodir-opt-tooltip">View Details</span></a>
                    </div>
                </div>
            </div>
        </article>
    </div>
    <!-- listing-item end -->
</div>
<?php
$i++; }

 }
 //else{
//     echo "No listing found";
// }
?>     
<span class="loadchk"></span>                                    
