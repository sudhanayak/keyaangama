<?php
include_once'header1.php';
?>
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    
                     <section class="flat-header color-bg adm-header">
                        <div class="wave-bg wave-bg2"></div>
                        <div class="container">
                            <?php include_once 'user_dashboard.php';?>
                        </div>
                    </section>
                    <!-- section end-->
                    <!-- section-->
                    <section class="middle-padding">
                        <div class="container">
                            <!--dasboard-wrap-->
                            <div class="dasboard-wrap fl-wrap">
                                <!-- dashboard-content-->
                                <form method="POST">
                                <div class="dashboard-content fl-wrap">
                                    <div class="box-widget-item-header">
                                        <h3> Update Profile</h3>
                                    </div>
                                    <center><span class="errorList" style="color:green;"></span>
                                    <span class="errorList2"  style="color:red;"></span></center>
                                    <div class="custom-form no-icons">
                                        <div class="pass-input-wrap fl-wrap">
                                            <label>Name</label>
                                            <input type="text" class="pass-input name"  value="<?php echo $result[0]['name']; ?>"/>
                                        </div>
                                        <div class="pass-input-wrap fl-wrap">
                                            <label>Mobile</label>
                                            <input type="text" class="pass-input mobile" value="<?php echo $result[0]['mobile']; ?>"/ pattern="[0-9]{10}" maxlength="10" minlength="10">
                                        </div>
                                        <div class="pass-input-wrap fl-wrap">
                                            <label>Email</label>
                                            <input type="text" class="pass-input email" value="<?php echo $result[0]['email']; ?>"/>
                                        </div>
                                        <input type="hidden" class="form-control id" value="<?php echo $result[0]['id']; ?>">
                                        <button class="btn  big-btn  color2-bg flat-btn float-btn updateProfileBtn">Update Profile<i class="fal fa-save"></i></button>
                                    </div>
                                </div>
                                </form>
                                <!-- dashboard-list-box end--> 
                            </div>
                            <!-- dasboard-wrap end-->
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                    <!-- section end -->
                </div>
                <!-- content end-->
            </div>
<?php
include_once'footer.php';
?>