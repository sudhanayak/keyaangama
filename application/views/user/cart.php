<?php
include_once'header1.php';
?>
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    
                    <section class="middle-padding gre y-blue-bg">
                        <div class="container">
                            <div class="list-main-wrap-title single-main-wrap-title fl-wrap">
                                <h2>Cart Page</h2>
                            </div>
                            <div class="row">
                                
                                <div class="col-md-8">
                                    <table class="table-responsive keyaan_table" style="margin-bottom:40px">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Vendor Name</th>
                                                <th>Item Name</th>
                                                <th>Price</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php                                                                    
                                            $count2 = count(array_filter($result));
                                            if($count2 > 0) {
                                            $sno=1;
                                            foreach($result as $key => $row22){                                       
                                            ?>          
                                            <tr>
                                                <td><?php echo $sno; ?>.</td>
                                                <td><?php echo  $row22['vendorName']; ?></td>
                                                <td><?php echo  $row22['itemName']; ?></td>
                                                <td>Starting From: <?php echo  $row22['price']; ?></td>
                                                <td><a  onclick="return confirm('Are You Sure to Delete ?');" href="<?php echo base_url()?>Cart/deleteCart/<?php echo $row22['item_id'];?>"><i class="fa fa-trash"></i></a></td>
                                            </tr>
                                            <?php 
                                           $sno++;
                                        }
                                        }else{
                                            ?>
                                            <tr><td colspan='6'>No Themes In Your Cart</td></tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <table class="table-responsive keyaan_table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Package Name</th>
                                                <th>Itinerary Name</th>
                                                <th>Price</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php                                                                    
                                            $count2 = count(array_filter($result1));
                                            if($count2 > 0) {
                                            $sno=1;
                                            foreach($result1 as $key => $row22){                                       
                                            ?>          
                                            <tr>
                                                <td><?php echo $sno; ?>.</td>
                                                <td><?php echo  $row22['packageName']; ?></td>
                                                <td><?php echo  $row22['itineraryName']; ?></td>
                                                <td>Starting From: <?php echo  $row22['itinerary_price']; ?></td>
                                                <td><a  onclick="return confirm('Are You Sure to Delete ?');" href="<?php echo base_url()?>Cart/deletePackageCart/<?php echo $row22['iteinerary_id'];?>"><i class="fa fa-trash"></i></a></td>
                                            </tr>
                                            <?php 
                                           $sno++;
                                        }
                                        }else{
                                            ?>
                                            <tr><td colspan='6'>No Packages In Your Cart</td></tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <?php
                                    $count1 = count(array_filter($result));
                                    $count2 = count(array_filter($result1));
                                    if($count1 > 0 || $count2 > 0) {
                                        ?>
                                    <a href="<?php echo base_url() ?>Checkout" class="action-button btn no-shdow-btn color-bg pull-right mt10">Proceed To Checkout<i class="fal fa-angle-right"></i></a>
                                    <?php
                                }
                                    ?>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                    </section>
                    <!-- section end -->
                </div>
                <!-- content end-->
            </div>
<?php
include_once'footer.php';
?>