<?php
include_once 'header1.php';

?>
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    
                    <div class="breadcrumbs-fs fl-wrap">
                        <div class="container">
                            <div class="breadcrumbs fl-wrap"><a href="<?php base_url(); ?>">Home</a><span><?php echo $category ?></span></div>
                        </div>
                    </div>
                    <!--  section-->
                    <section class="grey-blue-bg small-padding" id="sec1">
                        <div class="container-fluid">
                            <div class="row">
                                <!--filter sidebar -->
                                <div class="col-md-3">
                                   <?php include_once 'venue_filters2.php';?>
                                </div>
                                <!--filter sidebar end-->
                                <!--listing -->
                                <div class="col-md-9">
                                    
                                    <!--col-list-wrap -->
                                    <div class="col-list-wrap fw-col-list-wrap post-container">
                                        
                                        <!-- list-main-wrap-->
                                        <div class="list-main-wrap fl-wrap card-listing">
                                            <!-- list-main-wrap-opt-->
                                            <div class="list-main-wrap-opt fl-wrap">
                                                <div class="list-main-wrap-title fl-wrap col-title">
                                                    <h2>Results For : <span><?php echo $category; ?></span></h2>
                                                </div>
                                                <!-- price-opt-->
                                                <div class="price-opt" style='display:none;'>
                                                    <span class="price-opt-title">Sort results by:</span>
                                                    <div class="listsearch-input-item">
                                                        <select data-placeholder="Popularity" class="chosen-select no-search-select" >
                                                            <option>Popularity</option>
                                                            <option>Average rating</option>
                                                            <option>Price: low to high</option>
                                                            <option>Price: high to low</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!-- price-opt end-->
                                                <!-- price-opt-->
                                                
                                                <!-- price-opt end-->                               
                                            </div>
                                            <!-- list-main-wrap-opt end-->
                                            <div class="listing-item-container init-grid-items fl-wrap">
                                            <?php
                                            $i = 0;
                                            $count = count(array_filter($result));
                                            if($count > 0) {
                                                foreach($result as $key => $row){
                                                    $urldata = base_url().'packageDetailList/'.$row['package_code'];
                                            ?>
                                            
                                           
                                            <!-- listing-item-container -->
                                                <!-- listing-item  -->
                                                <div class="listing-item has_two_column has_one_column">
                                                    <article class="geodir-category-listing fl-wrap venue_list_art">
                                                        <div class="col-md-4 col-sm-6 col-xs-12 col-lg-4 padd0">
                                                            <div class="geodir-category-img venue_list_img">
                                                                <a  href="<?php echo $urldata;?>">
                                                                <img src="<?php echo $row['packageImg'];?>" alt="" class="image_responisve packimg"></a>
                                                                <div class="listing-avatar"><a href="<?php echo $urldata;?>"><img src="<?php echo base_url(); ?>images/avatar/1.jpg" alt=""></a>
                                                                    <span class="avatar-tooltip">Added By  <strong>Keyaan</strong></span>
                                                                </div>
                                                                <div class="sale-window" style='display:none'>Sale 20%</div>
                                                                <div class="geodir-category-opt">
                                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                                    <div class="rate-class-name">
                                                                        <div class="score"><strong>Very Good</strong>27 Reviews </div>
                                                                        <span>5.0</span>                                             
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12 col-lg-6 padd0">
                                                            <div class="geodir-category-content fl-wrap title-sin_item wdth100">
                                                                <div class="geodir-category-content-title fl-wrap">
                                                                    <div class="geodir-category-content-title-item">
                                                                        <h3 class="title-sin_map"><a  href="<?php echo $urldata;?>"><?php echo $row['package_name'];?></a></h3>
                                                                        
                                                                    </div>
                                                                </div>
                                                                <p><?php echo substr($row['package_detail'],0,350)."....";?></p>
                                                                <?php
                                                                $ik = 0;
                                                                $countkk = count(array_filter($row['highlights']));
                                                                if($countkk > 0){
                                                               ?>
                                                                <div class="geodir-category-footer fl-wrap venue_highlights">
                                                                    <h4 class="pull-left mb10">Highlights :</h4>
                                                                    <div class="listing-features fl-wrap">
                                                                        <ul> 
                                                                            <?php
                                                                            
                                                                            if($countkk > 0) {
                                                                            foreach($row['highlights'] as $key2 => $row22){                                                 
                                                                            ?>
                                                                            <li><i class="fal fa-check"></i><?php echo $row22['highlights']; ?></li>
                                                                            <?php
                                                                            }
                                                                        }
                                                                            ?>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-12 col-xs-12 col-lg-2">
                                                            <h1 style="font-size: 25px;"><i class="fal fa-rupee-sign"></i> <?php echo $row['price'];?></h1>
                                                            <div class="col-list-search-input-item fl-wrap">
                                                                
                                                            </div>
                                                            <div class="geodir-category-footer fl-wrap venue_highlights btopn">
                                                                <div class="geodir-opt-list mt10p">
                                                                    <button class="header-search-button" onclick="window.location='<?php echo $urldata;?>';" >
                                                                         View Detail <i class="far fa-eye"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </article>
                                                </div>
                                                <!-- listing-item end -->
                                               
                                                                                              
                                           
                                            <?php
                                             } 
                                            }
                                            ?>
                                                 </div>
                                            <!-- listing-item-container end-->
                                            <a class="load-more-button" href="#" style='display:none'>Load more <i class="fal fa-spinner"></i> </a>
                                        </div>
                                        <!-- list-main-wrap end-->
                                    </div>
                                    <!--col-list-wrap end -->
                                </div>
                                <!--listing  end-->
                            </div>
                            <!--row end-->
                        </div>
                        <div class="limit-box fl-wrap"></div>
                    </section>
                </div>
                <!-- content end-->
            </div>
            <!--wrapper end -->
          <?php
          include_once'footer.php';
          ?>