<?php
include_once'header1.php';
?>
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    
                     <section class="flat-header color-bg adm-header">
                        <div class="wave-bg wave-bg2"></div>
                        <div class="container">
                            <?php include_once 'user_dashboard.php';?>
                        </div>
                    </section>
                    <!-- section end-->
                    <!-- section-->
                     <section class="middle-padding">
                        <div class="container">
                            <!--dasboard-wrap-->
                            <div class="dasboard-wrap fl-wrap">
                                <!-- dashboard-content--> 
                                <div class="dashboard-content fl-wrap">
                                    <div class="dashboard-list-box fl-wrap">
                                        <div class="dashboard-header fl-wrap">
                                            <h3>Your Wishlist</h3>
                                        </div>
                                        <?php
                                            $i = 0;
                                            $count = count(array_filter($result));
                                            if($count > 0) {
                                                foreach($result as $key => $row){
                                        ?>

                                        <!-- dashboard-list  -->    
                                        <div class="dashboard-list">
                                            <div class="dashboard-message">
                                                
                                                <div class="dashboard-listing-table-image">
                                                    <a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>/<?php echo $row['vendor_code']?>"><img src="<?php echo base_url(); ?>uploads/basic_image/<?php echo $row['basic_image'] ?>" alt=""></a>
                                                </div>
                                                <div class="dashboard-listing-table-text">
                                                    <h4><a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>/<?php echo $row['vendor_code']?>"><?php echo $row['name']?></a></h4>
                                                    <span class="dashboard-listing-table-address"><?php echo substr($row['vendor_detail'],0, 150)?>
                                                    </span>
                                                    <ul class="dashboard-listing-table-opt  fl-wrap">
                                                        <li><a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>/<?php echo $row['vendor_code']?>">View <i class="fal fa-eye"></i></a></li>
                                                        <li><a href="#" class="del-btn">Delete <i class="fal fa-trash-alt"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                                $i++; }
                                                }else{
                                                    echo "No listing found";
                                                }
                                                ?>
                                        <!-- dashboard-list end-->    
                                       
                                    </div>
                                 
                                </div>
                                <!-- dashboard-list-box end--> 
                            </div>
                            <!-- dasboard-wrap end-->
                        </div>
                    </section>
                        
                    </section>
                    <div class="limit-box fl-wrap"></div>
                    <!-- section end -->
                </div>
                <!-- content end-->
            </div>
<?php
include_once'footer.php';
?>