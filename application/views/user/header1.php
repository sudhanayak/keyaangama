<!DOCTYPE HTML>
<html lang="en">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Keyaan :: You Create We Celebrate</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/reset.css">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/color.css">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/icomoon.css">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/easy-responsive-tabs.css">
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/<?php echo base_url(); ?>assets/images/favicon.ico">
        <?php include_once 'chat.php';?>
        
    </head>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="pin">
                <div class="pulse"></div>
            </div>
        </div>
        <!--loader end-->
        <!-- Main  -->
        <a href="#" class="frilst_rgt"></a>
        <a href="#" class="frilst_rgt_support"></a>
        <a href="#" class="frilst_rgt_customize"></a>
        <div id="main">
            <!-- header-->
            <header class="main-header">
                <!-- header-top-->
                <div class="header-top fl-wrap">
                    <div class="container-fluid">
                        <!--
                            header1 code start here 
                        -->
                        <div class="row">
                            <div class="col-md-2 padd0">
                                <div class="logo-holder">
                                   <?php                                                                    
                                    $count_basicsettings = count(array_filter($basicsettingsList));
                                    if($count_basicsettings > 0) {
                                    foreach($basicsettingsList as $key => $row_basicsettingsList){   
                                        $url = base_url().'uploads/site_logo/'.$row_basicsettingsList['site_logo'];
                                  ?>
                                  <a href="<?php echo base_url(); ?>"><img src="<?php echo $url?>" class="img_keyaan_responsive" alt=""></a>
                                  <?php } } ?>
                                </div>
                            </div>
                            <div class="col-md-10 hidden-xs padd0">
                                <div class="col-md-12 hidden-xs short-div2 padd0 brdr_btm">
                                    <form action="<?php echo base_url() ?>searchList" method="GET" enctype="multipart/form-data">
                                        <div class="col-md-7">
                                            <div class="main-search-input-wrap mt10">
                                                <div class="main-search-input fl-wrap srch" style="background: #ccc;">                                       
                                                    <div class="main-search-input-item location slctwdth" id="autocomplete-container">
                                                                        <select data-placeholder="Apartments" class="chosen-select no-search-select slct" name="city_id" required>
                                                                            <option value="">Select Location</option>
                                                        <?php                                                                    
                                                        $count2 = count(array_filter($cityList));
                                                        if($count2 > 0) {

                                                        foreach($cityList as $key => $row22){                                       
                                                        ?>                              
                                                        <option value="<?php echo $row22['id'] ?>"> 
                                                            <?php echo $row22['city_name'] ?>
                                                            
                                                        </option>

                                                        <?php
                                                        }
                                                        }
                                                        ?>
                                                                        </select>
                                                                    </div>
                                                    <div class="main-search-input-item main-date-parent main-search-input-item_small txtwdth">
                                                        <input type="text" placeholder="Search for an event......" value=""/ name="search_txt">
                                                    </div>
                                                    <button class="main-search-button color2-bg">Search <i class="fal fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="col-md-5 col-lg-5 pull-right">
                                        <a href="#" class="show-reg-form add-hotel vndr modal-open5">List Your Business</a> 
                                        <?php
                                        if($this->session->userdata('userCId') ==""){
                                        ?> 
                                        <a href="#" class="show-reg-form modal-open add-hotel"><b>Login / Signup</b></a>  
                                        <?php
                                           }else{
                                        ?>
                                        <div class="header-user-menu">
                                            <div class="header-user-name">
                                            <span><img src="images/profile.jpg" alt=""></span>
                                            <?php
                                                echo "Hi ".$this->session->userdata('userCId');
                                               ?>
                                            </div>
                                            <ul>
                                                <!-- <li><a href="#"> Edit profile</a></li>
                                                <li><a href="#"> Add Listing</a></li>
                                                <li><a href="#">  Bookings  </a></li>
                                                <li><a href="#"> Reviews </a></li> -->
                                                <li><a href="<?php echo base_url() ?>MyDasboard">My Profile</a></li>
                                                <li><a href="<?php echo base_url() ?>User/logout">Log Out</a></li>
                                            </ul>
                                        </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="wishlist-wrap scrollbar-inner novis_wishlist">
                                        <div class="box-widget-content">
                                            <div class="widget-posts fl-wrap">
                                                <ul>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 short-div1 padd0">
                                <div class="col-md-7 col-sm-7">
            <!-- nav-button-wrap-->
            <div class="nav-button-wrap color-bg">
               <div class="nav-button">
                  <span></span><span></span><span></span>
               </div>
            </div>
            <!-- nav-button-wrap end-->
            <!--  navigation -->
          <!--   <div class="nav-holder main-menu"> -->
                <div class="no-js">
               <div class="cd-dropdown-wrapper cd-dropdown-wrapper2" style="left:-348px !important">
                  <span><a class="cd-dropdown-trigger" href="#" style="left:145px !important">Categories<i class="fas fa-caret-down"></i></a></span>
                  <div class="cd-dropdown cd-dropdown1" style="margin-left:143px !important">
                     <ul class="cd-dropdown-content cd-dropdown-content2">

                      <?php                                                                    
                        $count_cat = count(array_filter($menuCategoryAll));
                        if($count_cat > 0) {
                        foreach($menuCategoryAll as $key => $row_catList){   
                            $urlCode = base_url().'listing/'.$row_catList['id']                                    
                      ?>   
                        <li class="has-children">
                           <a href="<?php echo $urlCode; ?>"><span style="font-family:'Saira', Arial, Helvetica, sans-serif !important"><?php echo $row_catList['category_name']; ?></span></a>
                            <ul class="cd-dropdown-icons is-hidden" style="background-image:url('../images/menu_border_has_child.png');">
                               
                                <?php                                                                   
                                $count_subcat = count(array_filter($row_catList['subcatList']));
                                if($count_subcat > 0) {
                                foreach($row_catList['subcatList'] as $key => $row_subcatList){                 $urlCode1 = base_url().'subcatlist/'.$row_subcatList['id']   
                                ?>  
                                <li>
                                   <a class="cd-dropdown-item item-1" href="<?php echo $urlCode1; ?>">
                                      <h3><?php echo $row_subcatList['subcategory_name'] ?></h3>
                                       <?php                                                                   
              $count_subsubcat = count(array_filter($row_subcatList['subsubcatList']));
                                      if($count_subsubcat > 0) {
              foreach($row_subcatList['subsubcatList'] as $key => $row_subsubcatList){                                    
                                      ?>  
                                      <p><?php echo $row_subsubcatList['subsubcat_name'] ?></p>
                                      <?php
                                    }
                                  }
                                      ?>
                                   </a>
                                </li>

                                <?php
                                  }
                                }
                                ?>

                              <img style='margin-top:-310px;' src="<?php echo base_url() ?>assets/images/cat_banner.PNG">
                          </ul>

                        </li>
                      <?php
                        }
                      }
                      ?>
                       
                     </ul>
                    
                  </div>
                 
               </div>
               <div class="cd-dropdown-wrapper cd-dropdown-wrapper1"  style="left:92px !important">
                  <span><a class="cd-dropdown-trigger" id="triggerc" href="#">Packages<i class="fas fa-caret-down"></i></a></span>
                  <div class="cd-dropdown ">
                     <ul class="cd-dropdown-content cd-dropdown-content1">
                     <?php                                                                    
                        $count_cat = count(array_filter($menuPackageAll));
                        if($count_cat > 0) {
                        foreach($menuPackageAll as $key => $row_catList){   
                            $urlCode = base_url().'listing/'.$row_catList['id']                                    
                      ?>   
                        <li class="has-children">
                           <a href="<?php echo $urlCode; ?>"><span style="font-family:'Saira', Arial, Helvetica, sans-serif !important"><?php echo $row_catList['category_name']; ?></span></a>
                            <ul class="cd-dropdown-icons is-hidden" style="background-image:url('../images/menu_border_has_child.png');">
                               
                                <?php                                                                   
                                $count_subcat = count(array_filter($row_catList['subcatList']));
                                if($count_subcat > 0) {
                                foreach($row_catList['subcatList'] as $key => $row_subcatList){                                    
                                ?>  
                                <li>
                                   <a class="cd-dropdown-item item-1" href="#">
                                      <h3><?php echo $row_subcatList['subcategory_name'] ?></h3>
                                     
                                   </a>
                                </li>

                                <?php
                                  }
                                }
                                ?>
                          </ul>

                        </li>
                      <?php
                        }
                      }
                      ?>
                     </ul>
                    
                  </div>
                 
               </div>
               <div class="cd-dropdown-wrapper cd-dropdown-wrapper1"  style="left:399px !important">
                  <span><a class="cd-dropdown-trigger" href="#">Events<i class="fas fa-caret-down"></i></a></span>
                  <div class="cd-dropdown ">
                     <ul class="cd-dropdown-content cd-dropdown-content1">
                        <li class="has-children">
                           <a href="#"><span style="font-family:'Saira', Arial, Helvetica, sans-serif !important">Echo$ alxa</span></a>
                           <ul class="cd-dropdown-icons is-hidden">
                              <li class="go-back"><a href="#">Menu</a></li>
                              <li>
                                 <a class="cd-dropdown-item item-1" href="#">
                                    <h3>Service #1</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-2" href="#">
                                    <h3>Service #2</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-3" href="#">
                                    <h3>Service #3</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-4" href="#">
                                    <h3>Service #4</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li class="has-children">
                           <a href="#">FireTv Stick</a>
                           <ul class="cd-dropdown-icons is-hidden">
                              <li class="go-back"><a href="#">Menu</a></li>
                              <li>
                                 <a class="cd-dropdown-item item-1" href="#">
                                    <h3>Service #1</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                             
                              <li>
                                 <a class="cd-dropdown-item item-3" href="#">
                                    <h3>Service #3</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-4" href="#">
                                    <h3>Service #4</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li class="has-children">
                           <a href="#">Kindle E-Readers &amp; eBooks</a>
                           <ul class="cd-dropdown-icons is-hidden">
                              <li class="go-back"><a href="#">Menu</a></li>
                              <li>
                                 <a class="cd-dropdown-item item-1" href="#">
                                    <h3>Service #1</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-2" href="#">
                                    <h3>Service #2</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-3" href="#">
                                    <h3>Service #3</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-4" href="#">
                                    <h3>Service #4</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li class="has-children">
                           <a href="#">Amazon Prime Video</a>
                           <ul class="cd-dropdown-icons is-hidden">
                              <li class="go-back"><a href="#">Menu</a></li>
                              <li>
                                 <a class="cd-dropdown-item item-1" href="#">
                                    <h3>Service #1</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-2" href="#">
                                    <h3>Service #2</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-3" href="#">
                                    <h3>Service #3</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-4" href="#">
                                    <h3>Service #4</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li class="has-children">
                           <a href="#">Services</a>
                           <ul class="cd-dropdown-icons is-hidden">
                              <li class="go-back"><a href="#">Menu</a></li>
                              <li>
                                 <a class="cd-dropdown-item item-1" href="#">
                                    <h3>Service #1</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-2" href="#">
                                    <h3>Service #2</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-3" href="#">
                                    <h3>Service #3</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-4" href="#">
                                    <h3>Service #4</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li class="has-children">
                           <a href="#">Services</a>
                           <ul class="cd-dropdown-icons is-hidden">
                              <li class="go-back"><a href="#">Menu</a></li>
                              <li>
                                 <a class="cd-dropdown-item item-1" href="#">
                                    <h3>Service #1</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-2" href="#">
                                    <h3>Service #2</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-3" href="#">
                                    <h3>Service #3</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-4" href="#">
                                    <h3>Service #4</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li class="has-children">
                           <a href="#">Services</a>
                           <ul class="cd-dropdown-icons is-hidden">
                              <li class="go-back"><a href="#0">Menu</a></li>
                              <li>
                                 <a class="cd-dropdown-item item-1" href="#">
                                    <h3>Service #1</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                              <li>
                                 <a class="cd-dropdown-item item-2" href="#">
                                    <h3>Service #2</h3>
                                    <p>This is the item description</p>
                                 </a>
                              </li>
                           </ul>
                        </li>
                     </ul>
                   
                  </div>
                 
               </div>

            </div>
              


                                        <!-- navigation  end -->
                                    </div>
                                    <div class="col-md-5 padd0">
                                        <div class="wishlist-link brdrrt"><i class="fal fa-heart"></i><span class="wl_counter">0</span></div>
                                        <a href="<?php echo base_url() ?>Cart"><div class="wishlist-link"><i class="fal fa-shopping-cart"></i><span class="wl_counter"><?php echo $cartCount; ?></span></div></a>
                                        <a style="top: 0px; background: #828387; color: #fff !important; " href="#" class="show-reg-form add-hotel vndr">Download App</a> 
                                        <a style="top: 0px; background: #828387; color: #fff !important; " href="#" class="show-reg-form add-hotel vndr">Need Help ?</a> 
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <!--
                            header1 code end here
                        -->
                    </div>
                </div> 
            </header>
            <header class="main-header2">
               
                <div class="header-top"  id= "header-top" fl-wrap>
                    <div class="container-fluid">
                        
                        <?php include_once 'header_sticky.php';?>
                    </div>
                </div>
             
            </header> 




























