
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Keyaan:: We craete You Celebrate</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->
        <link type="text/css" rel="stylesheet" href="css/reset.css">
        <link type="text/css" rel="stylesheet" href="css/plugins.css">
        <link type="text/css" rel="stylesheet" href="css/style.css">
        <link type="text/css" rel="stylesheet" href="css/color.css">
        <link rel="stylesheet" href="css/responsive.css">
        <link rel="stylesheet" href="css/easy-responsive-tabs.css">
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="images/favicon.ico">
         <?php include_once 'chat.php';?>
    </head>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="pin">
                <div class="pulse"></div>
            </div>
        </div>
        <!--loader end-->
        <!-- Main  -->
        <div id="main">
             <a href="#" class="frilst_rgt"></a>
        <a href="#" class="frilst_rgt_support"></a>
        <a href="#" class="frilst_rgt_customize"></a>
            <!-- header-->
            <header class="main-header">
                <!-- header-top-->
                <div class="header-top fl-wrap">
                    <div class="container-fluid">
                        
                        <?php include_once './header_login.php';?>
                    </div>
                </div>
                <!-- header-top end-->
                <!-- header-inner-->
                
                <!-- header-inner end-->
                <!-- header-search -->
                
                <!-- header-search  end -->
            </header>
            <!--  header end -->
            <!--  wrapper  -->
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    <!--  section  -->
                    <section class="grey-blue-bg" data-scrollax-parent="true" id="sec1" style="padding: 40px 0 0 !important;">
                        <div class="container">
                            <div class="col-md-8">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>Package Name :: Galley</h3>
                                </div>
                                <div class="list-single-main-media fl-wrap">
                                            <!-- gallery-items   -->
                                    <div class="gallery-items grid-small-pad  list-single-gallery three-coulms lightgallery">
                                        <!-- 1 -->
                                        <div class="gallery-item ">
                                            <div class="grid-item-holder">
                                                <div class="box-item">
                                                    <img  src="images/wedding/1.jpg"   alt="">
                                                    <a href="images/wedding/1.jpg" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 1 end -->
                                        <!-- 2 -->
                                        <div class="gallery-item">
                                            <div class="grid-item-holder">
                                                <div class="box-item">
                                                    <img  src="images/wedding/2.jpg"   alt="">
                                                    <a href="images/wedding/2.jpg" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 2 end -->
                                        <!-- 3 -->
                                        <div class="gallery-item gallery-item-second">
                                            <div class="grid-item-holder">
                                                <div class="box-item">
                                                    <img  src="images/wedding/3.jpg"   alt="">
                                                    <a href="images/wedding/3.jpg" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 3 end -->
                                        <!-- 4 -->
                                        <div class="gallery-item">
                                            <div class="grid-item-holder">
                                                <div class="box-item">
                                                    <img  src="images/wedding/4.jpg"   alt="">
                                                    <a href="images/wedding/4.jpg" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 4 end -->
                                       
                                        <!-- 7 -->
                                        <div class="gallery-item">
                                            <div class="grid-item-holder">
                                                <div class="box-item">
                                                    <img  src="images/wedding/6.jpg"   alt="">
                                                    <div class="more-photos-button dynamic-gal"  data-dynamicPath="[{'src': 'images/wedding/8.jpg'}, {'src': 'images/wedding/9.jpg'},{'src': 'images/wedding/10.jpg'}]">Other <span>4 photos</span><i class="far fa-long-arrow-right"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 7 end -->
                                    </div>
                                    <!-- end gallery items -->                                          
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <div class="box-widget-item-header">
                                                <h3> Short Description</h3>
                                            </div>
                                            <p>Wedding is a lifetime bond between two individuals who are truly in love with one another
Who wish to spend their entire life being partners for each other. It is a lifetime event that has a lot of stories to tell about. Keyaan are a highly dedicated team of professionals who offer a complete package that helps 
to remain stress free for the entire wedding festival.
Keyaan provides the clients assistance in all the fields required, right from planning of the event till the actual execution of the event is taken care of by the team members</p>
                                            
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <div class="box-widget-item-header">
                                                <h3> Price</h3>
                                            </div>
                                            <div class="claim-price-wdget fl-wrap">
                                                <div class="claim-price-wdget-content fl-wrap">
                                                    <div class="pricerange fl-wrap"><span>Package Starts From : </span>Rs. 3,00,000/-</div>
                                                    <div class="claim-widget-link fl-wrap"><span>Sample Text?</span><a href="#">Check Availability</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--  section  end-->
                    <!--  section  -->
                   <section class="grey-blue-bg small-padding scroll-nav-container" id="sec2">
                        <!--  scroll-nav-wrapper  -->
                        <div class="scroll-nav-wrapper fl-wrap">
                            
                            <div class="clearfix"></div>
                            <div class="container">
                                <nav class="scroll-nav scroll-init">
                                    <ul>
                                        
                                        <li><a class="act-scrlink" href="#sec2">Day 1 Itinerary</a></li>
                                        <li><a href="#sec3">Day 2 Itinerary</a></li>
                                        
                                        <li><a href="#sec5">Day 3 Itinerary</a></li>
                                    </ul>
                                </nav>
                                <button class="btn  big-btn flat-btn float-btn color2-bg pull-right" style="margin: 0px;">Book Now<i class="fal fa-paper-plane"></i></button>
                                <a href="#" class="btn flat-btn color-bg float-btn image-popup pull-right" style="margin: 0px; margin-right: 5px;">Add TO Cart<i class="fal fa-shopping-cart"></i></a>
                            </div>
                        </div>
                        <!--  scroll-nav-wrapper end  -->                    
                        <!--   container  -->
                        <div class="container">
                            <!--   row  -->
                            <div class="row">
                                <!--   datails -->
                                <div class="col-md-12">
                                    <div class="list-single-main-container ">
                                        <!-- fixed-scroll-column  -->
                                        <div class="fixed-scroll-column">
                                            <div class="fixed-scroll-column-item fl-wrap">
                                                <div class="showshare sfcs fc-button"><i class="far fa-share-alt"></i><span>Share </span></div>
                                                <div class="share-holder fixed-scroll-column-share-container">
                                                    <div class="share-container  isShare"></div>
                                                </div>
                                                <a class="fc-button custom-scroll-link" href="#sec6"><i class="far fa-comment-alt-check"></i> <span>  Add review </span></a>
                                                <a class="fc-button" href="#"><i class="far fa-heart"></i> <span>Save</span></a>
                                                <a class="fc-button" href="cart.php"><i class="far fa-bookmark"></i> <span> Book Now </span></a>
                                            </div>
                                        </div>
                                       
                                        <div class="list-single-main-item fl-wrap" id="sec2">
                                            <div class="list-single-main-item-title fl-wrap">
                                                <h3>Day 1 Itinerary</h3>
                                            </div>
                                            <?php include './tabbing_vertical.php';?>
                                        </div>
                                        <div class="list-single-main-item fl-wrap" id="sec3">
                                            <div class="list-single-main-item-title fl-wrap">
                                                <h3>Day 2 Itinerary</h3>
                                            </div>
                                            <?php include './tabbing_hor_vertical.php';?>
                                        </div>
                                        <div class="list-single-main-item fl-wrap" id="sec5">
                                            <div class="list-single-main-item-title fl-wrap">
                                                <h3>Day 3 Itinerary</h3>
                                            </div>
                                            <?php include './tabbing_vertical1.php';?>
                                        </div>
                                                                      
                                    </div>
                                </div>
                                <!--   datails end  -->
                               
                            </div>
                            <!--   row end  -->
                        </div>
                        <!--   container  end  -->
                    </section>
                    <!--  section  end-->
                </div>
                <!-- content end-->
                <div class="limit-box fl-wrap"></div>
            </div>
            <!--wrapper end -->
             <footer class="main-footer">
                <?php include_once './footer.php';?>
            </footer>
            <!--footer end -->
            <!--map-modal -->
            
            <!--map-modal end -->           
            <!--register form -->
            <?php include_once 'login.php';?>
            <a class="to-top"><i class="fas fa-caret-up"></i></a>
            <!--ajax-modal-container-->
            <div class="ajax-modal-overlay"></div>
            <div class="ajax-modal-container">
                <!--ajax-modal -->
                <div class='ajax-loader'>
                    <div class='ajax-loader-cirle'></div>
                </div>
                <div id="ajax-modal" class="fl-wrap"> 
                </div>
                <!--ajax-modal-container end -->
            </div>
            <!--ajax-modal-container end -->
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
                 <script type="text/javascript">
    $(document).ready(function() {

        //Vertical Tab
        $('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
        $('#parentVerticalTab1').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
        $('#parentVerticalTab2').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
    });
</script>
        <script src="js/easyResponsiveTabs.js"></script>     
    </body>
</html>