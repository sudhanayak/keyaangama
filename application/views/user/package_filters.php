 <div class="mobile-list-controls fl-wrap">
    <div class="mlc show-list-wrap-search fl-wrap"><i class="fal fa-filter"></i> Filter</div>
</div>
<div class="fl-wrap filter-sidebar_item fixed-bar">
    <div class="filter-sidebar fl-wrap lws_mobile">
        <div class="col-list-search-input-item fl-wrap">
            <label>Price</label>
            <div class="search-opt-container fl-wrap">
                <!-- Checkboxes -->
                <ul class="fl-wrap filter-tags">
                    <li>
                        <input id="price1" type="checkbox" name="venue_price" class="venue_price pricelist_filter" value="1">
                        <label for="price1">0 - 5000</label>
                    </li>
                    <li>
                        <input id="price2" type="checkbox" name="venue_price" class="venue_price pricelist_filter" value="2">
                        <label for="price2">5001 - 10000</label>
                    </li>
                    <li>
                        <input id="price3" type="checkbox" name="venue_price" class="venue_price pricelist_filter" value="3">
                        <label for="price3">10001 - 50000</label>
                    </li>
                    <li>
                        <input id="price4" type="checkbox" name="venue_price" class="venue_price pricelist_filter" value="4">
                        <label for="price4">50001 - 100000</label>
                    </li>
                    <li>
                        <input id="price5" type="checkbox" name="venue_price" class="venue_price pricelist_filter" value="5">
                        <label for="price5">100001 - 5000000</label>
                    </li>
                </ul>
                <!-- Checkboxes end -->
                <!-- <button class="header-search-button wdthauto pull-right" onclick="window.location.href='#'">View More</button> -->
            </div>
        </div>
        <!--col-list-search-input-item  -->                                         
        <!-- <div class="col-list-search-input-item fl-wrap">
            <button class="header-search-button" onclick="window.location.href='listing.html'">Search <i class="far fa-search"></i></button>
        </div> -->
        <!--col-list-search-input-item end--> 
    </div>
</div>