 <div class="mobile-list-controls fl-wrap">
                                        <div class="mlc show-list-wrap-search fl-wrap"><i class="fal fa-filter"></i> Filter</div>
                                    </div>
                                    <div class="fl-wrap filter-sidebar_item fixed-bar">
                                        <div class="filter-sidebar fl-wrap lws_mobile">
                                            <!--col-list-search-input-item -->
                                            <div class="col-list-search-input-item fl-wrap">
                                                <label>Venue Type</label>
                                                <div class="search-opt-container fl-wrap">
                                                    <!-- Checkboxes -->
                                                    <ul class="fl-wrap filter-tags">
                                                        <?php 
                                                        foreach($venueTypes as $key=>$field) { ?>
                                                        <li>
                                                            <input id="pathway" type="checkbox" class="venue_type" name="venue_type[]" value="<?php echo $field['id']; ?>">
                                                            <label for="pathway"><?php echo $field['venue_type']; ?></label>
                                                        </li>
                                                        <?php } ?>
                                                    </ul>
                                                    <!-- Checkboxes end -->
                                                    <!-- <button class="header-search-button wdthauto pull-right" onclick="window.location.href='#'">View More</button> -->
                                                </div>
                                            </div> 
                                            
                                            <div class="col-list-search-input-item fl-wrap">
                                                <label>Price</label>
                                                <div class="search-opt-container fl-wrap">
                                                    <!-- Checkboxes -->
                                                    <ul class="fl-wrap filter-tags">
                                                        <li>
                                                            <input id="price1" type="checkbox" name="venue_price" class="venue_price pricelist_filter" value="1">
                                                            <label for="price1">0 - 5000</label>
                                                        </li>
                                                        <li>
                                                            <input id="price2" type="checkbox" name="venue_price" class="venue_price pricelist_filter" value="2">
                                                            <label for="price2">5001 - 10000</label>
                                                        </li>
                                                        <li>
                                                            <input id="price3" type="checkbox" name="venue_price" class="venue_price pricelist_filter" value="3">
                                                            <label for="price3">10001 - 50000</label>
                                                        </li>
                                                        <li>
                                                            <input id="price4" type="checkbox" name="venue_price" class="venue_price pricelist_filter" value="4">
                                                            <label for="price4">50001 - 100000</label>
                                                        </li>
                                                        <li>
                                                            <input id="price5" type="checkbox" name="venue_price" class="venue_price pricelist_filter" value="5">
                                                            <label for="price5">100001 - 5000000</label>
                                                        </li>
                                                    </ul>
                                                    <!-- Checkboxes end -->
                                                    <!-- <button class="header-search-button wdthauto pull-right" onclick="window.location.href='#'">View More</button> -->
                                                </div>
                                            </div> 
                                            <div class="col-list-search-input-item fl-wrap">
                                                <label>Audience</label>
                                                <div class="search-opt-container fl-wrap">
                                                    <!-- Checkboxes -->
                                                    <ul class="fl-wrap filter-tags">
                                                        <li>
                                                            <input id="200-500" type="checkbox" name="audience" class="audience" value="1">
                                                            <label for="200-500">200 - 500</label>
                                                        </li>
                                                        <li>
                                                            <input id="500-1000" type="checkbox" name="audience" class="audience" value="2">
                                                            <label for="500-1000">500 - 1000</label>
                                                        </li>
                                                        <li>
                                                            <input id="1000-1500" type="checkbox" name="audience" class="audience" value="3">
                                                            <label for="1000-1500">1000 - 1500</label>
                                                        </li>
                                                        <li>
                                                            <input id="1500-2000" type="checkbox" name="audience" class="audience" value="4">
                                                            <label for="1500-2000">1500 - 2000</label>
                                                        </li>
                                                    </ul>
                                                    <!-- Checkboxes end -->
                                                    <!-- <button class="header-search-button wdthauto pull-right" onclick="window.location.href='#'">View More</button> -->
                                                </div>
                                            </div> 
                                            <!--col-list-search-input-item end-->                      
                                                                               
                                            <!--col-list-search-input-item -->
                                            <div class="col-list-search-input-item fl-wrap">
                                                <label>Star Rating</label>
                                                <div class="search-opt-container fl-wrap">
                                                    <!-- Checkboxes -->
                                                    <ul class="fl-wrap filter-tags">
                                                        <li class="five-star-rating">
                                                            <input value="5" id="check-aa2" type="checkbox" name="rating_filter[]" >
                                                            <label for="check-aa2"><span class="listing-rating card-popup-rainingvis venue_rating" data-starrating2="5"><span>5 Stars</span></span></label>
                                                        </li>
                                                        <li class="four-star-rating">
                                                            <input value="4" id="check-aa3" type="checkbox" name="rating_filter[]">
                                                            <label for="check-aa3"><span class="listing-rating card-popup-rainingvis venue_rating" data-starrating2="5"><span>4 Star</span></span></label>
                                                        </li>
                                                        <li class="three-star-rating"> 
                                                            <input value="3" id="check-aa4" type="checkbox" name="rating_filter[]">
                                                            <label for="check-aa4"><span class="listing-rating card-popup-rainingvis venue_rating" data-starrating2="5"><span>3 Star</span></span></label>
                                                        </li>
                                                    </ul>
                                                    <!-- Checkboxes end -->
                                                </div>
                                            </div>
                                            <!--col-list-search-input-item end-->
                                            <div class="col-list-search-input-item fl-wrap">
                                                <label>Facility</label>
                                                <div class="search-opt-container fl-wrap">
                                                    <!-- Checkboxes -->
                                                    <ul class="fl-wrap filter-tags">
                                                        <?php 
                                                        foreach($facilities as $key=>$field) { ?>
                                                        <li>
                                                            <input id="pathway" type="checkbox" class="facility" name="facility[]" value="<?php echo $field['id']; ?>">
                                                            <label for="pathway"><?php echo $field['faciility_name']; ?></label>
                                                        </li>
                                                        <?php } ?>
                                                    </ul>
                                                    <!-- Checkboxes end -->
                                                    <!-- <button class="header-search-button wdthauto pull-right" onclick="window.location.href='#'">View More</button> -->
                                                </div>
                                            </div>
                                            <!--col-list-search-input-item end-->  
                                            <!--col-list-search-input-item  -->                                         
                                            <!-- <div class="col-list-search-input-item fl-wrap">
                                                <button class="header-search-button" onclick="window.location.href='listing.html'">Search <i class="far fa-search"></i></button>
                                            </div> -->
                                            <!--col-list-search-input-item end--> 
                                        </div>
                                    </div>