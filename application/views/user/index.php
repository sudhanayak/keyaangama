<?php
include_once'header1.php';
?>
            <!--  header end -->
            <!--  wrapper  -->
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    <!--section -->
                    <section class="hero-section" data-scrollax-parent="true" id="sec1">
                        <div class="hero-parallax">
                        <div class="slideshow-container" data-scrollax="properties: { translateY: '200px' }" >
                            <!-- slideshow-item --> 
                            <?php               
                            $count2 = count(array_filter($bannerList));         
                            if($count2 > 0) {
                                $i=0;
                                $c=0;
                                foreach($bannerList as $key => $row2){  
                                    ?>
                                        <div class="slideshow-item">
                                            <div class="bg"  data-bg="<?php echo base_url(); ?>uploads/banners/<?php echo $row2['image'] ?>"></div>
                                        </div>
                                        <!--  slideshow-item end  -->   
                                    <?php
                                }
                            }

                            ?>
                                                    
                        </div>
                            <div class="overlay op7"></div>
                        </div>
                        <?php include_once 'banner_search_icons.php';?>
                        <div class="header-sec-link">
                            <div class="container"><a href="#sec2" class="custom-scroll-link color-bg"><i class="fal fa-angle-double-down"></i></a></div>
                        </div>
                    </section>
                    <!-- section end -->
                     <!--section -->
                    <section id="sec2">
                        <div class="container">
                                <div class="section-title">
                                    <div class="section-title-separator"><span></span></div>
                                    <h2>Our Packages <span></span></h2>
                                    <span class="section-separator"></span>
                               </div>
                            </div>
                            <!-- portfolio start -->
                            <div class="container">
                               
                                <div class="row">
                                <?php
                                $i = 0;
                                $count = count(array_filter($package));
                                if($count > 0) {
                                    foreach($package as $key => $row){
                                        $i++;
                                        if($i < 9) {
                                ?>
                                    <div class="col-sm-3">
                                        <div class="listing-item list_wdth">
                                            <article class="geodir-category-listing fl-wrap shdw">
                                                <div class="geodir-category-img">
                                                    <a href="<?php echo base_url(); ?>packagelist/<?php echo $row['id'];?>"><img src="<?php echo $row['packageImg'];?>" alt=""></a>

                                                   
                                                    <div class="geodir-category-opt">
                                                        <div class="listing-rating card-popup-rainingvis" style="color: #fff;">27 Reviews </div>
                                                        <div class="rate-class-name">
                                                            
                                                            <span>5.0</span>                                             
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-content fl-wrap title-sin_item">
                                                    <div class="geodir-category-content-title fl-wrap">
                                                        <div class="geodir-category-content-title-item">
                                                            <h3 class="title-sin_map"><a href="<?php echo base_url(); ?>packagelist/<?php echo $row['id'];?>"><?php echo $row['category_name'];?></a></h3>
                                                            <div class="geodir-category-location fl-wrap"><a href="<?php echo base_url(); ?>packageListing/<?php echo $row['id'];?>" class="map-item">Starts From:</a> <span class="pull-right text-right fnt15">Rs. <?php echo $row['base_price'];?>/-</span></div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                    <?php } } }?>
                                </div>
                            </div>
                            <!-- portfolio end -->
                                
                    </section>
                    <!-- section end -->
                    <!-- section-->
                     
                    <section class="grey-blue-bg">
                        <!-- container-->
                        <div class="container">
                            <div class="section-title">
                                <div class="section-title-separator"><span></span></div>
                                <h2>Why Choose Us</h2>
                                <span class="section-separator"></span>
                                
                            </div>
                        </div>
                        <div class="container">
                            <!-- container end-->
                            <div class="row">
                                <div class="col-md-4">
                                    <!-- process-item-->
                                    <div class="process-item big-pad-pr-item">
                                        <span class="process-count"> </span>
                                        <div class="time-line-icon"><i class="fal fa-headset"></i></div>
                                        <h4><a href="#"> Best service guarantee</a></h4>
                                        <p>Proin dapibus nisl ornare diam varius tempus. Aenean a quam luctus, finibus tellus ut, convallis eros sollicitudin turpis.</p>
                                    </div>
                                    <!-- process-item end -->
                                </div>
                                <div class="col-md-4">
                                    <!-- process-item-->
                                    <div class="process-item big-pad-pr-item">
                                        <span class="process-count"> </span>
                                        <div class="time-line-icon"><i class="fal fa-gift"></i></div>
                                        <h4> <a href="#">Exclusive gifts</a></h4>
                                        <p>Proin dapibus nisl ornare diam varius tempus. Aenean a quam luctus, finibus tellus ut, convallis eros sollicitudin turpis.</p>
                                    </div>
                                    <!-- process-item end -->                                
                                </div>
                                <div class="col-md-4">
                                    <!-- process-item-->
                                    <div class="process-item big-pad-pr-item nodecpre">
                                        <span class="process-count"> </span>
                                        <div class="time-line-icon"><i class="fal fa-credit-card"></i></div>
                                        <h4><a href="#"> Get more from your card</a></h4>
                                        <p>Proin dapibus nisl ornare diam varius tempus. Aenean a quam luctus, finibus tellus ut, convallis eros sollicitudin turpis.</p>
                                    </div>
                                    <!-- process-item end -->                                
                                </div>
                            </div>
                            <!--process-wrap   end-->
                            <div class=" single-facts fl-wrap mar-top">
                                <!-- inline-facts -->
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <i class="fal fa-users"></i>
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="254">154</div>
                                            </div>
                                        </div>
                                        <h6>New Visiters Every Week</h6>
                                    </div>
                                </div>
                                <!-- inline-facts end -->
                                <!-- inline-facts  -->
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <i class="fal fa-thumbs-up"></i>
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="12168">12168</div>
                                            </div>
                                        </div>
                                        <h6>Happy customers every year</h6>
                                    </div>
                                </div>
                                <!-- inline-facts end -->
                                <!-- inline-facts  -->
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <i class="fal fa-award"></i>
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="172">172</div>
                                            </div>
                                        </div>
                                        <h6>Won Awards</h6>
                                    </div>
                                </div>
                                <!-- inline-facts end -->
                                <!-- inline-facts  -->
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <i class="fal fa-hotel"></i>
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="732">732</div>
                                            </div>
                                        </div>
                                        <h6>New Listing Every Week</h6>
                                    </div>
                                </div>
                                <!-- inline-facts end -->
                            </div>
                        </div>
                    </section>
                   <section class="sec3">
                        <!-- container-->
                        <div class="container">
                            <div class="section-title">
                                <div class="section-title-separator"><span></span></div>
                                <h2>Top Rated Vendors</h2>
                                <span class="section-separator"></span>
                                
                            </div>
                        </div>
                        <!-- container end-->
                        <!-- carousel -->
                        <div class="list-carousel fl-wrap card-listing ">
                            <!--listing-carousel-->
                            <div class="listing-carousel  fl-wrap ">
                                <!--slick-slide-item-->
                                <?php
                                $i = 0;
                                $count = count(array_filter($result));
                                if($count > 0) {
                                    foreach($result as $key => $row){
                                ?>
                                <div class="slick-slide-item">
                                   
                                    <!-- listing-item  -->
                                    <div class="listing-item">
                                        <article class="geodir-category-listing fl-wrap">
                                            <div class="geodir-category-img">
                                                <a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>"><img src="<?php echo $row['image']?>" style="height:273px" alt=""></a>
                                                <div class="listing-avatar"><a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>"><img src="<?php echo base_url(); ?>assets/images/keyaan_latest_logo2.jpg" alt=""></a>
                                                    <span class="avatar-tooltip">View <strong class="vndrclr"><?php echo $row['name']?></strong></span>
                                                </div>
                                                
                                                <div class="geodir-category-opt">
                                                    <div class="listing-rating card-popup-rainingvis" style="color: #fff;">27 Reviews </div>
                                                    <div class="rate-class-name">

                                                        <span>5.0</span>                                             
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="geodir-category-content fl-wrap title-sin_item">
                                                <div class="geodir-category-content-title fl-wrap">
                                                    <div class="geodir-category-content-title-item">
                                                        <h3 class="title-sin_map"><a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>"><?php echo $row['name']?></a></h3>
                                                        <div class="geodir-category-location fl-wrap"><a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>" class="map-item">Specialized: <?php echo $row['subcategory']?>, <?php echo $row['subsubcategory']?></a></div>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="geodir-category-footer fl-wrap">
                                                    <div class="geodir-category-price">Starts From: <span> Rs. <?php echo $row['category_base_price']?></span></div>
                                                    <div class="geodir-opt-list">
                                                        <a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>" class="geodir-js-favorite"><i class="fal fa-heart"></i><span class="geodir-opt-tooltip">Save</span></a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- listing-item end -->
                                    
                                </div>
                                <?php
                                $i++; }
                                }else{
                                    echo "No listing found";
                                }
                                ?>
                                <!--slick-slide-item end-->
                                
                            </div>
                            <!--listing-carousel end-->
                            <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
                            <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>
                        </div>
                        <!--  carousel end-->
                    </section>
                    <!-- section end -->
                   
                  
                    
                    <!-- section end-->
                    <section class="parallax-section" data-scrollax-parent="true">
                        <div class="bg"  data-bg="<?php echo base_url(); ?>assets/images/bg/14.jpg" data-scrollax="properties: { translateY: '100px' }"></div>
                        <div class="overlay"></div>
                        <!--container-->
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8">
                                    <!-- column text-->
                                    <div class="colomn-text fl-wrap">
                                        <div class="colomn-text-title">
                                            <h3>Run and grow your business with KEYAAN from any where</h3>
                                            <a href="#" class="btn  color-bg float-btn">Join As Vendor<i class="fal fa-plus"></i></a>
                                        </div>
                                    </div>
                                    <!--column text   end-->
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- section end -->
                    <!--section -->                       
                    <section class=" middle-padding listar-spanishresturent">
                        <div class="container">
                            <div class="section-title">
                                <div class="section-title-separator"><span></span></div>
                                <h2>About Keyaan</h2>
                                <span class="section-separator"></span>
                                
                            </div>
                            <div class="row home-posts">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="listar-spanishresturentbox">
                                            <div class="listar-spanishresturentcontent">
                                                    <h2>About Keyaan</h2>
                                                    <div class="listar-description">
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut Aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit</p>
                                                    </div>
                                                    <a href="#" class="btn    color-bg ">Read More<i class="fas fa-caret-right"></i></a>
                                            </div>
                                    </div>
                            </div>
                            </div>
                            
                        </div>
                        <div class="section-decor"></div>
                    </section>
                </div>
                <!-- content end-->
            </div>
            <!--wrapper end -->
            <!--footer -->
            <?php
            include_once'footer.php';
            ?>