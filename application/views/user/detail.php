<?php include_once 'header1.php';?>
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    <!--  section  -->
                    <section class="grey-blue-bg" data-scrollax-parent="true" id="sec1" style="padding: 40px 0 0 !important;">
                        <div class="container">
                            <div class="col-md-8">
                                <div class="list-single-main-item-title fl-wrap" style="padding-bottom: 0px;">
                                    <h3>Vendor / Company Name:: <?php echo $vendor_name; ?></h3>
                                </div>
                                 <div class="listing-item-container init-grid-items fl-wrap">
                                    <?php
                                        $i = 0;
                                        $count = count(array_filter($resultTheme));
                                        if($count > 0) {
                                            foreach($resultTheme as $key => $row){
                                    ?>
                                    <div class="col-md-4 paddlr5">
                                                     <!-- listing-item  -->
                                        <div class="listing-item wdth100">
                                            <article class="geodir-category-listing fl-wrap">
                                                <div class="geodir-category-img">
                                                    <a href="#"><img src="<?php echo base_url()."uploads/thumbnail/bannerimage/".$row['theme_banner']; ?>" alt=""></a>
                                                    

                                                    <div class="geodir-category-opt">
                                                        <div class="listing-rating card-popup-rainingvis pull-right" style="color: #fff;">27 Photos</div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="geodir-category-content fl-wrap title-sin_item">
                                                    <div class="geodir-category-content-title fl-wrap">
                                                        <div class="geodir-category-content-title-item">
                                                            <h3 class="title-sin_map"><a href="#"><?php echo $row['theme_name']; ?></a></h3>
                                                            
                                                        </div>
                                                        <p><?php echo substr($row['theme_detail'],0,20); ?></p>
                                                    </div>
                                                    
                                                    <form method="POST">
                                                    <div class="geodir-category-footer fl-wrap">
                                                        <div class="geodir-category-price"><span> Rs. <?php echo $row['price']; ?></span></div>
                                                        <input type='hidden' value="<?php echo $row['vendor_code']; ?>" class='vendorId1'>
                                                        <input type='hidden' id="theme<?php echo $row['id']; ?>" value="<?php echo $row['id']; ?>" class='themeId1'>
                                                        <input type="hidden" id="price<?php echo $row['id']; ?>" class="theme_price1" value="<?php echo $row['price']; ?>"/>
                                                        <input type='hidden' value="<?php echo @$this->session->userdata('userCCode'); ?>" class='userId1'>
                                                        <div class="geodir-opt-list">
                                                            <?php if($this->session->userdata('userCId') !=""){ ?>
                                                            <a href="#" class="geodir-js-favorite addToCart1" id="<?php echo $row['id']; ?>"><i class="fal fa-shopping-cart"></i><span class="geodir-opt-tooltip">Add To Cart</span></a>
                                                            <?php } else { ?>
                                                                <a href="#" class="geodir-js-favorite modal-open" ><i class="fal fa-shopping-cart"></i><span class="geodir-opt-tooltip">Add To Cart</span></a>
                                                            <?php } ?>
                                                            <a href="#"
                                                             class="modal-open4 show-reg-form modal-view-detail add-hotel geodir-js-booking" themeId="<?php echo $row['id']; ?>" style="top: 0px !important; padding: 0px;"><i class="fal fa-eye"></i><span class="geodir-opt-tooltip">View Details</span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                            </article>
                                        </div>
                                        <!-- listing-item end -->
                                    </div> 
                                    <?php } } else { ?>
                                        <div class="col-md-4 paddlr5">
                                        <!-- listing-item  -->
                                            <div class="listing-item wdth100">
                                                <article class="geodir-category-listing fl-wrap">
                                                    <div class="geodir-category-img">
                                                        <a href="#"><img src="<?php echo base_url()."uploads/basic_image/".$result[0]['basic_image']; ?>" alt=""></a>
                                                        <!-- <div class="geodir-category-opt">
                                                            <div class="listing-rating card-popup-rainingvis pull-right" style="color: #fff;">27 Photos</div>
                                                        </div> -->
                                                    </div>
                                                    <div class="geodir-category-content fl-wrap title-sin_item">
                                                        <div class="geodir-category-content-title fl-wrap">
                                                            <div class="geodir-category-content-title-item">
                                                                <h3 class="title-sin_map"><a href="#"><?php echo $result[0]['name']; ?></a></h3>
                                                            </div>
                                                            <p><?php echo substr($result[0]['vendor_detail'],0,20); ?></p>
                                                        </div>
                                                        
                                                        <form method="POST">
                                                        <div class="geodir-category-footer fl-wrap">
                                                            <div class="geodir-category-price"><span> Rs. <?php echo $result[0]['category_base_price']; ?></span></div>
                                                            <div class="geodir-opt-list">
                                                                <!-- <a href="#" class="geodir-js-favorite" ><i class="fal fa-shopping-cart"></i><span class="geodir-opt-tooltip">Add To Cart</span></a> -->
                                                                <!-- <a href="#"
                                                                    class="modal-open4 show-reg-form modal-view-detail add-hotel geodir-js-booking" themeId="<?php echo $row['id']; ?>" style="top: 0px !important; padding: 0px;"><i class="fal fa-eye"></i><span class="geodir-opt-tooltip">View Details</span></a> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </form>
                                                </article>
                                            </div>
                                            <!-- listing-item end -->
                                        </div> 
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <div class="box-widget-item-header">
                                                <h3> Short Description</h3>
                                            </div>
                                            <p><?php echo $result[0]['vendor_detail']; ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <div class="box-widget-item-header">
                                                <h3> Price</h3>
                                            </div>
                                            <div class="claim-price-wdget fl-wrap">
                                                <div class="claim-price-wdget-content fl-wrap">
                                                    <div class="pricerange fl-wrap"><span>Starts From : </span>Rs. <?php echo $result[0]['category_base_price']; ?>/-</div>

                                                    <div class="claim-widget-link fl-wrap">
                                                        <form method="POST">
                                                             <input type='hidden' value="<?php echo $result[0]['vendor_code']; ?>" class='listId'>
                                                             <input type="hidden" class="amount"   value="<?php echo $result[0]['category_base_price']; ?>"/>
                                                             <input type='hidden' value="<?php echo @$this->session->userdata('userCCode'); ?>" class='userId'>
                                                             <?php
                                                            $i = 0;
                                                            $count = count(array_filter($resultTheme));
                                                            if($count > 0) {
                                                        if($this->session->userdata('userCId') !=""){
                                                        ?> 
                                                        <!-- <a id='addToCart' style="top: 0px; background: #828387; color: #fff; " href="#" class="show-reg-form add-hotel color2-bg">Add To Cart</a> -->
                                                         <?php
                                                     }else{
                                                         ?>
                                                         <!-- <a  style="top: 0px; background: #828387; color: #fff; " href="#" class="show-reg-form add-hotel color2-bg modal-open">Add To Cart</a> -->
                                                         
                                                         <?php
                                                     } }
                                                         ?>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--  section  end-->
                    <!--  section  -->
                    <section class="grey-blue-bg small-padding scroll-nav-container" id="sec2">
                        <!--  scroll-nav-wrapper  -->
                        <div class="scroll-nav-wrapper fl-wrap">
                            
                            <div class="clearfix"></div>
                            <div class="container ">
                                <nav class="scroll-nav scroll-init">
                                    <ul>
                                        
                                        <li><a class="act-scrlink" href="#sec2">About Vendor</a></li>
                                        <li><a href="#sec3">Vendor Protfolio</a></li>
                                        
                                        <li><a href="#sec5">Reviews</a></li>
                                    </ul>
                                </nav>
                                
                            </div>
                        </div>
                        <!--  scroll-nav-wrapper end  -->                    
                        <!--   container  -->
                        <div class="container mt2">
                            <!--   row  -->
                            <div class="row">
                                <!--   datails -->
                                <div class="col-md-8">
                                    <div class="list-single-main-container ">
                                        <!-- fixed-scroll-column  -->
                                        <div class="fixed-scroll-column">
                                            <div class="fixed-scroll-column-item fl-wrap">
                                                <div class="showshare sfcs fc-button"><i class="far fa-share-alt"></i><span>Share </span></div>
                                                <div class="share-holder fixed-scroll-column-share-container">
                                                    <div class="share-container  isShare"></div>
                                                </div>
                                                <a class="fc-button custom-scroll-link" href="#sec6"><i class="far fa-comment-alt-check"></i> <span>  Add review </span></a>
                                                <a class="fc-button" href="#"><i class="far fa-heart"></i> <span>Save</span></a>
                                                <!-- <a class="fc-button" href="cart.php"><i class="far fa-bookmark"></i> <span> Book Now </span></a> -->
                                            </div>
                                        </div>
                                       
                                        <!--<div class="list-single-facts fl-wrap">
                                            
                                            <div class="inline-facts-wrap">
                                                <div class="inline-facts">
                                                    <i class="fal fa-users"></i>
                                                    <div class="milestone-counter">
                                                        <div class="stats animaper">
                                                        <?php echo $result[0]['themeCount']; ?>
                                                        </div>
                                                    </div>
                                                    <h6>Events</h6>
                                                </div>
                                            </div>
                                           
                                            <?php      
                                            $count = count(array_filter($themelistcount));
                                            if($count > 0) {
                                            $i = 0;
                                            foreach($themelistcount as $key => $row){
                                            if($i<2) {
                                            $i++;
                                            ?>
                                           
                                            <div class="inline-facts-wrap">
                                                <div class="inline-facts">
                                                    <i class="fal fa-user"></i>
                                                    <div class="milestone-counter">
                                                        <div class="stats animaper">
                                                        <?php echo $row['themelistCount']; ?>
                                                        </div>
                                                    </div>
                                                    <h6><?php echo $row['category']; ?></h6>
                                                </div>
                                            </div>
                                            
                                            <?php } } }?>
                                            
                                            
                                            <div class="inline-facts-wrap">
                                                <div class="inline-facts">
                                                    <i class="fal fa-cocktail"></i>
                                                    <div class="milestone-counter">
                                                        <div class="stats animaper">7
                                                        </div>
                                                    </div>
                                                    <h6>Other Events</h6>
                                                </div>
                                            </div>
                                                                                                              
                                        </div>-->

                                        <!--   list-single-main-item -->
                                        <div class="list-single-main-item fl-wrap">
                                            <div class="list-single-main-item-title fl-wrap">
                                                <h3>About Vendor </h3>
                                            </div>
                                            <p><?php echo $result[0]['about_vendor']; ?></p>
                                            <?php
                                                if($result['vendorimgList'] != "") { 
                                                    foreach ($result['vendorimgList'] as $key => $img) {
                                                        if($img['media_type'] == 2) { 
                                            ?>
                                            
<iframe width="560" height="315" src="<?php echo $img['image']; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

                                            <?php } } } ?>
                                        </div>
                                        <!--   list-single-main-item end -->
                                        <!--   list-single-main-item -->
                                        <div class="list-single-main-item fl-wrap" id="sec3">
                                            <div class="list-single-main-item-title fl-wrap">
                                                <h3>Vendor Portfolio</h3>
                                            </div>
                                            <div class="gallery-items fl-wrap mr-bot spad home-grid">
                                <!-- gallery-item-->
                                <?php
                                                    if($result['vendorimgList'] != "") { 
                                                        foreach ($result['vendorimgList'] as $key => $img) {
                                                            if($img['media_type'] == 1) {
                                                ?>
                                                <div class="gallery-item col-md-6 wdth50">
                                                    <div class="grid-item-holder">
                                                        <div class="listing-item-grid">
                                                            <!-- <div class="listing-counter tl0"><span>79 </span> Photos</div> -->
                                                            <img  src="<?php echo $img['image']; ?>"   alt="">
                                                            <!-- <div class="listing-item-cat fnt_vend_list">
                                                                
                                                               
                                                                <div class="clearfix"></div>
                                                                <p class="fnt_list_clr"><?php echo $row['theme_name']; ?></p>
                                                            </div> -->
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php } } } ?>
                                            </div>
                                            
                                        </div>
                                        <!--   list-single-main-item end -->     
                                       
                                        <!-- list-single-main-item -->   
                                        <div class="list-single-main-item fl-wrap" id="sec5">
                                            <div class="list-single-main-item-title fl-wrap">
                                                <h3>successful Reviews -  <span> 2 </span></h3>
                                            </div>
                                            <!--reviews-score-wrap-->   
                                            <div class="reviews-score-wrap fl-wrap">
                                                <div class="review-score-total">
                                                    <span>
                                                    4.5
                                                    <strong>Very Good</strong>
                                                    </span>
                                                    
                                                </div>
                                                <div class="review-score-detail">
                                                    <!-- review-score-detail-list-->
                                                    <div class="review-score-detail-list">
                                                        <!-- rate item-->
                                                        <div class="rate-item fl-wrap">
                                                            <div class="rate-item-title fl-wrap"><span>Dedication</span></div>
                                                            <div class="rate-item-bg" data-percent="100%">
                                                                <div class="rate-item-line color-bg"></div>
                                                            </div>
                                                            <div class="rate-item-percent">5.0</div>
                                                        </div>
                                                        <!-- rate item end-->
                                                        <!-- rate item-->
                                                        <div class="rate-item fl-wrap">
                                                            <div class="rate-item-title fl-wrap"><span>Execution</span></div>
                                                            <div class="rate-item-bg" data-percent="90%">
                                                                <div class="rate-item-line color-bg"></div>
                                                            </div>
                                                            <div class="rate-item-percent">5.0</div>
                                                        </div>
                                                        <!-- rate item end-->                                                        
                                                        <!-- rate item-->
                                                        <div class="rate-item fl-wrap">
                                                            <div class="rate-item-title fl-wrap"><span>New Innovations</span></div>
                                                            <div class="rate-item-bg" data-percent="80%">
                                                                <div class="rate-item-line color-bg"></div>
                                                            </div>
                                                            <div class="rate-item-percent">4.0</div>
                                                        </div>
                                                        <!-- rate item end-->  
                                                        <!-- rate item-->
                                                        <div class="rate-item fl-wrap">
                                                            <div class="rate-item-title fl-wrap"><span>Customer Satisfaction</span></div>
                                                            <div class="rate-item-bg" data-percent="90%">
                                                                <div class="rate-item-line color-bg"></div>
                                                            </div>
                                                            <div class="rate-item-percent">4.5</div>
                                                        </div>
                                                        <!-- rate item end--> 
                                                    </div>
                                                    <!-- review-score-detail-list end-->
                                                </div>
                                            </div>
                                            <!-- reviews-score-wrap end -->   
                                            <div class="reviews-comments-wrap">
                                                <!-- reviews-comments-item -->  
                                                <div class="reviews-comments-item">
                                                    <div class="review-comments-avatar">
                                                        <img src="<?php echo base_url(); ?>assets/images/avatar/2.jpg" alt=""> 
                                                    </div>
                                                    <div class="reviews-comments-item-text">
                                                        <h4><a href="#">Customer Name</a></h4>
                                                        <div class="review-score-user">
                                                            <span>4.4</span>
                                                            <strong>Good</strong>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <p>" Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. "</p>
                                                        <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>12 April 2018</span><a href="#"><i class="fal fa-reply"></i> Reply</a></div>
                                                    </div>
                                                </div>
                                                <!--reviews-comments-item end--> 
                                                <!-- reviews-comments-item -->  
                                                <div class="reviews-comments-item">
                                                    <div class="review-comments-avatar">
                                                        <img src="<?php echo base_url(); ?>assets/images/avatar/5.jpg" alt=""> 
                                                    </div>
                                                    <div class="reviews-comments-item-text">
                                                        <h4><a href="#">Customer Name</a></h4>
                                                        <div class="review-score-user">
                                                            <span>4.7</span>
                                                            <strong>Very Good</strong>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <p>" Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc posuere convallis purus non cursus. Cras metus neque, gravida sodales massa ut. "</p>
                                                        <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>03 December 2017</span><a href="#"><i class="fal fa-reply"></i> Reply</a></div>
                                                    </div>
                                                </div>
                                                <!--reviews-comments-item end-->                                                                  
                                            </div>
                                        </div>
                                        <!-- list-single-main-item end -->

                                           <a style="top: 0px; background: #828387; color: #fff; " href="#" class="show-reg-form add-hotel vndr">Need Help ?</a>                         
                                    </div>
                                </div>
                                <!--   datails end  -->
                                <!--   sidebar  -->
                                <div class="col-md-4">
                                    <!--box-widget-wrap -->  
                                    <div class="box-widget-wrap">
                                        <!--box-widget-item -->
                                        <div class="box-widget-item fl-wrap">
                                            <div class="box-widget">
                                                <div class="box-widget-content">
                                                    <div class="box-widget-item-header">
                                                        <h3> Book Now</h3>
                                                    </div>
                                                     <form method='POST' name="bookFormCalc"   
                                                          class="book-form custom-form">
                                                        <fieldset>
                                                            <input type='hidden' value="<?php echo $result[0]['vendor_code']; ?>" class='listId'>
                                                             <input type="hidden" class="amount"   value="<?php echo $result[0]['category_base_price']; ?>"/>
                                                             <input type='hidden' value="<?php echo @$this->session->userdata('userCCode'); ?>" class='userId'>
                                                             
                                                            <div class="cal-item">
                                                                <div class="listsearch-input-item">
                                                                    <label>Name</label>
                                                                    <input value="<?php echo @$this->session->userdata('userCId'); ?>" type="text" class='bname' name="bname">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="cal-item">
                                                                <div class="listsearch-input-item">
                                                                    <label>Email</label>
                                                                    <input value="<?php echo @$this->session->userdata('userCEmail'); ?>" class="bemail" type="text"
                                                                      name="bemail">
                                                                    
                                                                </div>
                                                            </div>
                                                             <div class="cal-item">
                                                                <div class="listsearch-input-item">
                                                                    <label>Mobile Number</label>
                                                                    <input value="<?php echo @$this->session->userdata('userCMobile'); ?>" type="text" 
                                                                    maxlength="10" class="bmobile mobNumber">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="cal-item">
                                                                <div class="bookdate-container  fl-wrap">
                                                                    <label><i class="fal fa-calendar-check"></i> When </label>
                                                                    <input type="text" placeholder="Date" class="datepicker"   data-large-mode="true" data-large-default="true" value=""/>
                                                                </div>
                                                            </div>
                                                           
                                                            <div class="cal-item">
                                                                <div class="listsearch-input-item">
                                                                    <label>Select City</label>
                                                                     <select data-placeholder="Room Type" name="bcity"   class="chosen-select no-search-select bcity" >
                                                                    <option value="">Select City</option>
                                                                    <?php                                                                    
                                                                        $count2 = count(array_filter($cityList));
                                                                        if($count2 > 0) {
                                                                            foreach($cityList as $key => $row22){                                       
                                                                            ?>                              
                                                                                <option value="<?php echo $row22['city_name'] ?>"> <?php echo $row22['city_name'] ?></option>
                                                                            <?php
                                                                            }
                                                                        }
                                                                    ?>
                                                                    </select>

                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="cal-item">
                                                                <div class="listsearch-input-item">
                                                                    <label>Additional Information</label>
                                                                    <textarea cols="40" class="baddress" rows="3" placeholder="Additional Information:"></textarea>
                                                                    
                                                                </div>
                                                            </div>

                                                        </fieldset>
                                                        <?php
                                                        if($this->session->userdata('userCId') !=""){
                                                        ?> 
                                                        <button type='button' class="btnaplly color2-bg"
                                                         id="submitBookNow">Book Now<i class="fal fa-paper-plane"></i></button>
                                                         <?php
                                                     }else{
                                                         ?>
                                                         <button type='button' class="btnaplly color2-bg modal-open"
                                                         >Book Now<i class="fal fa-paper-plane"></i></button>
                                                         <?php
                                                     }
                                                         ?>
                                                         <div class='col-md-12'>
                                                            <span class="errorList100"></span>
                                                            <span class='sucessbook'> </span>
                                                            <span class='failbook'> </span>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!--box-widget-item end -->                                      
                                        <!--box-widget-item -->
                                        <div class="box-widget-item fl-wrap">
                                            <div class="box-widget counter-widget" data-countDate="09/12/2019">
                                                <div class="banner-wdget fl-wrap">
                                                    <div class="overlay"></div>
                                                    <div class="bg"  data-bg="<?php echo base_url(); ?>assets/images/bg/10.jpg"></div>
                                                    <div class="banner-wdget-content fl-wrap">
                                                        <h4>Get a discount <i>20%</i> discount if order <i>60 days</i> before from the date of event.</h4>
                                                        
                                                        <a href="cart.php">Book Now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--box-widget-item end -->                                       
                                                                 
                                        
                                       
                                        <div class="box-widget-item fl-wrap">
                                            <div class="box-widget">
                                                <div class="box-widget-content">
                                                    <div class="box-widget-item-header">
                                                        <h3>Similar Listings</h3>
                                                    </div>
                                                    <div class="widget-posts fl-wrap">
                                                        <ul>
                                                            <?php
                                                            $i = 0;
                                                            $count = count(array_filter($othervendorThemelist));
                                                            if($count > 0) {
                                                                foreach($othervendorThemelist as $key => $row){
                                                            ?>
                                                            <li class="clearfix">
                                                                
                                                                <div class="widget-posts-descr" style="width: 100%;">
                                                                    <a href="<?php echo base_url() ?>detail/<?php echo $row['id']?>" title=""><?php echo $row['name']; ?></a>
                                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                                    <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> From <?php echo $row['city']; ?></a></div>
                                                                    <span class="rooms-price pull-right"><strong>Starts From: </strong> Rs. <?php echo $row['category_base_price']; ?> </span>
                                                                </div>
                                                            </li>
                                                            <?php } } ?>
                                                            
                                                        </ul>
                                                        <!-- <a class="widget-posts-link" href="listing.php">See All Listing <i class="fal fa-long-arrow-right"></i> </a> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--box-widget-item end -->                            
                                    </div>
                                    <!--box-widget-wrap end -->  
                                </div>
                                <!--   sidebar end  -->
                            </div>
                            <!--   row end  -->
                        </div>
                        <!--   container  end  -->
                    </section>
                    <!--  section  end-->
                </div>
                <!-- content end-->
                <div class="limit-box fl-wrap"></div>
            </div>
            <!--
           gallery code here
       -->
       <div class="main-register-wrap modal4">
            <div class="reg-overlay4"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pop_gal-holder">
                            <div class="pop_gal fl-wrap">
                                <div class="close-reg close-reg4 color-bg"><i class="fal fa-times"></i></div>
                                <!--tabs -->
                                <div id="modal_view"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
            <?php include_once 'footer.php';?>