<?php
include_once'header1.php';
?>
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    
                    <section class="middle-padding gre y-blue-bg">
                        <div class="container">
                            <div class="list-main-wrap-title single-main-wrap-title fl-wrap">
                                <h2>Checkout Process</h2>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="bookiing-form-wrap">
                                        <ul id="progressbar">
                                            <li class="active"><span>01.</span>Personal Info</li>
                                            <li><span>02.</span>Billing Address</li>
                                            <li><span>03.</span>Payment</li>
                                            <li><span>04.</span>Confirm</li>
                                        </ul>
                                        <!--   list-single-main-item -->
                                        <div class="list-single-main-item fl-wrap hidden-section tr-sec">
                                            <div class="profile-edit-container">
                                                <div class="custom-form">
                                                    <form>
                                                        <fieldset class="fl-wrap">
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Your personal Information</h3>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label> Name <i class="far fa-user"></i></label>
                                                                    <input type='hidden' value="<?php echo @$this->session->userdata('userCCode'); ?>" class='userId'>
                                                                    <input type="text" class='order_name' name='order_name'   value=""/>
                                                                    <span class='name_error span-error'></span>                                                  
                                                                </div>
                                                                 <div class="col-sm-6">
                                                                    <label>Email Address<i class="far fa-envelope"></i>  </label>
                                                                    <input type="text" class='order_email' name='order_email'  value=""/> 
                                                                    <span class='email_error span-error'></span>                                         
                                                                </div>
                                                               
                                                            </div>
                                                            <div class="row">
                                                               
                                                                <div class="col-sm-6">
                                                                    <label>Phone<i class="far fa-phone"></i>  </label>
                                                                    <input 
                                                                    type="text" 
                                                                    class='order_mobile' value="" name='order_mobile'/>
                                                                    <span class='mobile_error span-error'></span>
                                                                </div>
                                                            </div>
                                                           
                                                            <div class="filter-tags">
                                                                <input checked='checked' id="check-a" type="checkbox" name="check">
                                                                <label for="check-a">By continuing, you agree to the<a href="#" target="_blank">Terms and Conditions</a>.</label>
                                                            </div>
                                                            <span class="fw-separator"></span>
                                                            <a id="basicInfo" href="#"  class="step_checkout next-form next-form1  action-button btn no-shdow-btn color-bg">Billing Address <i class="fal fa-angle-right"></i></a>
                                                        </fieldset>
                                                        <fieldset class="fl-wrap">
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Billing Address</h3>
                                                            </div>
                                                            <div class="row">
                                                               <div class="col-sm-6">
                                                                   <label>Country </label>
                                                                   <div class="listsearch-input-item ">
                                                                       <select id="countryId" readonly='true' class="chosen-select no-search-select order_country" name="billing_country" >
                                                                           <option value="">Select Country</option>
                                                                           <?php
                                                                               $count = count(array_filter($resultCnt));
                                                                               if($count > 0) {
                                                                               $i=0;
                                                                               foreach($resultCnt as $key => $row){
                                                                               ?>
                                                                                   <option value="<?php echo  $row['id'] ?>"><?php echo  $row['country_name'] ?></option>
                                                                               <?php
                                                                               }
                                                                               }
                                                                           ?>


                                                                       </select>
                                                                   </div>
                                                               </div>
                                                               <div class="col-sm-6">
                                                                   <label>State<i class="fal fa-street-view"></i></label>
                                                                  <select id="stateId" name='billing_state'  class="chosen-select no-search-select order_state States" >
                                                                           <option>Select State</option>
                                                                       </select>
                                                                       <span class='state_error span-error'></span>
                                                               </div>
                                                           </div>
                                                           
                                                           <div class="row">
                                                               <div class="col-sm-8">
                                                                   <label>City <i class="fal fa-globe-asia"></i></label>
                                                                   <select  name='billing_city' id="cityId"  class="chosen-select no-search-select order_city Cities" >
                                                               <option value="">Select City</option>
                                                              </select>
                                                               <span class='city_error span-error'>
                                                               </span>
                                                               </div>
                                                               <div class="col-sm-4">
                                                                   <label>Postal code<i class="fal fa-barcode"></i> </label>
                                                                   <select id="pincodeId" name='billing_pincode' class="chosen-select no-search-select order_pincode" >
                                                                       <option>Select Postal code</option>
                                                                       </select>
                                                                       <span class='pincode_error span-error'></span>
                                                               </div>
                                                           </div>
                                                            <div class="row">
                                                               <div class="col-sm-12">
                                                                   <label>landmark <i class="fal fa-road"></i> </label>
                                                                   <input type="text"
                                                                   class='order_landmark' name='billing_landmark' value=""/>

                                                               </div>
                                                           </div>
                                                           <div class="list-single-main-item-title fl-wrap">
                                                               <h3>Addtional Notes</h3>
                                                           </div>
                                                           <textarea name="billing_message" class="order_mesage"   vcols="40" rows="3" placeholder="Notes"></textarea>
                                                            <span class="fw-separator"></span>
                                                            <a  href="#"  class="previous-form action-button back-form  back-form1   color-bg"><i class="fal fa-angle-left"></i> Back</a>
                                                            <a  href="#" id="billingAddress" class="next-form   next-form1 action-button btn no-shdow-btn color-bg">Payment Step <i class="fal fa-angle-right"></i></a>
                                                        </fieldset>
                                                        <fieldset class="fl-wrap">
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Payment</h3>
                                                            </div>
                                                            
                                                            
                                                            <div class="soc-log fl-wrap">
                                                                <div class="row">
                                                                    <div class="col-sm-8">
                                                                        <label>Enter Advance Amount<i class="fal fa-street-view"></i></label>
                                                                        <input type="text"  value="" name='advance_amount'  class='advance_amount mobNumber'/> 
                                                                        <span class='advance_error span-error'></span> 
                                                                        <input type="text"  value="<?php echo 
                                                                        $carTotalAmount ?>" name='total_amount' class='total_amount'/>

                                                                    </div>
                                                                    
                                                                </div>

                                                                 <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <div class="add-list-media-header" >
                                                                            <label class="radio inline"> 
                                                                            <input value='0' type="radio" name="payment_type" checked class='payment_type'>
                                                                            <span>Pay At Office</span> 
                                                                            </label>
                                                                        </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                        <div class="add-list-media-header" >
                                                                            <label class="radio inline"> 
                                                                            <input value='1' type="radio" name="payment_type" class='payment_type'>
                                                                            <span>Online</span> 
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>






                                                            </div>
                                                            <span class="fw-separator"></span>
                                                            <a  href="#"  class="previous-form  back-form  back-form1 action-button    color-bg"><i class="fal fa-angle-left"></i> Back</a>
                                                            <a id="payment" href="#"  class="add_order action-button btn color2-bg no-shdow-btn">Confirm and Pay<i class="fal fa-angle-right"></i></a>                                               
                                                        </fieldset>
                                                        <fieldset class="fl-wrap">
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Confirmation</h3>
                                                            </div>
                                                            <div class="success-table-container">
                                                                <div class="success-table-header fl-wrap">
                                                                    <i class="fal fa-check-circle decsth"></i>
                                                                    <h4>Thank you. Your Event Enquiry Has Been Received.</h4>
                                                                    <div class="clearfix"></div>
                                                                    <p>Your payment has been processed successfully.</p>
                                                                   <!--  <a href="#" target="_blank" class="color-bg">View Invoice</a> -->
                                                                </div>
                                                            </div>
                                                            <!-- <span class="fw-separator"></span>
                                                            <a  href="#"  class="previous-form action-button  back-form  back-form1   color-bg"><i class="fal fa-angle-left"></i> Back</a> -->
                                                        </fieldset>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!--   list-single-main-item end -->
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="box-widget-item-header">
                                        <h3> Your  Details</h3>
                                    </div>
                                    <!--cart-details  --> 
                                    <div class="cart-details fl-wrap">
                                        <!--cart-details_header--> 
                                       
                                        <!--cart-details_header end-->       
                                        <!--ccart-details_text-->          
                                        <div class="cart-details_text">
                                            <ul class="cart_list">
                                                 <li>Name: <span><?php echo @$this->session->userdata('userCId'); ?></span></li>
                                                <li>Email: <span><?php echo @$this->session->userdata('userCEmail'); ?></span></li>
                                                <li>Mobile Number <span>+91 - <?php echo @$this->session->userdata('userCMobile'); ?></span></li>
                                               
                                                
                                            </ul>
                                        </div>
                                        <!--cart-details_text end --> 
                                    </div>
                                    <!--cart-details end --> 
                                    <!--cart-total --> 
                                    <div class="cart-total">
                                        <span class="cart-total_item_title">Total Amount</span>
                                        <strong>Rs.<?php echo $carTotalAmount ?>/-</strong>                                
                                    </div>
                                    <!--cart-total end -->                             
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- section end -->
                </div>
                <!-- content end-->
            </div>
            <?php
                include_once'footer.php';
            ?>