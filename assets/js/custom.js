 
//for login
$(document).ready(function() { 
   
    var urlSite ='http://keyaan.co/keyaan_ghama/';   
	// for login  
    $(".userLogin").on('click',function(){  
        $(".errorList").text(""); 
        $(".errorList2").text("");
        $(".errorList3").text("");       
        var userName = $(".username").val();
        var password = $(".password").val(); 
        var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var mob = /^[1-9]{1}[0-9]{9}$/;        
		if(userName == "" ){
			$(".errorList").text("Email should not be empty");
			return false;
		}		
		if(!emailReg.test(userName)){
			$(".errorList").text("Enter valid email");
			return false;
		}    
		if(password == "" ){
			$(".errorList").text("Password should not  be empty");
			return false;
		}  
        url= urlSite+"User/userLogin/";
        dataList ={"username":userName,"password":password};
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList));
        if(alert2.responseCode =='0'){
           location.reload(true);
        } else{
        	$(".errorList").text("Invalid credentials ");
			return false;
        }  
    });
    // for signUp regBtn    
    $(".regBtn").on('click',function(){ 
        $(".errorList").text(""); 
        $(".errorList2").text("");
        $(".errorList3").text("");           
        var name = $(".rname").val();
        var email = $(".remail").val(); 
        var mobile = $(".rmobile").val();  
        var password = $(".rpassword").val();         
        var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var mob = /^[1-9]{1}[0-9]{9}$/;
        if(name == "" ){
			$(".errorList2").text("Name should not be empty");
			return false;
		}
		if(mobile == "" ){
			$(".errorList2").text("mobile should not be empty");
			return false;
		} 
		if(!mob.test(mobile)){
			$(".errorList2").text("Enter valid Mobile number");
			return false;
		}	
		if(email == "" ){
			$(".errorList2").text("Email should not be empty");
			return false;
		}		
		if(!emailReg.test(email)){
			$(".errorList2").text("Enter valid email");
			return false;
		}    
		if(password == "" ){
			$(".errorList2").text("Password should not  be empty");
			return false;
		}   
        url= urlSite+"User/userSignUp/";
        dataList ={"name":name,"email":email,"mobile":mobile,"password":password};
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList));
        if(alert2.responseCode =='1'){
           $(".errorList2").text("email exist");
            return false;
        }
        if(alert2.responseCode =='2'){
           $(".errorList2").text("mobile exist");
            return false;
        }
        if(alert2.responseCode =='0'){
            $("#regControl").hide(); 
            $("#otpControl").show();  
            return false;
        }       
    });
    $(".otpBtn").on('click',function(){ 
        $(".errorList").text(""); 
        $(".errorList2").text("");
        $(".errorList3").text("");                
        var name = $(".rname").val();
        var email = $(".remail").val(); 
        var mobile = $(".rmobile").val();  
        var password = $(".rpassword").val();  
        var code = $(".code").val();
        if(code == "" ){
			$(".errorList3").text("Please enter Code");
			return false;
		}     
        url= urlSite+"User/userVerifyOtp/";
        dataList ={"name":name,"email":email,"mobile":mobile,"password":password,"code":code};
        alert((JSON.stringify(dataList)));
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList)); 
        if(alert2.responseCode =='0'){
           location.reload(true);
        } else{
        	$(".errorList3").text("Please enter Valid Code");
			return false;
        }     
    });
    
    // for Vendor Registartion    
    $(".vendor_regBtn").on('click',function(){ 
        $(".errorList").text(""); 
        $(".errorList2").text("");
        $(".errorList3").text("");           
        var name = $(".vname").val();
        var email = $(".vemail").val(); 
        var mobile = $(".vmobile").val();      
        var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var mob = /^[1-9]{1}[0-9]{9}$/;
        if(name == "" ){
			$(".errorList5").text("Name should not be empty");
			return false;
		}
		if(mobile == "" ){
			$(".errorList5").text("mobile should not be empty");
			return false;
		} 
		if(!mob.test(mobile)){
			$(".errorList5").text("Enter valid Mobile number");
			return false;
		}	
		if(email == "" ){
			$(".errorList5").text("Email should not be empty");
			return false;
		}		
		if(!emailReg.test(email)){
			$(".errorList5").text("Enter valid email");
			return false;
		}
        url= urlSite+"Uservendor/vendorRegistartion/";
        dataList ={"name":name,"email":email,"mobile":mobile};
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList));
        if(alert2.responseCode =='1'){
           $(".errorList5").text("email exist");
            return false;
        }
        if(alert2.responseCode =='2'){
           $(".errorList5").text("mobile exist");
            return false;
        }
        if(alert2.responseCode =='0'){
            $("#vendor_regControl").hide(); 
            $("#vendor_otpControl").show();  
            return false;
        }       
    });

    //Vendor Otp
    $(".vendor_otpBtn").on('click',function(){ 
        $(".errorList").text(""); 
        $(".errorList2").text("");
        $(".errorList3").text("");                
        var name = $(".vname").val();
        var email = $(".vemail").val(); 
        var mobile = $(".vmobile").val();
        var code = $(".vcode").val();
        if(code == "" ){
			$(".errorList6").text("Please enter Code");
			return false;
		}     
        url= urlSite+"Uservendor/vendorVerifyOtp/";
        dataList ={"name":name,"email":email,"mobile":mobile,"code":code};
        //alert((JSON.stringify(dataList)));
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList)); 
        if(alert2.responseCode =='0'){
            window.location.href = urlSite+"Uservendor";
        } else{
        	$(".errorList6").text("Please enter Valid Code");
			return false;
        }     
    });

	 $('.mobNumber').keypress(function(event){
        
       if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
           event.preventDefault(); //stop character from entering input
       }

    });
	
    //User Profile
    $(".updateProfileBtn").on('click',function(){
        var name = $(".name").val();
        var email = $(".email").val(); 
        var mobile = $(".mobile").val();
        var id = $(".id").val();
        var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var mob = /^[1-9]{1}[0-9]{9}$/;
        url= urlSite+"MyDasboard/ChangeProfile/";
        dataList ={"name":name,"email":email,"mobile":mobile,"id":id};
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList));

        if(alert2.responseCode =='0'){
            $(".errorList").text("Profile Updated Successfully");
            $('.errorList').delay(10000).fadeOut('slow');
        }       
    });
 
    //Change Password
    $(".changePasswordBtn").on('click',function(){        
        var old_password = $(".old_password").val();
        var new_password = $(".new_password").val();
        var id = $(".id").val();
        url= urlSite+"MyDasboard/ChangePassword/";
        dataList ={"old_password":old_password,"new_password":new_password,"id":id};
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList));
        if(alert2.responseCode =='1'){
        $(".errorList2").text("Current Password does not Match!");
        $('.errorList2').delay(4000).fadeOut('slow');
        }
        if(alert2.responseCode =='0'){
            $(".errorList").text("Password Changed Successfully");
            $('.errorList').delay(5000).fadeOut('slow');
        }       
    });

	// for forget password


	// for signUp regBtn    
    $(".forget_regBtn").on('click',function(){ 
        $(".errorList").text(""); 
        $(".errorList2").text("");
        $(".errorList3").text("");           
        $(".errorList4").text("");       
        var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var mob = /^[1-9]{1}[0-9]{9}$/;
        var mobile = $(".fmobile").val();  
        
		if(mobile == "" ){
			$(".errorList4").text("mobile should not be empty");
			return false;
		} 
		if(!mob.test(mobile)){
			$(".errorList4").text("Enter valid Mobile number");
			return false;
		}	
		 
        url= urlSite+"User/userForgetPassword/";
        dataList ={"mobile":mobile};
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList));
        if(alert2.responseCode =='1'){
           $(".errorList4").text("mobile does not  exist");
            return false;
        }
        if(alert2.responseCode =='0'){
            $("#forget_regControl").hide(); 
            $("#forget_otpControl").show();  
            return false;
        }       
    });
    $(".forget_otpBtn").on('click',function(){ 
        $(".errorList").text(""); 
        $(".errorList2").text("");
        $(".errorList3").text("");                
      
        var mobile = $(".fmobile").val();         
        var code = $(".fcode").val();
        if(code == "" ){
			$(".errorList4").text("Please enter Code");
			return false;
		}     
        url= urlSite+"User/forgetVerifyOtp/";
        dataList ={"mobile":mobile,"code":code};       
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList));
        alert((JSON.stringify(alert2)));  
        if(alert2.responseCode =='0'){
           $(".errorList4").text("password forworded to your mobile");
           $(".fmobile").val(''); 
           return false;
        } else{
        	$(".errorList4").text("Please enter Valid Code");
			return false;
        }     
    });
	
	
	
    $("#submitBookNow").on('click',function(){ 
        $(".errorList100").text("");  	
        var amount = $(".amount").val(); 
        var userId = $(".userId").val(); 		
        var listId = $(".listId").val();
        var bemail = $(".bemail").val(); 
        var bmobile = $(".bmobile").val();  
        var bname   = $(".bname").val();  
        var blocation =$(".bcity").val(); ;  
        var baddress = $('.baddress').val();
        var dateofevent = $('.datepicker').val(); 
		var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var mob = /^[1-9]{1}[0-9]{9}$/;
        if(bname == "" ){
			$(".errorList100").text("name should not be Empty");
			return false;
		}
		if(bemail == "" ){
			$(".errorList100").text("email should not be Empty");
			return false;
		}		
		if(!emailReg.test(bemail)){
			$(".errorList100").text("Enter valid email");
			return false;
		}
		
		if(bmobile == "" ){
			$(".errorList100").text("mobile should not be Empty");
			return false;
		} 
		if(!mob.test(bmobile)){
			$(".errorList100").text("Enter valid Mobile number");
			return false;
		}
		if(dateofevent == "" ){
			$(".errorList100").text("Event Date should not be Empty");
			return false;
		}
		if(blocation == "" ){
			$(".errorList100").text("location should not be Empty");
			return false;
		}
		if(baddress == "" ){
			$(".errorList100").text("address should not be Empty");
			return false;
		}
		
        url= urlSite+"Detail/booknow/";
        dataList ={"amount":amount,"listId":listId,"bemail":bemail,"bmobile":bmobile,"bname":bname,"blocation":blocation,"baddress":baddress,"dateofevent":dateofevent};
        //alert((JSON.stringify(dataList)));
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList));
        
        if(alert2.responseCode =='0'){
            $('.sucessbook').show();
            $('.sucessbook').text('Thanks for booking ,Our admin person contact You soon');
            $('.failbook').hide();
            $("#submitBookNow").hide();
            setInterval(function(){
                location.reload(true);
            }, 3000);

        }else{
            $("#submitBookNow").show();
            $('.failbook').show();
            $('.failbook').text('Sorry ! please Book once again');
            $('.sucessbook').hide();
        }
        //alert((JSON.stringify(alert2)));       
    });

    // enquery
    $("#submitBookNow2").on('click',function(){ 
        $(".errorList2").text("");  	
        var amount = $(".amount").val(); 
		
        var listId = $(".listId2").val();
        var bemail = $(".bemail2").val(); 
        var bmobile = $(".bmobile2").val();  
        var bname   = $(".bname2").val();  
        var blocation = 1;  
        var baddress = $('.baddress2').val();
        var dateofevent = $('.datepicker2').val(); 
		var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var mob = /^[1-9]{1}[0-9]{9}$/;
        if(bname == "" ){
			$(".errorList2").text("name should not black");
			return false;
		}
		if(bemail == "" ){
			$(".errorList2").text("email should not black");
			return false;
		}		
		if(!emailReg.test(bemail)){
			$(".errorList2").text("Enter valid email");
			return false;
		}
		
		if(bmobile == "" ){
			$(".errorList2").text("mobile should not black");
			return false;
		} 
		if(!mob.test(bmobile)){
			$(".errorList2").text("Enter valid Mobile number");
			return false;
		}
		if(dateofevent == "" ){
			$(".errorList2").text("Event Date should not black");
			return false;
		}
		if(blocation == "" ){
			$(".errorList2").text("location should not black");
			return false;
		}
		if(baddress == "" ){
			$(".errorList2").text("address should not black");
			return false;
		}
		
        url= urlSite+"Detail/booknow/";
        dataList ={"amount":amount,"listId":listId,"bemail":bemail,"bmobile":bmobile,"bname":bname,"blocation":blocation,"baddress":baddress,"dateofevent":dateofevent};
        //alert((JSON.stringify(dataList)));
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList));
        
        if(alert2.responseCode =='0'){
            $('.sucessbook2').show();
            $('.sucessbook2').text('Thanks for booking ,Our admin person contact You soon');
            $('.failbook2').hide();
            $("#submitBookNow2").hide();
            setInterval(function(){
                location.reload(true);
            }, 3000);

        }else{
            $("#submitBookNow2").show();
            $('.failbook2').show();
            $('.failbook2').text('Sorry ! please Book once again');
            $('.sucessbook2').hide();
        }
        //alert((JSON.stringify(alert2)));       
    });
	
	$(".catVal").on('change',function(){ 
		var codeVal =$(this).val();		
		var act = "http://keyaan.co/keyaan_ghama/detail/"+codeVal;		  
		$('#ourform').attr("action",act);
	});
	$("#catVal2").on('change',function(){ 
		var codeVal2 =$(this).val();		
		var act2 = "http://keyaan.co/keyaan_ghama/detail/"+codeVal2;	
			
		$('#ourform2').attr("action",act2);
	});
	//catVal
	
	// enquery
    $("#submitEnquery").on('click',function(){ 	
        $(".errorList1").text("");	
        var email = $(".eemail").val(); 
        var mobile = $(".emobile").val();  
        var name   = $(".ename").val();          
        var message = $('.message').val();       
		var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var mob = /^[1-9]{1}[0-9]{9}$/;
		
		if(name == "" ){
			$(".errorList1").text("name should not be empty");
			return false;
		}
		if(email == "" ){
			$(".errorList1").text("email should not be empty");
			return false;
		}		
		if(!emailReg.test(email)){
			$(".errorList1").text("Enter valid email");
			return false;
		}		
		if(mobile == "" ){
			$(".errorList1").text("mobile should not be empty");
			return false;
		} 
		if(!mob.test(mobile)){
			$(".errorList1").text("Enter valid Mobile number");
			return false;
		}

		if(message == "" ){
			$(".errorList1").text("message should not black");
			return false;
		}
        url= urlSite+"Conatctus/enquery/";
        dataList ={"email":email,"mobile":mobile,"message":message,"name":name,"message":message};
        //alert((JSON.stringify(dataList)));
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList));
        
        if(alert2.responseCode =='0'){
            $('.sucessbook1').show();
            $('.sucessbook1').text('Thanks for showing your Interest  ,Our admin person contact You soon');
            $('.failbook1').hide();
            $("#submitEnquery").hide();
            setInterval(function(){
                location.reload(true);
            }, 3000);

        }else{
            $("#submitEnquery").show();
            $('.failbook1').show();
            $('.failbook1').text('Sorry ! please add your Enquery once again');
            $('.sucessbook1').hide();
        }
        //alert((JSON.stringify(alert2)));       
    });

    // add to cart step_checkout sudha1 in modal
    $(document).on('click','.addToCart', function() {
        $(".errorList1000").text("");  	
        var amount = $(".theme_price").val(); 
        var userId = $(".userId").val(); 		
        var vendorId = $(".vendorId").val();
        var themeId = $(".themeId").val();        
        url= urlSite+"Modalviewdetail/addToCart/";
        dataList ={"amount":amount,"vendorId":vendorId,"themeId":themeId,"userId":userId};
        //alert((JSON.stringify(dataList)));
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList));        
        if(alert2.responseCode =='11'){
            swal("Item Alreday added into Cart");
            setInterval(function(){
                 location.reload(true);
             }, 3000);
        }else if(alert2.responseCode =='0'){
            //alert('added in cart');
            swal("One Item Added To Your Cart");
            setInterval(function(){
                 location.reload(true);
             }, 3000);
        } else{
            alert('not added');
        }
        //alert((JSON.stringify(alert2)));       
    });
    // add to cart in detail page
    $(document).on('click','.addToCart1', function() {
        var CurrId = $(this).attr("id");
        $(".errorList1000").text("");  	
        var amount = $("#price"+CurrId).val(); 
        var userId = $(".userId1").val(); 		
        var vendorId = $(".vendorId1").val();
        var themeId = $("#theme"+CurrId).val();
        url= urlSite+"Modalviewdetail/addToCart/";
        dataList ={"amount":amount,"vendorId":vendorId,"themeId":themeId,"userId":userId};
        //alert((JSON.stringify(dataList)));
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList));        
        if(alert2.responseCode =='11'){
            swal("Item Alreday added into Cart");
            setInterval(function(){
                 location.reload(true);
             }, 1000);
        }else if(alert2.responseCode =='0'){
            //alert('added in cart');
            swal("One Item Added To Your Cart");
            setInterval(function(){
                 location.reload(true);
             }, 1000);
        } else{
            alert('not added');
        }
        //alert((JSON.stringify(alert2)));       
    });
    
    $(document).on('click','.addToFavourate', function() {
        var CurrId = $(this).attr("id");
        $(".errorList1000").text("");
        var userId = $(".userId1").val();
        var themeId = $("#theme"+CurrId).val();
        url= urlSite+"Modalviewdetail/addFavourate/";
        dataList ={"themeId":themeId,"userId":userId};
        //alert((JSON.stringify(dataList)));
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList));
        if(alert2.responseCode =='11'){
            swal("Item Alreday added into Favourate");
            setInterval(function(){
                 location.reload(true);
             }, 1000);
        }else if(alert2.responseCode =='0'){
            //alert('added in cart');
            swal("One Item Added To Favourate");
            setInterval(function(){
                 location.reload(true);
             }, 1000);
        } else{
            alert('not added');
        }
        //alert((JSON.stringify(alert2)));
    });
    $(document).on('change','.itinerary', function() {
        var itinerary = [];
        $.each($("input[name='check']:checked"), function(){            
            itinerary.push($(this).val());
        });
        var value = itinerary.toString();
        $('#itinerary_code').val(value);
        var countItinary = $("input[name='check']:checked").length;
        if(countItinary > 0){
            $("#addToPackageCart").show();
        }else{
            $("#addToPackageCart").hide();
        }

    });

    // add to cart Pacakge
    $(document).on('click','#addToPackageCart', function() {
        var itinerary_id = $('#itinerary_code').val();
        var package_id = $('#package_code').val();
        var userId = $(".userId").val();
        url= urlSite+"Custompackage/addToCart/";
        dataList ={"itinerary_id":itinerary_id,"package_id":package_id,"userId":userId};
        //alert((JSON.stringify(dataList)));
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList));        
        if(alert2.responseCode =='11'){
            swal("Item Alreday added into Cart");
            setInterval(function(){
                 location.reload(true);
             }, 1000);
        }else if(alert2.responseCode =='0'){
            //alert('added in cart');
            swal("One Item Added To Your Cart");
            setInterval(function(){
                 location.reload(true);
             }, 1000);
        } else{
            alert('not added');
        }
        //alert((JSON.stringify(alert2)));  
    });

    // Add to cart For Total Pacakge
    $(document).on('click','.packaddtocart', function() {
        var package_id = $('.packageId').val();
        var userId = $(".userId").val();
        var price = $(".price").val();
        url= urlSite+"Packagedetails/addToCart/";
        dataList ={"package_id":package_id,"userId":userId,"price":price};
        //alert((JSON.stringify(dataList)));
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList));
        if(alert2.responseCode =='11'){
            swal("Item Alreday added into Cart");
            setInterval(function(){
                 location.reload(true);
             }, 1000);
        }else if(alert2.responseCode =='0'){
            //alert('added in cart');
            swal("One Item Added To Your Cart");
            setInterval(function(){
                 location.reload(true);
             }, 1000);
        } else{
            alert('not added');
        }
        //alert((JSON.stringify(alert2)));  
    });

   $("#payment").on('click',function(){ 
        $(".errorList1000").text("");   
        var order_name = $(".order_name").val(); 
        var order_email = $(".order_email").val();        
        var order_mobile = $(".order_mobile").val();  
        var order_state = $(".order_state").val(); 
        var order_city = $(".order_city").val();        
        var order_pincode = $(".order_pincode").val();  
        var advance_amount = $(".advance_amount").val(); 
        var total_amount = $(".total_amount").val(); 
        var payment_type = $(".payment_type").val();
        var order_landmark=$(".order_landmark").val();
        var order_country=$(".order_country").val();
        var order_mesage=$(".order_mesage").val();        
        var userId =$(".userId").val();
        url= urlSite+"Checkout/addOrder/";
        dataList ={"user_id":userId,"order_name":order_name,"order_email":order_email,"order_mobile":order_mobile,"order_state":order_state,"order_city":order_city,"order_pincode":order_pincode,"advance_amount":advance_amount,"total_amount":total_amount,"payment_type":payment_type,"order_landmark":order_landmark,"order_country":order_country,"order_mesage":order_mesage};
        //alert((JSON.stringify(dataList)));
        var alert2 = jQuery.parseJSON($("body").sampleData(url,dataList));        
        if(alert2.responseCode =='0'){
           //alert('added in cart');
           //swal("One Item Added To Your Cart");
           // setInterval(function(){
           //      location.reload(true);
           //  }, 3000);


         
        animating = true;
        current_fs = $("#payment").parent();
        next_fs = $("#payment").parent().next();
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
        next_fs.show();
        current_fs.animate({
            opacity: 0
        }, {
            step: function (now, mx) {
                scale = 1 - (1 - now) * 0.2;
                left = (now * 50) + "%";
                opacity = 1 - now;
                current_fs.css({
                    'transform': 'scale(' + scale + ')',
                    'position': 'absolute'
                });
                next_fs.css({
                    'left': left,
                    'opacity': opacity,
                    'position': 'relative'
                });
            },
            duration: 1200,
            complete: function () {
                current_fs.hide();
                animating = false;
            },
            easing: 'easeInOutBack'
        });

       setInterval(function(){          
               window.location.href = urlSite; 
            }, 3000);






        }else{
            alert('not added');
        }
        //alert((JSON.stringify(alert2)));       
    });
   





   $("#countryId").change(function(){
        id =$(this).val();
        var url =urlSite+"State/state";
        $.ajax({
          url: url,
          type: 'POST',
          data: 'id='+id,
          dataType: 'html',
          success: function(response) {
            
            $("#stateId").html(response);
            $('.chosen-select').niceSelect('update');
            $(".Cities").html('');
            $("#pincodeId").html('');
          },
          error: function(jqXHR, textStatus, errorThrown){
             console.log(textStatus, errorThrown);
         }
       });
    });
    $("#stateId").change(function(){
        id =$(this).val();
        var url =urlSite+"District/district";
        $.ajax({
          url: url,
          type: 'POST',
          data: 'id='+id,
          dataType: 'html',
          success: function(response) {
              //alert(response);
           
            $("#districtId").html(response);
            $('.chosen-select').niceSelect('update');
          },
          error: function(jqXHR, textStatus, errorThrown){
             console.log(textStatus, errorThrown);
         }
       });
    });
    $(".States").change(function(){
        id =$(this).val();
        var url =urlSite+"City/cities";
        $.ajax({
          url: url,
          type: 'POST',
          data: 'id='+id,
          dataType: 'html',
          success: function(response) {
              //alert(response);
 
            $(".Cities").html(response);
            $('.chosen-select').niceSelect('update');
            $("#pincodeId").html('');
          },
          error: function(jqXHR, textStatus, errorThrown){
             console.log(textStatus, errorThrown);
         }
       });
    });
    $("#districtId").change(function(){
        id =$(this).val();
        var url =urlSite+"City/city";
        $.ajax({
          url: url,
          type: 'POST',
          data: 'id='+id,
          dataType: 'html',
          success: function(response) {
           
            $("#cityId").html(response);
            $('.chosen-select').niceSelect('update');
          },
          error: function(jqXHR, textStatus, errorThrown){
             console.log(textStatus, errorThrown);
         }
       });
    });
    $("#cityId").change(function(){
        id =$(this).val();
        var url =urlSite+"Pincode/pincode";
        $.ajax({
          url: url,
          type: 'POST',
          data: 'id='+id,
          dataType: 'html',
          success: function(response) {
              //alert(response);
           
            $("#pincodeId").html(response);
            $('.chosen-select').niceSelect('update');
          },
          error: function(jqXHR, textStatus, errorThrown){
             console.log(textStatus, errorThrown);
         }
       });
    });
    $("#pincodeId").change(function(){
        id =$(this).val();
        var url =urlSite+"Location/location";
        $.ajax({
          url: url,
          type: 'POST',
          data: 'id='+id,
          dataType: 'html',
          success: function(response) {
              //alert(response);
            
            $("#locationId").html(response);
            $('.chosen-select').niceSelect('update');
          },
          error: function(jqXHR, textStatus, errorThrown){
             console.log(textStatus, errorThrown);
         }
       });
    });
    $("#catId").change(function(){
        id =$(this).val();
        var url =urlSite+"Subcategory/subcatAjax";
            $.ajax({
              url: url,
              type: 'POST',
              data: 'id='+id,
              dataType: 'html',
              success: function(response) {
                 // alert(response);
               
                $("#subCatId").html(response);
                $('.chosen-select').niceSelect('update');
              },
              error: function(jqXHR, textStatus, errorThrown){
                console.log(textStatus, errorThrown);
             }
            });
        });
       $("#subCatId").change(function(){
            id =$(this).val();
            var url =urlSite+"Subsubcategory/subsubcatAjax";
            $.ajax({
              url: url,
              type: 'POST',
              data: 'id='+id,
              dataType: 'html',
              success: function(response) {
                
                $("#subsubCatId").html(response);
                $('.chosen-select').niceSelect('update');

              },
              error: function(jqXHR, textStatus, errorThrown){
                 console.log(textStatus, errorThrown);
             }
           });
        });

    //For Search
    $("#search_btn").click(function(){
        var cat = $("#search_cat").val();
        var city = $("#search_city").val();
        var price = $("#search_price").val();
        if(cat != '' && city != '' && price != '') {
            var url = urlSite+"listing/"+cat+"/"+city+"/"+price;
        } else if(cat != '' && city != '') {
            var url = urlSite+"listing/"+cat+"/"+city;
        } 
        $('#search_form').attr("action",url);
    });

    //For Search in header Sticky
    $("#search_btn1").click(function(){
        var cat1 = $("#search_cat1").val();
        var city1 = $("#search_city1").val();
        var price1 = $("#search_price1").val();
        if(cat1 != '' && city1 != '' && price1 != '') {
            var url1 = urlSite+"listing/"+cat1+"/"+city1+"/"+price1;
        } else if(cat1 != '' && city1 != '') {
            var url1 = urlSite+"listing/"+cat1+"/"+city1;
        } 
        $('#search_form1').attr("action",url1);
    });

    //For Modal View Details
    //$(".modal-view-detail").click(function(){
        $(document).on('click','.modal-view-detail', function() {
        //$(".modal-view-detail").on("click", function(){
        //alert();
        id =$(this).attr('themeId');
        var url =urlSite+"Modalviewdetail/modal";
        $.ajax({
          url: url,
          type: 'POST',
          data: 'id='+id,
          dataType: 'html',
          success: function(response) {
            $("#modal_view").html(response);  
           initCitybook();         
           // $('.modal4 , .reg-overlay4').fadeIn(200);
           // $("html, body").addClass("hid-body");
            
          },
          error: function(jqXHR, textStatus, errorThrown){
             console.log(textStatus, errorThrown);
         }
       });
    });

    

  

//     $(document).on('click','.dynamic-gal', function() {
        
//     var dynamicgal = "http://keyaan.co/keyaan_ghama/assets/images/gal/5.jpg";
    
//     $(this).lightGallery({
//         dynamic: true,
//         dynamicEl:  "http://keyaan.co/keyaan_ghama/assets/images/gal/5.jpg",
//         download: false,
//         loop: false,
//         counter: false
//     });

// });
    
 



});
   var busy =false; 
   $('.pricelist_filter').on('change', function() { 
        $('.pricelist_filter').not(this).prop('checked', false);
    });
    var urlSite ='http://keyaan.co/keyaan_ghama/';  
    $(document).on('change','.subcategory_filters',function(){
        //alert(urlSite);
        $.ajax({    
            type: 'post',
            url: urlSite+'listing/filter',
            data: $("#check_filter_form").serialize(),
            success: function (response) {
                //alert(response);             
                $(".filter_grid").html(response);  

            }
        });   
    });
    
    $(document).on('change','.pricelist_filter',function(){
        $.ajax({    
            type: 'post',
            url: urlSite+'listing/filter',
            data: $("#check_filter_form").serialize(),
            success: function (response) {
             $(".filter_grid").html(response);  
        }
        });   

    });
    $(document).on('change','.rating_filter',function(){
        $.ajax({    
            type: 'post',
            url: urlSite+'listing/filter',
            data: $("#check_filter_form").serialize(),
            success: function (response) {
             $(".filter_grid").html(response);  
        }
        });   

    });
    $(document).on('change','.sortingList',function(){
      //  $("#sortTypedf").val("0");
        $.ajax({    
            type: 'post',
            url: urlSite+'listing/filter',
            data: $("#check_filter_form").serialize(),
            success: function (response) {
             $(".filter_grid").html(response);  
        }
        });   

    });
    $(document).on('change','.sortingList2',function(){
        
      $("#sortTypedf").val("1");
        $.ajax({    
            type: 'post',
            url: urlSite+'listing/filter',
            data: $("#check_filter_form").serialize(),
            success: function (response) {
             $(".filter_grid").html(response);  
        }
        });   

    });
    //For Venue Type Filter
    $(document).on('change','.venue_type',function(){
        //  $("#sortTypedf").val("0");
          $.ajax({    
              type: 'post',
              url: urlSite+'Venuelist/venueFilter',
              data: $("#venue_filter_form").serialize(),
              success: function (response) {
               $(".venue_filter").html(response);  
          }
          });   
  
      });
      //For Venue Price Filter
    $(document).on('change','.venue_price',function(){
        //  $("#sortTypedf").val("0");
          $.ajax({    
              type: 'post',
              url: urlSite+'Venuelist/venueFilter',
              data: $("#venue_filter_form").serialize(),
              success: function (response) {
               $(".venue_filter").html(response);  
          }
          });   
  
      });
      //For Venue Audience Filter
    $(document).on('change','.audience',function(){
        //  $("#sortTypedf").val("0");
          $.ajax({    
              type: 'post',
              url: urlSite+'Venuelist/venueFilter',
              data: $("#venue_filter_form").serialize(),
              success: function (response) {
               $(".venue_filter").html(response);  
          }
          });   
  
      });//For Venue Rating Filter
      $(document).on('change','.venue_rating',function(){
          //  $("#sortTypedf").val("0");
            $.ajax({    
                type: 'post',
                url: urlSite+'Venuelist/venueFilter',
                data: $("#venue_filter_form").serialize(),
                success: function (response) {
                 $(".venue_filter").html(response);  
            }
            });   
    
        });//For Venue facilities Filter
        $(document).on('change','.facility',function(){
            //  $("#sortTypedf").val("0");
              $.ajax({    
                  type: 'post',
                  url: urlSite+'Venuelist/venueFilter',
                  data: $("#venue_filter_form").serialize(),
                  success: function (response) {
                   $(".venue_filter").html(response);  
              }
              });   
      
          });
        $(document).on('change','.subcat_price',function(){
            $.ajax({    
                type: 'post',
                url: urlSite+'Subcatlisting/subcatfilter',
                data: $("#check_filter_form").serialize(),
                success: function (response) {
                 $(".subcatfilter_grid").html(response);  
            }
            });   
    
        });
  var themeCountLimit = $(".themeCountLimit").val();  
   $(window).scroll(function() {
           
          // make sure u give the container id of the data to be loaded in.
           if($(window).scrollTop() == $(document).height() - $(window).height()) {
            busy = true;
            
            var themeCountLimit_all = Number(themeCountLimit)+1;
            $(".themeCountLimit").val(themeCountLimit_all); 
            console.log(themeCountLimit_all);

           $.ajax({    
            type: 'post',
            url: urlSite+'listing/filter',
            data: $("#check_filter_form").serialize(),
            beforeSend: function() {
            $(".load-more-button").show();
           
          },
            success: function (response) {
           //  $(".filter_grid").append(response);  
             $(".themeCountLimit").val(themeCountLimit_all); 
             $(".load-more-button").hide();
        }
        });  
         
 
          }
    });

   

   

 // code by sandhya for header menu changed
 $(document).scroll(function () {
    var y = $(this).scrollTop();
    if (y >400) {
		$('.main-header').hide();
       $('.main-header2').show();
    	
   }
     else {
      
     $('.main-header2').hide();
       $('.main-header').show();   
     }

 });


