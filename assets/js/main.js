jQuery(document).ready(function($){
	


    //on desktop - differentiate between a user trying to hover over a dropdown item vs trying to navigate into a submenu's contents
	var submenuDirection1 = ( !$('.cd-dropdown-wrapper1').hasClass('open-to-left') ) ? 'right' : 'left';
	$('.cd-dropdown-content1').menuAim({
        activate: function(row) {
        	$(row).children().addClass('is-active').removeClass('fade-out');
        	if( $('.cd-dropdown-content1 .fade-in').length == 0 ) $(row).children('ul').addClass('fade-in');
        },
        deactivate: function(row) {
        	$(row).children().removeClass('is-active');
        	if( $('li.has-children:hover').length == 0 || $('li.has-children:hover').is($(row)) ) {
        		$('.cd-dropdown-content1').find('.fade-in').removeClass('fade-in');
        		$(row).children('ul').addClass('fade-out')
        	}
        },
        exitMenu: function() {
        	$('.cd-dropdown-content1').find('.is-active').removeClass('is-active');
        	return true;
        },
        submenuDirection: submenuDirection1,
    });
     var submenuDirection2 = ( !$('.cd-dropdown-wrapper2').hasClass('open-to-left') ) ? 'left' : 'left';
    $('.cd-dropdown-content2').menuAim({
         

        activate: function(row1) {
            $(row1).children().addClass('is-active').removeClass('fade-out');
            if( $('.cd-dropdown-content2 .fade-in').length == 0 ) $(row1).children('ul').addClass('fade-in');
        },
        deactivate: function(row1) {
            $(row1).children().removeClass('is-active');
            if( $('li.has-children:hover').length == 0 || $('li.has-children:hover').is($(row1)) ) {
                $('.cd-dropdown-content2').find('.fade-in').removeClass('fade-in');
                $(row1).children('ul').addClass('fade-out')
            }
        },
        exitMenu: function() {
            $('.cd-dropdown-content2').find('.is-active').removeClass('is-active');
            return true;
        },
        submenuDirection: submenuDirection2,
    });





    
     

	
});